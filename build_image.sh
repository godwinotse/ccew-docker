#!/usr/bin/env sh

docker build \
	--build-arg LABEL_BUILD_DATE=$(date --iso-8601=seconds) \
	--build-arg LABEL_NAME="CCEW Liferay" \
	--build-arg LABEL_VCS_REF=$(git rev-parse HEAD) \
	--build-arg LABEL_VCS_URL="https://ccew-gitlab/liferay/liferay-docker" \
	--build-arg LABEL_VERSION="1.0.0-SNAPSHOT" \
	-t ccew-liferay:latest .

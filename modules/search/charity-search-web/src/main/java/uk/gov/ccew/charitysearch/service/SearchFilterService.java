package uk.gov.ccew.charitysearch.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.facet.Facet;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;

import uk.gov.ccew.charitysearch.model.filter.CharityDropDownFilter;
import uk.gov.ccew.charitysearch.model.filter.CharityFilter;
import uk.gov.ccew.charitysearch.model.filter.CharityFilterType;

@Component(immediate = true, service = SearchFilterService.class)
public class SearchFilterService {

	@Reference
	private SearchContextService searchContextService;

	@Reference
	private SearchFacetService searchFacetService;

	@Reference
	private SearchIndexService searchIndexService;

	public Map<String, CharityFilter> getCharityFilters(PortletRequest portletRequest) throws PortalException {
		Map<String, CharityFilter> charityFilters = new HashMap<>();

		for (CharityFilterType charityFilterType : CharityFilterType.values()) {

			Optional<CharityFilter> charityFilter = SearchFilterFactory.getFilter(charityFilterType);
			if (charityFilter.isPresent()) {
				charityFilters.put(charityFilter.get().getFilterName(), charityFilter.get());

				if (charityFilter.get() instanceof CharityDropDownFilter) {
					setFilterOptions(portletRequest, (CharityDropDownFilter) charityFilter.get());
				}
			}

		}

		return charityFilters;

	}

	public void addRequestValuesToFilters(Collection<CharityFilter> charityFilters, PortletRequest portletRequest, SearchContext searchContext) {

		charityFilters.forEach(charityFilter -> {

			String value = ParamUtil.getString(portletRequest, charityFilter.getFilterName(), null);
			if (Validator.isNotNull(value) && !Boolean.FALSE.toString().equalsIgnoreCase(value)) {
				charityFilter.setSelected(true);
				charityFilter.setSelectedValue(value);
				searchContextService.addBooleanClausesToContext(searchContext, charityFilter.getBooleanClauses());

			}
		});
	}

	public void setFilterOptions(PortletRequest portletRequest, CharityDropDownFilter charityFilter) throws PortalException {
		SearchContext searchContext = searchContextService.getSearchContextForFilters(portletRequest);

		Facet filterFacet = SearchFacetFactory.getSimpleFacet(charityFilter.getFieldName(), searchContext);

		JSONObject facetData = SearchFacetFactory.getFacetConfigurationData();
		filterFacet.getFacetConfiguration().setDataJSONObject(facetData);
		searchContext.addFacet(filterFacet);
		searchContext.setStart(0);
		searchContext.setEnd(0);
		searchContextService.addBooleanClausesToContext(searchContext, charityFilter.getBooleanClauses());

		searchIndexService.search(searchContext);

		charityFilter.setFilterOptions(searchFacetService.getTermsAndFrequenciesFromFacet(filterFacet).keySet());
	}

	public void setIteratorUrlFilterParams(Map<String, CharityFilter> charityFilters, PortletURL iteratorURL) {

		charityFilters.values().forEach(charityFilter -> {
			iteratorURL.getRenderParameters().setValue(charityFilter.getFilterName(), charityFilter.getSelectedValue());
		});

	}

}

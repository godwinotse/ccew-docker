package uk.gov.ccew.charitysearch.model.sort;

import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;

import uk.gov.ccew.charitysearch.web.constant.SearchFieldKeys;

public class CharityReportingSortableColumn implements SortableColumn {

	public static CharityReportingSortableColumn getNewInstance() {
		return new CharityReportingSortableColumn();
	}

	@Override
	public Sort[] getDefaultSorts() {

		return getSorts(Boolean.FALSE);

	}

	@Override
	public Sort[] getSorts(boolean reverse) {

		Sort sortReportingStatus = SortFactoryUtil.create(SearchFieldKeys.REPORTING_STATUS_SORTABLE, reverse);
		Sort sortReportingDyasOverdue = SortFactoryUtil.create(SearchFieldKeys.REPORTING_DAYS_OVERDUE_SORTABLE, reverse);

		return new Sort[] { sortReportingStatus, sortReportingDyasOverdue };

	}

}

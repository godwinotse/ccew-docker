package uk.gov.ccew.charitysearch.service;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.facet.Facet;
import com.liferay.portal.kernel.search.facet.SimpleFacet;

import uk.gov.ccew.charitysearch.model.facet.CharityDateRangeFacet;
import uk.gov.ccew.charitysearch.model.facet.CharityDropdownSelectFacet;
import uk.gov.ccew.charitysearch.model.facet.CharityFacet;
import uk.gov.ccew.charitysearch.model.facet.CharityFacetField;
import uk.gov.ccew.charitysearch.model.facet.CharityMultiSelectFacet;
import uk.gov.ccew.charitysearch.model.facet.CharityRangeFacet;
import uk.gov.ccew.charitysearch.model.facet.CharitySingleSelectCheckboxFacet;
import uk.gov.ccew.charitysearch.model.facet.CharitySingleSelectTextboxFacet;

public final class SearchFacetFactory {

	private static final int MAX_NUMBER_OF_FACET_VALUES = 1000;

	private static final int MIN_NUMBER_OF_DOCS_PER_FACET = 1;

	private SearchFacetFactory() {

	}

	public static CharityFacet getCharitySearchFacet(CharityFacetField charityFacetField, SearchContext searchContext) {

		if (charityFacetField.getCharityFacetType().isRange()) {

			return new CharityRangeFacet(searchContext, charityFacetField.getFieldName());

		} else if (charityFacetField.getCharityFacetType().isMultiSelect()) {

			return new CharityMultiSelectFacet(searchContext, charityFacetField.getFieldName());

		} else if (charityFacetField.getCharityFacetType().isDateRange()) {

			return new CharityDateRangeFacet(searchContext, charityFacetField.getFieldName());

		} else if (charityFacetField.getCharityFacetType().isSingleSelectTextbox()) {

			return new CharitySingleSelectTextboxFacet(searchContext, charityFacetField.getFieldName());

		} else if (charityFacetField.getCharityFacetType().isSingleSelectCheckbox()) {

			return new CharitySingleSelectCheckboxFacet(searchContext, charityFacetField.getFieldName());

		}

		return new CharityDropdownSelectFacet(searchContext, charityFacetField.getFieldName());
	}

	public static Facet getSimpleFacet(String fieldName, SearchContext searchContext) {

		Facet simpleFacet = new SimpleFacet(searchContext);
		simpleFacet.setStatic(false);
		simpleFacet.setFieldName(fieldName);
		return simpleFacet;

	}

	public static JSONObject getFacetConfigurationData() {

		JSONObject facetData = JSONFactoryUtil.createJSONObject();
		facetData.put("maxTerms", MAX_NUMBER_OF_FACET_VALUES);
		facetData.put("frequencyThreshold", MIN_NUMBER_OF_DOCS_PER_FACET);
		return facetData;

	}

}

package uk.gov.ccew.charitysearch.asset;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.asset.kernel.model.BaseJSPAssetRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.util.JavaConstants;

import uk.gov.ccew.charitysearch.web.constant.PortletRequestKeys;
import uk.gov.ccew.onereg.service.OneRegCharityService;
import uk.gov.ccew.onereg.service.OneRegUrlService;
import uk.gov.ccew.service.liferaystore.model.Charity;

public class CharityAssetRenderer extends BaseJSPAssetRenderer<Charity> {

	public static final int DEFAULT_ID = 0;

	public static final int MAX_CONTENT_LENGTH = 200;

	private Charity liferayCharity;

	private OneRegCharityService oneRegCharityService;

	private OneRegUrlService oneRegUrlService;

	public CharityAssetRenderer(Charity liferayCharity, OneRegCharityService oneRegCharityService, OneRegUrlService oneRegUrlService) {

		this.liferayCharity = liferayCharity;
		this.oneRegCharityService = oneRegCharityService;
		this.oneRegUrlService = oneRegUrlService;

	}

	@Override
	public boolean hasViewPermission(PermissionChecker permissionChecker) throws PortalException {
		return Boolean.TRUE;
	}

	@Override
	public Charity getAssetObject() {
		return liferayCharity;
	}

	@Override
	public String getUuid() {
		return Long.toString(liferayCharity.getCharityId());
	}

	@Override
	public String getClassName() {
		return CharityAssetRendererFactory.CLASS_NAME;
	}

	@Override
	public long getClassPK() {
		return liferayCharity.getPrimaryKey();
	}

	public String getPortletId() {
		return getAssetRendererFactory().getPortletId();
	}

	@Override
	public String getSummary(PortletRequest portletRequest, PortletResponse portletResponse) {

		String summary = liferayCharity.getGovDocDescription();

		if (summary.length() > MAX_CONTENT_LENGTH) {
			summary = summary.substring(0, MAX_CONTENT_LENGTH);
		}

		return summary;
	}

	@Override
	public String getURLViewInContext(LiferayPortletRequest liferayPortletRequest, LiferayPortletResponse liferayPortletResponse, String noSuchEntryRedirect) throws Exception {

		return oneRegUrlService.getCharityDetailsUrl(liferayCharity.getOrganizationId(), liferayPortletRequest);
	}

	@Override
	public String getTitle(Locale locale) {
		return liferayCharity.getFullName();
	}

	@Override
	public long getGroupId() {
		return DEFAULT_ID;
	}

	@Override
	public long getUserId() {
		return DEFAULT_ID;
	}

	@Override
	public String getUserName() {
		return null;
	}

	@Override
	public String getJspPath(HttpServletRequest request, String template) {

		if (TEMPLATE_FULL_CONTENT.equals(template) || TEMPLATE_ABSTRACT.equals(template)) {
			return "/asset/" + template + ".jsp";
		} else {
			return null;
		}

	}

	@Override
	public boolean include(HttpServletRequest request, HttpServletResponse response, String template) throws Exception {

		PortletRequest portletRequest = (PortletRequest) request.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST);
		request.setAttribute(PortletRequestKeys.LIFERAY_CHARITY, liferayCharity);
		request.setAttribute(PortletRequestKeys.PUBLIC_DATA_CHARITY, oneRegCharityService.getCharity(liferayCharity.getOrganizationId(), portletRequest));

		return super.include(request, response, template);
	}
}

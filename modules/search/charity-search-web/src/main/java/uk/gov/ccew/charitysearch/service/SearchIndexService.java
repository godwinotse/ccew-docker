package uk.gov.ccew.charitysearch.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.facet.faceted.searcher.FacetedSearcher;
import com.liferay.portal.kernel.search.facet.faceted.searcher.FacetedSearcherManager;

@Component(immediate = true, service = SearchIndexService.class)
public class SearchIndexService {

	@Reference
	private FacetedSearcherManager facetedSearcherManager;

	public Hits search(SearchContext searchContext) throws SearchException {

		FacetedSearcher facetedSearcher = facetedSearcherManager.createFacetedSearcher();

		return facetedSearcher.search(searchContext);
	}

}

package uk.gov.ccew.charitysearch.model.filter;

import java.util.List;

import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.Query;

public interface CharityFilter {

	void addRequiredFilterTerm(String fieldName, String value);

	List<BooleanClause<Query>> getBooleanClauses();

	String getFilterName();

	String getSelectedValue();

	boolean isSelected();

	void setSelected(boolean selected);

	void setSelectedValue(String selectedValue);
}

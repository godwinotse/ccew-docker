package uk.gov.ccew.charitysearch.web.portlet;

import java.util.List;
import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;

import uk.gov.ccew.charitysearch.model.filter.CharityFilter;
import uk.gov.ccew.charitysearch.service.CharitySearchService;
import uk.gov.ccew.charitysearch.service.SearchContainerService;
import uk.gov.ccew.charitysearch.service.SearchContextService;
import uk.gov.ccew.charitysearch.service.SearchFacetService;
import uk.gov.ccew.charitysearch.service.SearchFilterService;
import uk.gov.ccew.charitysearch.service.SearchIndexService;
import uk.gov.ccew.charitysearch.service.SearchSortService;
import uk.gov.ccew.charitysearch.web.constant.MVCCommandKeys;
import uk.gov.ccew.charitysearch.web.constant.PortletKeys;
import uk.gov.ccew.charitysearch.web.constant.PortletRequestKeys;
import uk.gov.ccew.onereg.model.OneRegCharity;
import uk.gov.ccew.publicdata.model.Charity;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.CHARITY_SEARCH_PORTLET, "mvc.command.name=" + MVCCommandKeys.SEARCH_RESULTS_RENDER }, service = MVCRenderCommand.class)
public class CharitySearchResultsMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(CharitySearchResultsMVCRenderCommand.class);

	@Reference
	private CharitySearchService charitySearchService;

	@Reference
	private SearchContainerService searchContainerService;

	@Reference
	private SearchContextService searchContextService;

	@Reference
	private SearchFilterService searchFilterService;

	@Reference
	private SearchSortService searchSortService;

	@Reference
	private SearchIndexService searchIndexService;

	@Reference
	private SearchFacetService searchFacetService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		try {

			SearchContainer<Charity> searchContainer = searchContainerService.createSearchContainer(renderRequest, renderResponse);
			SearchContext searchContext = searchContextService.getSearchContext(renderRequest, searchContainer);
			searchSortService.setContainerAndContextSorts(searchContext, searchContainer);

			searchFacetService.setFacetsToDisplayOnSearchContext(searchContext);
			searchFacetService.addRequestValuesToFacets(searchContext, renderRequest);
			searchFacetService.setIteratorUrlFacetParams(searchContext, searchContainer.getIteratorURL());

			Map<String, CharityFilter> charityFilters = searchFilterService.getCharityFilters(renderRequest);
			searchFilterService.addRequestValuesToFilters(charityFilters.values(), renderRequest, searchContext);
			searchFilterService.setIteratorUrlFilterParams(charityFilters, searchContainer.getIteratorURL());

			Hits hits = searchIndexService.search(searchContext);
			searchContainer.setTotal(hits.getLength());
			searchFacetService.addResultTermsAndFequenciesToFacets(renderRequest, searchContext);
			List<OneRegCharity> charities = charitySearchService.getCharitiesFromDocuments(hits.getDocs(), renderRequest);

			renderRequest.setAttribute(PortletRequestKeys.CHARITIES, charities);
			renderRequest.setAttribute(PortletRequestKeys.CHARITY_FILTERS, charityFilters);
			renderRequest.setAttribute(PortletRequestKeys.SEARCH_CONTEXT, searchContext);
			renderRequest.setAttribute(PortletRequestKeys.SEARCH_CONTAINER, searchContainer);
			renderRequest.setAttribute(PortletRequestKeys.TOTAL_RESULTS, String.format("%,d", hits.getLength()));
			renderRequest.setAttribute(PortletRequestKeys.SEARCH_PAGE_SECTIONS, searchFacetService.getPageSectionsFacetMap(searchContext));

			LOG.debug("Number of search results:" + hits.getLength());
		} catch (Exception e) {
			throw new PortletException(e);
		}

		return "/search/results.jsp";

	}

}

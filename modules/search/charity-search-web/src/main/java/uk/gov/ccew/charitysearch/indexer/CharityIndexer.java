package uk.gov.ccew.charitysearch.indexer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BaseIndexer;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.IndexWriterHelper;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Summary;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.search.filter.Filter;
import com.liferay.portal.kernel.search.highlight.HighlightUtil;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

import uk.gov.ccew.charitysearch.model.DateUtil;
import uk.gov.ccew.charitysearch.service.CharitySearchService;
import uk.gov.ccew.charitysearch.web.constant.FilterValueConstants;
import uk.gov.ccew.charitysearch.web.constant.SearchFieldKeys;
import uk.gov.ccew.charitysearch.web.util.FieldNameUtil;
import uk.gov.ccew.publicdata.model.AnnualReturn;
import uk.gov.ccew.publicdata.model.AnnualReturnAndAccountsAndTar;
import uk.gov.ccew.publicdata.model.AreaOfOperation;
import uk.gov.ccew.publicdata.model.CharityAnnualReport;
import uk.gov.ccew.publicdata.model.Continent;
import uk.gov.ccew.publicdata.model.Country;
import uk.gov.ccew.publicdata.model.Trustee;
import uk.gov.ccew.publicdata.service.CharityService;
import uk.gov.ccew.publicdata.service.FinancialService;
import uk.gov.ccew.publicdata.service.TrusteeService;
import uk.gov.ccew.service.liferaystore.model.Charity;
import uk.gov.ccew.service.liferaystore.service.CharityLocalService;

@Component(immediate = true, service = Indexer.class)
public class CharityIndexer extends BaseIndexer<Charity> {

	public static final int CHARITY_IMPORT_BATCH_SIZE = 1000;

	public static final int CHARITY_IMPORT_START_INDEX = 0;

	public static final String CLASS_NAME = Charity.class.getName();

	public static final int MAX_CONTENT_LENGTH = 200;

	private static final Log LOG = LogFactoryUtil.getLog(CharityIndexer.class);

	private static final String[] _ESCAPE_SAFE_HIGHLIGHTS = { "[@HIGHLIGHT1@]", "[@HIGHLIGHT2@]" };

	@Reference
	private CharitySearchService charitySearchService;

	@Reference
	private CharityLocalService charityLiferayService;

	@Reference
	private CharityService charityPublicDataService;

	@Reference
	private FinancialService financialService;

	@Reference
	private IndexWriterHelper indexWriterHelper;

	@Reference
	private Portal portal;

	@Reference
	private TrusteeService trusteeService;

	@Activate
	@Modified
	public void activate(Map<String, Object> properties) {

		setDefaultSelectedFieldNames(Field.ASSET_TAG_NAMES, Field.COMPANY_ID, Field.CONTENT, Field.ENTRY_CLASS_NAME, Field.ENTRY_CLASS_PK, Field.GROUP_ID, Field.MODIFIED_DATE, Field.SCOPE_GROUP_ID,
				Field.TITLE, Field.UID);
		setPermissionAware(false);
		setFilterSearch(true);

	}

	@Override
	public String getClassName() {
		return CLASS_NAME;
	}

	@Override
	public boolean hasPermission(PermissionChecker permissionChecker, String entryClassName, long entryClassPK, String actionId) throws Exception {
		return true;
	}

	@Override
	public void postProcessContextBooleanFilter(BooleanFilter contextBooleanFilter, SearchContext searchContext) throws Exception {

		addStatus(contextBooleanFilter, searchContext);

	}

	private void limitFacetCountsToCurrentResultSet(BooleanFilter fullQueryBooleanFilter, SearchContext searchContext) {

		searchContext.getFacets().values().forEach(facet -> {
			BooleanClause<Filter> facetClause = facet.getFacetFilterBooleanClause();
			if (Validator.isNotNull(facetClause)) {
				fullQueryBooleanFilter.add(facetClause.getClause(), BooleanClauseOccur.MUST);
			}
		});

	}

	@Override
	public void postProcessSearchQuery(BooleanQuery searchQuery, BooleanFilter fullQueryBooleanFilter, SearchContext searchContext) throws Exception {

		limitFacetCountsToCurrentResultSet(fullQueryBooleanFilter, searchContext);

		addSearchLocalizedTerm(searchQuery, searchContext, Field.TITLE, false);
		addSearchLocalizedTerm(searchQuery, searchContext, Field.CONTENT, false);
		addSearchLocalizedTerm(searchQuery, searchContext, SearchFieldKeys.ADDRESS, false);
		addSearchLocalizedTerm(searchQuery, searchContext, SearchFieldKeys.LOCATIONS, false);
		addSearchLocalizedTerm(searchQuery, searchContext, SearchFieldKeys.CHARITY_NAME, false);
		addSearchLocalizedTerm(searchQuery, searchContext, SearchFieldKeys.CHARITY_NUMBER, false);
		addSearchLocalizedTerm(searchQuery, searchContext, SearchFieldKeys.OBJECTS, false);
		addSearchLocalizedTerm(searchQuery, searchContext, SearchFieldKeys.WHAT_CHARITY_DOES, false);
		addSearchLocalizedTerm(searchQuery, searchContext, SearchFieldKeys.OTHER_NAMES, false);

	}

	@Override
	protected void doDelete(Charity liferayCharity) throws Exception {
		deleteDocument(liferayCharity.getCompanyId(), liferayCharity.getCharityId());
	}

	@Override
	protected Document doGetDocument(Charity liferayCharity) throws Exception {

		Document document = getBaseModelDocument(CLASS_NAME, liferayCharity);
		populateDocumentFromLiferayCharity(liferayCharity, document);

		uk.gov.ccew.publicdata.model.Charity publicCharityDetails = charitySearchService.getPublicDataCharity(liferayCharity.getOrganizationId());
		populateDocumentFromPublicCharity(publicCharityDetails, document);

		return document;
	}

	private String getCharityAnnualReportStatus(uk.gov.ccew.publicdata.model.Charity charity) {
		return charity.getCharityAnnualReport().map(charityAnnualReport -> getCharityAnnualReportStatus(charityAnnualReport)).orElse(StringPool.BLANK);
	}

	private String getCharityAnnualReportStatus(CharityAnnualReport charityAnnualReport) {
		return charityAnnualReport.getStatus().map(charityAnnualReportStatus -> charityAnnualReportStatus.name()).orElse(StringPool.BLANK);
	}

	private void populateDocumentFromLiferayCharity(Charity liferayCharity, Document document) throws PortalException {

		setDateFields(document);
		setTextFields(liferayCharity, document);

	}

	private void setDateFields(Document document) {

		document.addDate(Field.MODIFIED_DATE, Calendar.getInstance().getTime());

	}

	private void setTextFields(Charity liferayCharity, Document document) throws PortalException {

		long companyId = liferayCharity.getCompanyId();
		long groupId = charitySearchService.getCompanyGroupId(companyId);
		long userId = charitySearchService.getCompanyDefaultUserId(companyId);

		document.addText(Field.TITLE, liferayCharity.getFullName());
		document.addText(Field.COMPANY_ID, String.valueOf(companyId));
		document.addText(Field.USER_ID, String.valueOf(userId));
		document.addText(Field.GROUP_ID, String.valueOf(groupId));
		document.addText(Field.SCOPE_GROUP_ID, String.valueOf(groupId));
		document.addText(Field.STATUS, String.valueOf(WorkflowConstants.STATUS_APPROVED));

	}

	private void populateDocumentFromPublicCharity(uk.gov.ccew.publicdata.model.Charity publicCharityDetails, Document document) {

		setTextFields(publicCharityDetails, document);
		setSortableFields(publicCharityDetails, document);
		setKeywordFields(publicCharityDetails, document);
		setDateFields(publicCharityDetails, document);
		setNumberFields(publicCharityDetails, document);

		populateWithLatestAnnualReturn(publicCharityDetails, document);
		populateWithAreaOfOperation(publicCharityDetails, document);
		populateWithTrustees(publicCharityDetails, document);
	}

	private void populateWithTrustees(uk.gov.ccew.publicdata.model.Charity publicCharityDetails, Document document) {
		List<Trustee> trustees = trusteeService.getTrustees(publicCharityDetails.getOrganisationNumber());

		if (Validator.isNotNull(trustees) && !trustees.isEmpty()) {
			List<String> names = new ArrayList<>();
			trustees.stream().map(Trustee::getName).map(String::toLowerCase).forEach(name -> {
				names.add(name);

				for (String nameComponent : name.split(StringPool.SPACE)) {
					names.add(nameComponent);
				}
			});

			document.addText(SearchFieldKeys.TRUSTEES, names.stream().toArray(String[]::new));

		}

	}

	private void populateWithAreaOfOperation(uk.gov.ccew.publicdata.model.Charity publicCharityDetails, Document document) {

		if (publicCharityDetails.getAreaOfOperation().isPresent()) {
			AreaOfOperation areaOfOperation = publicCharityDetails.getAreaOfOperation().get();
			document.addText(SearchFieldKeys.CONSTITUENCY_KEYWORD, areaOfOperation.getConstituencies().stream().toArray(String[]::new));
			document.addText(SearchFieldKeys.COUNTRY_KEYWORD, areaOfOperation.getCountries().stream().map(Country::getName).toArray(String[]::new));
			document.addText(SearchFieldKeys.REGION_KEYWORD, areaOfOperation.getRegions().stream().toArray(String[]::new));

			indexContinentFields(areaOfOperation, document);
			indexLocalAuthorityFields(areaOfOperation, document);
		}

	}

	private void indexContinentFields(AreaOfOperation areaOfOperation, Document document) {
		Map<String, List<String>> continentAndCountries = new HashMap<>();
		areaOfOperation.getCountries().stream().filter(Objects::nonNull).forEach(country -> {

			if (country.getContinent().isPresent()) {
				Continent continent = country.getContinent().get();
				List<String> countries = continentAndCountries.get(continent.getName());
				if (countries == null) {
					countries = new ArrayList<>();
					continentAndCountries.put(continent.getName(), countries);
				}

				countries.add(country.getName());
			}
		});

		document.addText(SearchFieldKeys.CONTINENT_KEYWORD, continentAndCountries.keySet().stream().toArray(String[]::new));
		continentAndCountries.forEach((continent, countries) -> {
			document.addText(FieldNameUtil.getCountryFieldNameForContinent(continent), countries.stream().toArray(String[]::new));
		});
	}

	private void indexLocalAuthorityFields(AreaOfOperation areaOfOperation, Document document) {
		List<String> localAuthorityWales = new ArrayList<>();
		List<String> localAuthorityEngland = new ArrayList<>();
		List<String> localAuthorityLondon = new ArrayList<>();
		List<String> localAuthorityAll = new ArrayList<>();
		areaOfOperation.getLocalAuthorities().stream().filter(Objects::nonNull).forEach(localAuthority -> {

			String localAuthorityName = localAuthority.getLocalAuthorityName();
			localAuthorityAll.add(localAuthorityName);

			if (localAuthority.isWelsh()) {
				localAuthorityWales.add(localAuthorityName);
			} else {
				localAuthorityEngland.add(localAuthorityName);
			}

			if (FilterValueConstants.GREATER_LONDON.equals(localAuthority.getMetropolitanCouncilName())) {
				localAuthorityLondon.add(localAuthorityName);
			}

		});

		document.addText(SearchFieldKeys.LOCAL_AUTHORITY_ENGLAND_KEYWORD, localAuthorityEngland.stream().toArray(String[]::new));
		document.addText(SearchFieldKeys.LOCAL_AUTHORITY_WALES_KEYWORD, localAuthorityWales.stream().toArray(String[]::new));
		document.addText(SearchFieldKeys.LOCAL_AUTHORITY_LONDON_KEYWORD, localAuthorityLondon.stream().toArray(String[]::new));
		document.addText(SearchFieldKeys.LOCAL_AUTHORITY_KEYWORD, localAuthorityAll.stream().toArray(String[]::new));
	}

	private void populateWithLatestAnnualReturn(uk.gov.ccew.publicdata.model.Charity publicCharityDetails, Document document) {

		List<AnnualReturnAndAccountsAndTar> annualReturnAndAccountsAndTars = financialService.getAnnualReturnsAndAccountsAndTars(publicCharityDetails.getOrganisationNumber());

		Comparator<AnnualReturn> annualReturnComparator = (AnnualReturn annualReport1, AnnualReturn annualReport2) -> annualReport2.getReportingYear().get()
				.compareTo(annualReport1.getReportingYear().get());

		AnnualReturn latestAnnualReturn = annualReturnAndAccountsAndTars.stream().map(AnnualReturnAndAccountsAndTar::getAnnualReturn).filter(Optional::isPresent).map(Optional::get)
				.sorted(annualReturnComparator).findFirst().orElse(null);

		if (latestAnnualReturn != null) {
			document.addKeyword(SearchFieldKeys.GRANT_GIVING_IS_PRIMARY, latestAnnualReturn.isPrimaryPurposeGrantGiving());
		}

	}

	private void setDateFields(uk.gov.ccew.publicdata.model.Charity publicCharityDetails, Document document) {

		document.addDate(SearchFieldKeys.REGISTRATION_DATE, publicCharityDetails.getRegistrationDate().orElse(null));
		document.addDate(SearchFieldKeys.REMOVED_DATE, publicCharityDetails.getRemovedDate().orElse(null));

	}

	private void setNumberFields(uk.gov.ccew.publicdata.model.Charity publicCharityDetails, Document document) {

		document.addNumber(SearchFieldKeys.REGISTRATION_DATE, DateUtil.getTimeInMillis(publicCharityDetails.getRegistrationDate().orElse(null)));
		document.addNumber(SearchFieldKeys.REMOVED_DATE, DateUtil.getTimeInMillis(publicCharityDetails.getRemovedDate().orElse(null)));

	}

	private void setKeywordFields(uk.gov.ccew.publicdata.model.Charity publicCharityDetails, Document document) {

		document.addKeyword(SearchFieldKeys.ORGANISATION_NUMBER, publicCharityDetails.getOrganisationNumber());

	}

	private void setTextFields(uk.gov.ccew.publicdata.model.Charity publicCharityDetails, Document document) {

		document.addText(SearchFieldKeys.ADDRESS, publicCharityDetails.getAddress());
		document.addText(SearchFieldKeys.OBJECTS, publicCharityDetails.getCharitableObjects());
		document.addText(Field.DESCRIPTION, publicCharityDetails.getCharitableObjects());
		document.addText(Field.CONTENT, publicCharityDetails.getCharitableObjects());
		document.addText(SearchFieldKeys.CHARITY_NUMBER, String.valueOf(publicCharityDetails.getCharityNumber()));

		String reportingStatus = getCharityAnnualReportStatus(publicCharityDetails);
		document.addText(SearchFieldKeys.REPORTING_STATUS, reportingStatus);

	}

	private void setSortableFields(uk.gov.ccew.publicdata.model.Charity publicCharityDetails, Document document) {

		document.addNumberSortable(SearchFieldKeys.INCOME, publicCharityDetails.getIncome());
		document.addNumberSortable(SearchFieldKeys.EXPENDITURE, publicCharityDetails.getExpenditure());
		document.addNumberSortable(SearchFieldKeys.CHARITY_NUMBER, publicCharityDetails.getCharityNumber());
		document.addTextSortable(SearchFieldKeys.CHARITY_NAME, publicCharityDetails.getCharityName());
		document.addTextSortable(SearchFieldKeys.SUBSIDIARY_SUFFIX, String.valueOf(publicCharityDetails.getSubsidiarySuffix()));

		publicCharityDetails.getCharityStatus().ifPresent(charityStatus -> {
			document.addTextSortable(SearchFieldKeys.CHARITY_STATUS, String.valueOf(charityStatus));
		});

		Optional<CharityAnnualReport> charityAnnualReportOptional = publicCharityDetails.getCharityAnnualReport();
		charityAnnualReportOptional.ifPresent(charityAnnualReport -> {

			String reportingStatus = getCharityAnnualReportStatus(publicCharityDetails);
			document.addText(SearchFieldKeys.REPORTING_STATUS, reportingStatus);
			document.addText(SearchFieldKeys.REPORTING_STATUS_SORTABLE, reportingStatus);

			int daysOverdue = charityAnnualReport.getDaysOverdue();
			document.addNumberSortable(SearchFieldKeys.REPORTING_DAYS_OVERDUE, daysOverdue);

		});

		if (Validator.isNotNull(publicCharityDetails.getWhatTheCharityDoes())) {
			document.addText(SearchFieldKeys.WHAT_CHARITY_DOES, publicCharityDetails.getWhatTheCharityDoes().stream().toArray(String[]::new));
			document.addText(SearchFieldKeys.WHAT_CHARITY_DOES_KEYWORD, publicCharityDetails.getWhatTheCharityDoes().stream().toArray(String[]::new));
		}

		if (Validator.isNotNull(publicCharityDetails.getWhatTheCharityDoes())) {
			document.addText(SearchFieldKeys.WHO_CHARITY_HELPS_KEYWORD, publicCharityDetails.getWhoTheCharityHelps().stream().toArray(String[]::new));
		}

		if (Validator.isNotNull(publicCharityDetails.getWhatTheCharityDoes())) {
			document.addText(SearchFieldKeys.HOW_CHARITY_HELPS_KEYWORD, publicCharityDetails.getHowTheCharityHelps().stream().toArray(String[]::new));
		}

		if (Validator.isNotNull(publicCharityDetails.getOtherCharityNames())) {
			document.addText(SearchFieldKeys.OTHER_NAMES, publicCharityDetails.getOtherCharityNames().stream().toArray(String[]::new));
			document.addText(SearchFieldKeys.OTHER_NAMES_KEYWORD, publicCharityDetails.getOtherCharityNames().stream().toArray(String[]::new));
		}

		if (Validator.isNotNull(publicCharityDetails.getLocations())) {
			document.addText(SearchFieldKeys.LOCATIONS, publicCharityDetails.getLocations().stream().toArray(String[]::new));
			document.addText(SearchFieldKeys.LOCATIONS_KEYWORD, publicCharityDetails.getLocations().stream().toArray(String[]::new));
		}

	}

	@Override
	protected Summary doGetSummary(Document document, Locale locale, String snippet, PortletRequest portletRequest, PortletResponse portletResponse) throws Exception {

		Summary summary = createSummary(document);
		String replacement = StringUtil.replace(summary.getContent(), new String[] { HighlightUtil.HIGHLIGHT_TAG_OPEN, HighlightUtil.HIGHLIGHT_TAG_CLOSE }, _ESCAPE_SAFE_HIGHLIGHTS);
		replacement = replacement.replaceAll("<(\\/)?(.*?)>", StringPool.BLANK);
		replacement = StringUtil.replace(replacement, _ESCAPE_SAFE_HIGHLIGHTS, new String[] { HighlightUtil.HIGHLIGHT_TAG_OPEN, HighlightUtil.HIGHLIGHT_TAG_CLOSE });
		summary.setContent(replacement);
		summary.setMaxContentLength(MAX_CONTENT_LENGTH);

		return summary;
	}

	@Override
	protected void doReindex(String[] companyIds) throws Exception {

		for (String companyId : companyIds) {

			try {

				long companyIdLong = GetterUtil.getLong(companyId);

				if (companyIdLong == 0) {
					companyIdLong = portal.getDefaultCompanyId();
				}

				reIndexCompany(companyIdLong);

			} catch (Exception e) {
				LOG.error("Error re-indexing companyId: " + companyId, e);
			}

		}

	}

	private void reIndexCompany(long companyId) throws PortalException {
		long groupId = charitySearchService.getCompanyGroupId(companyId);

		int maxIndex = charityPublicDataService.getCharityCount();
		int startIndex = CHARITY_IMPORT_START_INDEX;
		int endIndex = getEndIndex(startIndex, maxIndex);

		while (startIndex < maxIndex) {
			List<uk.gov.ccew.publicdata.model.Charity> charities = charityPublicDataService.getCharities(startIndex, endIndex);

			LOG.debug("Getting batch:" + startIndex + "-" + endIndex + "/" + maxIndex);
			for (uk.gov.ccew.publicdata.model.Charity publicCharityDetails : charities) {

				try {
					long registeredNumber = GetterUtil.getLong(publicCharityDetails.getCharityNumber());
					long organisationNumber = GetterUtil.getLong(publicCharityDetails.getOrganisationNumber());
					Charity liferayCharity = charityLiferayService.createCharity(organisationNumber);

					liferayCharity.setOrganizationId(organisationNumber);
					liferayCharity.setRegistrationId(registeredNumber);
					liferayCharity.setCompanyId(companyId);
					liferayCharity.setFullName(publicCharityDetails.getCharityName());
					liferayCharity.setGovDocDescription(publicCharityDetails.getCharitableObjects());

					charityLiferayService.updateCharity(liferayCharity, groupId);
				} catch (Exception e) {
					LOG.error("Error importing charity. Charity Number: " + publicCharityDetails.getOrganisationNumber(), e);
				}
			}

			startIndex = endIndex;
			endIndex = getEndIndex(startIndex, maxIndex);
		}
	}

	private int getEndIndex(int endIndex, int maxIndex) {
		return endIndex + CHARITY_IMPORT_BATCH_SIZE > maxIndex ? maxIndex : endIndex + CHARITY_IMPORT_BATCH_SIZE;
	}

	@Override
	protected void doReindex(String className, long classPK) throws Exception {
		// Not implemented
	}

	@Override
	protected void doReindex(Charity liferayCharity) throws Exception {
		LOG.debug("Re-indexing charity:" + liferayCharity.getFullName());
		Document document = getDocument(liferayCharity);

		indexWriterHelper.updateDocument(getSearchEngineId(), liferayCharity.getCompanyId(), document, isCommitImmediately());
	}

}

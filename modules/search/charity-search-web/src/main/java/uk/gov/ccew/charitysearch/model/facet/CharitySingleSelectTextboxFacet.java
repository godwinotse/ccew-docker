package uk.gov.ccew.charitysearch.model.facet;

import java.util.Collections;
import java.util.Map;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.WildcardQuery;
import com.liferay.portal.kernel.search.facet.MultiValueFacet;
import com.liferay.portal.kernel.search.filter.Filter;
import com.liferay.portal.kernel.search.filter.QueryFilter;
import com.liferay.portal.kernel.search.generic.BooleanQueryImpl;
import com.liferay.portal.kernel.search.generic.WildcardQueryImpl;
import com.liferay.portal.kernel.util.Validator;

public class CharitySingleSelectTextboxFacet extends MultiValueFacet implements CharityFacet {

	private static final Log LOG = LogFactoryUtil.getLog(CharitySingleSelectTextboxFacet.class);

	private CharityFacetValue value;

	public CharitySingleSelectTextboxFacet(SearchContext searchContext, String fieldName) {
		super(searchContext);
		setFieldName(fieldName);
		setStatic(false);
	}

	@Override
	public BooleanClause<Filter> doGetFacetFilterBooleanClause() {

		BooleanQuery searchQuery = new BooleanQueryImpl();
		if (Validator.isNull(value) || !value.isSelected()) {
			return null;
		}

		for (String splitValue : value.getTerm().toLowerCase().split(StringPool.SPACE)) {
			String trimmedValue = splitValue.trim();
			try {
				WildcardQuery wildcardQuery;

				if (trimmedValue.contains(StringPool.STAR)) {
					wildcardQuery = new WildcardQueryImpl(getFieldName(), trimmedValue);
				} else {
					wildcardQuery = new WildcardQueryImpl(getFieldName(), StringPool.STAR + trimmedValue + StringPool.STAR);
				}

				searchQuery.add(wildcardQuery, BooleanClauseOccur.MUST);
			} catch (ParseException e) {
				LOG.error("Error creating boolean clause for term:" + trimmedValue, e);
			}

		}

		if (searchQuery.hasClauses()) {

			QueryFilter wildFilter = new QueryFilter(searchQuery);
			LOG.debug("SingleSelectTxtFacet:" + wildFilter.toString());
			return BooleanClauseFactoryUtil.createFilter(getSearchContext(), wildFilter, BooleanClauseOccur.MUST);

		}

		return null;

	}

	public void setValue(CharityFacetValue value) {
		this.value = value;
	}

	public CharityFacetValue getValue() {
		return value;
	}

	@Override
	public CharityFacetType getCharityFacetType() {
		return CharityFacetType.SINGLE_SELECT_TEXTBOX;
	}

	@Override
	public Map<String, String> getUrlParameters() {

		return value == null ? Collections.emptyMap() : Collections.singletonMap(getFieldName(), value.getTerm());

	}

}

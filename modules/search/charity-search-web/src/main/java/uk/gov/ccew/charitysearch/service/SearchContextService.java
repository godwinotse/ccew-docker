package uk.gov.ccew.charitysearch.service;

import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.QueryConfig;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.search.constants.SearchContextAttributes;

import uk.gov.ccew.service.liferaystore.model.Charity;

@Component(immediate = true, service = SearchContextService.class)
public class SearchContextService {

	@Reference
	private CharitySearchService charitySearchService;

	@Reference
	private Portal portal;

	@SuppressWarnings("unchecked")
	public void addRequiredClauseToContext(SearchContext searchContext, String fieldName, String fieldValue) {
		List<BooleanClause<Query>> booleanClauses = ListUtil.fromArray(searchContext.getBooleanClauses());
		BooleanClause<Query> clause = BooleanClauseFactoryUtil.create(fieldName, fieldValue, BooleanClauseOccur.MUST.toString());
		booleanClauses.add(clause);

		searchContext.setBooleanClauses(booleanClauses.toArray(new BooleanClause[booleanClauses.size()]));
	}

	@SuppressWarnings("unchecked")
	public void addBooleanClausesToContext(SearchContext searchContext, List<BooleanClause<Query>> clauses) {

		List<BooleanClause<Query>> existingClauses = ListUtil.fromArray(searchContext.getBooleanClauses());
		existingClauses.addAll(clauses);

		searchContext.setBooleanClauses(existingClauses.toArray(new BooleanClause[existingClauses.size()]));
	}

	public SearchContext getSearchContext(RenderRequest renderRequest, SearchContainer<uk.gov.ccew.publicdata.model.Charity> searchContainer) throws PortalException {
		SearchContext searchContext = getSearchContext(renderRequest);

		searchContext.setStart(searchContainer.getStart());
		searchContext.setEnd(searchContainer.getEnd());

		return searchContext;
	}

	public SearchContext getSearchContext(PortletRequest portletRequest) throws PortalException {

		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		SearchContext searchContext = SearchContextFactory.getInstance(portal.getHttpServletRequest(portletRequest));

		addCharitySettingsToContext(searchContext, themeDisplay);

		return searchContext;
	}

	private void addCharitySettingsToContext(SearchContext searchContext, ThemeDisplay themeDisplay) throws PortalException {

		setEntryClassNames(searchContext);
		setBooleanClauses(searchContext);
		searchContext.setGroupIds(new long[] { charitySearchService.getCompanyGroupId(searchContext.getCompanyId()) });
		searchContext.setAttribute(SearchContextAttributes.ATTRIBUTE_KEY_EMPTY_SEARCH, true);

		QueryConfig queryConfig = searchContext.getQueryConfig();
		queryConfig.setLocale(themeDisplay.getLocale());
		queryConfig.setScoreEnabled(true);

	}

	private void setBooleanClauses(SearchContext searchContext) {
		addRequiredClauseToContext(searchContext, Field.ENTRY_CLASS_NAME, Charity.class.getName());
	}

	private void setEntryClassNames(SearchContext searchContext) {
		searchContext.setEntryClassNames(new String[] { Charity.class.getName() });
	}

	public SearchContext getSearchContextForFilters(PortletRequest portletRequest) throws PortalException {

		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		SearchContext searchContext = new SearchContext();
		searchContext.setCompanyId(themeDisplay.getCompanyId());
		searchContext.setGroupIds(new long[] { themeDisplay.getScopeGroupId() });
		searchContext.setLayout(themeDisplay.getLayout());
		searchContext.setLocale(themeDisplay.getLocale());
		searchContext.setTimeZone(themeDisplay.getTimeZone());
		searchContext.setUserId(themeDisplay.getUserId());

		addCharitySettingsToContext(searchContext, themeDisplay);

		return searchContext;
	}

}

package uk.gov.ccew.charitysearch.model.facet;

public class CharityFacetValue {

	private int count = 0;

	private boolean selected = false;

	private String term;

	public CharityFacetValue(String term, int count, boolean selected) {
		this.count = count;
		this.selected = selected;
		this.term = term;
	}

	public int getCount() {
		return count;
	}

	public String getTerm() {
		return term;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public void setTerm(String term) {
		this.term = term;
	}

}

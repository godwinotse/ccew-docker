package uk.gov.ccew.charitysearch.model.facet;

import uk.gov.ccew.charitysearch.web.constant.SearchFieldKeys;

public enum CharityFacetField {

	CONTINENT_OF_OPERATION(SearchFieldKeys.CONTINENT_KEYWORD, CharityFacetType.MULTI_SELECT),

	COUNTRY_OF_OPERATION(SearchFieldKeys.COUNTRY_KEYWORD, CharityFacetType.MULTI_SELECT),

	EXPENDITURE(SearchFieldKeys.EXPENDITURE_SORTABLE, CharityFacetType.RANGE),

	HOW_CHARITY_HELPS(SearchFieldKeys.HOW_CHARITY_HELPS_KEYWORD, CharityFacetType.MULTI_SELECT),

	INCOME(SearchFieldKeys.INCOME_SORTABLE, CharityFacetType.RANGE),

	LOCAL_AUTHORITY_OPERATION(SearchFieldKeys.LOCAL_AUTHORITY_KEYWORD, CharityFacetType.MULTI_SELECT),

	LOCAL_AUTHORITY_OPERATION_ENGLAND(SearchFieldKeys.LOCAL_AUTHORITY_ENGLAND_KEYWORD, CharityFacetType.MULTI_SELECT),

	LOCAL_AUTHORITY_OPERATION_LONDON(SearchFieldKeys.LOCAL_AUTHORITY_LONDON_KEYWORD, CharityFacetType.MULTI_SELECT),

	LOCAL_AUTHORITY_OPERATION_WALES(SearchFieldKeys.LOCAL_AUTHORITY_WALES_KEYWORD, CharityFacetType.MULTI_SELECT),

	PRIMARY_PURPOSE_GRANT_GIVING(SearchFieldKeys.GRANT_GIVING_IS_PRIMARY, CharityFacetType.SINGLE_SELECT_CHECKBOX),

	REGISTRATION_DATE(SearchFieldKeys.REGISTRATION_DATE_SORTABLE, CharityFacetType.DATE_RANGE),

	REMOVED_DATE(SearchFieldKeys.REMOVED_DATE_SORTABLE, CharityFacetType.DATE_RANGE),

	STATUS(SearchFieldKeys.CHARITY_STATUS_SORTABLE, CharityFacetType.MULTI_SELECT),

	TRUSTEES(SearchFieldKeys.TRUSTEES, CharityFacetType.SINGLE_SELECT_TEXTBOX),

	WHAT_CHARITY_DOES(SearchFieldKeys.WHAT_CHARITY_DOES_KEYWORD, CharityFacetType.MULTI_SELECT),

	WHO_CHARITY_HELPS(SearchFieldKeys.WHO_CHARITY_HELPS_KEYWORD, CharityFacetType.MULTI_SELECT);

	private CharityFacetType charityFacetType;

	private String fieldName;

	CharityFacetField(String fieldName, CharityFacetType charityFacetType) {
		this.fieldName = fieldName;
		this.charityFacetType = charityFacetType;
	}

	public CharityFacetType getCharityFacetType() {
		return charityFacetType;
	}

	public String getFieldName() {
		return fieldName;
	}

}

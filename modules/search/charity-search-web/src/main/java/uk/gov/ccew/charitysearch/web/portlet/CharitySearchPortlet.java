package uk.gov.ccew.charitysearch.web.portlet;

import uk.gov.ccew.charitysearch.web.constant.PortletKeys;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.ccew", "com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.css-class-wrapper=charity-search", "com.liferay.portlet.instanceable=false", "javax.portlet.init-param.template-path=/", "javax.portlet.display-name=Charity Search",
		"javax.portlet.name=" + PortletKeys.CHARITY_SEARCH_PORTLET, "javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.supports.mime-type=text/html", "javax.portlet.version=3.0" }, service = Portlet.class)
public class CharitySearchPortlet extends MVCPortlet {

}

package uk.gov.ccew.charitysearch.model.facet;

import java.util.HashMap;
import java.util.Map;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.facet.RangeFacet;
import com.liferay.portal.kernel.search.filter.Filter;
import com.liferay.portal.kernel.search.filter.RangeTermFilter;
import com.liferay.portal.kernel.util.Validator;

public class CharityRangeFacet extends RangeFacet implements CharityFacet {

	private static final Log LOG = LogFactoryUtil.getLog(CharityRangeFacet.class);

	private String rangeFrom;

	private String rangeTo;

	public CharityRangeFacet(SearchContext searchContext, String fieldName) {
		super(searchContext);
		setFieldName(fieldName);
		setStatic(false);
	}

	@Override
	public BooleanClause<Filter> doGetFacetFilterBooleanClause() {
		SearchContext searchContext = getSearchContext();
		if (Validator.isNull(rangeFrom) || Validator.isNull(rangeTo)) {
			return null;
		} else {
			RangeTermFilter rangeTermFilter = new RangeTermFilter(getFieldName(), true, true, rangeFrom, rangeTo);

			LOG.debug("Range Filter:" + rangeTermFilter);
			return BooleanClauseFactoryUtil.createFilter(searchContext, rangeTermFilter, BooleanClauseOccur.MUST);
		}
	}

	@Override
	public CharityFacetType getCharityFacetType() {
		return CharityFacetType.RANGE;
	}

	public String getFromFieldName() {
		return getFieldName() + "From";
	}

	public String getRangeFrom() {
		return rangeFrom;
	}

	public String getRangeTo() {
		return rangeTo;
	}

	public String getToFieldName() {
		return getFieldName() + "To";
	}

	public void setRangeFrom(String rangeFrom) {
		this.rangeFrom = rangeFrom;
	}

	public void setRangeTo(String rangeTo) {
		this.rangeTo = rangeTo;
	}

	@Override
	public Map<String, String> getUrlParameters() {
		Map<String, String> params = new HashMap<>();

		params.put(getFromFieldName(), getRangeFrom());
		params.put(getToFieldName(), getRangeTo());

		return params;
	}

}

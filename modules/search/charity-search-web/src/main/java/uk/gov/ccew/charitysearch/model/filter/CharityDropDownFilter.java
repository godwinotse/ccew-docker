package uk.gov.ccew.charitysearch.model.filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.util.Validator;

public class CharityDropDownFilter extends BaseCharityFilter {

	private String fieldName;

	private List<String> filterOptions = new ArrayList<>();

	public CharityDropDownFilter(String filterName, String fieldName) {
		super(filterName);
		this.fieldName = fieldName;
	}

	@Override
	public List<BooleanClause<Query>> getBooleanClauses() {

		List<BooleanClause<Query>> booleanclauses = super.getBooleanClauses();

		if (Validator.isNotNull(getSelectedValue())) {
			BooleanClause<Query> booleanclause = BooleanClauseFactoryUtil.create(fieldName, getSelectedValue(), BooleanClauseOccur.MUST.toString());
			booleanclauses.add(booleanclause);
		}

		return booleanclauses;

	}

	public String getFieldName() {
		return fieldName;
	}

	public List<String> getFilterOptions() {
		return filterOptions.stream().sorted().collect(Collectors.toList());
	}

	public void setFilterOptions(Collection<String> filterOptions) {
		this.filterOptions.addAll(filterOptions);
	}

}

package uk.gov.ccew.charitysearch.web.constant;

public final class MVCCommandKeys {

	public static final String ADVANCED_SEARCH_RENDER = "/advanced-search";

	public static final String GET_CONTINENT_COUNTRIES = "/get-continent-countries";

	public static final String SEARCH_RESULTS_RENDER = "/search-results";

	public static final String SIMPLE_SEARCH_RENDER = "/simple-search";

	private MVCCommandKeys() {

	}

}

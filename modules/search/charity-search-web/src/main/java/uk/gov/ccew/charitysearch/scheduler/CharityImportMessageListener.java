package uk.gov.ccew.charitysearch.scheduler;

import java.util.Date;
import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.DestinationNames;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.liferay.portal.kernel.scheduler.SchedulerEngineHelper;
import com.liferay.portal.kernel.scheduler.SchedulerEntryImpl;
import com.liferay.portal.kernel.scheduler.SchedulerException;
import com.liferay.portal.kernel.scheduler.StorageType;
import com.liferay.portal.kernel.scheduler.Trigger;
import com.liferay.portal.kernel.scheduler.TriggerFactory;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;

import uk.gov.ccew.charitysearch.service.CharitySearchService;
import uk.gov.ccew.service.liferaystore.model.Charity;

@Component(immediate = true, service = CharityImportMessageListener.class)
public class CharityImportMessageListener extends BaseMessageListener {

	private static final String DEFAULT_CRON_EXPRESSION = "0 0 2/24 ? * * *";

	private static final Log LOG = LogFactoryUtil.getLog(CharityImportMessageListener.class);

	private volatile boolean initialized;

	private SchedulerEntryImpl schedulerEntryImpl;

	private StorageType storageType = StorageType.MEMORY_CLUSTERED;

	@Reference
	private SchedulerEngineHelper schedulerEngineHelper;

	@Reference
	private TriggerFactory triggerFactory;

	@Reference
	private CharitySearchService charitySearchService;

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED)
	private ModuleServiceLifecycle portalInitialized;

	@Override
	protected void doReceive(Message message) throws Exception {
		LOG.info("Beginning scheduled charity importer.... - " + DEFAULT_CRON_EXPRESSION);
		Indexer<Charity> charityIndexer = IndexerRegistryUtil.getIndexer(Charity.class);
		charityIndexer.reindex(charitySearchService.getCompanyIds());
	}

	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) throws SchedulerException {
		String listenerClass = getClass().getName();
		Trigger jobTrigger = triggerFactory.createTrigger(listenerClass, listenerClass, new Date(), null, DEFAULT_CRON_EXPRESSION);

		schedulerEntryImpl = CharityScheduleEntry.fromStorageType(storageType);
		schedulerEntryImpl.setEventListenerClass(listenerClass);
		schedulerEntryImpl.setTrigger(jobTrigger);

		schedulerEngineHelper.register(this, schedulerEntryImpl, DestinationNames.SCHEDULER_DISPATCH);
		initialized = true;
		LOG.info("Activated scheduler: " + listenerClass + " cronExpression: " + DEFAULT_CRON_EXPRESSION);

	}

	@Deactivate
	protected void deactivate() {
		if (initialized) {
			LOG.debug("Removing charity import schedule.");
			try {
				schedulerEngineHelper.unschedule(schedulerEntryImpl, storageType);
			} catch (SchedulerException e) {
				LOG.error("Error deactivating schedule entry:" + this);
			}
		}
	}

}

package uk.gov.ccew.charitysearch.model.facet;

public enum CharityFacetType {

	DATE_RANGE, DROPDOWN, MULTI_SELECT, RANGE, SINGLE_SELECT_CHECKBOX, SINGLE_SELECT_TEXTBOX;

	public boolean isDateRange() {
		return this == DATE_RANGE;
	}

	public boolean isDropDown() {
		return this == DROPDOWN;
	}

	public boolean isMultiSelect() {
		return this == MULTI_SELECT;
	}

	public boolean isRange() {
		return this == RANGE;
	}

	public boolean isSingleSelectCheckbox() {
		return this == SINGLE_SELECT_CHECKBOX;
	}

	public boolean isSingleSelectTextbox() {
		return this == SINGLE_SELECT_TEXTBOX;
	}

}

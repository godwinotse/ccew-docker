package uk.gov.ccew.charitysearch.model.facet;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.filter.Filter;
import com.liferay.portal.kernel.search.filter.RangeTermFilter;
import com.liferay.portal.kernel.util.Validator;

import uk.gov.ccew.charitysearch.model.DateUtil;

public class CharityDateRangeFacet extends CharityRangeFacet {

	private static final Log LOG = LogFactoryUtil.getLog(CharityDateRangeFacet.class);

	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.UK);

	public CharityDateRangeFacet(SearchContext searchContext, String fieldName) {
		super(searchContext, fieldName);
	}

	@Override
	public BooleanClause<Filter> doGetFacetFilterBooleanClause() {
		SearchContext searchContext = getSearchContext();

		if (Validator.isNull(getRangeFrom()) || Validator.isNull(getRangeTo())) {
			return null;
		} else {

			LocalDate fromDate = LocalDate.parse(getRangeFrom(), formatter);
			LocalDate toDate = LocalDate.parse(getRangeTo(), formatter);

			long fromEpoch = fromDate.atStartOfDay(DateUtil.getCharityDefaultZone()).toEpochSecond();
			long toEpoch = toDate.atTime(LocalTime.MAX).atZone(DateUtil.getCharityDefaultZone()).toEpochSecond();

			RangeTermFilter rangeTermFilter = new RangeTermFilter(getFieldName(), true, true, String.valueOf(fromEpoch), String.valueOf(toEpoch));

			LOG.debug("Date Range Filter:" + rangeTermFilter);

			return BooleanClauseFactoryUtil.createFilter(searchContext, rangeTermFilter, BooleanClauseOccur.MUST);

		}
	}

	@Override
	public CharityFacetType getCharityFacetType() {
		return CharityFacetType.DATE_RANGE;
	}

}

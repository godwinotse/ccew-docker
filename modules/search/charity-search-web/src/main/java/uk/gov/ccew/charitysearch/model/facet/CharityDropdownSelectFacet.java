package uk.gov.ccew.charitysearch.model.facet;

import java.util.Collections;
import java.util.Map;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.filter.Filter;
import com.liferay.portal.kernel.search.filter.TermsFilter;
import com.liferay.portal.kernel.util.Validator;

public class CharityDropdownSelectFacet extends CharityMultiSelectFacet {

	private static final Log LOG = LogFactoryUtil.getLog(CharityDropdownSelectFacet.class);

	private CharityFacetValue value;

	public CharityDropdownSelectFacet(SearchContext searchContext, String fieldName) {
		super(searchContext, fieldName);
		setStatic(false);
	}

	@Override
	public BooleanClause<Filter> doGetFacetFilterBooleanClause() {

		if (Validator.isNull(value) || !value.isSelected()) {
			return null;
		}

		SearchContext searchContext = getSearchContext();
		TermsFilter facetTermsFilter = new TermsFilter(getFieldName());
		facetTermsFilter.addValue(value.getTerm());

		LOG.debug("CharityDropdownSelectFacet Filter:" + facetTermsFilter);
		return BooleanClauseFactoryUtil.createFilter(searchContext, facetTermsFilter, BooleanClauseOccur.MUST);

	}

	public void setValue(CharityFacetValue value) {
		this.value = value;
	}

	public CharityFacetValue getValue() {
		return value;
	}

	@Override
	public CharityFacetType getCharityFacetType() {
		return CharityFacetType.DROPDOWN;
	}

	@Override
	public Map<String, String> getUrlParameters() {

		return value == null ? Collections.emptyMap() : Collections.singletonMap(getFieldName(), value.getTerm());

	}

}

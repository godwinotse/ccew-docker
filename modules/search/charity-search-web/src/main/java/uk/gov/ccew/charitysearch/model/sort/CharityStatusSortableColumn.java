package uk.gov.ccew.charitysearch.model.sort;

import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;

import uk.gov.ccew.charitysearch.web.constant.SearchFieldKeys;

public class CharityStatusSortableColumn implements SortableColumn {

	public static CharityStatusSortableColumn getNewInstance() {
		return new CharityStatusSortableColumn();
	}

	@Override
	public Sort[] getDefaultSorts() {

		return getSorts(Boolean.FALSE);

	}

	@Override
	public Sort[] getSorts(boolean reverse) {

		Sort sortRelevance = SortFactoryUtil.create(null, Sort.SCORE_TYPE, false);
		Sort sortStatus = SortFactoryUtil.create(SearchFieldKeys.CHARITY_STATUS_SORTABLE, reverse);

		return new Sort[] { sortStatus, sortRelevance };

	}

}

package uk.gov.ccew.charitysearch.web.util;

import uk.gov.ccew.charitysearch.web.constant.SearchFieldKeys;

public final class FieldNameUtil {

	public static String getCountryFieldNameForContinent(String continent) {
		return SearchFieldKeys.COUNTRY_KEYWORD.replace("Country", "Country_" + continent);
	}

}

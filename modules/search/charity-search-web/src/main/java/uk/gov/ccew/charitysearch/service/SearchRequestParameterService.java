package uk.gov.ccew.charitysearch.service;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;

import uk.gov.ccew.charitysearch.model.facet.CharityDropdownSelectFacet;
import uk.gov.ccew.charitysearch.model.facet.CharityFacet;
import uk.gov.ccew.charitysearch.model.facet.CharityFacetValue;
import uk.gov.ccew.charitysearch.model.facet.CharityMultiValueFacet;
import uk.gov.ccew.charitysearch.model.facet.CharityRangeFacet;
import uk.gov.ccew.charitysearch.model.facet.CharitySingleSelectCheckboxFacet;
import uk.gov.ccew.charitysearch.model.facet.CharitySingleSelectTextboxFacet;

@Component(immediate = true, service = SearchRequestParameterService.class)
public class SearchRequestParameterService {

	public void setRangeParametersOnFacet(CharityFacet charityFacet, RenderRequest renderRequest) {

		CharityRangeFacet charityRangeFacet = (CharityRangeFacet) charityFacet;
		String rangeFrom = ParamUtil.getString(renderRequest, charityRangeFacet.getFromFieldName());
		String rangeTo = ParamUtil.getString(renderRequest, charityRangeFacet.getToFieldName());

		charityRangeFacet.setRangeFrom(rangeFrom);
		charityRangeFacet.setRangeTo(rangeTo);

	}

	public void setMultiSelectParametersOnFacet(CharityFacet charityFacet, PortletRequest portletRequest) {

		String[] facetParams = ParamUtil.getParameterValues(portletRequest, charityFacet.getFieldName(), null);

		CharityMultiValueFacet charityMultiValueFacet = (CharityMultiValueFacet) charityFacet;
		if (Validator.isNotNull(facetParams) && facetParams.length > 0) {
			for (String facetParam : facetParams) {
				if (Boolean.FALSE.toString().equals(facetParam))
					continue;

				charityMultiValueFacet.addValue(new CharityFacetValue(facetParam, 0, true));

			}
		}

	}

	public void setSingleSelectTextboxParameterOnFacet(CharityFacet charityFacet, RenderRequest renderRequest) {

		CharitySingleSelectTextboxFacet charitySingleSelectTextboxtFacet = (CharitySingleSelectTextboxFacet) charityFacet;
		String value = ParamUtil.getString(renderRequest, charitySingleSelectTextboxtFacet.getFieldName());

		if (Validator.isNotNull(value)) {
			charitySingleSelectTextboxtFacet.setValue(new CharityFacetValue(value, 0, true));
		}

	}

	public void setSingleSelectCheckboxParameterOnFacet(CharityFacet charityFacet, RenderRequest renderRequest) {

		CharitySingleSelectCheckboxFacet charitySingleSelectCheckboxFacet = (CharitySingleSelectCheckboxFacet) charityFacet;
		String value = ParamUtil.getString(renderRequest, charitySingleSelectCheckboxFacet.getFieldName());

		if (Validator.isNotNull(value) && !Boolean.FALSE.toString().equalsIgnoreCase(value)) {
			charitySingleSelectCheckboxFacet.setValue(new CharityFacetValue(value, 0, true));
		}

	}

	public void setSingleSelectDropdownParameterOnFacet(CharityFacet charityFacet, RenderRequest renderRequest) {

		CharityDropdownSelectFacet charitySingleSelectDropdownFacet = (CharityDropdownSelectFacet) charityFacet;
		String value = ParamUtil.getString(renderRequest, charitySingleSelectDropdownFacet.getFieldName());

		if (Validator.isNotNull(value)) {
			charitySingleSelectDropdownFacet.setValue(new CharityFacetValue(value, 0, true));
		}

	}

}

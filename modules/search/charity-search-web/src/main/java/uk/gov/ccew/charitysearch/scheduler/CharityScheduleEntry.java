package uk.gov.ccew.charitysearch.scheduler;

import com.liferay.portal.kernel.scheduler.SchedulerEntry;
import com.liferay.portal.kernel.scheduler.SchedulerEntryImpl;
import com.liferay.portal.kernel.scheduler.StorageType;
import com.liferay.portal.kernel.scheduler.StorageTypeAware;
import com.liferay.portal.kernel.scheduler.Trigger;

@SuppressWarnings("serial")
public class CharityScheduleEntry extends SchedulerEntryImpl implements SchedulerEntry, StorageTypeAware {

	private SchedulerEntryImpl schedulerEntryImpl = new SchedulerEntryImpl();

	private StorageType storageType;

	public static CharityScheduleEntry fromStorageType(StorageType storageType) {
		return new CharityScheduleEntry(storageType);
	}

	private CharityScheduleEntry(StorageType storageType) {
		super();
		this.storageType = storageType;
	}

	@Override
	public String getDescription() {
		return schedulerEntryImpl.getDescription();
	}

	@Override
	public String getEventListenerClass() {
		return schedulerEntryImpl.getEventListenerClass();
	}

	@Override
	public StorageType getStorageType() {
		return this.storageType;
	}

	@Override
	public Trigger getTrigger() {
		return schedulerEntryImpl.getTrigger();
	}

	@Override
	public void setDescription(final String description) {
		schedulerEntryImpl.setDescription(description);
	}

	@Override
	public void setTrigger(final Trigger trigger) {
		schedulerEntryImpl.setTrigger(trigger);
	}

	@Override
	public void setEventListenerClass(final String eventListenerClass) {
		schedulerEntryImpl.setEventListenerClass(eventListenerClass);
	}

}

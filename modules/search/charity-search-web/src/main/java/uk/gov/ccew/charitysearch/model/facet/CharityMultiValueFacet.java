package uk.gov.ccew.charitysearch.model.facet;

import java.util.List;

public interface CharityMultiValueFacet extends CharityFacet {

	void addValue(CharityFacetValue charityFacetValue);

	CharityFacetValue getValue(String term);

	List<CharityFacetValue> getValues();
}

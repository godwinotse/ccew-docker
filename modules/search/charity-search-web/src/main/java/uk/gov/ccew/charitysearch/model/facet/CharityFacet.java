package uk.gov.ccew.charitysearch.model.facet;

import java.util.Map;

import com.liferay.portal.kernel.search.facet.Facet;

public interface CharityFacet extends Facet {

	CharityFacetType getCharityFacetType();

	Map<String, String> getUrlParameters();

}

package uk.gov.ccew.charitysearch.model.sort;

import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;

import uk.gov.ccew.charitysearch.web.constant.SearchFieldKeys;

public class CharityIncomeSortableColumn implements SortableColumn {

	public static CharityIncomeSortableColumn getNewInstance() {
		return new CharityIncomeSortableColumn();
	}

	@Override
	public Sort[] getDefaultSorts() {

		return getSorts(Boolean.TRUE);

	}

	@Override
	public Sort[] getSorts(boolean reverse) {

		return new Sort[] { SortFactoryUtil.create(SearchFieldKeys.INCOME_SORTABLE, reverse) };

	}

}

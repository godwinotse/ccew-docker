package uk.gov.ccew.charitysearch.service;

import java.util.Optional;

import uk.gov.ccew.charitysearch.model.filter.CharityCheckBoxFilter;
import uk.gov.ccew.charitysearch.model.filter.CharityDropDownFilter;
import uk.gov.ccew.charitysearch.model.filter.CharityFilter;
import uk.gov.ccew.charitysearch.model.filter.CharityFilterType;
import uk.gov.ccew.charitysearch.web.constant.FilterValueConstants;
import uk.gov.ccew.charitysearch.web.constant.SearchFieldKeys;

public class SearchFilterFactory {

	public static Optional<CharityFilter> getFilter(CharityFilterType charityFilterType) {

		CharityFilter charityFilter = null;

		if (charityFilterType == CharityFilterType.CONTINENT) {

			charityFilter = new CharityDropDownFilter(charityFilterType.getFilterName(), SearchFieldKeys.CONTINENT_KEYWORD);

		} else if (charityFilterType == CharityFilterType.COUNTRY) {

			charityFilter = new CharityDropDownFilter(charityFilterType.getFilterName(), SearchFieldKeys.COUNTRY_KEYWORD);

		} else if (charityFilterType == CharityFilterType.ENGLAND_AND_WALES_CHECK_BOX) {

			charityFilter = new CharityCheckBoxFilter(charityFilterType.getFilterName());

		} else if (charityFilterType == CharityFilterType.ENGLAND_CHECKBOX) {

			charityFilter = new CharityCheckBoxFilter(charityFilterType.getFilterName());
			charityFilter.addRequiredFilterTerm(SearchFieldKeys.REGION_KEYWORD, FilterValueConstants.REGION_ENGLAND);

		} else if (charityFilterType == CharityFilterType.LONDON_CHECKBOX) {

			charityFilter = new CharityCheckBoxFilter(charityFilterType.getFilterName());
			charityFilter.addRequiredFilterTerm(SearchFieldKeys.REGION_KEYWORD, FilterValueConstants.REGION_LONDON);

		} else if (charityFilterType == CharityFilterType.WALES_CHECKBOX) {

			charityFilter = new CharityCheckBoxFilter(charityFilterType.getFilterName());
			charityFilter.addRequiredFilterTerm(SearchFieldKeys.REGION_KEYWORD, FilterValueConstants.REGION_WALES);

		}

		return Optional.ofNullable(charityFilter);

	}

}

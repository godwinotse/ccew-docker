package uk.gov.ccew.charitysearch.web.portlet;

import java.util.Map;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.facet.Facet;
import com.liferay.portal.kernel.util.ParamUtil;

import uk.gov.ccew.charitysearch.service.SearchContextService;
import uk.gov.ccew.charitysearch.service.SearchFacetFactory;
import uk.gov.ccew.charitysearch.service.SearchFacetService;
import uk.gov.ccew.charitysearch.service.SearchIndexService;
import uk.gov.ccew.charitysearch.web.constant.MVCCommandKeys;
import uk.gov.ccew.charitysearch.web.constant.PortletKeys;
import uk.gov.ccew.charitysearch.web.constant.SearchFieldKeys;
import uk.gov.ccew.charitysearch.web.util.FieldNameUtil;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.CHARITY_SEARCH_PORTLET,
		"mvc.command.name=" + MVCCommandKeys.GET_CONTINENT_COUNTRIES }, service = MVCResourceCommand.class)
public class ContinentCountriesMVCResourceCommand implements MVCResourceCommand {

	private static final Log LOG = LogFactoryUtil.getLog(ContinentCountriesMVCResourceCommand.class);

	@Reference
	private SearchContextService searchContextService;

	@Reference
	private SearchFacetService searchFacetService;

	@Reference
	private SearchIndexService searchIndexService;

	@Override
	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) {
		String continent = ParamUtil.getString(resourceRequest, "continent");

		try {

			SearchContext searchContext = searchContextService.getSearchContext(resourceRequest);
			searchContextService.addRequiredClauseToContext(searchContext, SearchFieldKeys.CONTINENT_KEYWORD, continent);
			Facet facet = SearchFacetFactory.getSimpleFacet(FieldNameUtil.getCountryFieldNameForContinent(continent), searchContext);
			searchContext.addFacet(facet);

			searchIndexService.search(searchContext);

			Map<String, Integer> termsAndFrequencies = searchFacetService.getTermsAndFrequenciesFromFacet(facet);

			JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
			termsAndFrequencies.forEach((term, frequency) -> {
				jsonArray.put(term);
			});

			resourceResponse.getWriter().print(jsonArray.toJSONString());
			resourceResponse.getWriter().close();

			LOG.debug("JSON:" + jsonArray.toJSONString());
			return true;

		} catch (Exception e) {
			LOG.error("Error retrieving countries for continent: " + continent, e);
			return false;
		}
	}
}

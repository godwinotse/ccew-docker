package uk.gov.ccew.charitysearch.model.filter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Query;

public abstract class BaseCharityFilter implements CharityFilter {

	private String filterName;

	private boolean selected = false;

	private String selectedValue;

	private Map<String, String> queryFieldsAndValues = new HashMap<>();

	public BaseCharityFilter(String filterName) {
		this.filterName = filterName;
	}

	@Override
	public void addRequiredFilterTerm(String fieldName, String value) {
		queryFieldsAndValues.put(fieldName, value);
	}

	@Override
	public List<BooleanClause<Query>> getBooleanClauses() {

		List<BooleanClause<Query>> booleanclauses = new ArrayList<>();

		for (String fieldName : queryFieldsAndValues.keySet()) {
			BooleanClause<Query> booleanclause = BooleanClauseFactoryUtil.create(fieldName, queryFieldsAndValues.get(fieldName), BooleanClauseOccur.MUST.toString());
			booleanclauses.add(booleanclause);
		}

		return booleanclauses;

	}

	@Override
	public String getFilterName() {
		return filterName;
	}

	@Override
	public boolean isSelected() {
		return selected;
	}

	@Override
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Override
	public String getSelectedValue() {
		return selectedValue;
	}

	@Override
	public void setSelectedValue(String selectedValue) {
		this.selectedValue = selectedValue;
	}

}

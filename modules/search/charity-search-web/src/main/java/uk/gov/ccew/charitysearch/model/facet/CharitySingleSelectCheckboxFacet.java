package uk.gov.ccew.charitysearch.model.facet;

import java.util.Collections;
import java.util.Map;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.facet.MultiValueFacet;
import com.liferay.portal.kernel.search.filter.Filter;
import com.liferay.portal.kernel.search.filter.TermsFilter;
import com.liferay.portal.kernel.util.Validator;

public class CharitySingleSelectCheckboxFacet extends MultiValueFacet implements CharityFacet {

	private static final Log LOG = LogFactoryUtil.getLog(CharitySingleSelectCheckboxFacet.class);

	private CharityFacetValue value;

	public CharitySingleSelectCheckboxFacet(SearchContext searchContext, String fieldName) {
		super(searchContext);
		setFieldName(fieldName);
		setStatic(false);
	}

	@Override
	public BooleanClause<Filter> doGetFacetFilterBooleanClause() {
		SearchContext searchContext = getSearchContext();
		TermsFilter facetTermsFilter = new TermsFilter(getFieldName());

		if (Validator.isNotNull(value) && value.isSelected()) {

			facetTermsFilter.addValue(value.getTerm());

			LOG.debug("SingleSelectChkFacet Filter:" + facetTermsFilter);
			return BooleanClauseFactoryUtil.createFilter(searchContext, facetTermsFilter, BooleanClauseOccur.MUST);
		}

		return null;

	}

	public void setValue(CharityFacetValue value) {
		this.value = value;
	}

	public CharityFacetValue getValue() {
		return value;
	}

	@Override
	public CharityFacetType getCharityFacetType() {
		return CharityFacetType.SINGLE_SELECT_CHECKBOX;
	}

	@Override
	public Map<String, String> getUrlParameters() {

		return value == null ? Collections.emptyMap() : Collections.singletonMap(getFieldName(), value.getTerm());

	}

}

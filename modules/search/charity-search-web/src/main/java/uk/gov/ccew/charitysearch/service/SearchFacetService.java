package uk.gov.ccew.charitysearch.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.facet.Facet;
import com.liferay.portal.kernel.search.facet.collector.FacetCollector;
import com.liferay.portal.kernel.search.facet.collector.TermCollector;
import com.liferay.portal.kernel.util.Validator;

import uk.gov.ccew.charitysearch.model.facet.CharityFacet;
import uk.gov.ccew.charitysearch.model.facet.CharityFacetField;
import uk.gov.ccew.charitysearch.model.facet.CharityFacetValue;
import uk.gov.ccew.charitysearch.model.facet.CharityMultiValueFacet;

@Component(immediate = true, service = SearchFacetService.class)
public class SearchFacetService {

	@Reference
	private SearchContextService searchContextService;

	@Reference
	private SearchIndexService searchIndexService;

	@Reference
	private SearchRequestParameterService searchRequestParameterService;

	public void setFacetsToDisplayOnSearchContext(SearchContext searchContext) {

		for (CharityFacetField charityFacetField : CharityFacetField.values()) {
			CharityFacet charityFacet = SearchFacetFactory.getCharitySearchFacet(charityFacetField, searchContext);

			searchContext.addFacet(charityFacet);
		}

	}

	public Map<String, Integer> getTermsAndFrequenciesFromFacet(Facet facet) {
		Map<String, Integer> termsAndFrequencies = new HashMap<>();

		if (Validator.isNotNull(facet)) {
			FacetCollector facetCollector = facet.getFacetCollector();
			for (TermCollector termCollector : facetCollector.getTermCollectors()) {
				String term = termCollector.getTerm();
				int frequency = termCollector.getFrequency();
				termsAndFrequencies.put(term, frequency);
			}
		}

		return termsAndFrequencies;
	}

	public Map<String, List<Facet>> getPageSectionsFacetMap(SearchContext searchContext) {

		Map<String, List<Facet>> sectionFacets = new LinkedHashMap<>();

		List<Facet> sectionOneFacets = new ArrayList<>();
		sectionOneFacets.add(searchContext.getFacet(CharityFacetField.STATUS.getFieldName()));
		sectionOneFacets.add(searchContext.getFacet(CharityFacetField.REGISTRATION_DATE.getFieldName()));
		sectionOneFacets.add(searchContext.getFacet(CharityFacetField.REMOVED_DATE.getFieldName()));
		sectionFacets.put("advanced-section-1", sectionOneFacets);

		List<Facet> sectionTwoFacets = new ArrayList<>();
		sectionTwoFacets.add(searchContext.getFacet(CharityFacetField.INCOME.getFieldName()));
		sectionFacets.put("advanced-section-2", sectionTwoFacets);

		List<Facet> sectionThreeFacets = new ArrayList<>();
		sectionThreeFacets.add(searchContext.getFacet(CharityFacetField.EXPENDITURE.getFieldName()));
		sectionFacets.put("advanced-section-3", sectionThreeFacets);

		List<Facet> sectionFourFacets = new ArrayList<>();
		sectionFourFacets.add(searchContext.getFacet(CharityFacetField.WHAT_CHARITY_DOES.getFieldName()));
		sectionFourFacets.add(searchContext.getFacet(CharityFacetField.HOW_CHARITY_HELPS.getFieldName()));
		sectionFourFacets.add(searchContext.getFacet(CharityFacetField.WHO_CHARITY_HELPS.getFieldName()));
		sectionFourFacets.add(searchContext.getFacet(CharityFacetField.PRIMARY_PURPOSE_GRANT_GIVING.getFieldName()));
		sectionFacets.put("advanced-section-4", sectionFourFacets);

		sectionFacets.put("advanced-section-5", Collections.emptyList());

		List<Facet> sectionSixFacets = new ArrayList<>();
		sectionSixFacets.add(searchContext.getFacet(CharityFacetField.TRUSTEES.getFieldName()));
		sectionFacets.put("advanced-section-6", sectionSixFacets);

		return sectionFacets;

	}

	public void addResultTermsAndFequenciesToFacets(PortletRequest portletRequest, SearchContext searchContext) throws PortalException {

		Map<String, Facet> facetMap = searchContext.getFacets();

		if (facetMap == null)
			return;

		for (Facet liferayFacet : facetMap.values()) {
			if (liferayFacet instanceof CharityMultiValueFacet) {

				CharityMultiValueFacet charityMultiValueFacet = (CharityMultiValueFacet) liferayFacet;

				Map<String, Integer> termsAndFrequencies = getTermsAndFrequenciesFromFacet(charityMultiValueFacet);
				setTermsAndFrequenciesOnFacet(charityMultiValueFacet, termsAndFrequencies);

			}
		}
	}

	private void setTermsAndFrequenciesOnFacet(CharityMultiValueFacet charityMultiValueFacet, Map<String, Integer> termsAndFrequencies) {

		for (String term : termsAndFrequencies.keySet()) {
			int frequency = termsAndFrequencies.get(term);
			CharityFacetValue charityFacetValue = charityMultiValueFacet.getValue(term);

			if (Validator.isNull(charityFacetValue)) {
				charityMultiValueFacet.addValue(new CharityFacetValue(term, frequency, false));
			} else {
				charityFacetValue.setCount(frequency);
			}
		}

	}

	public void addRequestValuesToFacets(SearchContext searchContext, RenderRequest renderRequest) {

		for (Facet liferayFacet : searchContext.getFacets().values()) {
			CharityFacet charityFacet = (CharityFacet) liferayFacet;
			if (charityFacet.getCharityFacetType().isRange() || charityFacet.getCharityFacetType().isDateRange()) {

				searchRequestParameterService.setRangeParametersOnFacet(charityFacet, renderRequest);

			} else if (charityFacet.getCharityFacetType().isMultiSelect()) {

				searchRequestParameterService.setMultiSelectParametersOnFacet(charityFacet, renderRequest);

			} else if (charityFacet.getCharityFacetType().isSingleSelectCheckbox()) {

				searchRequestParameterService.setSingleSelectCheckboxParameterOnFacet(charityFacet, renderRequest);

			} else if (charityFacet.getCharityFacetType().isSingleSelectTextbox()) {

				searchRequestParameterService.setSingleSelectTextboxParameterOnFacet(charityFacet, renderRequest);

			} else if (charityFacet.getCharityFacetType().isDropDown()) {

				searchRequestParameterService.setSingleSelectDropdownParameterOnFacet(charityFacet, renderRequest);

			}
		}
	}

	public void setIteratorUrlFacetParams(SearchContext searchContext, PortletURL iteratorURL) {

		for (Facet liferayFacet : searchContext.getFacets().values()) {
			CharityFacet charityFacet = (CharityFacet) liferayFacet;

			Map<String, String> params = charityFacet.getUrlParameters();
			for (Entry<String, String> param : params.entrySet()) {
				iteratorURL.getRenderParameters().setValue(param.getKey(), param.getValue());
			}
		}

	}

}

package uk.gov.ccew.charitysearch.model.filter;

public class CharityFilterValue {

	private boolean selected = false;

	private String term;

	public CharityFilterValue(String term, boolean selected) {
		this.selected = selected;
		this.term = term;
	}

	public String getTerm() {
		return term;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setTerm(String term) {
		this.term = term;
	}

}

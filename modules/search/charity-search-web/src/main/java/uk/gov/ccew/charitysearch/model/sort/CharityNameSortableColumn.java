package uk.gov.ccew.charitysearch.model.sort;

import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;

import uk.gov.ccew.charitysearch.web.constant.SearchFieldKeys;

public class CharityNameSortableColumn implements SortableColumn {

	public static CharityNameSortableColumn getNewInstance() {
		return new CharityNameSortableColumn();
	}

	@Override
	public Sort[] getDefaultSorts() {

		return getSorts(Boolean.FALSE);

	}

	@Override
	public Sort[] getSorts(boolean reverse) {

		return new Sort[] { SortFactoryUtil.create(SearchFieldKeys.CHARITY_NAME_SORTABLE, reverse) };

	}

}

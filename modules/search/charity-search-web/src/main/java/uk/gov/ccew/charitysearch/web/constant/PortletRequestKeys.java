package uk.gov.ccew.charitysearch.web.constant;

public final class PortletRequestKeys {

	public static final String CHARITIES = "charities";

	public static final String CHARITY_FILTERS = "charityFilters";

	public static final String KEYWORDS = "keywords";

	public static final String LIFERAY_CHARITY = "liferayCharity";

	public static final String PREVIOUS_COLUMN_NAME = "prevCol";

	public static final String PUBLIC_DATA_CHARITY = "publicDataCharity";

	public static final String SEARCH_CONTAINER = "searchContainer";

	public static final String SEARCH_CONTEXT = "searchContext";

	public static final String SEARCH_PAGE_SECTIONS = "searchPageSections";

	public static final String TOTAL_RESULTS = "totalResults";

	private PortletRequestKeys() {
	}
}

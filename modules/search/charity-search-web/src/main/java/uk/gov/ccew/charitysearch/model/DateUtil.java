package uk.gov.ccew.charitysearch.model;

import java.time.ZoneId;
import java.util.Date;

public final class DateUtil {

	private static final long START_OF_EPOCH = 0l;

	private static final ZoneId CHARITY_DEFAULT_ZONE = ZoneId.of("Europe/London");

	private DateUtil() {

	}

	public static long getTimeInMillis(Date date) {

		if (date != null) {

			return date.toInstant().atZone(CHARITY_DEFAULT_ZONE).toEpochSecond();

		} else {

			return START_OF_EPOCH;

		}

	}

	public static ZoneId getCharityDefaultZone() {
		return CHARITY_DEFAULT_ZONE;
	}
}

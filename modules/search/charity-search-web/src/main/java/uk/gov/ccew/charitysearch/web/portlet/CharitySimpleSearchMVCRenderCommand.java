package uk.gov.ccew.charitysearch.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;

import uk.gov.ccew.charitysearch.web.constant.PortletKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.CHARITY_SEARCH_PORTLET, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class CharitySimpleSearchMVCRenderCommand implements MVCRenderCommand {

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		return "/search/simple_search.jsp";

	}

}

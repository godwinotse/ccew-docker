package uk.gov.ccew.charitysearch.web.constant;

public final class FilterValueConstants {

	public static final String GREATER_LONDON = "Greater London";

	public static final String REGION_ENGLAND = "Throughout England";

	public static final String REGION_LONDON = "Throughout London";

	public static final String REGION_WALES = "Throughout Wales";

	private FilterValueConstants() {

	}

}

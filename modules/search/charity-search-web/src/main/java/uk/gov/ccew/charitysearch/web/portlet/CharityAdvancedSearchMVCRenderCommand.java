package uk.gov.ccew.charitysearch.web.portlet;

import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.search.SearchContext;

import uk.gov.ccew.charitysearch.model.filter.CharityFilter;
import uk.gov.ccew.charitysearch.service.SearchContextService;
import uk.gov.ccew.charitysearch.service.SearchFacetService;
import uk.gov.ccew.charitysearch.service.SearchFilterService;
import uk.gov.ccew.charitysearch.service.SearchIndexService;
import uk.gov.ccew.charitysearch.web.constant.MVCCommandKeys;
import uk.gov.ccew.charitysearch.web.constant.PortletKeys;
import uk.gov.ccew.charitysearch.web.constant.PortletRequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.CHARITY_SEARCH_PORTLET, "mvc.command.name=" + MVCCommandKeys.ADVANCED_SEARCH_RENDER }, service = MVCRenderCommand.class)
public class CharityAdvancedSearchMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private SearchContextService searchContextService;

	@Reference
	private SearchIndexService searchIndexService;

	@Reference
	private SearchFacetService searchFacetService;

	@Reference
	private SearchFilterService searchFilterService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		try {
			SearchContext searchContext = searchContextService.getSearchContext(renderRequest);

			Map<String, CharityFilter> charityFilters = searchFilterService.getCharityFilters(renderRequest);
			searchFilterService.addRequestValuesToFilters(charityFilters.values(), renderRequest, searchContext);

			searchFacetService.setFacetsToDisplayOnSearchContext(searchContext);
			searchIndexService.search(searchContext);
			searchFacetService.addResultTermsAndFequenciesToFacets(renderRequest, searchContext);

			renderRequest.setAttribute(PortletRequestKeys.CHARITY_FILTERS, charityFilters);
			renderRequest.setAttribute(PortletRequestKeys.SEARCH_CONTEXT, searchContext);
			renderRequest.setAttribute(PortletRequestKeys.SEARCH_PAGE_SECTIONS, searchFacetService.getPageSectionsFacetMap(searchContext));

		} catch (Exception e) {
			throw new PortletException(e);
		}

		return "/search/advanced_search.jsp";

	}

}

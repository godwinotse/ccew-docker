package uk.gov.ccew.charitysearch.service;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;

import uk.gov.ccew.charitysearch.web.constant.SearchFieldKeys;
import uk.gov.ccew.onereg.model.OneRegCharity;
import uk.gov.ccew.onereg.service.OneRegCharityService;
import uk.gov.ccew.publicdata.service.CharityService;

@Component(immediate = true, service = CharitySearchService.class)
public class CharitySearchService {

	@Reference
	private CharityService charityPublicDataService;

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private OneRegCharityService oneRegCharityService;

	@Reference
	private Portal portal;

	@Reference
	private UserLocalService userLocalService;

	public String[] getCompanyIds() {

		Long[] companyIds = companyLocalService.getCompanies().stream().map(Company::getCompanyId).toArray(Long[]::new);

		return ArrayUtil.toStringArray(companyIds);
	}

	public long getCompanyGroupId(long companyId) throws PortalException {
		return companyLocalService.getCompany(companyId).getGroupId();
	}

	public long getCompanyDefaultUserId(long companyId) throws PortalException {
		return userLocalService.getDefaultUserId(companyId);
	}

	public uk.gov.ccew.publicdata.model.Charity getPublicDataCharity(long organisationId) {
		return charityPublicDataService.getCharityByOrganisationNumber(organisationId);
	}

	public List<OneRegCharity> getCharitiesFromDocuments(Document[] documents, PortletRequest portletRequest) throws PortalException {

		List<OneRegCharity> charities = new ArrayList<>();

		if (Validator.isNotNull(documents)) {

			for (Document result : documents) {

				long organisationNumber = GetterUtil.getLong(result.get(SearchFieldKeys.ORGANISATION_NUMBER));
				OneRegCharity charity = oneRegCharityService.getCharity(organisationNumber, portletRequest);
				charities.add(charity);

			}

		}

		return charities;

	}

}

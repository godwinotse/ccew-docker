package uk.gov.ccew.charitysearch.service;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.util.ParamUtil;

import uk.gov.ccew.charitysearch.web.constant.MVCCommandKeys;
import uk.gov.ccew.charitysearch.web.constant.PortletRequestKeys;
import uk.gov.ccew.publicdata.model.Charity;

@Component(immediate = true, service = SearchContainerService.class)
public class SearchContainerService {

	private static int NUMBER_OF_RESULTS_PER_PAGE = 10;

	public SearchContainer<Charity> createSearchContainer(RenderRequest renderRequest, RenderResponse renderResponse) {

		int delta = NUMBER_OF_RESULTS_PER_PAGE;
		String orderByCol = ParamUtil.getString(renderRequest, "orderByCol");
		String orderByType = ParamUtil.getString(renderRequest, "orderByType");

		PortletURL iteratorURL = renderResponse.createRenderURL();
		iteratorURL.getRenderParameters().setValue(PortletRequestKeys.PREVIOUS_COLUMN_NAME, orderByCol);
		iteratorURL.getRenderParameters().setValue("mvcRenderCommandName", MVCCommandKeys.SEARCH_RESULTS_RENDER);

		SearchContainer<Charity> searchContainer = new SearchContainer<>(renderRequest, null, null, SearchContainer.DEFAULT_CUR_PARAM, delta, iteratorURL, null, "no-search-results-were-found");
		searchContainer.setId("search-result-entries");
		searchContainer.setDeltaConfigurable(true);
		searchContainer.setOrderByCol(orderByCol);
		searchContainer.setOrderByType(orderByType);

		return searchContainer;

	}

}

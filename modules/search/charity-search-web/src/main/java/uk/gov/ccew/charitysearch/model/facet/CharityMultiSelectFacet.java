package uk.gov.ccew.charitysearch.model.facet;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.facet.MultiValueFacet;
import com.liferay.portal.kernel.search.facet.util.FacetValueValidator;
import com.liferay.portal.kernel.search.filter.Filter;
import com.liferay.portal.kernel.search.filter.TermsFilter;
import com.liferay.portal.kernel.util.StringUtil;

import uk.gov.ccew.charitysearch.service.SearchFacetFactory;

public class CharityMultiSelectFacet extends MultiValueFacet implements CharityMultiValueFacet {

	private static final Log LOG = LogFactoryUtil.getLog(CharityMultiSelectFacet.class);

	private Map<String, CharityFacetValue> charityFacetValues = new HashMap<>();

	public CharityMultiSelectFacet(SearchContext searchContext, String fieldName) {
		super(searchContext);
		setFieldName(fieldName);
		setStatic(false);

		JSONObject facetData = SearchFacetFactory.getFacetConfigurationData();
		getFacetConfiguration().setDataJSONObject(facetData);

	}

	@Override
	public void addValue(CharityFacetValue charityFacetValue) {
		charityFacetValues.put(charityFacetValue.getTerm(), charityFacetValue);
	}

	@Override
	public BooleanClause<Filter> doGetFacetFilterBooleanClause() {
		SearchContext searchContext = getSearchContext();
		TermsFilter facetTermsFilter = new TermsFilter(getFieldName());

		if (charityFacetValues.isEmpty())
			return null;

		for (CharityFacetValue filterValue : charityFacetValues.values()) {
			FacetValueValidator facetValueValidator = getFacetValueValidator();

			if (!facetValueValidator.check(searchContext, filterValue.getTerm())) {
				continue;
			}

			facetTermsFilter.addValue(filterValue.getTerm());
		}

		if (facetTermsFilter.isEmpty()) {
			return null;
		} else {
			LOG.debug("MultiSelectFacet Filter:" + facetTermsFilter);
			return BooleanClauseFactoryUtil.createFilter(searchContext, facetTermsFilter, BooleanClauseOccur.MUST);
		}

	}

	@Override
	public CharityFacetType getCharityFacetType() {
		return CharityFacetType.MULTI_SELECT;
	}

	@Override
	public CharityFacetValue getValue(String term) {
		return charityFacetValues.get(term);
	}

	@Override
	public List<CharityFacetValue> getValues() {
		Comparator<CharityFacetValue> charityValueComparator = (CharityFacetValue charityFacetValue1, CharityFacetValue charityFacetValue2) -> charityFacetValue1.getTerm()
				.compareTo(charityFacetValue2.getTerm());

		return charityFacetValues.values().stream().sorted(charityValueComparator).collect(Collectors.toList());
	}

	@Override
	public Map<String, String> getUrlParameters() {

		List<String> paramValues = charityFacetValues.values().stream().map(CharityFacetValue::getTerm).collect(Collectors.toList());
		return Collections.singletonMap(getFieldName(), StringUtil.merge(paramValues));

	}

}

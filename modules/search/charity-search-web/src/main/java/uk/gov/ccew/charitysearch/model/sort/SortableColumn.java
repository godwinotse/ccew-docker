package uk.gov.ccew.charitysearch.model.sort;

import com.liferay.portal.kernel.search.Sort;

public interface SortableColumn {

	Sort[] getDefaultSorts();

	Sort[] getSorts(boolean reverse);

}

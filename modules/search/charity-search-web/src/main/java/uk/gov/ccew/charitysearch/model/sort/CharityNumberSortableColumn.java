package uk.gov.ccew.charitysearch.model.sort;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;

import uk.gov.ccew.charitysearch.web.constant.SearchFieldKeys;

public class CharityNumberSortableColumn implements SortableColumn {

	public static CharityNumberSortableColumn getNewInstance() {
		return new CharityNumberSortableColumn();
	}

	@Override
	public Sort[] getDefaultSorts() {

		List<Sort> defaultSorts = new ArrayList<>(3);
		defaultSorts.add(SortFactoryUtil.create(null, Sort.SCORE_TYPE, false));
		defaultSorts.addAll(ListUtil.toList(getSorts(Boolean.FALSE)));

		return defaultSorts.stream().toArray(Sort[]::new);

	}

	@Override
	public Sort[] getSorts(boolean reverse) {

		Sort sortCharityNumber = SortFactoryUtil.create(SearchFieldKeys.CHARITY_NUMBER_SORTABLE, reverse);
		Sort sortSubsidiarySuffix = SortFactoryUtil.create(SearchFieldKeys.SUBSIDIARY_SUFFIX_SORTABLE, reverse);

		return new Sort[] { sortCharityNumber, sortSubsidiarySuffix };

	}

}

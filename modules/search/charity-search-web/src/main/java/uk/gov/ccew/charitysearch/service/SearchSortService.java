package uk.gov.ccew.charitysearch.service;

import java.util.HashMap;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;

import uk.gov.ccew.charitysearch.model.sort.CharityIncomeSortableColumn;
import uk.gov.ccew.charitysearch.model.sort.CharityNameSortableColumn;
import uk.gov.ccew.charitysearch.model.sort.CharityNumberSortableColumn;
import uk.gov.ccew.charitysearch.model.sort.CharityReportingSortableColumn;
import uk.gov.ccew.charitysearch.model.sort.CharityStatusSortableColumn;
import uk.gov.ccew.charitysearch.model.sort.SortableColumn;
import uk.gov.ccew.charitysearch.web.constant.PortletRequestKeys;

@Component(immediate = true, service = SearchSortService.class)
public class SearchSortService {

	private static final String CHARITY_INCOME_COLUMN = "charity-income";

	private static final String CHARITY_NAME_COLUMN = "charity-name";

	private static final String CHARITY_NUMBER_COLUMN = "charity-number";

	private static final String CHARITY_REPORTING_COLUMN = "charity-reporting";

	private static final String CHARITY_STATUS_COLUMN = "charity-status";

	private static final String SORT_ASC = "asc";

	private static final String SORT_DESC = "desc";

	private static final HashMap<String, SortableColumn> SORTABLE_COLUMNS = new HashMap<>();

	static {

		SORTABLE_COLUMNS.put(CHARITY_INCOME_COLUMN, CharityIncomeSortableColumn.getNewInstance());
		SORTABLE_COLUMNS.put(CHARITY_NAME_COLUMN, CharityNameSortableColumn.getNewInstance());
		SORTABLE_COLUMNS.put(CHARITY_NUMBER_COLUMN, CharityNumberSortableColumn.getNewInstance());
		SORTABLE_COLUMNS.put(CHARITY_REPORTING_COLUMN, CharityReportingSortableColumn.getNewInstance());
		SORTABLE_COLUMNS.put(CHARITY_STATUS_COLUMN, CharityStatusSortableColumn.getNewInstance());

	}

	private boolean hasColumnChanged(String previousColumn, String currentColumn) {
		if (Validator.isNull(previousColumn) || Validator.isNull(currentColumn)) {
			return true;
		} else {
			return !previousColumn.equals(currentColumn);
		}
	}

	private boolean isReverse(String orderByType) {
		return SORT_DESC.equals(orderByType);
	}

	public void setContainerAndContextSorts(SearchContext searchContext, SearchContainer<uk.gov.ccew.publicdata.model.Charity> searchContainer) {

		String previousColumnName = ParamUtil.getString(searchContainer.getPortletRequest(), PortletRequestKeys.PREVIOUS_COLUMN_NAME, null);
		String currentColumnName = searchContainer.getOrderByCol();
		boolean hasColumnChanged = hasColumnChanged(previousColumnName, currentColumnName);
		Sort[] sorts = null;
		boolean reverse = isReverse(searchContainer.getOrderByType());

		if (!Validator.isNull(currentColumnName)) {

			SortableColumn sortableColumn = SORTABLE_COLUMNS.get(currentColumnName);

			if (hasColumnChanged) {
				sorts = sortableColumn.getDefaultSorts();
			} else {
				sorts = sortableColumn.getSorts(reverse);
			}

		} else {

			sorts = SORTABLE_COLUMNS.get(CHARITY_NUMBER_COLUMN).getDefaultSorts();
			searchContainer.setOrderByCol(CHARITY_NUMBER_COLUMN);

		}

		searchContext.setSorts(sorts);
		setSearchContainerSortAttributes(searchContext, searchContainer);
	}

	private void setSearchContainerSortAttributes(SearchContext searchContext, SearchContainer<uk.gov.ccew.publicdata.model.Charity> searchContainer) {
		Sort[] sorts = searchContext.getSorts();

		if (sorts != null && sorts.length > 0) {
			Sort sort = searchContext.getSorts()[0];
			String orderByType = sort.isReverse() ? SORT_DESC : SORT_ASC;
			searchContainer.setOrderByType(orderByType);
			searchContainer.getIteratorURL().getRenderParameters().setValue(PortletRequestKeys.PREVIOUS_COLUMN_NAME, searchContainer.getOrderByCol());
		}
	}
}

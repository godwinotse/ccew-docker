package uk.gov.ccew.charitysearch.web.constant;

public final class PortletKeys {

	public static final String CHARITY_SEARCH_PORTLET = "uk_gov_ccew_portlet_CharitySearchPortlet";

	private PortletKeys() {
	}
}

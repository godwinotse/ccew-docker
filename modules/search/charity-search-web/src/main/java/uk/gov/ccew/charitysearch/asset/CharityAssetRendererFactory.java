package uk.gov.ccew.charitysearch.asset;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetRenderer;
import com.liferay.asset.kernel.model.AssetRendererFactory;
import com.liferay.asset.kernel.model.BaseAssetRendererFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.PermissionChecker;

import uk.gov.ccew.charitysearch.web.constant.PortletKeys;
import uk.gov.ccew.onereg.service.OneRegCharityService;
import uk.gov.ccew.onereg.service.OneRegUrlService;
import uk.gov.ccew.service.liferaystore.model.Charity;
import uk.gov.ccew.service.liferaystore.service.CharityLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.CHARITY_SEARCH_PORTLET }, service = AssetRendererFactory.class)
public class CharityAssetRendererFactory extends BaseAssetRendererFactory<Charity> {

	public static final String CLASS_NAME = Charity.class.getName();

	public static final String CSS_CLASS = "charity";

	private static final boolean LINKABLE = Boolean.FALSE;

	public static final String TYPE = "charity";

	@Reference
	private CharityLocalService charityLiferayService;

	@Reference
	private OneRegCharityService oneRegCharityService;

	@Reference
	private OneRegUrlService oneRegUrlService;

	private ServletContext servletContext;

	public CharityAssetRendererFactory() {
		setClassName(CLASS_NAME);
		setLinkable(LINKABLE);
		setPortletId(PortletKeys.CHARITY_SEARCH_PORTLET);
		setSearchable(true);
		setSelectable(true);
	}

	@Override
	public AssetRenderer<Charity> getAssetRenderer(long classPK) throws PortalException {

		Charity liferayCharity = charityLiferayService.getCharity(classPK);
		CharityAssetRenderer charityAssetRenderer = new CharityAssetRenderer(liferayCharity, oneRegCharityService, oneRegUrlService);

		charityAssetRenderer.setServletContext(servletContext);

		return charityAssetRenderer;
	}

	@Override
	public AssetRenderer<Charity> getAssetRenderer(long classPK, int type) throws PortalException {

		Charity liferayCharity = charityLiferayService.getCharity(classPK);
		CharityAssetRenderer charityAssetRenderer = new CharityAssetRenderer(liferayCharity, oneRegCharityService, oneRegUrlService);

		charityAssetRenderer.setAssetRendererType(type);
		charityAssetRenderer.setServletContext(servletContext);

		return charityAssetRenderer;
	}

	@Override
	public String getClassName() {
		return CLASS_NAME;
	}

	@Override
	public String getIconCssClass() {
		return CSS_CLASS;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public boolean hasPermission(PermissionChecker permissionChecker, long classPK, String actionId) throws Exception {
		return Boolean.TRUE;
	}

	@Override
	public boolean isLinkable() {
		return LINKABLE;
	}

	@Reference(target = "(osgi.web.symbolicname=uk.gov.ccew.charitysearch.web)", unbind = "-")
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

}

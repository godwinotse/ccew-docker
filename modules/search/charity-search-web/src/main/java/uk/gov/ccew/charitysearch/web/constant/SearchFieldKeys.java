package uk.gov.ccew.charitysearch.web.constant;

public final class SearchFieldKeys {

	public static final String ADDRESS = "charity_Address";

	public static final String CHARITY_NAME = "charity_Name";

	public static final String CHARITY_NAME_SORTABLE = "charity_Name_String_sortable";

	public static final String CHARITY_NUMBER = "charity_Number";

	public static final String CHARITY_NUMBER_SORTABLE = "charity_Number_Number_sortable";

	public static final String CHARITY_STATUS = "charity_Status";

	public static final String CHARITY_STATUS_SORTABLE = "charity_Status_String_sortable";

	public static final String CONSTITUENCY_KEYWORD = "charity_Constituency_sortable";

	public static final String CONTINENT_KEYWORD = "charity_Continent_sortable";

	public static final String COUNTRY_KEYWORD = "charity_Country_sortable";

	public static final String EXPENDITURE = "charity_Expenditure";

	public static final String EXPENDITURE_SORTABLE = "charity_Expenditure_Number_sortable";

	public static final String GRANT_GIVING_IS_PRIMARY = "charity_GrantGiving_Primary_sortable";

	public static final String HOW_CHARITY_HELPS_KEYWORD = "charity_How_Charity_Helps_sortable";

	public static final String INCOME = "charity_Income";

	public static final String INCOME_SORTABLE = "charity_Income_Number_sortable";

	public static final String LOCAL_AUTHORITY_KEYWORD = "charity_Local_Authority_sortable";
	
	public static final String LOCAL_AUTHORITY_WALES_KEYWORD = "charity_Local_WalesAuthority_sortable";
	
	public static final String LOCAL_AUTHORITY_ENGLAND_KEYWORD = "charity_Local_EnglandAuthority_sortable";
	
	public static final String LOCAL_AUTHORITY_LONDON_KEYWORD = "charity_Local_LondonAuthority_sortable";

	public static final String LOCATIONS = "charity_Locations";

	public static final String LOCATIONS_KEYWORD = "charity_Locations_sortable";

	public static final String OBJECTS = "charity_Objects";

	public static final String ORGANISATION_NUMBER = "charity_Organisation_Number";

	public static final String OTHER_NAMES = "charity_OtherNames";

	public static final String OTHER_NAMES_KEYWORD = "charity_OtherNames_sortable";

	public static final String REGION_KEYWORD = "charity_Region_sortable";

	public static final String REGISTRATION_DATE = "charity_Registration_Date";

	public static final String REGISTRATION_DATE_SORTABLE = "charity_Registration_Date_sortable";

	public static final String REMOVED_DATE = "charity_Removed_Date";

	public static final String REMOVED_DATE_SORTABLE = "charity_Removed_Date_sortable";

	public static final String REPORTING_DAYS_OVERDUE = "charity_ReportingDaysOverdue";

	public static final String REPORTING_DAYS_OVERDUE_SORTABLE = "charity_ReportingDaysOverdue_Number_sortable";

	public static final String REPORTING_STATUS = "charity_ReportingStatus";

	public static final String REPORTING_STATUS_SORTABLE = "charity_ReportingStatus_sortable";

	public static final String SUBSIDIARY_SUFFIX = "charity_Subsidiary_Suffix";

	public static final String SUBSIDIARY_SUFFIX_SORTABLE = "charity_Subsidiary_Suffix_String_sortable";

	public static final String TRUSTEES = "charity_Trustees_sortable";

	public static final String WHAT_CHARITY_DOES = "charity_What_Charity_Does";

	public static final String WHAT_CHARITY_DOES_KEYWORD = "charity_What_Charity_Does_sortable";

	public static final String WHO_CHARITY_HELPS_KEYWORD = "charity_Who_Charity_Helps_sortable";

	private SearchFieldKeys() {
	}
}

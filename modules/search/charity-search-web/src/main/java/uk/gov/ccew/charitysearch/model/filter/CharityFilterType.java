package uk.gov.ccew.charitysearch.model.filter;

public enum CharityFilterType {

	CONTINENT("filterContinent"),

	COUNTRY("filterCountry"),

	ENGLAND_AND_WALES_CHECK_BOX("filterInEnglandOrWalesCheckBox"),

	ENGLAND_CHECKBOX("filterInEnglandCheckBox"),

	LONDON_CHECKBOX("filterInLondonCheckBox"),

	WALES_CHECKBOX("filterInWalesCheckBox");

	private String filterName;

	CharityFilterType(String filterName) {
		this.filterName = filterName;
	}

	public String getFilterName() {
		return filterName;
	}

}

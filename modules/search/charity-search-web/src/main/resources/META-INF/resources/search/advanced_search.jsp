<%@ include file="../init.jsp"%>

<portlet:renderURL var="searchResultsRenderURL" >
	<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.SEARCH_RESULTS_RENDER %>" />
</portlet:renderURL>

<aui:form action="${ searchResultsRenderURL }" method="post" name="fm">
	<%@ include file="../fragments/display_search_box.jspf" %>

	<%@ include file="../fragments/display_sections.jspf" %>
</aui:form>

<aui:script use="uk-gov-ccew-search-facetselector">

	A.FacetSelector.initFacetSelector();

</aui:script>
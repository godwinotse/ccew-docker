<%@ include file="../init.jsp"%>

<portlet:renderURL var="searchResultsRenderURL" >
	<portlet:param name="mvcRenderCommandName" value="<%=MVCCommandKeys.SEARCH_RESULTS_RENDER %>" />
</portlet:renderURL>

<aui:form action="${ searchResultsRenderURL }" method="post" name="fm">
	<aui:container>
		<aui:row>
			<aui:col>
				<c:set var="showAdvancedLink" value="true" />
				<%@ include file="../fragments/display_search_box.jspf" %>

				${ totalResults }
				<c:choose>
					<c:when test="${ totalResults eq 1 }">
						<liferay-ui:message key="match-found" />
					</c:when>
					<c:otherwise>
						<liferay-ui:message key="matches-found" />
					</c:otherwise>
				</c:choose>
			</aui:col>
		</aui:row>
		<aui:row>
			<aui:col md="4">
				<%@ include file="../fragments/display_sections.jspf" %>
			</aui:col>

			<aui:col md="8">
				<liferay-ui:search-container searchContainer="${ searchContainer }" orderByType="${ searchContainer.orderByType }" >

					<liferay-ui:search-container-results results="${ charities }" />

					<liferay-ui:search-container-row className="uk.gov.ccew.onereg.model.OneRegCharity" modelVar="charity" keyProperty="organisationNumber">
						<%@ include file="../fragments/display_charity_entry.jspf" %>
					</liferay-ui:search-container-row>

					<liferay-ui:search-iterator />

				</liferay-ui:search-container>
			</aui:col>
		</aui:row>
	</aui:container>
</aui:form>

<aui:script use="uk-gov-ccew-search-facetselector">

	A.FacetSelector.initFacetSelector();

</aui:script>
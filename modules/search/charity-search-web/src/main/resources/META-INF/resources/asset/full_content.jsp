<%@ include file="../init.jsp" %>

<strong><liferay-ui:message key="charity-name"/>:</strong>${ publicDataCharity.charityName } <br>

<strong><liferay-ui:message key="charity-number"/>:</strong>${ publicDataCharity.charityNumberFormatted } <br>

<strong><liferay-ui:message key="charity-address"/>:</strong>${ publicDataCharity.address } <br>

<strong><liferay-ui:message key="charity-locations"/>:</strong><br>
<c:forEach items="${ publicDataCharity.getLocations() }" var="location">
	&nbsp; ${ location } <br>
</c:forEach>


<strong><liferay-ui:message key="charity-purposes"/>:</strong><br>
<c:forEach items="${ publicDataCharity.getWhatTheCharityDoes() }" var="purpose">
	&nbsp; ${ purpose } <br>
</c:forEach>

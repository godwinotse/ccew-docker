<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/frontend" prefix="liferay-frontend"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ taglib uri="http://gov.uk.ccew/tld/charity-search-taglib" prefix="charity-search"%>

<%@ page import="com.liferay.petra.string.StringPool"%>

<%@ page import="uk.gov.ccew.charitysearch.web.constant.MVCCommandKeys"%>
<%@ page import="uk.gov.ccew.charitysearch.web.constant.PortletKeys"%>
<%@ page import="uk.gov.ccew.charitysearch.web.constant.PortletRequestKeys"%>
<%@ page import="uk.gov.ccew.charitysearch.web.constant.SearchFieldKeys"%>
<%@ page import="uk.gov.ccew.service.liferaystore.model.Charity"%>

<liferay-theme:defineObjects />

<portlet:defineObjects />
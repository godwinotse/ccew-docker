<%@ include file="../init.jsp" %>fr

<strong><liferay-ui:message key="charity-name"/>:</b>${ publicDataCharity.charityName } </strong><br>

<strong><liferay-ui:message key="charity-number"/>:</b>${ publicDataCharity.charityNumberFormatted } </strong><br>

<strong><liferay-ui:message key="charity-address"/>:</b>${ publicDataCharity.address } </strong><br>
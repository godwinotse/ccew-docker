<%@ include file="../init.jsp"%>

<portlet:renderURL var="searchResultsRenderURL" >
	<portlet:param name="mvcRenderCommandName" value="<%=MVCCommandKeys.SEARCH_RESULTS_RENDER %>" />
</portlet:renderURL>

<aui:container cssClass="onereg__container">
	<aui:row cssClass="onereg__header mb-3">
		<aui:col md="6" cssClass="col-md-offset-2 py-3">
			<aui:form action="${ searchResultsRenderURL }" method="post" name="fm">

				<c:set var="showAdvancedLink" value="true" />
				<%@ include file="../fragments/display_search_box.jspf" %>

			</aui:form>
		</aui:col>
	</aui:row>
</aui:container>

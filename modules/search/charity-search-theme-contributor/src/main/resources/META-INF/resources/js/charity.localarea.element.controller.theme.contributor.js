AUI.add(

	'uk-gov-ccew-search-localarea-element-controller',

	function(A) {

		var localAreaFilterCheckBoxClass = '.filter-checkbox';

		var localAreaCheckBoxClass = '.local-area-facet-options';

		var localAreaOptionsClass = '.local-area-options';

		var filterInEnglandOrWalesCheckBox = 'filterInEnglandOrWalesCheckBox';

		var LocalAreaElementController = {

			init: function(params) {
				var instance = this;
				instance.namespace = params.namespace;
				instance.inEnglandWalesCheckbox = $('#' + instance.namespace + filterInEnglandOrWalesCheckBox);

				if (instance.inEnglandWalesCheckbox.prop('checked')) {
					instance.toggleAreaOptions(instance.inEnglandWalesCheckbox.prop('checked'));
				}

				instance.inEnglandWalesCheckbox.click(function(e) {
					instance.toggleAreaOptions(instance.inEnglandWalesCheckbox.prop('checked'));
				});

				$(localAreaFilterCheckBoxClass).click(function(e) {
					var disabled = $(e.target).prop('checked');
					$(e.target).closest('.row').find(localAreaCheckBoxClass + ' [type=checkbox]').each(function() {
						$(this).prop('disabled', disabled);
					});
				});

			},

			toggleAreaOptions: function(disable){
				$(localAreaOptionsClass).find('[type=checkbox]').each(function() {
					$(this).prop('disabled', disable);
				});
			}

		};

		A.LocalAreaElementController = LocalAreaElementController;
	},
	'',
	{
		requires: ['node-base']
	}
);

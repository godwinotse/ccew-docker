AUI.add(

	'uk-gov-ccew-search-continent-element-controller',

	function(A) {
		var continentCheckoBoxSelector = '.continent-checkbox';
		var countryDropDown;
		var firstItemMessage;
		var continentDropDown;
		var portletId;
		var worldCheckBox;

		var ContinentElementController = {

			init: function(params) {
				var instance = this;

				worldCheckBox = $('#' + params.namespace + 'filterContinentCheckboxWorld');
				continentDropDown = $('#' + params.namespace + 'filterContinent');
				countryDropDown = $('#' + params.namespace + 'filterCountry');
				firstItemMessage = params.firstItemMessage;
				portletId = params.portletId;

				countryDropDown.prop('disabled', true);

				continentDropDown.change(function(e) {
					var selectedValue = $(e.target).val();
					var disableContinentCheckboxes = false;
					if (selectedValue) {
						instance.getCountries($(e.target).val());
						disableContinentCheckboxes = true;
					}

					countryDropDown.prop('disabled', !disableContinentCheckboxes);
					instance.toggleContinentCheckBoxes(disableContinentCheckboxes, true);
				});

				$(continentCheckoBoxSelector + ' [type=checkbox]').click(function(e) {
					var disable = false;
					$(continentCheckoBoxSelector + ' [type=checkbox]').each(function() {
						if($(this).prop('checked')) {
							disable = true;
						}
					});

					instance.toggleContinentDropdown(disable);
				});

				if (worldCheckBox.prop('checked')) {
					instance.toggleContinentCheckBoxes.toggleContinentDropdown(worldCheckBox.prop('checked'));
					instance.toggleContinentCheckBoxes.toggleContinentCheckBoxes(worldCheckBox.prop('checked'), false);
				}

				worldCheckBox.click(function(e) {
					instance.toggleContinentDropdown(worldCheckBox.prop('checked'));
					instance.toggleContinentCheckBoxes(worldCheckBox.prop('checked'), false);
				});
			},

			toggleContinentDropdown: function (disable) {
				continentDropDown.prop('disabled', disable);
			},

			toggleContinentCheckBoxes: function (disable, includeWorldOption) {
				$(continentCheckoBoxSelector + ' [type=checkbox]').each(function() {
					$(this).prop('disabled', disable);
				});

				if (includeWorldOption) {
					worldCheckBox.prop('disabled', disable);
				}

			},

			getCountries: function(continent) {
				var resourceURL = Liferay.PortletURL.createResourceURL();
				resourceURL.setResourceId("/get-continent-countries");
				resourceURL.setParameter("p_p_id", portletId);
				resourceURL.setParameter("continent", continent);

				$.ajax({
					url: resourceURL.toString(),
					dataType: "JSON",
					success: function (response) {

						countryDropDown.empty();
						var firstOption = $(document.createElement('option'));
						firstOption.val('');
						firstOption.text(firstItemMessage);
						countryDropDown.append(firstOption);

						for(var i = 0; i < response.length; i++) {
							var option = $(document.createElement('option'));
							option.val(response[i]);
							option.text(response[i]);
							countryDropDown.append(option);
						}
					}
				});
			}

		};

		A.ContinentElementController = ContinentElementController;
	},
	'',
	{
		requires: ['node-base','liferay-portlet-url']
	}
);
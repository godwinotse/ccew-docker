AUI.add(

	'uk-gov-ccew-search-facetselector',

	function(A) {

		var itemsSelector = ".search-multi-select-items";
		
		var counterElementsSelector = ".search-facet-counter";
		
		var FacetSelector = {

			initFacetSelector: function() {
				
				$('.facet-checkbox').click(function(e) {
					A.FacetSelector.updateCounterBox($(e.target));
				});
				
				
				$('.facet-expand').click(function(e) {
					e.preventDefault();
					A.FacetSelector.toggleHide($(e.target));
				});

				$(".facet-auto-complete").keyup(function(e) {
					A.FacetSelector.searchFacetValues($(e.target));
				});

				$(itemsSelector).each(function(index) {
					$(this).hide();
					var checkedCount = $(this).find("input:checked").length;
					var counterElement = A.FacetSelector.getSelectedFacetCounter(this, checkedCount);
					A.FacetSelector.setSelectedFacetCounter(counterElement, checkedCount);
				});

			},
			
			updateCounterBox: function(checkbox){
				var checkedCounterElement = A.FacetSelector.getSelectedFacetCounter($(checkbox).parents(itemsSelector));
				var checkedCount = $(checkbox).parents(itemsSelector).find("input:checked").length;
				A.FacetSelector.setSelectedFacetCounter(checkedCounterElement, checkedCount);
			},

			toggleHide: function(href){
				var element = $(href).siblings(itemsSelector);
				
				if (element.is(":visible")) {
					element.hide();
				} else {
					element.show();
				}
				
				$(href).children("span").toggleClass("glyphicon-chevron-up");
				$(href).children("span").toggleClass("glyphicon-chevron-down");
			},
			
			setSelectedFacetCounter: function(counterElement, checkedCount) {

				if (checkedCount > 0) {
					counterElement.text(checkedCount + " selected");
				} else {
					counterElement.text("");
				}
				
			},

			getSelectedFacetCounter: function(itemsDiv) {
				return $(itemsDiv).siblings(counterElementsSelector);
			},
			
			searchFacetValues: function(inputBox) {
				var searchTerm = $(inputBox).val().toUpperCase();
				
				$(inputBox).parents(itemsSelector).find('.facet-checkbox').each(function(index) {
					var chkValue = $(this).val().toUpperCase();

					if (chkValue.indexOf(searchTerm) > -1) {
						$(this).parent('label').show();
					} else {
						$(this).parent('label').hide();
					}
				});
			}

		};
		
		A.FacetSelector = FacetSelector;
	},
	'',
	{
		requires: ['node-base']
	}
		
);

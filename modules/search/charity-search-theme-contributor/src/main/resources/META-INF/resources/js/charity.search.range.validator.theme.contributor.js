AUI.add(

	'uk-gov-ccew-search-facet-range-validator',

	function(A) {
		
		var FacetRangeValidator = {
			
			validateNumberRange: function(fromVal, toVal){
				var fromNum = Number(fromVal);
				var toNum = Number(toVal);
				
				if (isNaN(fromNum) || isNaN(toNum)) {
					return;
				}
				
				return (fromNum <= toNum);
			},
			
			getDateFrom_dd_MM_yyyy: function (val) {
				if (val == "") return;
				
				var dateParts = A.FacetRangeValidator.getDateParts_dd_MM_yyyy(val);
				
				return new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
			},
			
			getDateParts_dd_MM_yyyy: function(val) {
				return val.split('/');
			},
			
			validateDateRange: function (fromVal, toVal) {
				
				if (fromVal == "" || toVal == "") return true;
				
				var fromParts = A.FacetRangeValidator.getDateParts_dd_MM_yyyy(fromVal);
				var toParts = A.FacetRangeValidator.getDateParts_dd_MM_yyyy(toVal);
				if (!A.FacetRangeValidator.isValidDate(fromParts) || !A.FacetRangeValidator.isValidDate(toParts)) return false;
				
				var fromDate = new Date(fromParts[2], fromParts[1] - 1, fromParts[0]);
				var toDate = new Date(toParts[2], toParts[1] - 1, toParts[0]);
			
				return (fromDate.getTime() <= toDate.getTime());
			},
			
			daysInMonth: function (m, y) {
			    switch (m) {
			        case 1 :
			            return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
			        case 8 : case 3 : case 5 : case 10 :
			            return 30;
			        default :
			            return 31;
			    }
			},
			
			isValidDate: function (dateParts) {
				y = dateParts[2];
				m = dateParts[1];
				d = dateParts[0];
				
				if (isNaN(y) || isNaN(m) || isNaN(d)) return false;
				
			    m = parseInt(m, 10) - 1;
			    return m >= 0 && m < 12 && d > 0 && d <= A.FacetRangeValidator.daysInMonth(m, y);
			}

		};
		
		A.FacetRangeValidator = FacetRangeValidator;
	},
	'',
	{    
		requires: ['node-base']
	}
		
);
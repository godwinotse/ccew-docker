package uk.gov.ccew.charitysearch.taglib.facet.constant;

public final class FacetRequestAttributeKeys {

	public static final String FACET = "facet";

	public static final String FILTER = "filter";

	public static final String FIRST_ITEM_TITLE = "firstItemTitle";

	public static final String PLACEHOLDER = "placeholder";

	private FacetRequestAttributeKeys() {

	}

}

package uk.gov.ccew.charitysearch.taglib.facet.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import com.liferay.portal.kernel.search.facet.Facet;
import com.liferay.taglib.servlet.PipingServletResponse;
import com.liferay.taglib.util.IncludeTag;

import uk.gov.ccew.charitysearch.taglib.facet.constant.FacetRequestAttributeKeys;
import uk.gov.ccew.charitysearch.taglib.servletcontext.ServletContextUtil;

public class FacetNumberRangeSelectTag extends IncludeTag {

	private static final boolean CLEAN_UP_SET_ATTRIBUTES = false;

	private static final String PAGE = "/facet/range/number_range_page.jsp";

	private Facet facet;

	public HttpServletResponse createPipingServletResponse(PageContext pageContext) {
		return PipingServletResponse.createPipingServletResponse(pageContext);
	}

	@Override
	protected String getPage() {
		return PAGE;
	}

	@Override
	protected boolean isCleanUpSetAttributes() {
		return CLEAN_UP_SET_ATTRIBUTES;
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {
		request.setAttribute(FacetRequestAttributeKeys.FACET, facet);
	}

	@Override
	public void setPageContext(PageContext pageContext) {
		super.setPageContext(pageContext);

		setServletContext(ServletContextUtil.getServletContext());
	}

	public void setFacet(Facet facet) {
		this.facet = facet;
	}

}

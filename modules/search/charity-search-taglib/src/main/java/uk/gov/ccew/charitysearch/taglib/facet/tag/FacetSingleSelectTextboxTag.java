package uk.gov.ccew.charitysearch.taglib.facet.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import com.liferay.portal.kernel.search.facet.Facet;
import com.liferay.taglib.servlet.PipingServletResponse;
import com.liferay.taglib.util.IncludeTag;

import uk.gov.ccew.charitysearch.taglib.facet.constant.FacetRequestAttributeKeys;
import uk.gov.ccew.charitysearch.taglib.servletcontext.ServletContextUtil;

public class FacetSingleSelectTextboxTag extends IncludeTag {

	private static final String PAGE = "/facet/single_select/textbox_page.jsp";

	private static final boolean CLEAN_UP_SET_ATTRIBUTES = false;

	private Facet facet;

	private String placeholder;

	@Override
	public void setPageContext(PageContext pageContext) {
		super.setPageContext(pageContext);

		setServletContext(ServletContextUtil.getServletContext());
	}

	@Override
	protected String getPage() {
		return PAGE;
	}

	@Override
	protected boolean isCleanUpSetAttributes() {
		return CLEAN_UP_SET_ATTRIBUTES;
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {
		request.setAttribute(FacetRequestAttributeKeys.FACET, facet);
		request.setAttribute(FacetRequestAttributeKeys.PLACEHOLDER, placeholder);
	}

	public HttpServletResponse createPipingServletResponse(PageContext pageContext) {
		return PipingServletResponse.createPipingServletResponse(pageContext);
	}

	public void setFacet(Facet facet) {
		this.facet = facet;
	}

	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}

}

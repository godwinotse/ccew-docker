package uk.gov.ccew.charitysearch.taglib.filter.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import com.liferay.taglib.servlet.PipingServletResponse;
import com.liferay.taglib.util.IncludeTag;

import uk.gov.ccew.charitysearch.taglib.facet.constant.FacetRequestAttributeKeys;
import uk.gov.ccew.charitysearch.taglib.servletcontext.ServletContextUtil;

public class FilterDropdownTag extends IncludeTag {

	private static final boolean CLEAN_UP_SET_ATTRIBUTES = false;

	private static final String PAGE = "/filter/drop_down/page.jsp";

	private Object filter;

	private String firstItemTitle;

	public HttpServletResponse createPipingServletResponse(PageContext pageContext) {
		return PipingServletResponse.createPipingServletResponse(pageContext);
	}

	@Override
	protected String getPage() {
		return PAGE;
	}

	@Override
	protected boolean isCleanUpSetAttributes() {
		return CLEAN_UP_SET_ATTRIBUTES;
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {
		request.setAttribute(FacetRequestAttributeKeys.FILTER, filter);
		request.setAttribute(FacetRequestAttributeKeys.FIRST_ITEM_TITLE, firstItemTitle);
	}

	@Override
	public void setPageContext(PageContext pageContext) {
		super.setPageContext(pageContext);

		setServletContext(ServletContextUtil.getServletContext());
	}

	public void setFilter(Object filter) {
		this.filter = filter;
	}

	public void setFirstItemTitle(String firstItemTitle) {
		this.firstItemTitle = firstItemTitle;
	}

}

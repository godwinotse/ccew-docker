<%@ include file="../../init.jsp" %>

<div class="search-multi-select-facet">

	<aui:a href="#" cssClass="facet-expand">
		<span class="glyphicon glyphicon-chevron-down"></span><liferay-ui:message key="${ facet.fieldName }"/>
	</aui:a>

	<div class="search-facet-counter"></div>
	<div class="search-multi-select-items search-facet-hide">
		<c:choose>
			<c:when test="${ not empty facet.values}">

					<div class="navbar-left">
						<aui:icon cssClass="icon-monospaced" image="search" markupView="lexicon" />
						<aui:input cssClass="facet-auto-complete" label="<%=StringPool.BLANK%>" inlineField="true" name="findMultivalue_${ facet.fieldName }" />
					</div>
					<c:forEach items="${ facet.values }" var="facetValue">
						<aui:input cssClass="facet-checkbox" 
							label="${ facetValue.term } (${facetValue.count})" 
							name="${ facet.fieldName }"
							id="${ facet.fieldName }_${ facetValue.term.replaceAll('[^a-zA-Z0-9]', '') }" 
							type="checkbox" value="${ facetValue.term }" checked="${ facetValue.selected }" />
					</c:forEach>

			</c:when>
			<c:otherwise>
				<liferay-ui:message key="no-facet-results" />
			</c:otherwise>
		</c:choose>
	</div>
</div>

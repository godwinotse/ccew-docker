<%@ include file="../../init.jsp" %>

<div class="search-range-facet">
	<aui:row>
		<aui:col md="6">
			<aui:input type="text" label="${ facet.fromFieldName }" name="${ facet.fromFieldName }" value=" ${ facet.rangeFrom }">
				<aui:validator name="required" errorMessage="range-start-required">
					function() {
						var toVal = $('#<portlet:namespace />${ facet.toFieldName }').val();
						if (toVal != "") {
							var toDateParts = AUI($).FacetRangeValidator.getDateParts_dd_MM_yyyy(toVal)
							return (AUI($).FacetRangeValidator.isValidDate(toDateParts));
						}

						return false;
					}
				</aui:validator>
				
				<aui:validator name="custom" errorMessage="range-date-invalid">
					function(val, fieldNode, ruleValue) {
						var fromDateParts = AUI($).FacetRangeValidator.getDateParts_dd_MM_yyyy(val);
						return AUI($).FacetRangeValidator.isValidDate(fromDateParts);
					}
				</aui:validator>
				
				<aui:validator name="custom" errorMessage="range-start-must-be-less">
					function(val, fieldNode, ruleValue) {
					
						var fromVal = $('#<portlet:namespace />${ facet.fromFieldName }').val();
						var toVal = $('#<portlet:namespace />${ facet.toFieldName }').val();
						
						if (toVal == "") return true;
						
						return AUI($).FacetRangeValidator.validateDateRange(fromVal, toVal);
					}
				</aui:validator>
			</aui:input>
		</aui:col>
		<aui:col md="6">
			<aui:input type="text" label="${ facet.toFieldName }" name="${ facet.toFieldName }"  value=" ${ facet.rangeTo }">
				<aui:validator name="required" errorMessage="range-end-required">
					function() {
						var fromVal = $('#<portlet:namespace />${ facet.fromFieldName }').val();
						if (fromVal != "") {
							var fromDateParts = AUI($).FacetRangeValidator.getDateParts_dd_MM_yyyy(fromVal);
							return (AUI($).FacetRangeValidator.isValidDate(fromDateParts));
						}

						return false;
					}
				</aui:validator>
				
				<aui:validator name="custom" errorMessage="range-date-invalid">
					function(val, fieldNode, ruleValue) {
						var toDateParts = AUI($).FacetRangeValidator.getDateParts_dd_MM_yyyy(val)
						return AUI($).FacetRangeValidator.isValidDate(toDateParts);
					}
				</aui:validator>
				
				<aui:validator name="custom" errorMessage="range-end-must-be-greater-or-equal">
					function(val, fieldNode, ruleValue) {
						var fromVal = $('#<portlet:namespace />${ facet.fromFieldName }').val();
						var toVal = $('#<portlet:namespace />${ facet.toFieldName }').val();
						
						return AUI($).FacetRangeValidator.validateDateRange(fromVal, toVal);
					}
				</aui:validator>
				
			</aui:input>
		</aui:col>
	</aui:row>
</div>

<aui:script use="uk-gov-ccew-search-facet-range-validator" />
	
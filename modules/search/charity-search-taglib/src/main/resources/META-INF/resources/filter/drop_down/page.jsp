<%@ include file="../../init.jsp"%>

<aui:select label="<%=StringPool.BLANK%>" name="${ filter.filterName }" id="${ filter.filterName }">
	<aui:option value="" selected="true" >${ firstItemTitle }</aui:option>

	<c:forEach items="${ filter.filterOptions }" var="filterOption">
		<aui:option label="<%=StringPool.BLANK%>"
			value="${ filterOption }" selected="${ filterOption == filter.selectedValue ? true : false }">${ filterOption }</aui:option>
	</c:forEach>

</aui:select>
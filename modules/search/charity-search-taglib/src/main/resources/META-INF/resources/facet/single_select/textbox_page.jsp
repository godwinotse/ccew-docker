<%@ include file="../../init.jsp" %>

<div class="search-single-select-facet">
	<aui:input type="text"
		placeholder="${ placeholder }"
		label="${ facet.fieldName }"
		name="${ facet.fieldName }"
		id="${ facet.fieldName }_${ facet.value.term.replaceAll('[^a-zA-Z0-9]', '') }"
		value="${ facet.value }" />			
</div>
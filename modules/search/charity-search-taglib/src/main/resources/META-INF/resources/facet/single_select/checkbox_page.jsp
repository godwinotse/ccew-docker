<%@ include file="../../init.jsp" %>

<div class="search-single-select-facet">
	<aui:input cssClass="search-single-select-checkbox"
		label="${ facet.fieldName }"
		name="${ facet.fieldName }"
		id="${ facet.fieldName }_${ facetValue.value.term.replaceAll('[^a-zA-Z0-9]', '') }" 
		type="checkbox" checked="${ facetValue.selected }" />				
</div>
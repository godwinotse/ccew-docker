package uk.gov.ccew.publicdata.model;

import java.math.BigDecimal;

public interface AssetsAndLiabilities {

	BigDecimal getDefinedPensionBenefitTotal();

	BigDecimal getLiabilitiesTotal();

	BigDecimal getLongTermInvestmentTotal();

	BigDecimal getOtherAssetsTotal();

	BigDecimal getOwnUseAssetsTotal();

}

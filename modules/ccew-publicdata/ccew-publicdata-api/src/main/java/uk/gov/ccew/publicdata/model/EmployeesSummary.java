package uk.gov.ccew.publicdata.model;

import java.util.Map;

public interface EmployeesSummary {

	int getNoOfEmployees();

	Map<String, Integer> getNoOfPeopleBySalaryRange();

	int getNoOfVolunteers();

	boolean isEmployingFormerTrustee();

}

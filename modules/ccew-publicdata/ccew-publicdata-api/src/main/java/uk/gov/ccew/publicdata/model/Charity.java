package uk.gov.ccew.publicdata.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface Charity {

	String getActivities();

	String getAddress();

	String getAreaOfBenefit();

	Optional<AreaOfOperation> getAreaOfOperation();

	Optional<CharityAnnualReport> getCharityAnnualReport();

	String getCharitableObjects();

	String getCharityName();

	String getCharityType();

	long getCharityNumber();

	Optional<CharityStatus> getCharityStatus();

	String getCompanyNumber();

	String getEmail();

	BigDecimal getExpenditure();

	Optional<GiftAidStatus> getGiftAidStatus();

	String getGoverningDocument();

	List<String> getHowTheCharityHelps();

	BigDecimal getIncome();

	List<String> getLocations();

	long getOrganisationNumber();

	String getOrganisationType();

	List<String> getOtherCharityNames();

	List<String> getOtherRegulators();

	String getPhone();

	List<String> getPolicies();

	Optional<Date> getRegistrationDate();

	Optional<Date> getRemovedDate();

	List<RegistrationRecord> getRegistrationHistory();

	List<RegulatoryAlert> getRegulatoryAlerts();

	int getSubsidiarySuffix();

	String getWebsite();

	List<String> getWhatTheCharityDoes();

	List<String> getWhoTheCharityHelps();

	boolean isTradingSubsidiaries();

	boolean isGrantMaking();

	boolean isLandAndProperty();

	void setAreaOfOperation(AreaOfOperation areaOfOperation);

	void setCharityName(String charityName);

	void setCharityNumber(long charityNumber);

	void setCharityStatus(CharityStatus charityStatus);

	void setExpenditure(BigDecimal expenditure);

	void setHowTheCharityHelps(List<String> howTheCharityHelps);

	void setIncome(BigDecimal income);

	void setOrganisationNumber(long organisationNumber);

	void setOrganisationType(String organisationType);

	void setRegistrationDate(Date registrationDate);

	void setRegistrationHistory(List<RegistrationRecord> registrationHistory);

	void setRemovedDate(Date removedDate);

	void setSubsidiarySuffix(int subsidiarySuffix);

	void setWhatTheCharityDoes(List<String> whatTheCharityDoes);

	void setWhoTheCharityHelps(List<String> whoTheCharityHelps);

}

package uk.gov.ccew.publicdata.model;

import java.util.Optional;

public interface AnnualReturnAndAccountsAndTar {

	Optional<AccountsAndTar> getAccountsAndTar();

	Optional<AnnualReturn> getAnnualReturn();
	
	void setAnnualReturn(AnnualReturn annualReturn);

}

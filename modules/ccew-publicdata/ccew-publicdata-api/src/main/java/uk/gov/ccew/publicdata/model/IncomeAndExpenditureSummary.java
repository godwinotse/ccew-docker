package uk.gov.ccew.publicdata.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

public interface IncomeAndExpenditureSummary {

	Optional<Date> getCurrentFinancialYearEnd();

	Map<String, BigDecimal> getExpenditureByCategory();

	BigDecimal getGovernmentContractsIncome();

	int getGovernmentContractsQty();

	BigDecimal getGovernmentGrantsIncome();

	int getGovernmentGrantsQty();

	Map<String, BigDecimal> getIncomeByCategory();

	BigDecimal getInvestmentGains();

}

package uk.gov.ccew.publicdata.model;

import java.util.List;

public interface AreaOfOperation {

	List<String> getConstituencies();

	List<Country> getCountries();

	List<LocalAuthority> getLocalAuthorities();

	List<String> getRegions();

	void setConstituencies(List<String> constituencies);

	void setCountries(List<Country> countries);

	void setLocalAuthorities(List<LocalAuthority> localAuthorities);

	void setRegions(List<String> regions);

}

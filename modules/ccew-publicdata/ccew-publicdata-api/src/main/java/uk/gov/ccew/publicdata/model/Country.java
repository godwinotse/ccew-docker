package uk.gov.ccew.publicdata.model;

import java.util.Optional;

public interface Country {

	Optional<Continent> getContinent();

	String getName();

	void setContinent(Continent continent);

	void setName(String name);

}

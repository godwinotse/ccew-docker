package uk.gov.ccew.publicdata.model;

import java.util.Date;
import java.util.Optional;

public interface RegistrationRecord {

    Optional<Date> getRegistrationDate();

    String getRegistrationAction();

    void setRegistrationDate(Date registrationDate);

    void setRegistrationAction(String registrationAction);

}
package uk.gov.ccew.publicdata.service;

import java.util.List;

import uk.gov.ccew.publicdata.model.AnnualReturnAndAccountsAndTar;
import uk.gov.ccew.publicdata.model.AssetsAndLiabilities;
import uk.gov.ccew.publicdata.model.FinancialAnnualReport;
import uk.gov.ccew.publicdata.model.FundraisingSummary;
import uk.gov.ccew.publicdata.model.IncomeAndExpenditureSummary;

public interface FinancialService {

	List<AnnualReturnAndAccountsAndTar> getAnnualReturnsAndAccountsAndTars(long organisationNumber);

	AssetsAndLiabilities getAssetsAndLiabilities(long organisationNumber);

	List<FinancialAnnualReport> getFinancialHistory(long organisationNumber);

	FundraisingSummary getFundraisingSummary(long organisationNumber);

	IncomeAndExpenditureSummary getIncomeAndExpenditureSummary(long organisationNumber);

}

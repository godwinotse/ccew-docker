package uk.gov.ccew.publicdata.model;

public interface FundraisingSummary {

	boolean isCommercialAndProfessionalAgreementInPlace();

	boolean isRaisingFundsFromThePublic();

	boolean isWorkingWithCommercialParticipants();

}

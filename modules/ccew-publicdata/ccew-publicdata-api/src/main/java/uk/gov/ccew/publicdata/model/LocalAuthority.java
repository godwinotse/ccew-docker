package uk.gov.ccew.publicdata.model;

public interface LocalAuthority {

	String getLocalAuthorityName();

	String getMetropolitanCouncilName();

	boolean isWelsh();

	void setLocalAuthorityName(String localAuthorityName);

	void setMetropolitanCouncilName(String metropolitanCouncilName);

	void setWelsh(boolean isWelsh);
}

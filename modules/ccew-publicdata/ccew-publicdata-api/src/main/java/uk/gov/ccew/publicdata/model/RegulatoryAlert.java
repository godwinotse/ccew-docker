package uk.gov.ccew.publicdata.model;

import java.util.Date;
import java.util.Optional;

public interface RegulatoryAlert {

	Optional<Date> getIncidentDate();

	Optional<RegulatoryAlertIncidentType> getIncidentType();

	String getIncidentUserName();

	String getUrl();

}

package uk.gov.ccew.publicdata.model;

import com.google.gson.annotations.SerializedName;

public enum TrusteeRole {

	@SerializedName("chair") CHAIR,

	@SerializedName("trustee") TRUSTEE;

	public boolean isChair() {
		return this == CHAIR;
	}

	public boolean isTrustee() {
		return this == TRUSTEE;
	}

}

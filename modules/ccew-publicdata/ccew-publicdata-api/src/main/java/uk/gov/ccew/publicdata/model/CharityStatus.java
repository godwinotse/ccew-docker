package uk.gov.ccew.publicdata.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.google.gson.annotations.SerializedName;

public enum CharityStatus {
	
	@SerializedName("linked") LINKED("linked"),

	@SerializedName("recentlyRegistered") RECENTLY_REGISTERED("recentlyRegistered"),
	
	@SerializedName("registered") REGISTERED("registered"),

	@SerializedName("removed") REMOVED("removed"),

	@SerializedName("unknown") UNKNOWN("unknown");

	private String value;
	
	private static Map<String, CharityStatus> statusMap = new HashMap<>();
	
	static {
		for (CharityStatus charityStatus : CharityStatus.values()) {
			statusMap.put(charityStatus.value.toLowerCase(), charityStatus);
		}
	}

	CharityStatus(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
	public boolean isLinked() {
		return this == LINKED;
	}

	public boolean isRecentlyRegistered() {
		return this == RECENTLY_REGISTERED;
	}

	public boolean isRegistered() {
		return this == REGISTERED;
	}

	public boolean isRemoved() {
		return this == REMOVED;
	}
	
	public boolean isUnknown() {
		return this == UNKNOWN;
	}
	
	public static CharityStatus getCharityStatusFromString(String charityStatus) {
		return Optional.ofNullable(statusMap.get(charityStatus.toLowerCase())).orElse(UNKNOWN);
    }

}

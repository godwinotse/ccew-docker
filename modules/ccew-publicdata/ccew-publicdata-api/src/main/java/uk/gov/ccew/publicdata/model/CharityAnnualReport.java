package uk.gov.ccew.publicdata.model;

import java.util.Date;
import java.util.Optional;

public interface CharityAnnualReport {

	int getDaysOverdue();

	Optional<CharityAnnualReportStatus> getStatus();

	Optional<Date> getSubmittedDate();

}

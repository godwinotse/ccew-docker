package uk.gov.ccew.publicdata.model;

import com.google.gson.annotations.SerializedName;

public enum RegulatoryAlertIncidentType {

	@SerializedName("inquiryOpened") INQUIRY_OPENED,

	@SerializedName("inquiryResultsPublished") INQUIRY_RESULTS_PUBLISHED,

	@SerializedName("warningIssued") WARNING_ISSUED,

	@SerializedName("interimManagerAppointed") INTERIM_MANAGER_APPOINTED,

	@SerializedName("dissolutionNotice") DISSOLUTION_NOTICE,

	@SerializedName("caseReportPublished") CASE_REPORT_PUBLISHED,

	@SerializedName("charityInsolvent") CHARITY_INSOLVENT,

	@SerializedName("charityInAdministration") CHARITY_IN_ADMINISTRATION;

	public boolean isInquiryOpened() {
		return this == INQUIRY_OPENED;
	}

	public boolean isInquiryResultsPublished() {
		return this == INQUIRY_RESULTS_PUBLISHED;
	}

	public boolean isWarningIssued() {
		return this == WARNING_ISSUED;
	}

	public boolean isInterimManagerAppointed() {
		return this == INTERIM_MANAGER_APPOINTED;
	}

	public boolean isDissolutionNotice() {
		return this == DISSOLUTION_NOTICE;
	}

	public boolean isCaseReportPublished() {
		return this == CASE_REPORT_PUBLISHED;
	}

	public boolean isCharityInsolvent() {
		return this == CHARITY_INSOLVENT;
	}

	public boolean isCharityInAdministration() {
		return this == CHARITY_IN_ADMINISTRATION;
	}

}

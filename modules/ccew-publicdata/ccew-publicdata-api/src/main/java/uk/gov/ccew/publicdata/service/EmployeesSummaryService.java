package uk.gov.ccew.publicdata.service;

import uk.gov.ccew.publicdata.model.EmployeesSummary;

public interface EmployeesSummaryService {

	EmployeesSummary getEmployeesSummary(long organisationNumber);

}

package uk.gov.ccew.publicdata.model;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface Trustee {

	Optional<Date> getDateOfAppointment();

	String getName();

	Optional<TrusteeRole> getRole();

	List<Long> getOtherCharityOrganisationNumbers();

	boolean isDirectorOfSubsidiary();

	boolean isReceivingPaymentsFromCharity();
	
	void setName(String name);

}

package uk.gov.ccew.publicdata.model;

import com.google.gson.annotations.SerializedName;

public enum GiftAidStatus {

	@SerializedName("true") CLAIMS_GIFT_AID,

	@SerializedName("false") DOES_NOT_CLAIM_GIFT_AID;

	public boolean claimsGiftAid() {
		return this == CLAIMS_GIFT_AID;
	}

	public boolean doesNotClaimGiftAid() {
		return this == DOES_NOT_CLAIM_GIFT_AID;
	}

}
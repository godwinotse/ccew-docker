package uk.gov.ccew.publicdata.service;

import java.util.List;

import uk.gov.ccew.publicdata.model.Charity;

public interface CharityService {

	int getCharityCount();
	
	Charity getCharityByOrganisationNumber(long organisationNumber);

	List<Charity> getCharitiesByOrganisationNumbers(List<Long> organisationNumbers);

	List<Charity> getCharities(int start, int end);

}

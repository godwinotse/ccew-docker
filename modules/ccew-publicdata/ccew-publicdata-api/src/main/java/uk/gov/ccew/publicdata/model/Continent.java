package uk.gov.ccew.publicdata.model;

public interface Continent {

	String getName();

	void setName(String name);

}

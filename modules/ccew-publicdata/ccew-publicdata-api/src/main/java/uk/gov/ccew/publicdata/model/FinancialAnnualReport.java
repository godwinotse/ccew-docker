package uk.gov.ccew.publicdata.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

public interface FinancialAnnualReport {

	Map<String, BigDecimal> getExpenditureTotalsByCategory();

	Optional<Date> getFinancialYearEnd();

	Map<String, BigDecimal> getIncomeTotalsByCategory();

}

package uk.gov.ccew.publicdata.model;

import java.util.Date;
import java.util.Optional;

public interface AnnualReturn {

	int getDaysOverdue();

	Optional<Date> getReceivedDate();

	Optional<Date> getReportingYear();

	boolean isPrimaryPurposeGrantGiving();

	void setPrimaryPurposeGrantGiving(boolean primaryPurposeGrantGiving);

}

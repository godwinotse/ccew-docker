package uk.gov.ccew.publicdata.service;

import uk.gov.ccew.publicdata.model.Trustee;

import java.util.List;

public interface TrusteeService {

	int getTrusteeCount(long organisationNumber);

	List<Trustee> getTrustees(long organisationNumber);

}

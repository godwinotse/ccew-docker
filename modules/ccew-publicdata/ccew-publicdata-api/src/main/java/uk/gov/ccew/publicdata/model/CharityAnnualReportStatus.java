package uk.gov.ccew.publicdata.model;

import com.google.gson.annotations.SerializedName;

public enum CharityAnnualReportStatus {

	@SerializedName("overdue") OVERDUE("overdue"),

	@SerializedName("received") RECEIVED("received"),

	@SerializedName("upToDate") UP_TO_DATE("upToDate");

	private String value;

	CharityAnnualReportStatus(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public boolean isOverdue() {
		return this == OVERDUE;
	}

	public boolean isReceived() {
		return this == RECEIVED;
	}

	public boolean isUpToDate() {
		return this == UP_TO_DATE;
	}

}
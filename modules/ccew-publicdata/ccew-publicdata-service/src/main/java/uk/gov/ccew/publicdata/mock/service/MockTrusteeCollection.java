package uk.gov.ccew.publicdata.mock.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.osgi.service.component.annotations.Component;

import uk.gov.ccew.publicdata.model.Trustee;

@Component(immediate = true, service = MockTrusteeCollection.class)
public class MockTrusteeCollection {
	
	private Map<Long, List<Trustee>> mockTrustees = new HashMap<>();
	
	public int getMockTrusteeCount() {
		
		return mockTrustees.size();
		
	}
	
	public boolean isEmpty() {
		
		return mockTrustees.isEmpty();
		
	}
	
	public void addMockTrustees(Map<Long, List<Trustee>> mockTrusteeList) {
		
		mockTrustees.putAll(mockTrusteeList);
		
	}
	
	public List<Trustee> getMockTrustees(long organisationNumber) {
		
		return mockTrustees.getOrDefault(organisationNumber, Collections.emptyList());
		
	}

}

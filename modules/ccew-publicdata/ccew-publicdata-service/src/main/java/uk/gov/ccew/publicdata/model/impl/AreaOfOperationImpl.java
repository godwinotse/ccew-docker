package uk.gov.ccew.publicdata.model.impl;

import java.util.Collections;
import java.util.List;

import uk.gov.ccew.publicdata.model.AreaOfOperation;
import uk.gov.ccew.publicdata.model.Country;
import uk.gov.ccew.publicdata.model.LocalAuthority;

public class AreaOfOperationImpl implements AreaOfOperation {

	private List<String> constituencies;

	private List<Country> countries;

	private List<LocalAuthority> localAuthorities;

	private List<String> regions;

	@Override
	public List<String> getConstituencies() {
		return constituencies == null ? Collections.emptyList() : constituencies;
	}

	@Override
	public List<Country> getCountries() {
		return countries == null ? Collections.emptyList() : countries;
	}

	@Override
	public List<LocalAuthority> getLocalAuthorities() {
		return localAuthorities == null ? Collections.emptyList() : localAuthorities;
	}

	@Override
	public List<String> getRegions() {
		return regions == null ? Collections.emptyList() : regions;
	}

	@Override
	public void setConstituencies(List<String> constituencies) {
		this.constituencies = constituencies;
	}

	@Override
	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	@Override
	public void setLocalAuthorities(List<LocalAuthority> localAuthorities) {
		this.localAuthorities = localAuthorities;
	}

	@Override
	public void setRegions(List<String> regions) {
		this.regions = regions;
	}

}

package uk.gov.ccew.publicdata.parser;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;

import uk.gov.ccew.publicdata.model.AccountsAndTar;

public class AccountsAndTarTypeAdapterFactory implements TypeAdapterFactory {

	private final Class<? extends AccountsAndTar> implementationClass;

	public AccountsAndTarTypeAdapterFactory(Class<? extends AccountsAndTar> implementationClass) {
		this.implementationClass = implementationClass;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {

		TypeAdapter<T> typeAdapter;

		if (AccountsAndTar.class.equals(type.getRawType())) {

			typeAdapter = (TypeAdapter<T>) gson.getAdapter(implementationClass);

		} else {

			typeAdapter = null;

		}

		return typeAdapter;

	}

}

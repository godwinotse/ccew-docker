package uk.gov.ccew.publicdata.parser;

import uk.gov.ccew.publicdata.model.RegulatoryAlert;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;

public class RegulatoryAlertTypeAdapterFactory implements TypeAdapterFactory {

	private final Class<? extends RegulatoryAlert> implementationClass;

	public RegulatoryAlertTypeAdapterFactory(Class<? extends RegulatoryAlert> implementationClass) {
		this.implementationClass = implementationClass;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {

		TypeAdapter<T> typeAdapter;

		if (RegulatoryAlert.class.equals(type.getRawType())) {

			typeAdapter = (TypeAdapter<T>) gson.getAdapter(implementationClass);

		} else {

			typeAdapter = null;

		}

		return typeAdapter;
	}

}

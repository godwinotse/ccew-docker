package uk.gov.ccew.publicdata.model.impl;

import java.util.Date;
import java.util.Optional;

import uk.gov.ccew.publicdata.model.AnnualReturn;

public class AnnualReturnImpl implements AnnualReturn {

	private int daysOverdue;
	
	private boolean primaryPurposeIsGrantGiving;

	private Date receivedDate;

	private Date reportingYear;

	@Override
	public int getDaysOverdue() {
		return daysOverdue;
	}

	@Override
	public Optional<Date> getReceivedDate() {
		return Optional.ofNullable(receivedDate);
	}

	@Override
	public Optional<Date> getReportingYear() {
		return Optional.ofNullable(reportingYear);
	}

	@Override
	public boolean isPrimaryPurposeGrantGiving() {
		return primaryPurposeIsGrantGiving;
	}

	@Override
	public void setPrimaryPurposeGrantGiving(boolean primaryPurposeGrantGiving) {
		this.primaryPurposeIsGrantGiving = primaryPurposeGrantGiving;
	}

}

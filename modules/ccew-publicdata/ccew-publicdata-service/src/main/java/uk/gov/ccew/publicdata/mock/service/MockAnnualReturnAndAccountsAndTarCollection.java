package uk.gov.ccew.publicdata.mock.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.osgi.service.component.annotations.Component;

import uk.gov.ccew.publicdata.model.AnnualReturnAndAccountsAndTar;

@Component(immediate = true, service = MockAnnualReturnAndAccountsAndTarCollection.class)
public class MockAnnualReturnAndAccountsAndTarCollection {
	
	private Map<Long, List<AnnualReturnAndAccountsAndTar>> mockMockAnnualReturnAndAccountsAndTarCollection = new HashMap<>();
	
	public int getMockMockAnnualReturnAndAccountsAndTarCollectionCount() {
		
		return mockMockAnnualReturnAndAccountsAndTarCollection.size();
		
	}
	
	public boolean isEmpty() {
		
		return mockMockAnnualReturnAndAccountsAndTarCollection.isEmpty();
		
	}
	
	public void addMockAnnualReturnAndAccountsAndTarCollection(Map<Long, List<AnnualReturnAndAccountsAndTar>> mockAnnualReturnAndAccountsAndTars) {
		
		mockMockAnnualReturnAndAccountsAndTarCollection.putAll(mockAnnualReturnAndAccountsAndTars);
		
	}
	
	public List<AnnualReturnAndAccountsAndTar> getMockAnnualReturnAndAccountsAndTar(long organisationNumber) {
		
		return mockMockAnnualReturnAndAccountsAndTarCollection.get(organisationNumber);
		
	}

}

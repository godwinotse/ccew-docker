package uk.gov.ccew.publicdata.mock.service;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = MockResourceService.class)
public class MockResourceService {

	public InputStream getInputStream(String resourcePath) {
		return getClass().getClassLoader().getResourceAsStream(resourcePath);
	}

	public InputStream getInputStream(String resourcePath, long organisationNumber) {
		return getResourceAsStream(resourcePath, organisationNumber);
	}

	public InputStream getInputStream(String resourcePath, long organisationNumber, long defaultOrganisationNumber) {

		InputStream inputStream = getInputStream(resourcePath, organisationNumber);

		if (Validator.isNull(inputStream) && Validator.isNotNull(defaultOrganisationNumber)) {

			inputStream = getResourceAsStream(resourcePath, defaultOrganisationNumber);

		}

		if (inputStream == null) {

			throw new IllegalStateException("No default mock resource found - resourcePath: " + resourcePath + ", organisationNumber: " + organisationNumber);

		}

		return inputStream;

	}

	private InputStream getResourceAsStream(String resourcePath, long organisationNumber) {

		String resourcePathFormatted = String.format(resourcePath, organisationNumber);

		return getClass().getClassLoader().getResourceAsStream(resourcePathFormatted);

	}

}

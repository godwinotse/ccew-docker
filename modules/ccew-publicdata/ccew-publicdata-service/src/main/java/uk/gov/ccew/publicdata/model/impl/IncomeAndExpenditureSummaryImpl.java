package uk.gov.ccew.publicdata.model.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import uk.gov.ccew.publicdata.model.IncomeAndExpenditureSummary;

public class IncomeAndExpenditureSummaryImpl implements IncomeAndExpenditureSummary {

	private Date currentFinancialYearEnd;

	private Map<String, BigDecimal> expenditureBySector;

	private BigDecimal governmentContractsIncome;

	private int governmentContractsQty;

	private BigDecimal governmentGrantsIncome;

	private int governmentGrantsQty;

	private Map<String, BigDecimal> incomeBySector;

	private BigDecimal investmentGains;

	@Override
	public Optional<Date> getCurrentFinancialYearEnd() {
		return Optional.ofNullable(currentFinancialYearEnd);
	}

	@Override
	public Map<String, BigDecimal> getExpenditureByCategory() {
		return expenditureBySector == null ? Collections.emptyMap() : expenditureBySector;
	}

	@Override
	public BigDecimal getGovernmentContractsIncome() {
		return governmentContractsIncome == null ? BigDecimal.ZERO : governmentContractsIncome;
	}

	@Override
	public int getGovernmentContractsQty() {
		return governmentContractsQty;
	}

	@Override
	public BigDecimal getGovernmentGrantsIncome() {
		return governmentGrantsIncome == null ? BigDecimal.ZERO : governmentGrantsIncome;
	}

	@Override
	public int getGovernmentGrantsQty() {
		return governmentGrantsQty;
	}

	@Override
	public Map<String, BigDecimal> getIncomeByCategory() {
		return incomeBySector == null ? Collections.emptyMap() : incomeBySector;
	}

	@Override
	public BigDecimal getInvestmentGains() {
		return investmentGains == null ? BigDecimal.ZERO : investmentGains;
	}

}

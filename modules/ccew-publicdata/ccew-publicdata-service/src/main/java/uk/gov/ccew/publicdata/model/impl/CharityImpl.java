package uk.gov.ccew.publicdata.model.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.liferay.portal.kernel.util.GetterUtil;

import uk.gov.ccew.publicdata.model.AreaOfOperation;
import uk.gov.ccew.publicdata.model.Charity;
import uk.gov.ccew.publicdata.model.CharityAnnualReport;
import uk.gov.ccew.publicdata.model.CharityStatus;
import uk.gov.ccew.publicdata.model.GiftAidStatus;
import uk.gov.ccew.publicdata.model.RegistrationRecord;
import uk.gov.ccew.publicdata.model.RegulatoryAlert;

public class CharityImpl implements Charity {

	private String activities;

	private String address;
	
	private AreaOfOperation areaOfOperation;

	private String areaOfBenefit;

	private CharityAnnualReport charityAnnualReport;

	private String charitableObjects;

	private String charityName;

	private long charityNumber;

	private CharityStatus charityStatus;

	private String charityType;

	private String companyNumber;

	private String email;

	private BigDecimal expenditure;

	private GiftAidStatus giftAidStatus;

	private String governingDocument;

	private boolean grantMaking;

	private List<String> howTheCharityHelps;

	private BigDecimal income;

	private boolean landAndProperty;

	private List<String> locations;

	private long organisationNumber;

	private String organisationType;

	private List<String> otherCharityNames;

	private List<String> otherRegulators;

	private String phone;

	private List<String> policies;

	private Date registrationDate;
	
	private Date removedDate;

	private List<RegistrationRecord> registrationHistory;

	private List<RegulatoryAlert> regulatoryAlerts;

	private int subsidiarySuffix;

	private boolean tradingSubsidiaries;

	private String website;

	private List<String> whatTheCharityDoes;

	private List<String> whoTheCharityHelps;

	@Override
	public String getActivities() {
		return GetterUtil.getString(activities);
	}

	@Override
	public String getAddress() {
		return GetterUtil.getString(address);
	}
	
	@Override
	public Optional<AreaOfOperation> getAreaOfOperation() {
		return Optional.ofNullable(areaOfOperation);
	}

	@Override
	public String getAreaOfBenefit() {
		return GetterUtil.getString(areaOfBenefit);
	}

	@Override
	public Optional<CharityAnnualReport> getCharityAnnualReport() {
		return Optional.ofNullable(charityAnnualReport);
	}

	@Override
	public String getCharitableObjects() {
		return GetterUtil.getString(charitableObjects);
	}

	@Override
	public String getCharityName() {
		return GetterUtil.getString(charityName);
	}

	@Override
	public String getCharityType() {
		return GetterUtil.getString(charityType);
	}

	@Override
	public Optional<GiftAidStatus> getGiftAidStatus() {
		return Optional.ofNullable(giftAidStatus);
	}

	@Override
	public String getGoverningDocument() {
		return GetterUtil.getString(governingDocument);
	}

	@Override
	public long getCharityNumber() {
		return GetterUtil.getLong(charityNumber);
	}

	@Override
	public Optional<CharityStatus> getCharityStatus() {
		return Optional.ofNullable(charityStatus);
	}

	@Override
	public String getCompanyNumber() {
		return GetterUtil.getString(companyNumber);
	}

	@Override
	public String getEmail() {
		return GetterUtil.getString(email);
	}

	@Override
	public BigDecimal getExpenditure() {
		return expenditure;
	}

	@Override
	public List<String> getHowTheCharityHelps() {
		return howTheCharityHelps == null ? Collections.emptyList() : howTheCharityHelps;
	}

	@Override
	public BigDecimal getIncome() {
		return income;
	}

	@Override
	public List<String> getLocations() {
		return locations == null ? Collections.emptyList() : locations;
	}

	@Override
	public long getOrganisationNumber() {
		return organisationNumber;
	}

	@Override
	public List<String> getOtherCharityNames() {
		return otherCharityNames == null ? Collections.emptyList() : otherCharityNames;
	}

	@Override
	public List<String> getOtherRegulators() {
		return otherRegulators == null ? Collections.emptyList() : otherRegulators;
	}

	@Override
	public String getPhone() {
		return GetterUtil.getString(phone);
	}

	@Override
	public List<String> getPolicies() {
		return policies == null ? Collections.emptyList() : policies;
	}

	@Override
	public Optional<Date> getRegistrationDate() {
		return Optional.ofNullable(registrationDate);
	}
	
	@Override
	public Optional<Date> getRemovedDate() {
		return Optional.ofNullable(removedDate);
	}

	@Override
	public List<RegulatoryAlert> getRegulatoryAlerts() {
		return regulatoryAlerts == null ? Collections.emptyList() : regulatoryAlerts;
	}

	@Override
	public int getSubsidiarySuffix() {
		return subsidiarySuffix;
	}

	@Override
	public String getWebsite() {
		return GetterUtil.getString(website);
	}

	@Override
	public List<String> getWhatTheCharityDoes() {
		return whatTheCharityDoes == null ? Collections.emptyList() : whatTheCharityDoes;
	}

	@Override
	public List<String> getWhoTheCharityHelps() {
		return whoTheCharityHelps == null ? Collections.emptyList() : whoTheCharityHelps;
	}

	@Override
	public boolean isTradingSubsidiaries() {
		return tradingSubsidiaries;
	}

	@Override
	public boolean isGrantMaking() {
		return grantMaking;
	}

	@Override
	public boolean isLandAndProperty() {
		return landAndProperty;
	}

	@Override
	public void setAreaOfOperation(AreaOfOperation areaOfOperation) {
		this.areaOfOperation = areaOfOperation;
	}
	
	@Override
	public void setCharityName(String charityName) {
		this.charityName = charityName;
	}

	@Override
	public void setCharityNumber(long charityNumber) {
		this.charityNumber = charityNumber;
	}

	@Override
	public void setCharityStatus(CharityStatus charityStatus) {
		this.charityStatus = charityStatus;
	}
	
	@Override
	public void setExpenditure(BigDecimal expenditure) {
		this.expenditure = expenditure;
	}
	
	@Override
	public void setHowTheCharityHelps(List<String> howTheCharityHelps) {
		this.howTheCharityHelps = howTheCharityHelps;
	}

	@Override
	public void setIncome(BigDecimal income) {
		this.income = income;
	}

	@Override
	public void setOrganisationNumber(long organisationNumber) {
		this.organisationNumber = organisationNumber;
	}

	@Override
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	
	@Override
	public void setRemovedDate(Date removedDate) {
		this.removedDate = removedDate;
	}

	@Override
	public void setSubsidiarySuffix(int subsidiarySuffix) {
		this.subsidiarySuffix = subsidiarySuffix;
	}

	@Override
	public void setWhatTheCharityDoes(List<String> whatTheCharityDoes) {
		this.whatTheCharityDoes = whatTheCharityDoes;
	}

	@Override
	public void setWhoTheCharityHelps(List<String> whoTheCharityHelps) {
		this.whoTheCharityHelps = whoTheCharityHelps;
	}

	@Override
	public String getOrganisationType() {
		return GetterUtil.getString(organisationType);
	}

	@Override
	public void setOrganisationType(String organisationType) {
		this.organisationType = organisationType;
	}

	@Override
	public List<RegistrationRecord> getRegistrationHistory() {
		return registrationHistory == null ? Collections.emptyList() : registrationHistory;
	}

	@Override
	public void setRegistrationHistory(List<RegistrationRecord> registrationHistory) {
		this.registrationHistory = registrationHistory;
	}

}

package uk.gov.ccew.publicdata.mock.service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import uk.gov.ccew.publicdata.mock.constants.MockDataConstants;
import uk.gov.ccew.publicdata.model.AnnualReturn;
import uk.gov.ccew.publicdata.model.AnnualReturnAndAccountsAndTar;
import uk.gov.ccew.publicdata.model.impl.AnnualReturnAndAccountsAndTarImpl;
import uk.gov.ccew.publicdata.model.impl.AnnualReturnImpl;

@Component(immediate = true, service = MockAnnualReturnAndAccountsAndTarImport.class)
public class MockAnnualReturnAndAccountsAndTarImport {
	
	private static final String FILE_BASE_FOLDER = "mock/accounts-and-annual-returns/";

	private static final String FILE_PRIMARY_PURPOSE_GRANT_GIVING = FILE_BASE_FOLDER + "primary-purpose-grant-giving.csv";

	private static final Log LOG = LogFactoryUtil.getLog(MockAnnualReturnAndAccountsAndTarImport.class);

	private static Map<Long, List<AnnualReturnAndAccountsAndTar>> returnsAccountsTarItems = new HashMap<>();

	@Activate
	protected void activate() {
		importMockDataFromFiles();
	}

	public Map<Long, List<AnnualReturnAndAccountsAndTar>> getImportedAnnualReturnAndAccountsAndTar() {
		return returnsAccountsTarItems;
	}
	
	private List<AnnualReturnAndAccountsAndTar> getAnnualReturnAndAccountsAndTarAndAddToMap(Long organisationNumber) {
		
		List<AnnualReturnAndAccountsAndTar> annualReturnAndAccountsAndTars = returnsAccountsTarItems.get(organisationNumber);
		if (annualReturnAndAccountsAndTars == null) {
			if (returnsAccountsTarItems.size() >= MockDataConstants.MAX_IMPORT_COUNT) {
				return null;
			}
			
			annualReturnAndAccountsAndTars = new ArrayList<>();
			returnsAccountsTarItems.put(organisationNumber, annualReturnAndAccountsAndTars);
		}
		
		return annualReturnAndAccountsAndTars;
	}

	private void importFile(String fileName) {

		LOG.info("Importing AnnualReturnAndAccountsAndTar mock data file:" + fileName);

		InputStream dataFile = MockCharityImport.class.getClassLoader().getResourceAsStream(fileName);

		try (Reader in = new InputStreamReader(dataFile)) {

			Iterable<CSVRecord> mockAnnualReturnAndAccountsAndTars = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(in);
			for (CSVRecord mockAnnualReturnAndAccountsAndTarRecord : mockAnnualReturnAndAccountsAndTars) {

				if (returnsAccountsTarItems.size() > MockDataConstants.MAX_IMPORT_COUNT) continue;
				
				String organisationNumber = mockAnnualReturnAndAccountsAndTarRecord.get(MockDataConstants.FIELD_ORGANISATION_NUMBER);
				List<AnnualReturnAndAccountsAndTar> annualReturnAndAccountsAndTars = getAnnualReturnAndAccountsAndTarAndAddToMap(Long.parseLong(organisationNumber));
				
				if (annualReturnAndAccountsAndTars != null) {
					
					AnnualReturnAndAccountsAndTar annualReturnAndAccountsAndTar = new AnnualReturnAndAccountsAndTarImpl();
					AnnualReturn annualReturn = new AnnualReturnImpl();
					annualReturn.setPrimaryPurposeGrantGiving(Boolean.parseBoolean(mockAnnualReturnAndAccountsAndTarRecord.get(MockDataConstants.FIELD_VALUE)));
					annualReturnAndAccountsAndTars.add(annualReturnAndAccountsAndTar);
					annualReturnAndAccountsAndTar.setAnnualReturn(annualReturn);
					
				}
			}

			LOG.info("Finished importing AnnualReturnAndAccountsAndTar mock data file:" + fileName);

		} catch (Exception e) {
			LOG.error("Error importing AnnualReturnAndAccountsAndTar mock data file." + fileName, e);
		}
	}

	private void importMockDataFromFiles() {

		importFile(MockAnnualReturnAndAccountsAndTarImport.FILE_PRIMARY_PURPOSE_GRANT_GIVING);
		
	}
	
}

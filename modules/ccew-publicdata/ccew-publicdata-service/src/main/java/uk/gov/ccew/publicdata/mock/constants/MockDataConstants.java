package uk.gov.ccew.publicdata.mock.constants;

public final class MockDataConstants {

	public static final long DEFAULT_ORGANISATION_NUMBER = 306025l;

	public static final String FIELD_CHARITY_NAME = "charity_name";

	public static final String FIELD_CHARITY_NUMBER = "registered_charity_number";

	public static final String FIELD_CONTINENT = "continent";

	public static final String FIELD_IS_WELSH = "is_welsh";

	public static final String FIELD_LOCAL_AUTHORITY = "local_authority";

	public static final String FIELD_METROPOLITAN_COUNCIL = "metropolitan_council";

	public static final String FIELD_ORGANISATION_NUMBER = "organisation_number";

	public static final String FIELD_SUBSIDIARY_SUFFIX = "subsidiary_suffix";

	public static final String FIELD_VALUE = "value";

	public static final int MAX_IMPORT_COUNT = 500;

	public static final String MOCK_ACCOUNTS_AND_ANNUAL_RETURNS_RESOURCE_PATH = "mock/accounts-and-annual-returns/%d.json";

	public static final String MOCK_ASSETS_AND_LIABILITIES_RESOURCE_PATH = "mock/assets-and-liabilities/%d.json";

	public static final String MOCK_CHARITY_RESOURCE_PATH = "mock/charity/%d.json";

	public static final String MOCK_EMPLOYEE_RESOURCE_PATH = "mock/employees-summary/%d.json";

	public static final String MOCK_FINANCIAL_HISTORY_RESOURCE_PATH = "mock/financial-history/%d.json";

	public static final String MOCK_FUNDRAISING_SUMMARY_RESOURCE_PATH = "mock/fundraising-summary/%d.json";

	public static final String MOCK_INCOME_AND_EXPENDITURE_RESOURCE_PATH = "mock/income-and-expenditure-summary/%d.json";

	public static final String MOCK_TRUSTEE_RESOURCE_PATH = "mock/trustee/%s.json";

	private MockDataConstants() {

	}

}

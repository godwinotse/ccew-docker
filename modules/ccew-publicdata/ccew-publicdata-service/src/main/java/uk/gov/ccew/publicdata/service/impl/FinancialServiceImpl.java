package uk.gov.ccew.publicdata.service.impl;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;

import uk.gov.ccew.publicdata.internal.service.GsonService;
import uk.gov.ccew.publicdata.mock.constants.MockDataConstants;
import uk.gov.ccew.publicdata.mock.service.MockAnnualReturnAndAccountsAndTarService;
import uk.gov.ccew.publicdata.mock.service.MockResourceService;
import uk.gov.ccew.publicdata.model.AnnualReturnAndAccountsAndTar;
import uk.gov.ccew.publicdata.model.AssetsAndLiabilities;
import uk.gov.ccew.publicdata.model.FinancialAnnualReport;
import uk.gov.ccew.publicdata.model.FundraisingSummary;
import uk.gov.ccew.publicdata.model.IncomeAndExpenditureSummary;
import uk.gov.ccew.publicdata.model.impl.AccountsAndTarImpl;
import uk.gov.ccew.publicdata.model.impl.AnnualReturnAndAccountsAndTarImpl;
import uk.gov.ccew.publicdata.model.impl.AnnualReturnImpl;
import uk.gov.ccew.publicdata.model.impl.AssetsAndLiabilitiesImpl;
import uk.gov.ccew.publicdata.model.impl.FinancialAnnualReportImpl;
import uk.gov.ccew.publicdata.model.impl.FundraisingSummaryImpl;
import uk.gov.ccew.publicdata.model.impl.IncomeAndExpenditureSummaryImpl;
import uk.gov.ccew.publicdata.parser.AccountsAndTarTypeAdapterFactory;
import uk.gov.ccew.publicdata.parser.AnnualReturnAndAccountsAndTarTypeAdapterFactory;
import uk.gov.ccew.publicdata.parser.AnnualReturnTypeAdapterFactory;
import uk.gov.ccew.publicdata.parser.AssetsAndLiabilitiesTypeAdapterFactory;
import uk.gov.ccew.publicdata.parser.FinancialAnnualReportTypeAdapterFactory;
import uk.gov.ccew.publicdata.service.FinancialService;

@Component(immediate = true, service = FinancialService.class)
public class FinancialServiceImpl implements FinancialService {

	@Reference
	private GsonService gsonService;
	
	@Reference
	private MockAnnualReturnAndAccountsAndTarService mockAnnualReturnAndAccountsAndTarService;

	@Reference
	private MockResourceService mockResourceService;

	@Override
	public List<AnnualReturnAndAccountsAndTar> getAnnualReturnsAndAccountsAndTars(long organisationNumber) {
		return getMockAnnualReturnsAndAccountsAndTars(organisationNumber);
	}

	@Override
	public AssetsAndLiabilities getAssetsAndLiabilities(long organisationNumber) {
		return getMockAssetsAndLiabilities(organisationNumber);
	}

	@Override
	public List<FinancialAnnualReport> getFinancialHistory(long organisationNumber) {
		return getMockFinancialAnnualReports(306025);
	}

	@Override
	public FundraisingSummary getFundraisingSummary(long organisationNumber) {
		return getMockFundraisingSummary(organisationNumber);
	}

	@Override
	public IncomeAndExpenditureSummary getIncomeAndExpenditureSummary(long organisationNumber) {
		return getIncomeAndExpenditureSummaryMocked(organisationNumber);
	}

	private List<AnnualReturnAndAccountsAndTar> getMockAnnualReturnsAndAccountsAndTars(long organisationNumber) {

		return mockAnnualReturnAndAccountsAndTarService.getMockAnnualReturnsAndAccountsAndTars(organisationNumber);

	}

	private AssetsAndLiabilities getMockAssetsAndLiabilities(long organisationNumber) {

		InputStream inputStream = mockResourceService.getInputStream(MockDataConstants.MOCK_ASSETS_AND_LIABILITIES_RESOURCE_PATH, organisationNumber, MockDataConstants.DEFAULT_ORGANISATION_NUMBER);

		List<TypeAdapterFactory> typeAdapterFactories = new ArrayList<>();
		typeAdapterFactories.add(new AssetsAndLiabilitiesTypeAdapterFactory(AssetsAndLiabilitiesImpl.class));

		return gsonService.fromJson(inputStream, AssetsAndLiabilitiesImpl.class, typeAdapterFactories);

	}
	
	private List<FinancialAnnualReport> getMockFinancialAnnualReports(long organisationNumber) {

		InputStream inputStream = mockResourceService.getInputStream(MockDataConstants.MOCK_FINANCIAL_HISTORY_RESOURCE_PATH, organisationNumber, MockDataConstants.DEFAULT_ORGANISATION_NUMBER);
		Type financialAnnualReportListType = new TypeToken<ArrayList<FinancialAnnualReport>>() {

		}.getType();
		List<TypeAdapterFactory> typeAdapterFactories = Collections.singletonList(new FinancialAnnualReportTypeAdapterFactory(FinancialAnnualReportImpl.class));

		return gsonService.fromJson(inputStream, financialAnnualReportListType, typeAdapterFactories);

	}

	private FundraisingSummary getMockFundraisingSummary(long organisationNumber) {

		InputStream inputStream = mockResourceService.getInputStream(MockDataConstants.MOCK_FUNDRAISING_SUMMARY_RESOURCE_PATH, organisationNumber, MockDataConstants.DEFAULT_ORGANISATION_NUMBER);

		return gsonService.fromJson(inputStream, FundraisingSummaryImpl.class);

	}

	private IncomeAndExpenditureSummary getIncomeAndExpenditureSummaryMocked(long organisationNumber) {

		InputStream inputStream = mockResourceService.getInputStream(MockDataConstants.MOCK_INCOME_AND_EXPENDITURE_RESOURCE_PATH, organisationNumber, MockDataConstants.DEFAULT_ORGANISATION_NUMBER);

		return gsonService.fromJson(inputStream, IncomeAndExpenditureSummaryImpl.class);

	}

}

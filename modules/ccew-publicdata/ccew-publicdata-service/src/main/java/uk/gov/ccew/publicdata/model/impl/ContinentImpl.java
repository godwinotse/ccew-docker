package uk.gov.ccew.publicdata.model.impl;

import com.liferay.portal.kernel.util.GetterUtil;

import uk.gov.ccew.publicdata.model.Continent;

public class ContinentImpl implements Continent {

	private String name;

	@Override
	public String getName() {
		return GetterUtil.getString(name);
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

}

package uk.gov.ccew.publicdata.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import uk.gov.ccew.publicdata.mock.service.MockCharityService;
import uk.gov.ccew.publicdata.model.Charity;
import uk.gov.ccew.publicdata.service.CharityService;

@Component(immediate = true, service = CharityService.class)
public class CharityServiceImpl implements CharityService {

	@Reference
	private MockCharityService mockCharityService;

	@Override
	public Charity getCharityByOrganisationNumber(long organisationNumber) {
		return getMockCharity(organisationNumber);
	}

	@Override
	public int getCharityCount() {
		return mockCharityService.getCharityCount();
	}

	@Override
	public List<Charity> getCharities(int start, int end) {
		return mockCharityService.getCharities(start, end);
	}

	@Override
	public List<Charity> getCharitiesByOrganisationNumbers(List<Long> organisationNumbers) {
		return organisationNumbers.stream().map(this::getCharityByOrganisationNumber).collect(Collectors.toList());
	}

	private Charity getMockCharity(long organisationNumber) {
		return mockCharityService.getCharity(organisationNumber);
	}

}

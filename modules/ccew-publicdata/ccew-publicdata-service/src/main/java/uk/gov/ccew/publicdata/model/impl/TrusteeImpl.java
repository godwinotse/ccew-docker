package uk.gov.ccew.publicdata.model.impl;

import uk.gov.ccew.publicdata.model.Trustee;
import uk.gov.ccew.publicdata.model.TrusteeRole;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.liferay.portal.kernel.util.GetterUtil;

public class TrusteeImpl implements Trustee {

	private Date dateOfAppointment;

	private boolean directorOfSubsidiary;

	private String name;

	private List<Long> otherCharityNumbers;

	private TrusteeRole role;

	private List<Long> otherCharityOrganisationNumbers;

	private boolean receivingPaymentsFromCharity;

	@Override
	public Optional<Date> getDateOfAppointment() {
		return Optional.ofNullable(dateOfAppointment);
	}

	@Override
	public String getName() {
		return GetterUtil.getString(name);
	}

	@Override
	public Optional<TrusteeRole> getRole() {
		return Optional.ofNullable(role);
	}

	@Override
	public List<Long> getOtherCharityOrganisationNumbers() {
		return otherCharityOrganisationNumbers == null ? Collections.emptyList() : otherCharityOrganisationNumbers;
	}

	@Override
	public boolean isDirectorOfSubsidiary() {
		return directorOfSubsidiary;
	}

	@Override
	public boolean isReceivingPaymentsFromCharity() {
		return receivingPaymentsFromCharity;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

}

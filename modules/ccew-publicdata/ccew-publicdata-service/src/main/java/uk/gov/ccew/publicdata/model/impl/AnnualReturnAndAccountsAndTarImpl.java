package uk.gov.ccew.publicdata.model.impl;

import java.util.Optional;

import uk.gov.ccew.publicdata.model.AccountsAndTar;
import uk.gov.ccew.publicdata.model.AnnualReturn;
import uk.gov.ccew.publicdata.model.AnnualReturnAndAccountsAndTar;

public class AnnualReturnAndAccountsAndTarImpl implements AnnualReturnAndAccountsAndTar {

	private AccountsAndTar accountsAndTar;

	private AnnualReturn annualReturn;

	@Override
	public Optional<AccountsAndTar> getAccountsAndTar() {
		return Optional.ofNullable(accountsAndTar);
	}

	@Override
	public Optional<AnnualReturn> getAnnualReturn() {
		return Optional.ofNullable(annualReturn);
	}

	@Override
	public void setAnnualReturn(AnnualReturn annualReturn) {
		this.annualReturn = annualReturn;
		
	}

}

package uk.gov.ccew.publicdata.model.impl;

import uk.gov.ccew.publicdata.model.CharityAnnualReport;
import uk.gov.ccew.publicdata.model.CharityAnnualReportStatus;

import java.util.Date;
import java.util.Optional;

public class CharityAnnualReportImpl implements CharityAnnualReport {

	private int daysOverdue;

	private CharityAnnualReportStatus status;

	private Date submittedDate;

	@Override
	public Optional<CharityAnnualReportStatus> getStatus() {
		return Optional.ofNullable(status);
	}

	@Override
	public Optional<Date> getSubmittedDate() {
		return Optional.ofNullable(submittedDate);
	}

	@Override
	public int getDaysOverdue() {
		return daysOverdue;
	}

}

package uk.gov.ccew.publicdata.parser;

import uk.gov.ccew.publicdata.model.RegistrationRecord;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;

public class RegistrationRecordTypeAdapterFactory implements TypeAdapterFactory {

	private final Class<? extends RegistrationRecord> implementationClass;

	public RegistrationRecordTypeAdapterFactory(Class<? extends RegistrationRecord> implementationClass) {
		this.implementationClass = implementationClass;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {

		TypeAdapter<T> typeAdapter;

		if (RegistrationRecord.class.equals(type.getRawType())) {

			typeAdapter = (TypeAdapter<T>) gson.getAdapter(implementationClass);

		} else {

			typeAdapter = null;

		}

		return typeAdapter;
	}

}

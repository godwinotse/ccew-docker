package uk.gov.ccew.publicdata.mock.service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

import uk.gov.ccew.publicdata.mock.constants.MockDataConstants;
import uk.gov.ccew.publicdata.model.AreaOfOperation;
import uk.gov.ccew.publicdata.model.Charity;
import uk.gov.ccew.publicdata.model.CharityStatus;
import uk.gov.ccew.publicdata.model.Continent;
import uk.gov.ccew.publicdata.model.Country;
import uk.gov.ccew.publicdata.model.LocalAuthority;
import uk.gov.ccew.publicdata.model.impl.AreaOfOperationImpl;
import uk.gov.ccew.publicdata.model.impl.CharityImpl;
import uk.gov.ccew.publicdata.model.impl.ContinentImpl;
import uk.gov.ccew.publicdata.model.impl.CountryImpl;
import uk.gov.ccew.publicdata.model.impl.LocalAuthorityImpl;

@Component(immediate = true, service = MockCharityImport.class)
public class MockCharityImport {

	private static final String FILE_BASE_FOLDER = "mock/charity/";

	private static final String FILE_EXPENDITURE = FILE_BASE_FOLDER + "expenditure.csv";

	private static final String FILE_HOW_CHARITY_HELPS = FILE_BASE_FOLDER + "how-charity-helps.csv";

	private static final String FILE_INCOME = FILE_BASE_FOLDER + "income.csv";

	private static final String FILE_REGISTERED_STATUS = FILE_BASE_FOLDER + "registered-status.csv";

	private static final String FILE_REGISTRATION_DATE = FILE_BASE_FOLDER + "registration-date.csv";

	private static final String FILE_REMOVED_DATE = FILE_BASE_FOLDER + "removed-date.csv";

	private static final String FILE_WHAT_CHARITY_DOES = FILE_BASE_FOLDER + "what-charity-does.csv";

	private static final String FILE_WHO_CHARITY_HELPS = FILE_BASE_FOLDER + "who-charity-helps.csv";

	private static final String FILE_REGION_OF_OPERATION = FILE_BASE_FOLDER + "region-of-operation.csv";

	private static final String FILE_CONTINENT_OF_OPERATION = FILE_BASE_FOLDER + "continent-of-operation.csv";

	private static final String FILE_COUNTRY_OF_OPERATION = FILE_BASE_FOLDER + "country-of-operation.csv";

	private static final String FILE_LOCAL_AUTHORITY_OF_OPERATION = FILE_BASE_FOLDER + "local-authority-of-operation.csv";

	private static final Log LOG = LogFactoryUtil.getLog(MockCharityImport.class);

	private static Map<String, Charity> charityImportRecords = new HashMap<>();

	private static DateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

	public Map<String, Charity> getImportedCharities() {
		return charityImportRecords;
	}

	@Activate
	protected void activate() {
		importMockDataFromFiles();
	}

	private Charity getCharityAndAddToMap(String organisationNumber) {

		Charity charity = charityImportRecords.get(organisationNumber);
		if (charity == null) {
			if (charityImportRecords.size() > MockDataConstants.MAX_IMPORT_COUNT) {
				return null;
			}

			charity = new CharityImpl();
			initialiseCharityObjects(charity);
			charityImportRecords.put(organisationNumber, charity);
		}

		return charity;
	}

	private void importFile(String fileName) {

		LOG.info("Importing mock data file:" + fileName);

		InputStream dataFile = MockCharityImport.class.getClassLoader().getResourceAsStream(fileName);

		try (Reader in = new InputStreamReader(dataFile)) {

			Iterable<CSVRecord> mockCharityRecords = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(in);

			for (CSVRecord mockCharityRecord : mockCharityRecords) {

				String organisationNumber = mockCharityRecord.get(MockDataConstants.FIELD_ORGANISATION_NUMBER).trim();
				Charity charity = getCharityAndAddToMap(organisationNumber);

				if (charity != null) {

					setCharityValuesForThisFile(fileName, charity, mockCharityRecord);

				} else {
					LOG.info("No charity for..." + organisationNumber + ":" + fileName);
				}

			}

			LOG.info("Finished importing mock data file:" + fileName);

		} catch (Exception e) {
			LOG.error("Error importing mock data file." + fileName, e);
		}

	}

	private void importMockDataFromFiles() {

		importFile(MockCharityImport.FILE_REGISTRATION_DATE);
		importFile(MockCharityImport.FILE_REMOVED_DATE);
		importFile(MockCharityImport.FILE_EXPENDITURE);
		importFile(MockCharityImport.FILE_HOW_CHARITY_HELPS);
		importFile(MockCharityImport.FILE_INCOME);
		importFile(MockCharityImport.FILE_REGISTERED_STATUS);
		importFile(MockCharityImport.FILE_WHAT_CHARITY_DOES);
		importFile(MockCharityImport.FILE_WHO_CHARITY_HELPS);
		importFile(MockCharityImport.FILE_REGION_OF_OPERATION);
		importFile(MockCharityImport.FILE_COUNTRY_OF_OPERATION);
		importFile(MockCharityImport.FILE_LOCAL_AUTHORITY_OF_OPERATION);
		importFile(MockCharityImport.FILE_CONTINENT_OF_OPERATION);

	}

	private void setCharityValuesForThisFile(String fileName, Charity charity, CSVRecord mockCharityRecord) {

		try {
			setCommonProperties(charity, mockCharityRecord);
			setValuesForFacetName(fileName, mockCharityRecord, charity);
		} catch (Exception e) {
			LOG.error("Error importing mock data organisation number:" + charity.getOrganisationNumber() + " File:" + fileName, e);
		}

	}

	private void setCommonProperties(Charity charity, CSVRecord mockCharityRecord) {

		charity.setCharityName(mockCharityRecord.get(MockDataConstants.FIELD_CHARITY_NAME));
		charity.setOrganisationNumber(Long.parseLong(mockCharityRecord.get(MockDataConstants.FIELD_ORGANISATION_NUMBER)));
		charity.setCharityNumber(Long.parseLong(mockCharityRecord.get(MockDataConstants.FIELD_CHARITY_NUMBER)));
		charity.setSubsidiarySuffix(Integer.parseInt(mockCharityRecord.get(MockDataConstants.FIELD_SUBSIDIARY_SUFFIX)));

	}

	private void initialiseCharityObjects(Charity charity) {

		List<String> howTheCharityHelps = new ArrayList<>();
		charity.setHowTheCharityHelps(howTheCharityHelps);

		List<String> whatTheCharityDoes = new ArrayList<>();
		charity.setWhatTheCharityDoes(whatTheCharityDoes);

		List<String> whoTheCharityHelps = new ArrayList<>();
		charity.setWhoTheCharityHelps(whoTheCharityHelps);

		AreaOfOperation areaOfOperation = new AreaOfOperationImpl();
		areaOfOperation.setConstituencies(new ArrayList<>());
		areaOfOperation.setCountries(new ArrayList<>());
		areaOfOperation.setRegions(new ArrayList<>());
		areaOfOperation.setLocalAuthorities(new ArrayList<>());
		charity.setAreaOfOperation(areaOfOperation);

	}

	private void setValuesForFacetName(String fileName, CSVRecord mockCharityRecord, Charity charity) throws Exception {
		String facetValue = mockCharityRecord.get(MockDataConstants.FIELD_VALUE);

		if (FILE_EXPENDITURE.equals(fileName) && Validator.isNotNull(facetValue)) {

			setExpenditure(charity, facetValue);

		} else if (FILE_HOW_CHARITY_HELPS.equalsIgnoreCase(fileName)) {

			setHowCharityHelps(charity, facetValue);

		} else if (FILE_INCOME.equals(fileName) && Validator.isNotNull(facetValue)) {

			setIncome(charity, facetValue);

		} else if (FILE_REGISTERED_STATUS.equals(fileName)) {

			setRegisteredStatus(charity, facetValue);

		} else if (FILE_REGISTRATION_DATE.equals(fileName)) {

			setRegistrationDate(charity, facetValue);

		} else if (FILE_WHAT_CHARITY_DOES.equals(fileName)) {

			setWhatCharityDoes(charity, facetValue);

		} else if (FILE_WHO_CHARITY_HELPS.equals(fileName)) {

			setWhoCharityHelps(charity, facetValue);

		} else if (FILE_REGION_OF_OPERATION.equals(fileName)) {

			setRegionOfOperation(charity, mockCharityRecord);

		} else if (FILE_COUNTRY_OF_OPERATION.equals(fileName)) {

			setCountryOfOperation(charity, mockCharityRecord);

		} else if (FILE_LOCAL_AUTHORITY_OF_OPERATION.equals(fileName)) {

			setLocaAuthorityOfOperation(charity, mockCharityRecord);

		} else if (FILE_REMOVED_DATE.equals(fileName)) {

			setRemovedDate(charity, facetValue);

		}
	}

	private void setExpenditure(Charity charity, String facetValue) {

		try {
			charity.setExpenditure(new BigDecimal(facetValue));
		} catch (NumberFormatException e) {
			// Not all have values
		}

	}

	private void setHowCharityHelps(Charity charity, String facetValue) {

		charity.getHowTheCharityHelps().add(facetValue);

	}

	private void setIncome(Charity charity, String facetValue) {

		try {
			charity.setIncome(new BigDecimal(facetValue));
		} catch (NumberFormatException e) {
			// Not all have values
		}

	}

	private void setRegisteredStatus(Charity charity, String facetValue) {

		charity.setCharityStatus(CharityStatus.getCharityStatusFromString(facetValue));

	}

	private void setRegistrationDate(Charity charity, String facetValue) throws ParseException {

		charity.setRegistrationDate(dateFormatter.parse(facetValue));

	}

	private void setRemovedDate(Charity charity, String facetValue) throws ParseException {

		charity.setRemovedDate(dateFormatter.parse(facetValue));

	}

	private void setWhatCharityDoes(Charity charity, String facetValue) {

		charity.getWhatTheCharityDoes().add(facetValue);

	}

	private void setWhoCharityHelps(Charity charity, String facetValue) {

		charity.getWhoTheCharityHelps().add(facetValue);

	}

	private void setCountryOfOperation(Charity charity, CSVRecord mockCharityRecord) {

		charity.getAreaOfOperation().ifPresent(areaOfOperation -> {
			Country country = new CountryImpl();
			country.setName(mockCharityRecord.get(MockDataConstants.FIELD_VALUE));

			Continent continent = new ContinentImpl();
			continent.setName(mockCharityRecord.get(MockDataConstants.FIELD_CONTINENT));
			country.setContinent(continent);

			charity.getAreaOfOperation().get().getCountries().add(country);
		});

	}

	private void setLocaAuthorityOfOperation(Charity charity, CSVRecord mockCharityRecord) {

		charity.getAreaOfOperation().ifPresent(areaOfOperation -> {
			LocalAuthority localAuthority = new LocalAuthorityImpl();

			localAuthority.setWelsh(Boolean.parseBoolean(mockCharityRecord.get(MockDataConstants.FIELD_IS_WELSH)));
			localAuthority.setLocalAuthorityName(mockCharityRecord.get(MockDataConstants.FIELD_VALUE));
			localAuthority.setMetropolitanCouncilName(mockCharityRecord.get(MockDataConstants.FIELD_METROPOLITAN_COUNCIL));
			charity.getAreaOfOperation().get().getLocalAuthorities().add(localAuthority);
		});
	}

	private void setRegionOfOperation(Charity charity, CSVRecord mockCharityRecord) {

		charity.getAreaOfOperation().ifPresent(areaOfOperation -> {
			charity.getAreaOfOperation().get().getRegions().add(mockCharityRecord.get(MockDataConstants.FIELD_VALUE));
		});

	}
}

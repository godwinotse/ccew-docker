package uk.gov.ccew.publicdata.service.impl;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import uk.gov.ccew.publicdata.mock.service.MockTrusteeService;
import uk.gov.ccew.publicdata.model.Trustee;
import uk.gov.ccew.publicdata.service.TrusteeService;

@Component(immediate = true, service = TrusteeService.class)
public class TrusteeServiceImpl implements TrusteeService {
	
	@Reference
	private MockTrusteeService mockTrusteeService;

	@Override
	public int getTrusteeCount(long organisationNumber) {
		
		return getMockTrustees(organisationNumber).size();
		
	}

	@Override
	public List<Trustee> getTrustees(long organisationNumber) {
		
		return getMockTrustees(organisationNumber);
		
	}

	private List<Trustee> getMockTrustees(long organisationNumber) {

		return mockTrusteeService.getMockTrustees(organisationNumber);

	}

}

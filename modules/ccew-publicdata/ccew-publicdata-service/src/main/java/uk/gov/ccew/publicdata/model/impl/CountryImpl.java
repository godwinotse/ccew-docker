package uk.gov.ccew.publicdata.model.impl;

import java.util.Optional;

import com.liferay.portal.kernel.util.GetterUtil;

import uk.gov.ccew.publicdata.model.Continent;
import uk.gov.ccew.publicdata.model.Country;

public class CountryImpl implements Country {

	private Continent continent;

	private String name;

	@Override
	public Optional<Continent> getContinent() {
		return Optional.ofNullable(continent);
	}

	@Override
	public String getName() {
		return GetterUtil.getString(name);
	}

	@Override
	public void setContinent(Continent continent) {
		this.continent = continent;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

}

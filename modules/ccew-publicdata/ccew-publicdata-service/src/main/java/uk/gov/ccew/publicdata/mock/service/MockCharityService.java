package uk.gov.ccew.publicdata.mock.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.TypeAdapterFactory;

import uk.gov.ccew.publicdata.internal.service.GsonService;
import uk.gov.ccew.publicdata.mock.constants.MockDataConstants;
import uk.gov.ccew.publicdata.model.Charity;
import uk.gov.ccew.publicdata.model.impl.CharityAnnualReportImpl;
import uk.gov.ccew.publicdata.model.impl.CharityImpl;
import uk.gov.ccew.publicdata.model.impl.RegistrationRecordImpl;
import uk.gov.ccew.publicdata.model.impl.RegulatoryAlertImpl;
import uk.gov.ccew.publicdata.model.impl.TrusteeImpl;
import uk.gov.ccew.publicdata.parser.CharityAnnualReportTypeAdapterFactory;
import uk.gov.ccew.publicdata.parser.RegistrationRecordTypeAdapterFactory;
import uk.gov.ccew.publicdata.parser.RegulatoryAlertTypeAdapterFactory;
import uk.gov.ccew.publicdata.parser.TrusteeTypeAdapterFactory;

@Component(immediate = true, service = MockCharityService.class)
public class MockCharityService {

	@Reference
	private GsonService gsonService;

	private MockCharityCollection mockCharityCollection;

	@Reference
	private MockCharityImport mockCharityImport;

	@Reference
	private MockResourceService mockResourceService;

	public Charity getCharity(long organisationNumber) {
		return mockCharityCollection.getMockCharity(organisationNumber);
	}

	public int getCharityCount() {
		return mockCharityCollection.getMockCharityCount();
	}

	public List<Charity> getCharities(int start, int end) {
		return mockCharityCollection.getMockCharities(start, end);
	}

	@Activate
	protected void activate() {

		mockCharityCollection = new MockCharityCollection();
		populateMockCharityCollection();

	}

	private Charity getMockCharityFromFile(long organisationNumber) {

		InputStream inputStream = mockResourceService.getInputStream(MockDataConstants.MOCK_CHARITY_RESOURCE_PATH,
				organisationNumber, MockDataConstants.DEFAULT_ORGANISATION_NUMBER);

		List<TypeAdapterFactory> typeAdapterFactories = new ArrayList<>();
		typeAdapterFactories.add(new CharityAnnualReportTypeAdapterFactory(CharityAnnualReportImpl.class));
		typeAdapterFactories.add(new RegulatoryAlertTypeAdapterFactory(RegulatoryAlertImpl.class));
		typeAdapterFactories.add(new TrusteeTypeAdapterFactory(TrusteeImpl.class));
		typeAdapterFactories.add(new RegistrationRecordTypeAdapterFactory(RegistrationRecordImpl.class));

		return gsonService.fromJson(inputStream, CharityImpl.class, typeAdapterFactories);

	}

	private void populateMockCharityCollection() {

		Map<String, Charity> importedCharities = mockCharityImport.getImportedCharities();
		mockCharityCollection.addMockCharities(importedCharities.values());
		mockCharityCollection.addMockCharity(getMockCharityFromFile(1078963));
		mockCharityCollection.addMockCharity(getMockCharityFromFile(1183073));
		mockCharityCollection.addMockCharity(getMockCharityFromFile(306025));
		mockCharityCollection.addMockCharity(getMockCharityFromFile(521584));
		mockCharityCollection.addMockCharity(getMockCharityFromFile(216250));
		mockCharityCollection.addMockCharity(getMockCharityFromFile(3957094));
		mockCharityCollection.addMockCharity(getMockCharityFromFile(506417));

	}

}

package uk.gov.ccew.publicdata.parser;

import uk.gov.ccew.publicdata.model.Trustee;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;

public class TrusteeTypeAdapterFactory implements TypeAdapterFactory {

	private final Class<? extends Trustee> implementationClass;

	public TrusteeTypeAdapterFactory(Class<? extends Trustee> implementationClass) {

		this.implementationClass = implementationClass;

	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {

		TypeAdapter<T> typeAdapter;

		if (Trustee.class.equals(type.getRawType())) {

			typeAdapter = (TypeAdapter<T>) gson.getAdapter(implementationClass);

		} else {

			typeAdapter = null;

		}

		return typeAdapter;

	}

}

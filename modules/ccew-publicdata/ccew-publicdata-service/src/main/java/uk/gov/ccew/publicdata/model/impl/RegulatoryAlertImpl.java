package uk.gov.ccew.publicdata.model.impl;

import uk.gov.ccew.publicdata.model.RegulatoryAlert;
import uk.gov.ccew.publicdata.model.RegulatoryAlertIncidentType;

import java.util.Date;
import java.util.Optional;

import com.google.gson.annotations.SerializedName;
import com.liferay.portal.kernel.util.GetterUtil;

public class RegulatoryAlertImpl implements RegulatoryAlert {

	private Date incidentDate;

	private String incidentUserName;

	@SerializedName("type")
	private RegulatoryAlertIncidentType incidentType;

	private String url;

	@Override
	public Optional<Date> getIncidentDate() {
		return Optional.ofNullable(incidentDate);
	}

	@Override
	public Optional<RegulatoryAlertIncidentType> getIncidentType() {
		return Optional.ofNullable(incidentType);
	}

	@Override
	public String getIncidentUserName() {
		return GetterUtil.getString(incidentUserName, "unknown");
	}

	@Override
	public String getUrl() {
		return GetterUtil.getString(url);
	}

}

package uk.gov.ccew.publicdata.service.impl;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import uk.gov.ccew.publicdata.internal.service.GsonService;
import uk.gov.ccew.publicdata.mock.constants.MockDataConstants;
import uk.gov.ccew.publicdata.mock.service.MockResourceService;
import uk.gov.ccew.publicdata.model.EmployeesSummary;
import uk.gov.ccew.publicdata.model.impl.EmployeesSummaryImpl;
import uk.gov.ccew.publicdata.service.EmployeesSummaryService;

@Component(immediate = true, service = EmployeesSummaryService.class)
public class EmployeesSummaryServiceImpl implements EmployeesSummaryService {

	@Reference
	private GsonService gsonService;

	@Reference
	private MockResourceService mockResourceService;

	@Override
	public EmployeesSummary getEmployeesSummary(long organisationNumber) {
		return getMockEmployeesSummary(organisationNumber);
	}

	private EmployeesSummary getMockEmployeesSummary(long organisationNumber) {

		InputStream inputStream = mockResourceService.getInputStream(MockDataConstants.MOCK_EMPLOYEE_RESOURCE_PATH, organisationNumber, MockDataConstants.DEFAULT_ORGANISATION_NUMBER);

		return gsonService.fromJson(inputStream, EmployeesSummaryImpl.class);

	}

}

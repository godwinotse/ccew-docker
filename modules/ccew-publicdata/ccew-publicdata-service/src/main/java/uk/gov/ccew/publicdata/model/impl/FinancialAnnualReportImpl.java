package uk.gov.ccew.publicdata.model.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import uk.gov.ccew.publicdata.model.FinancialAnnualReport;

public class FinancialAnnualReportImpl implements FinancialAnnualReport {

	private Map<String, BigDecimal> expenditureTotalsByCategory;

	private Date financialYearEnd;

	private Map<String, BigDecimal> incomeTotalsByCategory;

	@Override
	public Map<String, BigDecimal> getExpenditureTotalsByCategory() {
		return expenditureTotalsByCategory == null ? Collections.emptyMap() : expenditureTotalsByCategory;
	}

	@Override
	public Optional<Date> getFinancialYearEnd() {
		return Optional.ofNullable(financialYearEnd);
	}

	@Override
	public Map<String, BigDecimal> getIncomeTotalsByCategory() {
		return incomeTotalsByCategory == null ? Collections.emptyMap() : incomeTotalsByCategory;
	}

}

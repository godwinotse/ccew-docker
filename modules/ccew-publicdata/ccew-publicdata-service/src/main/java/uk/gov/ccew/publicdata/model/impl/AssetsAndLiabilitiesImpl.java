package uk.gov.ccew.publicdata.model.impl;

import java.math.BigDecimal;

import uk.gov.ccew.publicdata.model.AssetsAndLiabilities;

public class AssetsAndLiabilitiesImpl implements AssetsAndLiabilities {

	private BigDecimal definedPensionBenefitTotal;

	private BigDecimal liabilitiesTotal;

	private BigDecimal longTermInvestmentTotal;

	private BigDecimal otherAssetsTotal;

	private BigDecimal ownUseAssetsTotal;

	@Override
	public BigDecimal getDefinedPensionBenefitTotal() {
		return definedPensionBenefitTotal == null ? BigDecimal.ZERO : definedPensionBenefitTotal;
	}

	@Override
	public BigDecimal getLiabilitiesTotal() {
		return liabilitiesTotal == null ? BigDecimal.ZERO : liabilitiesTotal;
	}

	@Override
	public BigDecimal getLongTermInvestmentTotal() {
		return longTermInvestmentTotal == null ? BigDecimal.ZERO : longTermInvestmentTotal;
	}

	@Override
	public BigDecimal getOtherAssetsTotal() {
		return otherAssetsTotal == null ? BigDecimal.ZERO : otherAssetsTotal;
	}

	@Override
	public BigDecimal getOwnUseAssetsTotal() {
		return ownUseAssetsTotal == null ? BigDecimal.ZERO : ownUseAssetsTotal;
	}

}

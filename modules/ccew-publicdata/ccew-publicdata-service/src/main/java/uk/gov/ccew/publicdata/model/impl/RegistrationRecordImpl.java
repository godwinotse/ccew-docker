package uk.gov.ccew.publicdata.model.impl;

import java.util.Date;
import java.util.Optional;

import com.liferay.portal.kernel.util.GetterUtil;

import uk.gov.ccew.publicdata.model.RegistrationRecord;

public class RegistrationRecordImpl implements RegistrationRecord {

    private Date registrationDate;
    private String registrationAction;

    @Override
    public Optional<Date> getRegistrationDate() {
        return Optional.ofNullable(registrationDate);
    }

    @Override
    public String getRegistrationAction() {
        return GetterUtil.getString(registrationAction);
    }

    @Override
    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Override
    public void setRegistrationAction(String registrationAction) {
        this.registrationAction = registrationAction;
    }
}
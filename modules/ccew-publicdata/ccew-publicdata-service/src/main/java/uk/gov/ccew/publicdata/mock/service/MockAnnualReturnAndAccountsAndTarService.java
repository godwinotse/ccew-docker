package uk.gov.ccew.publicdata.mock.service;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.liferay.portal.kernel.util.Validator;

import uk.gov.ccew.publicdata.internal.service.GsonService;
import uk.gov.ccew.publicdata.mock.constants.MockDataConstants;
import uk.gov.ccew.publicdata.model.AnnualReturnAndAccountsAndTar;
import uk.gov.ccew.publicdata.model.impl.AccountsAndTarImpl;
import uk.gov.ccew.publicdata.model.impl.AnnualReturnAndAccountsAndTarImpl;
import uk.gov.ccew.publicdata.model.impl.AnnualReturnImpl;
import uk.gov.ccew.publicdata.parser.AccountsAndTarTypeAdapterFactory;
import uk.gov.ccew.publicdata.parser.AnnualReturnAndAccountsAndTarTypeAdapterFactory;
import uk.gov.ccew.publicdata.parser.AnnualReturnTypeAdapterFactory;

@Component(immediate = true, service = MockAnnualReturnAndAccountsAndTarService.class)
public class MockAnnualReturnAndAccountsAndTarService {

	@Reference
	private GsonService gsonService;

	private MockAnnualReturnAndAccountsAndTarCollection mockAnnualReturnAndAccountsAndTarCollection;

	@Reference
	private MockAnnualReturnAndAccountsAndTarImport mockAnnualReturnAndAccountsAndTarImport;

	@Reference
	private MockResourceService mockResourceService;
	
	public List<AnnualReturnAndAccountsAndTar> getMockAnnualReturnsAndAccountsAndTars(long organisationNumber) {
		
		List<AnnualReturnAndAccountsAndTar> mockAnnualReturnAndAccountsAndTars = mockAnnualReturnAndAccountsAndTarCollection.getMockAnnualReturnAndAccountsAndTar(organisationNumber);

		if (Validator.isNotNull(mockAnnualReturnAndAccountsAndTars)) {
			return mockAnnualReturnAndAccountsAndTars;
		}
		
		InputStream inputStream = mockResourceService.getInputStream(MockDataConstants.MOCK_ACCOUNTS_AND_ANNUAL_RETURNS_RESOURCE_PATH, organisationNumber, MockDataConstants.DEFAULT_ORGANISATION_NUMBER);

		List<TypeAdapterFactory> typeAdapterFactories = new ArrayList<>();
		typeAdapterFactories.add(new AnnualReturnTypeAdapterFactory(AnnualReturnImpl.class));
		typeAdapterFactories.add(new AccountsAndTarTypeAdapterFactory(AccountsAndTarImpl.class));
		typeAdapterFactories.add(new AnnualReturnAndAccountsAndTarTypeAdapterFactory(AnnualReturnAndAccountsAndTarImpl.class));

		Type annualReturnAndAccountsAndTarListType = new TypeToken<ArrayList<AnnualReturnAndAccountsAndTar>>() {

		}.getType();

		return gsonService.fromJson(inputStream, annualReturnAndAccountsAndTarListType, typeAdapterFactories);

	}

	@Activate
	protected void activate() {

		mockAnnualReturnAndAccountsAndTarCollection = new MockAnnualReturnAndAccountsAndTarCollection();
		populateMockAnnualReturnAndAccountsAndTarCollection();

	}

	private void populateMockAnnualReturnAndAccountsAndTarCollection() {

		mockAnnualReturnAndAccountsAndTarCollection.addMockAnnualReturnAndAccountsAndTarCollection(mockAnnualReturnAndAccountsAndTarImport.getImportedAnnualReturnAndAccountsAndTar());

	}

}

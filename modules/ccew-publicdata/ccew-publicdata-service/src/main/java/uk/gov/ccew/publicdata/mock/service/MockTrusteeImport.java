package uk.gov.ccew.publicdata.mock.service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import uk.gov.ccew.publicdata.mock.constants.MockDataConstants;
import uk.gov.ccew.publicdata.model.Trustee;
import uk.gov.ccew.publicdata.model.impl.TrusteeImpl;

@Component(immediate = true, service = MockTrusteeImport.class)
public class MockTrusteeImport {
	
	private static final String FILE_BASE_FOLDER = "mock/trustee/";

	private static final String FILE_TRUSTEES = FILE_BASE_FOLDER + "trustees.csv";

	private static final Log LOG = LogFactoryUtil.getLog(MockTrusteeImport.class);

	private static Map<Long, List<Trustee>> trusteeImportRecords = new HashMap<>();

	@Activate
	protected void activate() {
		importMockDataFromFiles();
	}

	public Map<Long, List<Trustee>> getImportedTrustees() {
		return trusteeImportRecords;
	}

	private List<Trustee> getTrusteeAndAddToMap(Long organisationNumber) {

		List<Trustee> trustees = trusteeImportRecords.get(organisationNumber);
		if (trustees == null) {
			if (trusteeImportRecords.size() >= MockDataConstants.MAX_IMPORT_COUNT) {
				return null;
			}
			
			trustees = new ArrayList<>();
			trusteeImportRecords.put(organisationNumber, trustees);
		}

		return trustees;
	}

	private void importFile(String fileName) {

		LOG.info("Importing trustee mock data file:" + fileName);

		InputStream dataFile = MockCharityImport.class.getClassLoader().getResourceAsStream(fileName);

		try (Reader in = new InputStreamReader(dataFile)) {

			Iterable<CSVRecord> mockTrusteeRecords = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(in);
			for (CSVRecord mockTrusteeRecord : mockTrusteeRecords) {

				if (trusteeImportRecords.size() > MockDataConstants.MAX_IMPORT_COUNT) continue;
				
				String organisationNumber = mockTrusteeRecord.get(MockDataConstants.FIELD_ORGANISATION_NUMBER);
				List<Trustee> trustees = getTrusteeAndAddToMap(Long.parseLong(organisationNumber));
				
				if (trustees != null) {
					Trustee trustee = new TrusteeImpl();
					
					trustee.setName(mockTrusteeRecord.get(MockDataConstants.FIELD_VALUE));

					trustees.add(trustee);	
				}
				
			}

			LOG.info("Finished importing trustee mock data file:" + fileName);

		} catch (Exception e) {
			LOG.error("Error importing trustee mock data file." + fileName, e);
		}
	}

	private void importMockDataFromFiles() {

		importFile(MockTrusteeImport.FILE_TRUSTEES);
		
	}
	
}

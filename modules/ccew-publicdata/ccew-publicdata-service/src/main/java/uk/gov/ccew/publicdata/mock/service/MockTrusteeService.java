package uk.gov.ccew.publicdata.mock.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.TypeAdapterFactory;

import uk.gov.ccew.publicdata.internal.service.GsonService;
import uk.gov.ccew.publicdata.mock.constants.MockDataConstants;
import uk.gov.ccew.publicdata.model.Trustee;
import uk.gov.ccew.publicdata.model.impl.TrusteeImpl;
import uk.gov.ccew.publicdata.parser.TrusteeTypeAdapterFactory;

@Component(immediate = true, service = MockTrusteeService.class)
public class MockTrusteeService {

	@Reference
	private GsonService gsonService;

	private MockTrusteeCollection mockTrusteeCollection;

	@Reference
	private MockTrusteeImport mockTrusteeImport;

	@Reference
	private MockResourceService mockResourceService;
	
	public Trustee getMockTrustee(String trusteeName) {

		String resourcePath = String.format(MockDataConstants.MOCK_TRUSTEE_RESOURCE_PATH, trusteeName);
		InputStream inputStream = mockResourceService.getInputStream(resourcePath);
		List<TypeAdapterFactory> typeAdapterFactories = Collections.singletonList(new TrusteeTypeAdapterFactory(TrusteeImpl.class));

		return gsonService.fromJson(inputStream, TrusteeImpl.class, typeAdapterFactories);

	}
	
	public List<Trustee> getMockTrustees(long organisationNumber) {
		
		List<Trustee> trustees = new ArrayList<>();

		if (organisationNumber == 306025) {

			trustees.add(getMockTrustee("jill-osborn"));
			trustees.add(getMockTrustee("jack-obrian"));

		} else if (organisationNumber == 521584) {

			trustees.add(getMockTrustee("jill-osborn"));

		} else {

			List<Trustee> mockTrustees = mockTrusteeCollection.getMockTrustees(organisationNumber);
			trustees.addAll(mockTrustees);

		}

		return trustees;
		
	}

	@Activate
	protected void activate() {

		mockTrusteeCollection = new MockTrusteeCollection();
		populateMockTrusteeCollection();

	}

	private void populateMockTrusteeCollection() {

		mockTrusteeCollection.addMockTrustees(mockTrusteeImport.getImportedTrustees());

	}

}

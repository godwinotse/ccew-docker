package uk.gov.ccew.publicdata.internal.service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.stream.JsonReader;

@Component(immediate = true, service = GsonService.class)
public class GsonService {

	public <T> T fromJson(InputStream inputStream, Type typeOfT) {

		return fromJson(inputStream, typeOfT, Collections.emptyList());

	}

	public <T> T fromJson(InputStream inputStream, Type typeOfT, List<TypeAdapterFactory> typeAdapterFactories) {

		Reader inputReader = new InputStreamReader(inputStream);
		GsonBuilder gsonBuilder = new GsonBuilder();

		for (TypeAdapterFactory typeAdapterFactory : typeAdapterFactories) {
			gsonBuilder.registerTypeAdapterFactory(typeAdapterFactory);
		}

		Gson gson = gsonBuilder.create();
		JsonReader reader = new JsonReader(inputReader);

		return gson.fromJson(reader, typeOfT);

	}

}

package uk.gov.ccew.publicdata.model.impl;

import uk.gov.ccew.publicdata.model.EmployeesSummary;

import java.util.Collections;
import java.util.Map;

public class EmployeesSummaryImpl implements EmployeesSummary {

	private boolean employingFormerTrustee;

	private int noOfEmployees;

	private Map<String, Integer> noOfPeopleBySalaryRange;

	private int noOfVolunteers;

	@Override
	public int getNoOfEmployees() {
		return noOfEmployees;
	}

	@Override
	public Map<String, Integer> getNoOfPeopleBySalaryRange() {
		return noOfPeopleBySalaryRange == null ? Collections.emptyMap() : noOfPeopleBySalaryRange;
	}

	@Override
	public int getNoOfVolunteers() {
		return noOfVolunteers;
	}

	@Override
	public boolean isEmployingFormerTrustee() {
		return employingFormerTrustee;
	}

}

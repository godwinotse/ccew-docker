package uk.gov.ccew.publicdata.mock.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.osgi.service.component.annotations.Component;

import uk.gov.ccew.publicdata.model.Charity;

@Component(immediate = true, service = MockCharityCollection.class)
public class MockCharityCollection {
	
	private Map<Long, Charity> mockCharities = new HashMap<>();
	
	public int getMockCharityCount() {
		
		return mockCharities.size();
		
	}
	
	public boolean isEmpty() {
		
		return mockCharities.isEmpty();
		
	}
	
	public void addMockCharity(Charity mockCharity) {
		
		mockCharities.put(mockCharity.getOrganisationNumber(), mockCharity);
		
	}
	
	public void addMockCharities(Collection<Charity> mockCharityList) {
		
		for (Charity mockCharity : mockCharityList) {
			mockCharities.put(mockCharity.getOrganisationNumber(), mockCharity);	
		}
		
	}
	
	public Charity getMockCharity(long organisationNumber) {
		
		return mockCharities.get(organisationNumber);
		
	}
	
	public List<Charity> getMockCharities(int start, int end) {
		
		return new ArrayList<>(mockCharities.values()).subList(start, end);
		
	}

}

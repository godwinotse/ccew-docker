package uk.gov.ccew.publicdata.model.impl;

import uk.gov.ccew.publicdata.model.FundraisingSummary;

public class FundraisingSummaryImpl implements FundraisingSummary {

	private boolean commercialAndProfessionalAgreementInPlace;

	private boolean raisingFundsFromThePublic;

	private boolean workingWithCommercialParticipants;

	@Override
	public boolean isCommercialAndProfessionalAgreementInPlace() {
		return commercialAndProfessionalAgreementInPlace;
	}

	@Override
	public boolean isRaisingFundsFromThePublic() {
		return raisingFundsFromThePublic;
	}

	@Override
	public boolean isWorkingWithCommercialParticipants() {
		return workingWithCommercialParticipants;
	}

}

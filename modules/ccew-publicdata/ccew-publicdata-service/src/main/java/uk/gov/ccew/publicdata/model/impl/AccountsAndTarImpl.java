package uk.gov.ccew.publicdata.model.impl;

import java.util.Date;
import java.util.Optional;

import uk.gov.ccew.publicdata.model.AccountsAndTar;

public class AccountsAndTarImpl implements AccountsAndTar {

	private int daysOverdue;

	private Date receivedDate;

	private Date reportingYear;

	private boolean qualifiedAccounts;

	@Override
	public int getDaysOverdue() {
		return daysOverdue;
	}

	@Override
	public Optional<Date> getReceivedDate() {
		return Optional.ofNullable(receivedDate);
	}

	@Override
	public Optional<Date> getReportingYear() {
		return Optional.ofNullable(reportingYear);
	}

	@Override
	public boolean isQualifiedAccounts() {
		return qualifiedAccounts;
	}

}

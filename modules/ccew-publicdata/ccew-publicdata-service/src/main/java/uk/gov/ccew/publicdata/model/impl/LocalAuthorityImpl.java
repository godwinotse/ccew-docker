package uk.gov.ccew.publicdata.model.impl;

import com.liferay.portal.kernel.util.GetterUtil;

import uk.gov.ccew.publicdata.model.LocalAuthority;

public class LocalAuthorityImpl implements LocalAuthority {

	private boolean welsh;

	private String metropolitanCouncilName;

	private String localAuthorityName;

	@Override
	public String getMetropolitanCouncilName() {
		return GetterUtil.getString(metropolitanCouncilName);
	}

	@Override
	public String getLocalAuthorityName() {
		return GetterUtil.getString(localAuthorityName);
	}

	@Override
	public boolean isWelsh() {
		return welsh;
	}

	@Override
	public void setMetropolitanCouncilName(String metropolitanCouncilName) {
		this.metropolitanCouncilName = metropolitanCouncilName;
	}

	@Override
	public void setLocalAuthorityName(String localAuthorityName) {
		this.localAuthorityName = localAuthorityName;
	}

	@Override
	public void setWelsh(boolean welsh) {
		this.welsh = welsh;
	}

}

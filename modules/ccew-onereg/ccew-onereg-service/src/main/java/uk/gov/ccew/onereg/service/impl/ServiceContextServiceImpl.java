package uk.gov.ccew.onereg.service.impl;

import uk.gov.ccew.onereg.service.ServiceContextService;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.service.ServiceContext;

@Component(immediate = true, service = ServiceContextService.class)
public class ServiceContextServiceImpl implements ServiceContextService {

	@Override
	public ServiceContext getServiceContext(long companyId) {

		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setCompanyId(companyId);
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(true);

		return serviceContext;

	}

	@Override
	public ServiceContext getServiceContext(long companyId, long groupId, long userId, Locale locale) {

		ServiceContext serviceContext = getServiceContext(companyId);
		serviceContext.setScopeGroupId(groupId);
		serviceContext.setUserId(userId);
		serviceContext.setLanguageId(locale.getLanguage());

		return serviceContext;

	}

}

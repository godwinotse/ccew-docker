package uk.gov.ccew.onereg.upgrade;

import uk.gov.ccew.onereg.service.OneRegGroupService;
import uk.gov.ccew.onereg.service.OneRegWebContentService;
import uk.gov.ccew.onereg.service.ServiceContextService;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.Portal;

@Component(immediate = true, service = UpgradeContext.class)
public class UpgradeContext {

	@Reference
	private OneRegGroupService oneRegGroupService;

	@Reference
	private OneRegWebContentService oneRegWebContentService;

	@Reference
	private Portal portal;

	@Reference
	private ServiceContextService serviceContextService;

	@Reference
	private UserLocalService userLocalService;

	public OneRegGroupService getOneRegGroupService() {
		return oneRegGroupService;
	}

	public OneRegWebContentService getOneRegWebContentService() {
		return oneRegWebContentService;
	}

	public Portal getPortal() {
		return portal;
	}

	public ServiceContextService getServiceContextService() {
		return serviceContextService;
	}

	public UserLocalService getUserLocalService() {
		return userLocalService;
	}

}

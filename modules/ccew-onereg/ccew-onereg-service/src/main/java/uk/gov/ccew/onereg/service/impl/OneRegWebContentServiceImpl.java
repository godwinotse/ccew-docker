package uk.gov.ccew.onereg.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleConstants;
import com.liferay.journal.model.JournalFolderConstants;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.journal.service.JournalFolderLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;

import uk.gov.ccew.onereg.constant.OneRegWebContent;
import uk.gov.ccew.onereg.service.OneRegLocaleService;
import uk.gov.ccew.onereg.service.OneRegWebContentService;

@Component(immediate = true, service = OneRegWebContentService.class)
public class OneRegWebContentServiceImpl implements OneRegWebContentService {

	public static final String BASIC_WEB_CONTENT = "BASIC-WEB-CONTENT";
	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Reference
	private JournalFolderLocalService journalFolderLocalService;

	@Reference
	private OneRegLocaleService oneRegLocaleService;

	public JournalArticle addBasicWebContent(OneRegWebContent oneRegWebContent, ServiceContext serviceContext) throws PortalException {

		try {

			String articleId = oneRegWebContent.getArticleId();
			String articleContent = StringUtil.read(getClass().getClassLoader(), "uk/gov/ccew/onereg/service/dependencies/webcontent/" + oneRegWebContent.getArticleId() + ".xml");
			String articleUrl = StringPool.BLANK;
			long classPK = 0;
			List<Locale> supportedLocales = oneRegLocaleService.getSupportedLocales();
			Map<Locale, String> titleMap = oneRegLocaleService.getLocaleMap(supportedLocales, oneRegWebContent.getTitle());
			Map<Locale, String> descriptionMap = null;
			String ddmStructureKey = BASIC_WEB_CONTENT;
			String ddmTemplateKey = BASIC_WEB_CONTENT;
			String layoutUuid = null;
			Calendar now = Calendar.getInstance();
			int displayDateMonth = now.get(Calendar.MONTH);
			int displayDateDay = now.get(Calendar.DAY_OF_MONTH);
			int displayDateYear = now.get(Calendar.YEAR);
			int displayDateHour = 0;
			int displayDateMinute = 0;
			int expirationDateMonth = 0;
			int expirationDateDay = 0;
			int expirationDateYear = 0;
			int expirationDateHour = 0;
			int expirationDateMinute = 0;
			boolean neverExpire = true;
			int reviewDateDay = 0;
			int reviewDateYear = 0;
			int reviewDateMonth = 0;
			int reviewDateHour = 0;
			int reviewDateMinute = 0;
			boolean neverReview = true;
			boolean indexable = true;
			boolean smallImage = false;
			String smallImageURL = null;
			File smallImageFile = null;
			Map<String, byte[]> images = null;

			return journalArticleLocalService
					.addArticle(serviceContext.getUserId(), serviceContext.getScopeGroupId(), JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID, JournalArticleConstants.CLASSNAME_ID_DEFAULT, classPK,
							articleId, Validator.isNull(articleId), JournalArticleConstants.VERSION_DEFAULT, titleMap, descriptionMap, articleContent, ddmStructureKey, ddmTemplateKey, layoutUuid,
							displayDateMonth, displayDateDay, displayDateYear, displayDateHour, displayDateMinute, expirationDateMonth, expirationDateDay, expirationDateYear, expirationDateHour,
							expirationDateMinute, neverExpire, reviewDateMonth, reviewDateDay, reviewDateYear, reviewDateHour, reviewDateMinute, neverReview, indexable, smallImage, smallImageURL,
							smallImageFile, images, articleUrl, serviceContext);

		} catch (IOException e) {

			throw new PortalException(e);

		}

	}

}

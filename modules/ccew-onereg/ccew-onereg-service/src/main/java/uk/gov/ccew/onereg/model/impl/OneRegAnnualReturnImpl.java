package uk.gov.ccew.onereg.model.impl;

import java.util.Date;
import java.util.Optional;

import uk.gov.ccew.onereg.constant.DateFormatter;
import uk.gov.ccew.onereg.model.OneRegAnnualReturn;
import uk.gov.ccew.publicdata.model.AnnualReturn;

public class OneRegAnnualReturnImpl implements OneRegAnnualReturn {

	private final AnnualReturn annualReturn;

	public OneRegAnnualReturnImpl(AnnualReturn annualReturn) {
		this.annualReturn = annualReturn;
	}

	@Override
	public int getDaysOverdue() {
		return annualReturn.getDaysOverdue();
	}

	@Override
	public Optional<Date> getReceivedDate() {
		return annualReturn.getReceivedDate();
	}

	@Override
	public String getReceivedDateFormatted() {
		return DateFormatter.format_dd_MMMM_YYYY(annualReturn.getReceivedDate());
	}

	@Override
	public Optional<Date> getReportingYear() {
		return annualReturn.getReportingYear();
	}

	@Override
	public String getReportingYearFormatted() {
		return DateFormatter.format_dd_MMMM_YYYY(annualReturn.getReportingYear());
	}

	@Override
	public boolean isPrimaryPurposeGrantGiving() {
		return annualReturn.isPrimaryPurposeGrantGiving();
	}

	@Override
	public void setPrimaryPurposeGrantGiving(boolean primaryPurposeGrantGiving) {
		annualReturn.setPrimaryPurposeGrantGiving(primaryPurposeGrantGiving);		
	}

}

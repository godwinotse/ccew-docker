package uk.gov.ccew.onereg.model.impl;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.GetterUtil;

import uk.gov.ccew.onereg.model.ChartData;
import uk.gov.ccew.onereg.model.ChartDataSet;

public class ChartDataSetImpl implements ChartDataSet {

	private String label;

	private boolean stacked;

	private int stackId;

	private List<ChartData> chartData;

	public ChartDataSetImpl(List<ChartData> chartData) {
		this.label = StringPool.BLANK;
		this.chartData = chartData;
	}

	public ChartDataSetImpl(String label, List<ChartData> chartData) {
		this.label = label;
		this.chartData = chartData;
	}

	@Override
	public List<ChartData> getChartData() {
		return chartData == null ? Collections.emptyList() : chartData;
	}

	@Override
	public String getJs() {

		String dataCsv = chartData.stream().map(ChartData::getValue).collect(Collectors.joining("','"));
		String colourCsv = chartData.stream().map(ChartData::getColour).collect(Collectors.joining("','"));

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(StringPool.OPEN_CURLY_BRACE);

		if (!label.isEmpty()) {
			stringBuilder.append(String.format("label: '%s',", label));
		}

		if (stacked) {
			stringBuilder.append(String.format("stack: 'Stack %s',", stackId));
		}

		stringBuilder.append(String.format("backgroundColor: ['%s'],", colourCsv));
		stringBuilder.append(String.format("data: ['%s']", dataCsv));
		stringBuilder.append(StringPool.CLOSE_CURLY_BRACE);

		return stringBuilder.toString();

	}

	@Override
	public String getLabel() {
		return GetterUtil.getString(label);
	}

	@Override
	public int getStackId() {
		return stackId;
	}

	@Override
	public boolean isStacked() {
		return stacked;
	}

	@Override
	public void setStacked(boolean stacked) {
		this.stacked = stacked;
	}

	@Override
	public void setStackId(int stackId) {
		this.stackId = stackId;
	}

}


package uk.gov.ccew.onereg.model.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.liferay.petra.string.StringPool;

import uk.gov.ccew.onereg.model.Chart;
import uk.gov.ccew.onereg.model.ChartDataSet;

public class ChartImpl implements Chart {

	private final Collection<String> labels;

	private final List<ChartDataSet> dataSets;

	public ChartImpl(List<ChartDataSet> dataSets) {

		this.labels = Collections.emptyList();
		this.dataSets = dataSets;

	}

	public ChartImpl(Collection<String> labels, List<ChartDataSet> dataSets) {

		this.labels = labels;
		this.dataSets = dataSets;

	}

	@Override
	public List<ChartDataSet> getDataSets() {
		return dataSets;
	}

	@Override
	public String getDataJs() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(StringPool.OPEN_CURLY_BRACE);

		if (labels.size() > 0) {

			String labelsCsv = String.join("','", labels);
			stringBuilder.append(String.format("labels: ['%s'],", labelsCsv));

		}

		String dataSetsCsv = dataSets.stream().map(ChartDataSet::getJs).collect(Collectors.joining(StringPool.COMMA));
		stringBuilder.append(String.format("datasets: [%s]", dataSetsCsv));

		stringBuilder.append(StringPool.CLOSE_CURLY_BRACE);

		return stringBuilder.toString();

	}

	@Override
	public Collection<String> getLabels() {
		return labels;
	}

}

package uk.gov.ccew.onereg.model.impl;

import java.util.Date;
import java.util.Optional;

import uk.gov.ccew.onereg.constant.DateFormatter;
import uk.gov.ccew.onereg.model.OneRegRegulatoryAlert;
import uk.gov.ccew.publicdata.model.RegulatoryAlert;
import uk.gov.ccew.publicdata.model.RegulatoryAlertIncidentType;

public class OneRegRegulatoryAlertImpl implements OneRegRegulatoryAlert {

	private RegulatoryAlert regulatoryAlert;

	public OneRegRegulatoryAlertImpl(RegulatoryAlert regulatoryAlert) {
		this.regulatoryAlert = regulatoryAlert;
	}

	@Override
	public Optional<Date> getIncidentDate() {
		return regulatoryAlert.getIncidentDate();
	}

	@Override
	public String getIncidentDateFormatted() {
		return DateFormatter.format_dd_MMMM_YYYY(regulatoryAlert.getIncidentDate());
	}

	@Override
	public Optional<RegulatoryAlertIncidentType> getIncidentType() {
		return regulatoryAlert.getIncidentType();
	}

	@Override
	public String getIncidentUserName() {
		return regulatoryAlert.getIncidentUserName();
	}

	@Override
	public Object[] getMessageArguments() {

		Object[] messageArguments = new Object[3];
		messageArguments[0] = getUrl();
		messageArguments[1] = getIncidentDateFormatted();
		messageArguments[2] = getIncidentUserName();

		return messageArguments;

	}

	@Override
	public String getUrl() {
		return regulatoryAlert.getUrl();
	}

}

package uk.gov.ccew.onereg.service.impl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;

import uk.gov.ccew.onereg.configuration.ChartConfiguration;
import uk.gov.ccew.onereg.service.ChartConfigurationUtil;

@Component(immediate = true, configurationPid = "uk.gov.ccew.onereg.configuration.ChartConfiguration", configurationPolicy = ConfigurationPolicy.OPTIONAL, service = ChartConfigurationUtil.class)
public class ChartConfigurationUtilImpl implements ChartConfigurationUtil {

	private volatile ChartConfiguration chartConfiguration;

	@Override
	public Map<String, String> getColourNamesAndValues() {
		return Stream.of(chartConfiguration.coloursNamesAndValues())
				.collect(Collectors.toMap(key -> key.split(StringPool.FORWARD_SLASH)[0], value -> value.split(StringPool.FORWARD_SLASH)[1], (e1, e2) -> e1, LinkedHashMap::new));
	}

	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {

		chartConfiguration = ConfigurableUtil.createConfigurable(ChartConfiguration.class, properties);

		if (chartConfiguration == null) {
			throw new IllegalStateException("Chart configuration missing - PID: uk.gov.ccew.onereg.configuration.ChartConfiguration");
		}

	}

}

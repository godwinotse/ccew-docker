package uk.gov.ccew.onereg.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;

import uk.gov.ccew.onereg.model.Chart;
import uk.gov.ccew.onereg.model.ChartDataSet;
import uk.gov.ccew.onereg.model.OneRegEmployeesSummary;
import uk.gov.ccew.onereg.model.Table;
import uk.gov.ccew.onereg.model.TableCell;
import uk.gov.ccew.onereg.model.TableRow;
import uk.gov.ccew.onereg.model.impl.OneRegEmployeesSummaryImpl;
import uk.gov.ccew.onereg.model.impl.TableCellImpl;
import uk.gov.ccew.onereg.model.impl.TableImpl;
import uk.gov.ccew.onereg.model.impl.TableRowImpl;
import uk.gov.ccew.onereg.service.ChartConfigurationUtil;
import uk.gov.ccew.onereg.service.ChartService;
import uk.gov.ccew.onereg.service.OneRegEmployeesSummaryService;
import uk.gov.ccew.publicdata.model.EmployeesSummary;
import uk.gov.ccew.publicdata.service.EmployeesSummaryService;

@Component(immediate = true, service = OneRegEmployeesSummaryService.class)
public class OneRegEmployeesSummaryServiceImpl implements OneRegEmployeesSummaryService {

	@Reference
	private ChartConfigurationUtil chartConfigurationUtil;

	@Reference
	private ChartService chartService;

	@Reference
	private EmployeesSummaryService employeesSummaryService;

	@Override
	public OneRegEmployeesSummary getOneRegEmployeesSummary(long organisationNumber) {

		EmployeesSummary employeesSummary = employeesSummaryService.getEmployeesSummary(organisationNumber);

		return new OneRegEmployeesSummaryImpl(employeesSummary);
	}

	@Override
	public Chart getNoOfPeopleBySalaryChart(OneRegEmployeesSummary employeesSummary) {

		Map<String, String> colourNamesAndValues = chartConfigurationUtil.getColourNamesAndValues();
		String colourValue = colourNamesAndValues.get("blue");

		Map<String, Integer> noOfPeopleBySalaryRange = employeesSummary.getNoOfPeopleBySalaryRange();
		List<String> chartLabels = new ArrayList<>();
		List<String> chartValues = new ArrayList<>();
		List<String> salaryRanges = new ArrayList<>(noOfPeopleBySalaryRange.keySet());

		Collections.reverse(salaryRanges);

		for (String salaryRange : salaryRanges) {

			Integer noOfPeople = noOfPeopleBySalaryRange.get(salaryRange);

			chartLabels.add(salaryRange);
			chartValues.add(String.valueOf(noOfPeople));

		}

		ChartDataSet chartDataSet = chartService.getChartDataSet(StringPool.BLANK, chartValues, colourValue);

		return chartService.getChart(chartLabels, Collections.singletonList(chartDataSet));

	}

	@Override
	public Table getNoOfPeopleBySalaryTable(OneRegEmployeesSummary employeesSummary) {

		List<TableCell> headerCells = new ArrayList<>();
		headerCells.add(new TableCellImpl(StringPool.BLANK));

		List<TableCell> bodyCells = new ArrayList<>();
		bodyCells.add(new TableCellImpl("Number of staff"));

		Map<String, Integer> noOfPeopleBySalaryRange = employeesSummary.getNoOfPeopleBySalaryRange();

		for (Map.Entry<String, Integer> salaryRangeAndNoOfPeople : noOfPeopleBySalaryRange.entrySet()) {

			String salaryRange = salaryRangeAndNoOfPeople.getKey();
			Integer noOfPeople = salaryRangeAndNoOfPeople.getValue();

			headerCells.add(new TableCellImpl(salaryRange, true));
			bodyCells.add(new TableCellImpl(String.valueOf(noOfPeople), true));

		}

		TableRow headerRow = new TableRowImpl(headerCells);
		TableRow bodyRow = new TableRowImpl(bodyCells);

		return new TableImpl(headerRow, Collections.singletonList(bodyRow));

	}

}

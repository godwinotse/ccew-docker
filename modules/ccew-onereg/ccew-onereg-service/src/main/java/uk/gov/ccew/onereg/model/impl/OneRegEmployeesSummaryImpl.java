package uk.gov.ccew.onereg.model.impl;

import java.util.Map;

import uk.gov.ccew.onereg.model.OneRegEmployeesSummary;
import uk.gov.ccew.publicdata.model.EmployeesSummary;

public class OneRegEmployeesSummaryImpl implements OneRegEmployeesSummary {

	private EmployeesSummary employeesSummary;

	public OneRegEmployeesSummaryImpl(EmployeesSummary employeesSummary) {
		this.employeesSummary = employeesSummary;
	}

	@Override
	public int getNoOfEmployees() {
		return employeesSummary.getNoOfEmployees();
	}

	@Override
	public Map<String, Integer> getNoOfPeopleBySalaryRange() {
		return employeesSummary.getNoOfPeopleBySalaryRange();
	}

	@Override
	public int getNoOfVolunteers() {
		return employeesSummary.getNoOfVolunteers();
	}

	@Override
	public boolean isEmployingFormerTrustee() {
		return employeesSummary.isEmployingFormerTrustee();
	}

}

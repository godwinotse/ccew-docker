package uk.gov.ccew.onereg.service.impl;

import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;

import uk.gov.ccew.onereg.configuration.ExternalSiteUrlsConfiguration;
import uk.gov.ccew.onereg.service.ExternalSiteUrlsConfigurationUtil;

@Component(immediate = true, configurationPid = "uk.gov.ccew.onereg.configuration.ExternalSiteUrlsConfiguration", configurationPolicy = ConfigurationPolicy.OPTIONAL, service = ExternalSiteUrlsConfigurationUtil.class)
public class ExternalSiteUrlsConfigurationUtilImpl implements ExternalSiteUrlsConfigurationUtil {

	private volatile ExternalSiteUrlsConfiguration externalSiteUrlsConfiguration;

	@Override
	public String getCharityLoginUrl() {
		return externalSiteUrlsConfiguration.charityLoginUrl();
	}

	@Override
	public String getCompanyHouseBaseUrl() {
		return externalSiteUrlsConfiguration.companyHouseBaseUrl();
	}

	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {

		externalSiteUrlsConfiguration = ConfigurableUtil.createConfigurable(ExternalSiteUrlsConfiguration.class, properties);

		if (externalSiteUrlsConfiguration == null) {
			throw new IllegalStateException("External site URLs configuration missing - PID: uk.gov.ccew.onereg.configuration.ExternalSiteUrlsConfiguration");
		}

	}

}

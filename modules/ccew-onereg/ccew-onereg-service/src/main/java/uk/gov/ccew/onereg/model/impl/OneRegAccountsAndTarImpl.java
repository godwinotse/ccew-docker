package uk.gov.ccew.onereg.model.impl;

import java.util.Date;
import java.util.Optional;

import uk.gov.ccew.onereg.constant.DateFormatter;
import uk.gov.ccew.onereg.model.OneRegAccountsAndTar;
import uk.gov.ccew.publicdata.model.AccountsAndTar;

public class OneRegAccountsAndTarImpl implements OneRegAccountsAndTar {

	private final AccountsAndTar accountsAndTar;

	public OneRegAccountsAndTarImpl(AccountsAndTar accountsAndTar) {
		this.accountsAndTar = accountsAndTar;
	}

	@Override
	public int getDaysOverdue() {
		return accountsAndTar.getDaysOverdue();
	}

	@Override
	public Optional<Date> getReceivedDate() {
		return accountsAndTar.getReceivedDate();
	}

	@Override
	public String getReceivedDateFormatted() {
		return DateFormatter.format_dd_MMMM_YYYY(accountsAndTar.getReceivedDate());
	}

	@Override
	public Optional<Date> getReportingYear() {
		return accountsAndTar.getReportingYear();
	}

	@Override
	public String getReportingYearFormatted() {
		return DateFormatter.format_dd_MMMM_YYYY(accountsAndTar.getReportingYear());
	}

	@Override
	public boolean isQualifiedAccounts() {
		return accountsAndTar.isQualifiedAccounts();
	}

}

package uk.gov.ccew.onereg.model.impl;

import java.util.List;
import java.util.Optional;

import uk.gov.ccew.onereg.model.Table;
import uk.gov.ccew.onereg.model.TableRow;

public class TableImpl implements Table {

	private final TableRow headerRow;

	private final List<TableRow> bodyRows;

	public TableImpl(List<TableRow> bodyRows) {

		this.headerRow = null;
		this.bodyRows = bodyRows;

	}

	public TableImpl(TableRow headerRow, List<TableRow> bodyRows) {

		this.headerRow = headerRow;
		this.bodyRows = bodyRows;

	}

	@Override
	public List<TableRow> getBodyRows() {
		return bodyRows;
	}

	@Override
	public Optional<TableRow> getHeaderRow() {
		return Optional.ofNullable(headerRow);
	}

}

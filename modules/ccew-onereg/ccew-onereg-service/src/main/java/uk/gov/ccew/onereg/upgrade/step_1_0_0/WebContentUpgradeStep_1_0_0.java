package uk.gov.ccew.onereg.upgrade.step_1_0_0;

import uk.gov.ccew.onereg.constant.OneRegWebContent;
import uk.gov.ccew.onereg.service.OneRegGroupService;
import uk.gov.ccew.onereg.service.OneRegWebContentService;
import uk.gov.ccew.onereg.service.ServiceContextService;
import uk.gov.ccew.onereg.upgrade.UpgradeContext;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.Portal;

public class WebContentUpgradeStep_1_0_0 extends UpgradeProcess {

	private final OneRegGroupService oneRegGroupService;

	private final OneRegWebContentService oneRegWebContentService;

	private final Portal portal;

	private final ServiceContextService serviceContextService;

	private final UserLocalService userLocalService;

	public WebContentUpgradeStep_1_0_0(UpgradeContext upgradeContext) {

		this.oneRegGroupService = upgradeContext.getOneRegGroupService();
		this.oneRegWebContentService = upgradeContext.getOneRegWebContentService();
		this.portal = upgradeContext.getPortal();
		this.serviceContextService = upgradeContext.getServiceContextService();
		this.userLocalService = upgradeContext.getUserLocalService();

	}

	@Override
	protected void doUpgrade() throws Exception {

		Group oneRegGroup = oneRegGroupService.getOneRegGroup();
		long companyId = portal.getDefaultCompanyId();
		User user = userLocalService.getDefaultUser(companyId);
		ServiceContext serviceContext = serviceContextService.getServiceContext(companyId, oneRegGroup.getGroupId(), user.getUserId(), LocaleUtil.getDefault());

		oneRegWebContentService.addBasicWebContent(OneRegWebContent.TRUSTEES_INTRO, serviceContext);

	}

}

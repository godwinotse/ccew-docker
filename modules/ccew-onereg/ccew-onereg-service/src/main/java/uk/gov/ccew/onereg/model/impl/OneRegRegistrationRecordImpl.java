package uk.gov.ccew.onereg.model.impl;

import java.util.Date;
import java.util.Optional;

import uk.gov.ccew.onereg.constant.DateFormatter;
import uk.gov.ccew.onereg.model.OneRegRegistrationRecord;
import uk.gov.ccew.publicdata.model.RegistrationRecord;

public class OneRegRegistrationRecordImpl implements OneRegRegistrationRecord {

    private RegistrationRecord record;

    public OneRegRegistrationRecordImpl(RegistrationRecord record) {
        this.record = record;
    }

    @Override
    public Optional<Date> getRegistrationDate() {
        return record.getRegistrationDate();
    }

    @Override
    public String getRegistrationAction() {
        return record.getRegistrationAction();
    }

    @Override
    public void setRegistrationDate(Date registrationDate) {
        record.setRegistrationDate(registrationDate);
    }

    @Override
    public void setRegistrationAction(String registrationAction) {
       record.setRegistrationAction(registrationAction);
    }

    @Override
    public String getRegistrationDateFormatted() {
        return DateFormatter.format_dd_MMMM_YYYY(this.getRegistrationDate());
    }
    
}
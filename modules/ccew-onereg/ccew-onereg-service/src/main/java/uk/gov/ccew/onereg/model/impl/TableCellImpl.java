package uk.gov.ccew.onereg.model.impl;

import com.liferay.petra.string.StringPool;

import uk.gov.ccew.onereg.model.TableCell;
import uk.gov.ccew.onereg.model.TableCellType;

public class TableCellImpl implements TableCell {

	private final boolean numeric;

	private final TableCellType type;

	private final String value;

	public TableCellImpl() {
		this.numeric = false;
		this.type = TableCellType.DATA;
		this.value = StringPool.BLANK;
	}

	public TableCellImpl(String value) {
		this.numeric = false;
		this.type = TableCellType.DATA;
		this.value = value;
	}

	public TableCellImpl(String value, boolean numeric) {
		this.numeric = numeric;
		this.type = TableCellType.DATA;;
		this.value = value;
	}

	public TableCellImpl(String value, TableCellType type) {
		this.numeric = false;
		this.type = type;
		this.value = value;
	}

	public TableCellImpl(String value, TableCellType type, boolean numeric) {
		this.numeric = numeric;
		this.type = type;
		this.value = value;
	}

	@Override
	public TableCellType getType() {
		return type;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public boolean isNumeric() {
		return numeric;
	}

}

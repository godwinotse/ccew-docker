package uk.gov.ccew.onereg.model.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.Validator;

import uk.gov.ccew.onereg.constant.DateFormatter;
import uk.gov.ccew.onereg.constant.NumberFormatter;
import uk.gov.ccew.onereg.model.OneRegCharity;
import uk.gov.ccew.onereg.model.OneRegCharityAnnualReport;
import uk.gov.ccew.onereg.model.OneRegRegistrationRecord;
import uk.gov.ccew.onereg.model.OneRegRegulatoryAlert;
import uk.gov.ccew.publicdata.model.AreaOfOperation;
import uk.gov.ccew.publicdata.model.Charity;
import uk.gov.ccew.publicdata.model.CharityAnnualReport;
import uk.gov.ccew.publicdata.model.CharityStatus;
import uk.gov.ccew.publicdata.model.GiftAidStatus;
import uk.gov.ccew.publicdata.model.RegistrationRecord;
import uk.gov.ccew.publicdata.model.RegulatoryAlert;

public class OneRegCharityImpl implements OneRegCharity {

	private Charity charity;

	private String displayUrl;

	private List<OneRegRegulatoryAlert> oneRegRegulatoryAlerts;

	private List<OneRegRegistrationRecord> oneRegRegistrationHistory;

	private OneRegCharityAnnualReport oneRegCharityAnnualReport;

	public OneRegCharityImpl(Charity charity) {

		this.charity = charity;
		this.displayUrl = StringPool.BLANK;

	}

	@Override
	public String getActivities() {
		return charity.getActivities();
	}

	@Override
	public String getAddress() {
		return charity.getAddress();
	}
	
	@Override
	public Optional<AreaOfOperation> getAreaOfOperation() {
		return charity.getAreaOfOperation();
	}

	@Override
	public String getAreaOfBenefit() {
		return charity.getAreaOfBenefit();
	}

	@Override
	public Optional<CharityAnnualReport> getCharityAnnualReport() {
		return charity.getCharityAnnualReport();
	}

	@Override
	public String getCharityName() {
		return charity.getCharityName();
	}

	@Override
	public String getCharityType() {
		return charity.getCharityType();
	}

	@Override
	public Optional<CharityStatus> getCharityStatus() {
		return charity.getCharityStatus();
	}

	@Override
	public String getDisplayUrl() {
		return displayUrl;
	}

	@Override
	public void setDisplayUrl(String displayUrl) {
		this.displayUrl = displayUrl;
	}

	@Override
	public Optional<OneRegCharityAnnualReport> getOneRegCharityAnnualReport() {
		return Optional.ofNullable(oneRegCharityAnnualReport);
	}

	@Override
	public void setOneRegCharityAnnualReport(OneRegCharityAnnualReport oneRegCharityAnnualReport) {
		this.oneRegCharityAnnualReport = oneRegCharityAnnualReport;
	}

	@Override
	public List<OneRegRegulatoryAlert> getOneRegRegulatoryAlerts() {
		return oneRegRegulatoryAlerts == null ? Collections.emptyList() : oneRegRegulatoryAlerts;
	}

	@Override
	public void setOneRegRegulatoryAlerts(List<OneRegRegulatoryAlert> oneRegRegulatoryAlerts) {
		this.oneRegRegulatoryAlerts = oneRegRegulatoryAlerts;
	}

	@Override
	public List<String> getOtherCharityNames() {
		return charity.getOtherCharityNames();
	}

	@Override
	public long getCharityNumber() {
		return charity.getCharityNumber();
	}

	@Override
	public String getCharitableObjects() {
		return charity.getCharitableObjects();
	}

	@Override
	public String getCompanyNumber() {
		return charity.getCompanyNumber();
	}

	@Override
	public String getEmail() {
		return charity.getEmail();
	}

	@Override
	public Optional<GiftAidStatus> getGiftAidStatus() {
		return charity.getGiftAidStatus();
	}

	@Override
	public String getGoverningDocument() {
		return charity.getGoverningDocument();
	}

	@Override
	public List<String> getHowTheCharityHelps() {
		return charity.getHowTheCharityHelps();
	}

	@Override
	public List<String> getLocations() {
		return charity.getLocations();
	}

	@Override
	public List<String> getOtherRegulators() {
		return charity.getOtherRegulators();
	}

	@Override
	public String getPhone() {
		return charity.getPhone();
	}

	@Override
	public List<String> getPolicies() {
		return charity.getPolicies();
	}

	@Override
	public Optional<Date> getRegistrationDate() {
		return charity.getRegistrationDate();
	}
	
	@Override
	public Optional<Date> getRemovedDate() {
		return charity.getRemovedDate();
	}

	@Override
	public String getRegistrationDateFormatted() {
		return DateFormatter.format_dd_MMMM_YYYY(charity.getRegistrationDate());
	}

	@Override
	public List<RegulatoryAlert> getRegulatoryAlerts() {
		return charity.getRegulatoryAlerts();
	}

	@Override
	public String getWebsite() {
		return charity.getWebsite();
	}

	@Override
	public List<String> getWhatTheCharityDoes() {
		return charity.getWhatTheCharityDoes();
	}

	@Override
	public List<String> getWhoTheCharityHelps() {
		return charity.getWhoTheCharityHelps();
	}

	@Override
	public boolean isTradingSubsidiaries() {
		return charity.isTradingSubsidiaries();
	}

	@Override
	public boolean isGrantMaking() {
		return charity.isGrantMaking();
	}

	@Override
	public boolean isLandAndProperty() {
		return charity.isLandAndProperty();
	}

	@Override
	public BigDecimal getIncome() {
		return charity.getIncome();
	}

	@Override
	public long getOrganisationNumber() {
		return charity.getOrganisationNumber();
	}

	@Override
	public int getSubsidiarySuffix() {
		return charity.getSubsidiarySuffix();
	}

	@Override
	public String getIncomeFormatted() {
		if (Validator.isNotNull(charity.getIncome())) {
			return NumberFormatter.formatUkCurrencyShort(charity.getIncome());
		} else {
			return StringPool.BLANK;
		}
	}

	@Override
	public String getCharityNumberFormatted() {
		if (charity.getSubsidiarySuffix() > 0) {
			return charity.getCharityNumber() + StringPool.DASH + charity.getSubsidiarySuffix();
		} else {
			return String.valueOf((charity.getCharityNumber()));
		}
	}

	@Override
	public BigDecimal getExpenditure() {
		return charity.getExpenditure();
	}

	@Override
	public void setIncome(BigDecimal income) {
		charity.setIncome(income);
	}
	
	@Override
	public void setAreaOfOperation(AreaOfOperation areaOfOperation) {
		charity.setAreaOfOperation(areaOfOperation);
	}

	@Override
	public void setOrganisationNumber(long organisationNumber) {
		charity.setOrganisationNumber(organisationNumber);
	}

	@Override
	public void setRegistrationDate(Date registrationDate) {
		charity.setRegistrationDate(registrationDate);
	}

	@Override
	public void setRemovedDate(Date removedDate) {
		charity.setRemovedDate(removedDate);
	}

	@Override
	public void setSubsidiarySuffix(int setSubsidiarySuffix) {
		charity.setSubsidiarySuffix(setSubsidiarySuffix);
	}

	@Override
	public void setHowTheCharityHelps(List<String> howTheCharityHelps) {
		charity.setHowTheCharityHelps(howTheCharityHelps);
	}

	@Override
	public void setWhatTheCharityDoes(List<String> whatTheCharityDoes) {
		charity.setWhatTheCharityDoes(whatTheCharityDoes);
	}

	@Override
	public void setWhoTheCharityHelps(List<String> whoTheCharityHelps) {
		charity.setWhoTheCharityHelps(whoTheCharityHelps);
	}

	@Override
	public void setCharityName(String charityName) {
		charity.setCharityName(charityName);
	}

	@Override
	public void setCharityNumber(long charityNumber) {
		charity.setCharityNumber(charityNumber);
	}

	@Override
	public void setCharityStatus(CharityStatus charityStatus) {
		charity.setCharityStatus(charityStatus);
	}

	public void setExpenditure(BigDecimal expenditure) {
		this.setExpenditure(expenditure);
	}

	@Override
	public String getOrganisationType() {
		return charity.getOrganisationType();
	}

	@Override
	public void setOrganisationType(String organisationType) {
		charity.setOrganisationType(organisationType);
	}

	@Override
	public List<RegistrationRecord> getRegistrationHistory() {
		return charity.getRegistrationHistory();
	}

	@Override
	public List<OneRegRegistrationRecord> getOneRegRegistrationHistory() {
		return oneRegRegistrationHistory == null ? Collections.emptyList() : oneRegRegistrationHistory;
	}

	@Override
	public void setOneRegRegistrationHistory(List<OneRegRegistrationRecord> oneRegRegistrationHistory) {
		this.oneRegRegistrationHistory = oneRegRegistrationHistory;
	}

	@Override
	public void setRegistrationHistory(List<RegistrationRecord> registrationHistory) {
		charity.setRegistrationHistory(registrationHistory);
	}

}

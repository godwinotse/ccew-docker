package uk.gov.ccew.onereg.model.impl;

import uk.gov.ccew.onereg.model.ChartData;

public class ChartDataImpl implements ChartData {

	private final String colour;

	private final String value;

	public ChartDataImpl(String value, String colour) {
		this.value = value;
		this.colour = colour;
	}

	@Override
	public String getColour() {
		return colour;
	}

	@Override
	public String getValue() {
		return value;
	}

}

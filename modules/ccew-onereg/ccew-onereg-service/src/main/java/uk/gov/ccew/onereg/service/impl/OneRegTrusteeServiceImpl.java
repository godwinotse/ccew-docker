package uk.gov.ccew.onereg.service.impl;

import uk.gov.ccew.onereg.model.OneRegTrustee;
import uk.gov.ccew.onereg.model.impl.OneRegTrusteeImpl;
import uk.gov.ccew.onereg.service.OneRegTrusteeService;
import uk.gov.ccew.publicdata.model.Trustee;
import uk.gov.ccew.publicdata.service.TrusteeService;

import java.util.List;
import java.util.stream.Collectors;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;

@Component(immediate = true, service = OneRegTrusteeService.class)
public class OneRegTrusteeServiceImpl implements OneRegTrusteeService {

	@Reference
	private TrusteeService trusteeService;

	@Override
	public OneRegTrustee getTrustee(Trustee trustee) {

		return new OneRegTrusteeImpl(trustee);

	}

	@Override
	public int getTrusteeCount(long organisationNumber) {
		return trusteeService.getTrusteeCount(organisationNumber);
	}

	@Override
	public List<OneRegTrustee> getTrustees(long organisationNumber, PortletRequest portletRequest) throws PortalException {

		List<Trustee> trustees = trusteeService.getTrustees(organisationNumber);

		return trustees.stream().map(trustee -> getTrustee(trustee)).collect(Collectors.toList());

	}

}

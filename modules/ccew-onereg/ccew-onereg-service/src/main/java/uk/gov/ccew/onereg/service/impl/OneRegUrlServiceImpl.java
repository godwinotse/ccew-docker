package uk.gov.ccew.onereg.service.impl;

import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.WindowStateException;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

import uk.gov.ccew.onereg.constant.PortletKeys;
import uk.gov.ccew.onereg.constant.RequestKeys;
import uk.gov.ccew.onereg.service.OneRegUrlService;

@Component(immediate = true, service = OneRegUrlService.class)
public class OneRegUrlServiceImpl implements OneRegUrlService {

	@Override
	public String getCharityDetailsUrl(long organisationNumber, PortletRequest portletRequest) throws PortalException {

		LiferayPortletRequest liferayPortletRequest = PortalUtil.getLiferayPortletRequest(portletRequest);

		return getCharityDetailsUrl(organisationNumber, liferayPortletRequest);

	}

	@Override
	public String getCharityDetailsUrl(long organisationNumber, LiferayPortletRequest liferayPortletRequest) throws PortalException {

		ThemeDisplay themeDisplay = (ThemeDisplay) liferayPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		try {

			PortletURL viewContentURL = PortletURLFactoryUtil.create(liferayPortletRequest, PortletKeys.CHARITY_DETAILS, themeDisplay.getPlid(), PortletRequest.RENDER_PHASE);
			viewContentURL.setWindowState(LiferayWindowState.MAXIMIZED);
			viewContentURL.setPortletMode(PortletMode.VIEW);
			viewContentURL.setParameter(WebKeys.REFERER, themeDisplay.getURLCurrent());
			viewContentURL.setParameter(RequestKeys.ORGANISATION_NUMBER, String.valueOf(organisationNumber));

			return viewContentURL.toString();

		} catch (WindowStateException | PortletModeException e) {

			throw new PortalException("Unable to retrieve charity details URL - organisation number: " + organisationNumber, e);

		}

	}
	
}

package uk.gov.ccew.onereg.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;

import uk.gov.ccew.onereg.model.Chart;
import uk.gov.ccew.onereg.model.ChartData;
import uk.gov.ccew.onereg.model.ChartDataSet;
import uk.gov.ccew.onereg.model.impl.ChartDataImpl;
import uk.gov.ccew.onereg.model.impl.ChartDataSetImpl;
import uk.gov.ccew.onereg.model.impl.ChartImpl;
import uk.gov.ccew.onereg.service.ChartService;

@Component(immediate = true, service = ChartService.class)
public class ChartServiceImpl implements ChartService {

	@Override
	public Chart getChart(List<ChartDataSet> dataSets) {
		return new ChartImpl(dataSets);
	}

	@Override
	public Chart getChart(Collection<String> labels, List<ChartDataSet> dataSets) {
		return new ChartImpl(labels, dataSets);
	}

	@Override
	public ChartDataSet getChartDataSet(List<ChartData> data) {
		return new ChartDataSetImpl(data);
	}

	@Override
	public ChartDataSet getChartDataSet(String label, List<String> data) {

		List<ChartData> chartData = data.stream().map(value -> new ChartDataImpl(value, StringPool.BLANK)).collect(Collectors.toList());

		return new ChartDataSetImpl(label, chartData);

	}

	@Override
	public ChartDataSet getChartDataSet(String label, List<String> data, String colour) {

		List<ChartData> chartData = data.stream().map(value -> new ChartDataImpl(value, colour)).collect(Collectors.toList());

		return new ChartDataSetImpl(label, chartData);

	}

}

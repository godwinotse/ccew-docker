package uk.gov.ccew.onereg.model.impl;

import java.util.Optional;

import uk.gov.ccew.onereg.model.OneRegAccountsAndTar;
import uk.gov.ccew.onereg.model.OneRegAnnualReturn;
import uk.gov.ccew.onereg.model.OneRegAnnualReturnAndAccountsAndTar;
import uk.gov.ccew.publicdata.model.AccountsAndTar;
import uk.gov.ccew.publicdata.model.AnnualReturn;
import uk.gov.ccew.publicdata.model.AnnualReturnAndAccountsAndTar;

public class OneRegAnnualReturnAndAccountsAndTarImpl implements OneRegAnnualReturnAndAccountsAndTar {

	private final AnnualReturnAndAccountsAndTar annualReturnAndAccountsAndTar;

	public OneRegAnnualReturnAndAccountsAndTarImpl(AnnualReturnAndAccountsAndTar annualReturnAndAccountsAndTar) {
		this.annualReturnAndAccountsAndTar = annualReturnAndAccountsAndTar;
	}

	@Override
	public Optional<AccountsAndTar> getAccountsAndTar() {
		return annualReturnAndAccountsAndTar.getAccountsAndTar();
	}

	@Override
	public Optional<AnnualReturn> getAnnualReturn() {
		return annualReturnAndAccountsAndTar.getAnnualReturn();
	}

	@Override
	public Optional<OneRegAccountsAndTar> getOneRegAccountsAndTar() {

		Optional<OneRegAccountsAndTar> oneRegAccountsAndTar;

		Optional<AccountsAndTar> accountsAndTarOptional = annualReturnAndAccountsAndTar.getAccountsAndTar();

		if (accountsAndTarOptional.isPresent()) {

			AccountsAndTar accountsAndTar = accountsAndTarOptional.get();
			oneRegAccountsAndTar = Optional.of(new OneRegAccountsAndTarImpl(accountsAndTar));

		} else {

			oneRegAccountsAndTar = Optional.empty();

		}

		return oneRegAccountsAndTar;

	}

	@Override
	public Optional<OneRegAnnualReturn> getOneRegAnnualReturn() {

		Optional<OneRegAnnualReturn> oneRegAnnualReturn;

		Optional<AnnualReturn> annualReturnOptional = annualReturnAndAccountsAndTar.getAnnualReturn();

		if (annualReturnOptional.isPresent()) {

			AnnualReturn annualReturn = annualReturnOptional.get();
			oneRegAnnualReturn = Optional.of(new OneRegAnnualReturnImpl(annualReturn));

		} else {

			oneRegAnnualReturn = Optional.empty();

		}

		return oneRegAnnualReturn;

	}

	@Override
	public void setAnnualReturn(AnnualReturn annualReturn) {
		this.annualReturnAndAccountsAndTar.setAnnualReturn(annualReturn);
	}

}

package uk.gov.ccew.onereg.upgrade;

import uk.gov.ccew.onereg.upgrade.step_1_0_0.GroupUpgradeStep_1_0_0;
import uk.gov.ccew.onereg.upgrade.step_1_0_0.WebContentUpgradeStep_1_0_0;
import uk.gov.ccew.onereg.upgrade.step_1_0_0.WebContentUpgradeStep_1_1_0;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class OneRegUpgradeStepRegistrar implements UpgradeStepRegistrator {

	@Reference
	private UpgradeContext upgradeContext;

	@Override
	public void register(Registry registry) {

		registry.register( "0.0.0", "1.0.0", new GroupUpgradeStep_1_0_0(upgradeContext), new WebContentUpgradeStep_1_0_0(upgradeContext));
		registry.register( "1.0.0", "1.1.0", new WebContentUpgradeStep_1_1_0(upgradeContext));

	}

}

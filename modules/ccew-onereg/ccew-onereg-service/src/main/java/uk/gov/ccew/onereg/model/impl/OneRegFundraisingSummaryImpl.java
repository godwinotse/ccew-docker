package uk.gov.ccew.onereg.model.impl;

import uk.gov.ccew.onereg.model.OneRegFundraisingSummary;
import uk.gov.ccew.publicdata.model.FundraisingSummary;

public class OneRegFundraisingSummaryImpl implements OneRegFundraisingSummary {

	private FundraisingSummary fundraisingSummary;

	public OneRegFundraisingSummaryImpl(FundraisingSummary fundraisingSummary) {
		this.fundraisingSummary = fundraisingSummary;
	}

	@Override
	public boolean isRaisingFundsFromThePublic() {
		return fundraisingSummary.isRaisingFundsFromThePublic();
	}

	@Override
	public boolean isWorkingWithCommercialParticipants() {
		return fundraisingSummary.isWorkingWithCommercialParticipants();
	}

	@Override
	public boolean isCommercialAndProfessionalAgreementInPlace() {
		return fundraisingSummary.isCommercialAndProfessionalAgreementInPlace();
	}

}

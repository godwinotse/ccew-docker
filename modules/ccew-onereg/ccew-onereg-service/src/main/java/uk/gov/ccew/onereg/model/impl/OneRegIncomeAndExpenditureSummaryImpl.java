package uk.gov.ccew.onereg.model.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import uk.gov.ccew.onereg.constant.DateFormatter;
import uk.gov.ccew.onereg.constant.NumberFormatter;
import uk.gov.ccew.onereg.model.OneRegIncomeAndExpenditureSummary;
import uk.gov.ccew.publicdata.model.IncomeAndExpenditureSummary;

public class OneRegIncomeAndExpenditureSummaryImpl implements OneRegIncomeAndExpenditureSummary {

	private IncomeAndExpenditureSummary incomeAndExpenditureSummary;

	public OneRegIncomeAndExpenditureSummaryImpl(IncomeAndExpenditureSummary incomeAndExpenditureSummary) {
		this.incomeAndExpenditureSummary = incomeAndExpenditureSummary;
	}

	@Override
	public Optional<Date> getCurrentFinancialYearEnd() {
		return incomeAndExpenditureSummary.getCurrentFinancialYearEnd();
	}

	@Override
	public String getCurrentFinancialYearEndFormatted() {
		return DateFormatter.format_dd_MMMM(incomeAndExpenditureSummary.getCurrentFinancialYearEnd());
	}

	@Override
	public Map<String, BigDecimal> getExpenditureByCategory() {
		return incomeAndExpenditureSummary.getExpenditureByCategory();
	}

	@Override
	public Map<String, String> getExpenditureByCategoryFormatted() {
		return getCurrencyFormatMap(incomeAndExpenditureSummary.getExpenditureByCategory());
	}

	@Override
	public BigDecimal getGovernmentContractsIncome() {
		return incomeAndExpenditureSummary.getGovernmentContractsIncome();
	}

	@Override
	public int getGovernmentContractsQty() {
		return incomeAndExpenditureSummary.getGovernmentContractsQty();
	}

	@Override
	public BigDecimal getGovernmentGrantsIncome() {
		return incomeAndExpenditureSummary.getGovernmentGrantsIncome();
	}

	@Override
	public int getGovernmentGrantsQty() {
		return incomeAndExpenditureSummary.getGovernmentGrantsQty();
	}

	@Override
	public Map<String, BigDecimal> getIncomeByCategory() {
		return incomeAndExpenditureSummary.getIncomeByCategory();
	}

	@Override
	public BigDecimal getInvestmentGains() {
		return incomeAndExpenditureSummary.getInvestmentGains();
	}

	@Override
	public Map<String, String> getIncomeByCategoryFormatted() {
		return getCurrencyFormatMap(incomeAndExpenditureSummary.getIncomeByCategory());
	}

	@Override
	public String getInvestmentGainsFormatted() {
		return NumberFormatter.formatUkCurrencyShort(incomeAndExpenditureSummary.getInvestmentGains());
	}

	@Override
	public Object[] getGovernmentContractsAndGrantsMessageArguments() {

		Object[] messageArguments;

		String governmentContractsIncome = NumberFormatter.formatUkCurrencyShort(incomeAndExpenditureSummary.getGovernmentContractsIncome());
		String governmentGrantsIncome = NumberFormatter.formatUkCurrencyShort(incomeAndExpenditureSummary.getGovernmentGrantsIncome());

		if (getGovernmentContractsQty() == 0 || getGovernmentGrantsQty() == 0) {

			messageArguments = new Object[]{ governmentContractsIncome, governmentGrantsIncome };

		} else {

			messageArguments = new Object[]{ governmentContractsIncome, getGovernmentContractsQty(), governmentGrantsIncome, getGovernmentGrantsQty() };

		}

		return messageArguments;
	}

	private Map<String, String> getCurrencyFormatMap(Map<String, BigDecimal> incomeByCategory) {
		return incomeByCategory.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> NumberFormatter.formatUkCurrencyShort(e.getValue())));
	}

}

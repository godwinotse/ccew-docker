package uk.gov.ccew.onereg.service.impl;

import uk.gov.ccew.onereg.service.OneRegLocaleService;
import uk.gov.ccew.onereg.service.OneRegGroupService;
import uk.gov.ccew.onereg.service.ServiceContextService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.Portal;

@Component(immediate = true, service = OneRegGroupService.class)
public class OneRegGroupServiceImpl implements OneRegGroupService {

	@Reference
	private CounterLocalService counterLocalService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private OneRegLocaleService oneRegLocaleService;

	@Reference
	private ServiceContextService serviceContextService;

	@Reference
	private UserLocalService userLocalService;

	@Reference
	private Portal portal;

	@Override
	public Group addOneRegGroup() throws PortalException {

		long groupId = counterLocalService.increment(Group.class.getName());
		long companyId = portal.getDefaultCompanyId();
		User user = userLocalService.getDefaultUser(companyId);

		List<Locale> locales = new ArrayList<>();
		locales.add(LocaleUtil.getDefault());
		Map<Locale, String> localisedGroupNames = oneRegLocaleService.getLocaleMap(locales, "Register");

		ServiceContext serviceContext = serviceContextService.getServiceContext(companyId);

		return groupLocalService
				.addGroup(user.getUserId(), GroupConstants.DEFAULT_PARENT_GROUP_ID, Group.class.getName(), groupId, GroupConstants.DEFAULT_LIVE_GROUP_ID, localisedGroupNames, localisedGroupNames,
						GroupConstants.TYPE_SITE_OPEN, true, GroupConstants.DEFAULT_MEMBERSHIP_RESTRICTION, REGISTER_FRIENDLY_URL, true, false, true, serviceContext);

	}

	@Override
	public Group getOneRegGroup() throws PortalException {

		long companyId = portal.getDefaultCompanyId();
		return groupLocalService.getFriendlyURLGroup(companyId, REGISTER_FRIENDLY_URL);

	}

}

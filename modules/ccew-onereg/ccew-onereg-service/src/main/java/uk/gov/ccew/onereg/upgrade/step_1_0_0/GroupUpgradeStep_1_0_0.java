package uk.gov.ccew.onereg.upgrade.step_1_0_0;

import uk.gov.ccew.onereg.service.OneRegGroupService;
import uk.gov.ccew.onereg.upgrade.UpgradeContext;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class GroupUpgradeStep_1_0_0 extends UpgradeProcess {

	private final OneRegGroupService oneRegGroupService;

	public GroupUpgradeStep_1_0_0(UpgradeContext upgradeContext) {

		this.oneRegGroupService = upgradeContext.getOneRegGroupService();

	}

	@Override
	protected void doUpgrade() throws Exception {

		oneRegGroupService.addOneRegGroup();

	}

}

package uk.gov.ccew.onereg.service.impl;

import java.math.BigDecimal;
import java.util.Collection;

import org.osgi.service.component.annotations.Component;

import uk.gov.ccew.onereg.constant.NumberFormatter;
import uk.gov.ccew.onereg.service.CurrencyService;

@Component(immediate = true, service = CurrencyService.class)
public class CurrencyServiceImpl implements CurrencyService {

	@Override
	public String getTotalFormatted(Collection<BigDecimal> values) {

		BigDecimal total = values.stream().reduce(BigDecimal.ZERO, (valueA, valueB) -> valueA.add(valueB));

		return NumberFormatter.formatUkCurrencyShort(total);

	}

}

package uk.gov.ccew.onereg.model.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.liferay.petra.string.StringPool;

import uk.gov.ccew.onereg.constant.DateFormatter;
import uk.gov.ccew.onereg.model.OneRegTrustee;
import uk.gov.ccew.publicdata.model.Trustee;
import uk.gov.ccew.publicdata.model.TrusteeRole;

public class OneRegTrusteeImpl implements OneRegTrustee {

	private Trustee trustee;

	public OneRegTrusteeImpl(Trustee trustee) {

		this.trustee = trustee;

	}

	@Override
	public Optional<Date> getDateOfAppointment() {
		return trustee.getDateOfAppointment();
	}

	@Override
	public String getDateOfAppointmentFormatted() {

		Optional<Date> dateOfAppointmentOptional = trustee.getDateOfAppointment();

		return dateOfAppointmentOptional.map(DateFormatter::format_dd_MMMM_YYYY).orElse(StringPool.BLANK);

	}

	@Override
	public String getName() {
		return trustee.getName();
	}

	@Override
	public List<Long> getOtherCharityOrganisationNumbers() {
		return trustee.getOtherCharityOrganisationNumbers();
	}

	@Override
	public Optional<TrusteeRole> getRole() {
		return trustee.getRole();
	}

	@Override
	public boolean isDirectorOfSubsidiary() {
		return trustee.isDirectorOfSubsidiary();
	}

	@Override
	public boolean isReceivingPaymentsFromCharity() {
		return trustee.isReceivingPaymentsFromCharity();
	}

	@Override
	public void setName(String name) {
		trustee.setName(name);
	}

}

package uk.gov.ccew.onereg.model.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import uk.gov.ccew.onereg.constant.DateFormatter;
import uk.gov.ccew.onereg.constant.NumberFormatter;
import uk.gov.ccew.onereg.model.OneRegFinancialAnnualReport;
import uk.gov.ccew.publicdata.model.FinancialAnnualReport;

public class OneRegFinancialAnnualReportImpl implements OneRegFinancialAnnualReport {

	private final FinancialAnnualReport financialAnnualReport;

	public OneRegFinancialAnnualReportImpl(FinancialAnnualReport financialAnnualReport) {
		this.financialAnnualReport = financialAnnualReport;
	}

	@Override
	public Map<String, BigDecimal> getExpenditureTotalsByCategory() {
		return financialAnnualReport.getExpenditureTotalsByCategory();
	}

	@Override
	public Map<String, String> getExpenditureTotalsByCategoryFormatted() {
		return financialAnnualReport.getExpenditureTotalsByCategory().entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> NumberFormatter.formatUkCurrencyShort(e.getValue())));
	}

	@Override
	public Optional<Date> getFinancialYearEnd() {
		return financialAnnualReport.getFinancialYearEnd();
	}

	@Override
	public String getFinancialYearEndFormatted() {
		return DateFormatter.format_dd_MM_YY(financialAnnualReport.getFinancialYearEnd());
	}

	@Override
	public Map<String, BigDecimal> getIncomeTotalsByCategory() {
		return financialAnnualReport.getIncomeTotalsByCategory();
	}

	@Override
	public Map<String, String> getIncomeTotalsByCategoryFormatted() {
		return financialAnnualReport.getIncomeTotalsByCategory().entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> NumberFormatter.formatUkCurrencyShort(e.getValue())));
	}

}

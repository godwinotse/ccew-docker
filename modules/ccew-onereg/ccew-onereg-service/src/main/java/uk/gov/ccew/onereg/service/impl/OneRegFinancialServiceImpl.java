package uk.gov.ccew.onereg.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import uk.gov.ccew.onereg.constant.NumberFormatter;
import uk.gov.ccew.onereg.constant.SpendingType;
import uk.gov.ccew.onereg.model.Chart;
import uk.gov.ccew.onereg.model.ChartData;
import uk.gov.ccew.onereg.model.ChartDataSet;
import uk.gov.ccew.onereg.model.OneRegAnnualReturnAndAccountsAndTar;
import uk.gov.ccew.onereg.model.OneRegAssetsAndLiabilities;
import uk.gov.ccew.onereg.model.OneRegFinancialAnnualReport;
import uk.gov.ccew.onereg.model.OneRegFundraisingSummary;
import uk.gov.ccew.onereg.model.OneRegIncomeAndExpenditureSummary;
import uk.gov.ccew.onereg.model.Table;
import uk.gov.ccew.onereg.model.TableCell;
import uk.gov.ccew.onereg.model.TableCellType;
import uk.gov.ccew.onereg.model.TableRow;
import uk.gov.ccew.onereg.model.impl.ChartDataImpl;
import uk.gov.ccew.onereg.model.impl.ChartDataSetImpl;
import uk.gov.ccew.onereg.model.impl.OneRegAnnualReturnAndAccountsAndTarImpl;
import uk.gov.ccew.onereg.model.impl.OneRegAssetsAndLiabilitiesImpl;
import uk.gov.ccew.onereg.model.impl.OneRegFinancialAnnualReportImpl;
import uk.gov.ccew.onereg.model.impl.OneRegFundraisingSummaryImpl;
import uk.gov.ccew.onereg.model.impl.OneRegIncomeAndExpenditureSummaryImpl;
import uk.gov.ccew.onereg.model.impl.TableCellImpl;
import uk.gov.ccew.onereg.model.impl.TableImpl;
import uk.gov.ccew.onereg.model.impl.TableRowImpl;
import uk.gov.ccew.onereg.service.ChartConfigurationUtil;
import uk.gov.ccew.onereg.service.ChartService;
import uk.gov.ccew.onereg.service.OneRegFinancialService;
import uk.gov.ccew.publicdata.model.AnnualReturnAndAccountsAndTar;
import uk.gov.ccew.publicdata.model.AssetsAndLiabilities;
import uk.gov.ccew.publicdata.model.FundraisingSummary;
import uk.gov.ccew.publicdata.model.IncomeAndExpenditureSummary;
import uk.gov.ccew.publicdata.service.FinancialService;

@Component(immediate = true, service = OneRegFinancialService.class)
public class OneRegFinancialServiceImpl implements OneRegFinancialService {

	private static final Map<String, String> CATEGORY_COLOUR_MAPPINGS;

	static {

		CATEGORY_COLOUR_MAPPINGS = new HashMap<>();
		CATEGORY_COLOUR_MAPPINGS.put("Charitable activities", "blue");
		CATEGORY_COLOUR_MAPPINGS.put("Donations and legacies", "yellow");
		CATEGORY_COLOUR_MAPPINGS.put("Investments", "mid-grey");
		CATEGORY_COLOUR_MAPPINGS.put("Other", "purple");
		CATEGORY_COLOUR_MAPPINGS.put("Other trading activities", "green");
		CATEGORY_COLOUR_MAPPINGS.put("Raising funds", "orange");

	}

	@Reference
	private ChartConfigurationUtil chartConfigurationUtil;

	@Reference
	private ChartService chartService;

	@Reference
	private FinancialService financialService;

	@Override
	public OneRegAssetsAndLiabilities getAssetsAndLiabilities(long organisationNumber) {

		AssetsAndLiabilities assetsAndLiabilities = financialService.getAssetsAndLiabilities(organisationNumber);

		return new OneRegAssetsAndLiabilitiesImpl(assetsAndLiabilities);

	}

	@Override
	public Chart getAssetsAndLiabilitiesChart(OneRegAssetsAndLiabilities assetsAndLiabilities) {

		Map<String, String> colourNamesAndValues = chartConfigurationUtil.getColourNamesAndValues();

		ChartDataSet ownUseAssetsChartDataSet = chartService
				.getChartDataSet("Own use assets", Collections.singletonList(assetsAndLiabilities.getOwnUseAssetsTotal().toString()), colourNamesAndValues.get("blue"));
		ChartDataSet longTermInvestmentChartDataSet = chartService
				.getChartDataSet("Long term investment", Collections.singletonList(assetsAndLiabilities.getLongTermInvestmentTotal().toString()), colourNamesAndValues.get("yellow"));
		ChartDataSet otherAssetsChartDataSet = chartService
				.getChartDataSet("Other assets", Collections.singletonList(assetsAndLiabilities.getOtherAssetsTotal().toString()), colourNamesAndValues.get("green"));
		ChartDataSet definedPensionBenefitsChartDataSet = chartService
				.getChartDataSet("Defined pension benefits", Collections.singletonList(assetsAndLiabilities.getDefinedPensionBenefitTotal().toString()), colourNamesAndValues.get("purple"));
		ChartDataSet totalLiabilitiesChartDataSet = chartService
				.getChartDataSet("Total liabilities", Collections.singletonList(assetsAndLiabilities.getLiabilitiesTotal().negate().toString()), colourNamesAndValues.get("orange"));

		List<ChartDataSet> chartDataSets = Arrays
				.asList(ownUseAssetsChartDataSet, longTermInvestmentChartDataSet, otherAssetsChartDataSet, definedPensionBenefitsChartDataSet, totalLiabilitiesChartDataSet);

		return chartService.getChart(Collections.emptyList(), chartDataSets);

	}

	@Override
	public Table getAssetsAndLiabilitiesTable(OneRegAssetsAndLiabilities assetsAndLiabilities) {

		List<TableRow> tableRows = new ArrayList<>();
		tableRows.add(getAssetsAndLiabilitiesTableRow("Own use assets", assetsAndLiabilities.getOwnUseAssetsTotalFormatted()));
		tableRows.add(getAssetsAndLiabilitiesTableRow("Long term investment", assetsAndLiabilities.getLongTermInvestmentTotalFormatted()));
		tableRows.add(getAssetsAndLiabilitiesTableRow("Other assets total", assetsAndLiabilities.getOtherAssetsTotalFormatted()));
		tableRows.add(getAssetsAndLiabilitiesTableRow("Defined pension benefit", assetsAndLiabilities.getDefinedPensionBenefitTotalFormatted()));
		tableRows.add(getAssetsAndLiabilitiesTableRow("Total liabilities", assetsAndLiabilities.getLiabilitiesTotalFormatted()));

		return new TableImpl(tableRows);

	}

	@Override
	public List<OneRegAnnualReturnAndAccountsAndTar> getAnnualReturnsAndAccountsAndTars(long organisationNumber) {

		List<AnnualReturnAndAccountsAndTar> annualReturnsAndAccountsAndTars = financialService.getAnnualReturnsAndAccountsAndTars(organisationNumber);

		return annualReturnsAndAccountsAndTars.stream().map(OneRegAnnualReturnAndAccountsAndTarImpl::new).collect(Collectors.toList());

	}

	@Override
	public Chart getExpenditureChart(OneRegIncomeAndExpenditureSummary incomeAndExpenditureSummary) {

		Map<String, BigDecimal> expenditureByCategory = incomeAndExpenditureSummary.getExpenditureByCategory();

		return getChart(expenditureByCategory);

	}

	@Override
	public Table getExpenditureTable(OneRegIncomeAndExpenditureSummary incomeAndExpenditureSummary) {

		Map<String, String> expenditureByCategory = incomeAndExpenditureSummary.getExpenditureByCategoryFormatted();

		return getTableForPieChart(expenditureByCategory);

	}

	@Override
	public List<OneRegFinancialAnnualReport> getFinancialHistory(long organisationNumber) {
		return financialService.getFinancialHistory(organisationNumber).stream().map(OneRegFinancialAnnualReportImpl::new).collect(Collectors.toList());
	}

	@Override
	public Chart getFinancialHistoryChart(List<OneRegFinancialAnnualReport> financialHistory) {

		Set<String> columnLabels = financialHistory.stream().map(OneRegFinancialAnnualReport::getFinancialYearEndFormatted).collect(Collectors.toCollection(LinkedHashSet::new));
		Set<String> incomeCategories = getCategories(financialHistory, SpendingType.INCOME);
		Set<String> expenditureCategories = getCategories(financialHistory, SpendingType.EXPENDITURE);

		List<ChartDataSet> incomeCategorisedTotals = getCategorisedTotals(financialHistory, incomeCategories, SpendingType.INCOME);
		List<ChartDataSet> expenditureCategorisedTotals = getCategorisedTotals(financialHistory, expenditureCategories, SpendingType.EXPENDITURE);
		List<ChartDataSet> chartDataSets = getChartDataSets(incomeCategorisedTotals, expenditureCategorisedTotals);

		return chartService.getChart(columnLabels, chartDataSets);

	}

	@Override
	public OneRegFundraisingSummary getFundraisingSummary(long organisationNumber) {

		FundraisingSummary fundraisingSummary = financialService.getFundraisingSummary(organisationNumber);

		return new OneRegFundraisingSummaryImpl(fundraisingSummary);

	}

	@Override
	public OneRegIncomeAndExpenditureSummary getIncomeAndExpenditureSummary(long organisationNumber) {

		IncomeAndExpenditureSummary incomeAndExpenditureSummary = financialService.getIncomeAndExpenditureSummary(organisationNumber);

		return new OneRegIncomeAndExpenditureSummaryImpl(incomeAndExpenditureSummary);

	}

	@Override
	public Chart getIncomeChart(OneRegIncomeAndExpenditureSummary incomeAndExpenditureSummary) {

		Map<String, BigDecimal> incomeByCategory = incomeAndExpenditureSummary.getIncomeByCategory();

		return getChart(incomeByCategory);

	}

	@Override
	public Table getIncomeTable(OneRegIncomeAndExpenditureSummary incomeAndExpenditureSummary) {

		Map<String, String> incomeByCategory = incomeAndExpenditureSummary.getIncomeByCategoryFormatted();

		return getTableForPieChart(incomeByCategory);

	}

	@Override
	public Table getSpendingTable(List<OneRegFinancialAnnualReport> financialHistory) {

		TableRow headerRow = getTableHeaderRow(financialHistory);
		List<TableRow> bodyRows = getSpendingTableBodyRows(financialHistory, SpendingType.INCOME);
		bodyRows.addAll(getSpendingTableBodyRows(financialHistory, SpendingType.EXPENDITURE));
		return new TableImpl(headerRow, bodyRows);

	}

	@Override
	public String getSpendingTotal(List<OneRegFinancialAnnualReport> financialHistory, SpendingType spendingType) {

		Set<String> expenditureCategories = getCategories(financialHistory, spendingType);
		List<ChartDataSet> categorisedTotals = getCategorisedTotals(financialHistory, expenditureCategories, spendingType);

		BigDecimal totalExpenditure = categorisedTotals.stream()
				.map(chartDataSet -> chartDataSet.getChartData().stream().map(chartData -> new BigDecimal(chartData.getValue())).reduce(BigDecimal.ZERO, BigDecimal::add))
				.reduce(BigDecimal.ZERO, BigDecimal::add);

		return NumberFormatter.formatUkCurrencyShort(totalExpenditure);

	}

	private Map<String, String> getCategoryColourMap(List<OneRegFinancialAnnualReport> financialHistory, SpendingType spendingType) {
		Set<String> incomeCategories = getCategories(financialHistory, spendingType);
		List<String> colours = getColours(spendingType);

		int index = 0;
		int coloursQty = colours.size();

		Map<String, String> categoriesAndColours = new HashMap<>();

		for (String category : incomeCategories) {

			if (index >= coloursQty) {
				index = 0;
			}
			categoriesAndColours.put(category, colours.get(index));
			index++;
		}
		return categoriesAndColours;
	}

	private TableRow getAssetsAndLiabilitiesTableRow(String category, String value) {

		List<TableCell> cells = new ArrayList<>();
		cells.add(new TableCellImpl(category));
		cells.add(new TableCellImpl(value, true));

		return new TableRowImpl(cells);

	}

	private Set<String> getCategories(List<OneRegFinancialAnnualReport> financialHistory, SpendingType spendingType) {

		Set<String> categories = new LinkedHashSet<>();

		financialHistory.forEach(financialAnnualReport -> {

			Map<String, BigDecimal> totalsByCategory =
					spendingType == SpendingType.EXPENDITURE ? financialAnnualReport.getExpenditureTotalsByCategory() : financialAnnualReport.getIncomeTotalsByCategory();
			categories.addAll(totalsByCategory.keySet());

		});

		return categories;

	}

	private List<ChartDataSet> getCategorisedTotals(List<OneRegFinancialAnnualReport> financialHistory, Set<String> categories, SpendingType spendingType) {

		List<ChartDataSet> categorisedTotals = new ArrayList<>();
		List<String> colours = getColours(spendingType);

		int index = 0;
		int coloursQty = colours.size();

		for (String category : categories) {

			if (index >= coloursQty) {
				index = 0;
			}

			List<String> categoryTotals = getTotals(financialHistory, category, spendingType);
			ChartDataSet chartDataSet = getChartDataSet(category, categoryTotals, spendingType, colours.get(index));
			categorisedTotals.add(chartDataSet);

			index++;

		}

		return categorisedTotals;

	}

	private Map<String, List<String>> getCategorisedTotals(List<OneRegFinancialAnnualReport> financialHistory, SpendingType spendingType) {

		Map<String, List<String>> categorisedTotals = new LinkedHashMap<>();

		financialHistory.forEach(annualReport -> {

			Map<String, String> totalsByCategoryFormatted =
					spendingType == SpendingType.INCOME ? annualReport.getIncomeTotalsByCategoryFormatted() : annualReport.getExpenditureTotalsByCategoryFormatted();

			totalsByCategoryFormatted.forEach((k, v) -> {

				List<String> totals = categorisedTotals.getOrDefault(k, new ArrayList<>());
				totals.add(v);
				categorisedTotals.put(k, totals);

			});

		});

		return categorisedTotals;

	}

	private Chart getChart(Map<String, BigDecimal> categorisedTotals) {

		Map<String, String> colourNamesAndValues = chartConfigurationUtil.getColourNamesAndValues();

		List<ChartData> chartData = new ArrayList<>();
		List<String> labels = new ArrayList<>();

		for (Map.Entry<String, BigDecimal> categoryAndTotal : categorisedTotals.entrySet()) {

			String category = categoryAndTotal.getKey();
			BigDecimal total = categoryAndTotal.getValue();

			labels.add(category);

			String colourName = CATEGORY_COLOUR_MAPPINGS.get(category);
			String colourValue = colourNamesAndValues.get(colourName);

			chartData.add(new ChartDataImpl(total.toString(), colourValue));

		}

		ChartDataSetImpl chartDataSet = new ChartDataSetImpl(chartData);

		return chartService.getChart(labels, Collections.singletonList(chartDataSet));

	}

	private ChartDataSet getChartDataSet(String category, List<String> categoryTotals, SpendingType spendingType, String colour) {

		int stackId = spendingType == SpendingType.INCOME ? 0 : 1;

		ChartDataSet chartDataSet = chartService.getChartDataSet(category, categoryTotals, colour);
		chartDataSet.setStacked(true);
		chartDataSet.setStackId(stackId);

		return chartDataSet;

	}

	private List<ChartDataSet> getChartDataSets(List<ChartDataSet> incomeCategorisedTotals, List<ChartDataSet> expenditureCategorisedTotals) {

		List<ChartDataSet> allChartDataSets = new ArrayList<>();
		allChartDataSets.addAll(incomeCategorisedTotals);
		allChartDataSets.addAll(expenditureCategorisedTotals);

		return allChartDataSets;

	}

	private List<String> getColours(SpendingType spendingType) {

		Map<String, String> colourNamesAndValues = chartConfigurationUtil.getColourNamesAndValues();
		List<String> colours = new ArrayList<>();

		if (spendingType == SpendingType.INCOME) {

			colours.add(colourNamesAndValues.get("blue"));
			colours.add(colourNamesAndValues.get("yellow"));
			colours.add(colourNamesAndValues.get("green"));
			colours.add(colourNamesAndValues.get("purple"));

		} else {

			colours.add(colourNamesAndValues.get("orange"));

		}

		return colours;

	}

	private List<TableRow> getSpendingTableBodyRows(List<OneRegFinancialAnnualReport> financialHistory, SpendingType spendingType) {

		Map<String, List<String>> categorisedTotals = getCategorisedTotals(financialHistory, spendingType);
		Map<String, String> categoriesAndColours = getCategoryColourMap(financialHistory, spendingType);

		return categorisedTotals.entrySet().stream().map(entry -> {

			List<TableCell> rowData = new ArrayList<>();
			rowData.add(new TableCellImpl(categoriesAndColours.get(entry.getKey()), TableCellType.COLOUR_BLOCK));
			rowData.add(new TableCellImpl(entry.getKey()));
			entry.getValue().stream().map(otherColumnValue -> new TableCellImpl(otherColumnValue, true)).collect(Collectors.toCollection(() -> rowData));

			return new TableRowImpl(rowData);

		}).collect(Collectors.toList());
	}

	private Table getTableForPieChart(Map<String, String> categorisedValues) {

		Map<String, String> colourNamesAndValues = chartConfigurationUtil.getColourNamesAndValues();

		List<TableRow> tableBody = new ArrayList<>();

		for (Map.Entry<String, String> categoryAndIncome : categorisedValues.entrySet()) {

			String category = categoryAndIncome.getKey();
			String total = categoryAndIncome.getValue();

			String colourName = CATEGORY_COLOUR_MAPPINGS.get(category);
			String colourValue = colourNamesAndValues.get(colourName);

			List<TableCell> tableCells = new ArrayList<>();
			tableCells.add(new TableCellImpl(colourValue, TableCellType.COLOUR_BLOCK));
			tableCells.add(new TableCellImpl(category));
			tableCells.add(new TableCellImpl(total, true));

			tableBody.add(new TableRowImpl(tableCells));

		}

		return new TableImpl(tableBody);

	}

	private TableRow getTableHeaderRow(List<OneRegFinancialAnnualReport> financialHistory) {
		
		TableCellImpl colourHeader = new TableCellImpl();
		TableCellImpl titleHeader = new TableCellImpl();
		
		List<TableCell> headerRow = new ArrayList<>();
		headerRow.add(colourHeader);
		headerRow.add(titleHeader);

		financialHistory.stream().map(financialAnnualReport -> {

			String financialYearEnd = financialAnnualReport.getFinancialYearEndFormatted();

			return new TableCellImpl(financialYearEnd);

		}).collect(Collectors.toCollection(() -> headerRow));

		return new TableRowImpl(headerRow);

	}

	private List<String> getTotals(List<OneRegFinancialAnnualReport> financialHistory, String category, SpendingType spendingType) {

		List<String> totals = new ArrayList<>();

		financialHistory.forEach(financialAnnualReport -> {

			Map<String, BigDecimal> totalsByCategory =
					spendingType == SpendingType.EXPENDITURE ? financialAnnualReport.getExpenditureTotalsByCategory() : financialAnnualReport.getIncomeTotalsByCategory();
			BigDecimal categoryTotal = totalsByCategory.getOrDefault(category, BigDecimal.ZERO);
			totals.add(categoryTotal.toString());

		});

		return totals;

	}

}

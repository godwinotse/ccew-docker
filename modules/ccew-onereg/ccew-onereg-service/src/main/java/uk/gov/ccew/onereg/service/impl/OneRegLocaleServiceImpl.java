package uk.gov.ccew.onereg.service.impl;

import uk.gov.ccew.onereg.service.OneRegLocaleService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = OneRegLocaleService.class)
public class OneRegLocaleServiceImpl implements OneRegLocaleService {

	@Override
	public List<Locale> getSupportedLocales() {

		Locale walesLocale = new Locale.Builder().setLanguage("cy").setRegion("GB").build();
		Locale englishLocale = new Locale.Builder().setLanguage("en").setRegion("GB").build();

		List<Locale> supportedLocales = new ArrayList<>();
		supportedLocales.add(englishLocale);
		supportedLocales.add(walesLocale);

		return supportedLocales;

	}

	@Override
	public Map<Locale, String> getLocaleMap(List<Locale> locales, String value) {

		Map<Locale, String> localeMap = new HashMap<>();

		if (locales != null && !locales.isEmpty()) {
			locales.forEach(locale -> localeMap.put(locale, value));
		}

		return localeMap;

	}

}

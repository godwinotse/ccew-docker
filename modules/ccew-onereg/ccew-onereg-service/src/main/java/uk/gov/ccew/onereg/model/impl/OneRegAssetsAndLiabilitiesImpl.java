package uk.gov.ccew.onereg.model.impl;

import java.math.BigDecimal;

import uk.gov.ccew.onereg.constant.NumberFormatter;
import uk.gov.ccew.onereg.model.OneRegAssetsAndLiabilities;
import uk.gov.ccew.publicdata.model.AssetsAndLiabilities;

public class OneRegAssetsAndLiabilitiesImpl implements OneRegAssetsAndLiabilities {

	private final AssetsAndLiabilities assetsAndLiabilities;

	public OneRegAssetsAndLiabilitiesImpl(AssetsAndLiabilities assetsAndLiabilities) {
		this.assetsAndLiabilities = assetsAndLiabilities;
	}

	@Override
	public BigDecimal getDefinedPensionBenefitTotal() {
		return assetsAndLiabilities.getDefinedPensionBenefitTotal();
	}

	@Override
	public String getDefinedPensionBenefitTotalFormatted() {
		return NumberFormatter.formatUkCurrencyShort(assetsAndLiabilities.getDefinedPensionBenefitTotal());
	}

	@Override
	public BigDecimal getLiabilitiesTotal() {
		return assetsAndLiabilities.getLiabilitiesTotal();
	}

	@Override
	public String getLiabilitiesTotalFormatted() {
		return NumberFormatter.formatUkCurrencyShort(assetsAndLiabilities.getLiabilitiesTotal());
	}

	@Override
	public BigDecimal getLongTermInvestmentTotal() {
		return assetsAndLiabilities.getLongTermInvestmentTotal();
	}

	@Override
	public String getLongTermInvestmentTotalFormatted() {
		return NumberFormatter.formatUkCurrencyShort(assetsAndLiabilities.getLongTermInvestmentTotal());
	}

	@Override
	public BigDecimal getOtherAssetsTotal() {
		return assetsAndLiabilities.getOtherAssetsTotal();
	}

	@Override
	public String getOtherAssetsTotalFormatted() {
		return NumberFormatter.formatUkCurrencyShort(assetsAndLiabilities.getOtherAssetsTotal());
	}

	@Override
	public BigDecimal getOwnUseAssetsTotal() {
		return assetsAndLiabilities.getOwnUseAssetsTotal();
	}

	@Override
	public String getOwnUseAssetsTotalFormatted() {
		return NumberFormatter.formatUkCurrencyShort(assetsAndLiabilities.getOwnUseAssetsTotal());
	}

}

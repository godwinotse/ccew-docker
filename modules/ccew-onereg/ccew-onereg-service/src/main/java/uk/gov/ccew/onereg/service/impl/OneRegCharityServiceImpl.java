package uk.gov.ccew.onereg.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.exception.PortalException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import uk.gov.ccew.onereg.model.OneRegCharity;
import uk.gov.ccew.onereg.model.OneRegCharityAnnualReport;
import uk.gov.ccew.onereg.model.OneRegRegistrationRecord;
import uk.gov.ccew.onereg.model.OneRegRegulatoryAlert;
import uk.gov.ccew.onereg.model.impl.OneRegCharityAnnualReportImpl;
import uk.gov.ccew.onereg.model.impl.OneRegCharityImpl;
import uk.gov.ccew.onereg.model.impl.OneRegRegistrationRecordImpl;
import uk.gov.ccew.onereg.model.impl.OneRegRegulatoryAlertImpl;
import uk.gov.ccew.onereg.service.OneRegCharityService;
import uk.gov.ccew.onereg.service.OneRegUrlService;
import uk.gov.ccew.publicdata.model.Charity;
import uk.gov.ccew.publicdata.service.CharityService;

@Component(immediate = true, service = OneRegCharityService.class)
public class OneRegCharityServiceImpl implements OneRegCharityService {

	@Reference
	private CharityService charityService;

	@Reference
	private OneRegUrlService oneRegUrlService;

	@Override
	public List<OneRegCharity> getCharities(List<Long> organisationNumbers, PortletRequest portletRequest) throws PortalException {

		List<OneRegCharity> charities = new ArrayList<>();

		for (Long organisationNumber : organisationNumbers) {

			OneRegCharity charity = getCharity(organisationNumber, portletRequest);
			charities.add(charity);

		}

		return charities;

	}

	@Override
	public OneRegCharity getCharity(long organisationNumber, PortletRequest portletRequest) throws PortalException {

		Charity charity = charityService.getCharityByOrganisationNumber(organisationNumber);
		String displayUrl = oneRegUrlService.getCharityDetailsUrl(organisationNumber, portletRequest);
		List<OneRegRegulatoryAlert> oneRegRegulatoryAlerts = getOneRegRegulatoryAlerts(charity);
		List<OneRegRegistrationRecord> oneRegRegistrationHistory = getOneRegRegistrationHistory(charity);

		OneRegCharity oneRegCharity = new OneRegCharityImpl(charity);
		oneRegCharity.setDisplayUrl(displayUrl);
		oneRegCharity.setOneRegRegulatoryAlerts(oneRegRegulatoryAlerts);
		oneRegCharity.setOneRegRegistrationHistory(oneRegRegistrationHistory);

		charity.getCharityAnnualReport().ifPresent(charityAnnualReport -> {

			OneRegCharityAnnualReport oneRegCharityAnnualReport = new OneRegCharityAnnualReportImpl(charityAnnualReport);
			oneRegCharity.setOneRegCharityAnnualReport(oneRegCharityAnnualReport);

		});

		return oneRegCharity;

	}

	private List<OneRegRegistrationRecord> getOneRegRegistrationHistory(Charity charity) {
		return charity.getRegistrationHistory().stream().map(OneRegRegistrationRecordImpl::new).collect(Collectors.toList());
	}

	private List<OneRegRegulatoryAlert> getOneRegRegulatoryAlerts(Charity charity) {
		return charity.getRegulatoryAlerts().stream().map(OneRegRegulatoryAlertImpl::new).collect(Collectors.toList());
	}

}

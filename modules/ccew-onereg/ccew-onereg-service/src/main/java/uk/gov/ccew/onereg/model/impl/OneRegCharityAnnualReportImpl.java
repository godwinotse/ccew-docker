package uk.gov.ccew.onereg.model.impl;

import java.util.Date;
import java.util.Optional;

import uk.gov.ccew.onereg.constant.DateFormatter;
import uk.gov.ccew.onereg.model.OneRegCharityAnnualReport;
import uk.gov.ccew.publicdata.model.CharityAnnualReport;
import uk.gov.ccew.publicdata.model.CharityAnnualReportStatus;

public class OneRegCharityAnnualReportImpl implements OneRegCharityAnnualReport {

	private CharityAnnualReport charityAnnualReport;

	public OneRegCharityAnnualReportImpl(CharityAnnualReport charityAnnualReport) {

		this.charityAnnualReport = charityAnnualReport;

	}

	@Override
	public String getSubmittedDateFormatted() {
		return DateFormatter.format_dd_MMMM_YYYY(charityAnnualReport.getSubmittedDate());
	}

	@Override
	public Optional<CharityAnnualReportStatus> getStatus() {
		return charityAnnualReport.getStatus();
	}

	@Override
	public Optional<Date> getSubmittedDate() {
		return charityAnnualReport.getSubmittedDate();
	}

	@Override
	public int getDaysOverdue() {
		return charityAnnualReport.getDaysOverdue();
	}

}

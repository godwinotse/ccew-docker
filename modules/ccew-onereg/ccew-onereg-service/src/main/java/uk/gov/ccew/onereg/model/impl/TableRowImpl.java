package uk.gov.ccew.onereg.model.impl;

import java.util.List;

import uk.gov.ccew.onereg.model.TableCell;
import uk.gov.ccew.onereg.model.TableRow;

public class TableRowImpl implements TableRow {

	private final List<TableCell> cells;

	public TableRowImpl(List<TableCell> cells) {
		this.cells = cells;
	}

	@Override
	public List<TableCell> getCells() {
		return cells;
	}

}

<table class="govuk-table">
	<thead class="govuk-table__head">
		<tr class="govuk-table__row">
			<th scope="col" class="govuk-table__header">
				<liferay-ui:message key="title"/>
			</th>
			<th scope="col" class="govuk-table__header">
				<liferay-ui:message key="reporting-year"/>
			</th>
			<th scope="col" class="govuk-table__header">
				<liferay-ui:message key="date-received"/>
			</th>
			<th scope="col" class="govuk-table__header">
				<liferay-ui:message key="received"/>
			</th>
		</tr>
	</thead>
	<tbody class="govuk-table__body">

		<c:forEach var="annualReturnAndAccountsAndTar" items="${ annualReturnsAndAccountsAndTars }">

			<c:set var="annualReturn" value="${ annualReturnAndAccountsAndTar.getOneRegAnnualReturn().isPresent() ? annualReturnAndAccountsAndTar.getOneRegAnnualReturn().get() : '' }"/>
			<c:set var="accountsAndTar" value="${ annualReturnAndAccountsAndTar.getOneRegAccountsAndTar().isPresent() ? annualReturnAndAccountsAndTar.getOneRegAccountsAndTar().get() : '' }"/>
			<c:set var="primaryRow" value="${ not empty annualReturn && not empty accountsAndTar }"/>

			<c:if test="${ not empty annualReturn }">

				<c:set var="annualReturnOverdue" value="${ annualReturn.daysOverdue > 0 }" />
				<c:set var="annualReturnCssClass" value="${ primaryRow ? 'onereg-table__primary-row ' : '' }" />
				<c:set var="annualReturnCssClass" value="${ annualReturnOverdue ? annualReturnCssClass.concat('onereg-colour__error ') : annualReturnCssClass }" />

				<tr class="govuk-table__row ${ annualReturnCssClass }">
					<td class="govuk-table__cell">
						<liferay-ui:message key="annual-return"/>
					</td>
					<td class="govuk-table__cell">${ annualReturn.reportingYearFormatted }</td>
					<td class="govuk-table__cell">${ annualReturn.receivedDateFormatted }</td>
					<td class="govuk-table__cell">

						<c:choose>
							<c:when test="${ annualReturnOverdue }">

								<liferay-ui:message key="x-days-late" arguments="${ annualReturn.daysOverdue }"/>

							</c:when>
							<c:otherwise>

								<liferay-ui:message key="on-time"/>

							</c:otherwise>
						</c:choose>

					</td>
				</tr>

			</c:if>

			<c:if test="${ not empty accountsAndTar }">

				<c:set var="accountsAndTarOverdue" value="${ accountsAndTar.daysOverdue > 0 }" />
				<c:set var="accountsAndTarCssClass" value="${ accountsAndTarOverdue ? 'onereg-colour__error ' : '' }" />

				<tr class="govuk-table__row ${ accountsAndTarCssClass }">
					<td class="govuk-table__cell">
						<liferay-ui:message key="accounts-and-tar"/>
					</td>
					<td class="govuk-table__cell">
						${ accountsAndTar.reportingYearFormatted }

						<c:if test="${ accountsAndTar.qualifiedAccounts }">

							<span class="onereg-colour__error">
								(<liferay-ui:message key="qualified"/>)
							</span>

						</c:if>

					</td>
					<td class="govuk-table__cell">${ accountsAndTar.receivedDateFormatted }</td>
					<td class="govuk-table__cell">

						<c:choose>
							<c:when test="${ accountsAndTarOverdue }">

								<liferay-ui:message key="x-days-late" arguments="${ accountsAndTar.daysOverdue }"/>

							</c:when>
							<c:otherwise>

								<liferay-ui:message key="on-time"/>

							</c:otherwise>
						</c:choose>

					</td>
				</tr>

			</c:if>

		</c:forEach>
	</tbody>
</table>

<aui:button-row>
	<div class="d-inline-block text-center">
		<a class="govuk-button mb-2" href="${ charityLoginUrl }">
			<liferay-ui:message key="submit-annual-return"/>
		</a>
		<p>
			(<liferay-ui:message key="charity-users-only"/>)
		</p>
	</div>
</aui:button-row>
$(document).on('click', '.charity-register-charity-details .visibility-toggle', function() {

	var contentId = $(this).attr('data-content-id');
	var content = $('#' + contentId);

	if (content.hasClass('d-none')) {

		$(this).text(Liferay.Language.get('less'));

	} else {

		$(this).text(Liferay.Language.get('more'));

	}

	content.toggleClass('d-none');

})
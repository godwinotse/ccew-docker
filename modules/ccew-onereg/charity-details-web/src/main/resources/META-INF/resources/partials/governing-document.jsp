<c:if test="${ not empty oneRegCharity.governingDocument }">

	<c:set var="governingDocument" value="${ oneRegCharity.governingDocument }"/>
	<c:set var="governingDocumentHidden" value="${ governingDocument.length() > 100 }"/>

	<aui:row>
		<aui:col md="11">

			<h3 class="govuk-heading-m">
				<liferay-ui:message key="governing-document"/>
			</h3>

		</aui:col>
		<aui:col md="1">

			<span class="visibility-toggle" data-content-id="content__governing-document">
				<liferay-ui:message key="${ governingDocumentHidden ? 'more' : 'less' }"/>
			</span>

		</aui:col>
	</aui:row>
	<aui:row>
		<aui:col>

			<div class="collapsible-content">
				<p id="content__governing-document" class="${ governingDocumentHidden ? 'd-none' : '' }">${ governingDocument }</p>
			</div>

		</aui:col>
	</aui:row>

</c:if>

<c:if test="${ not empty oneRegCharity.charitableObjects }">

	<c:set var="charitableObjects" value="${ oneRegCharity.charitableObjects }"/>
	<c:set var="charitableObjectsHidden" value="${ charitableObjects.length() > 200 }"/>

	<aui:row>
		<aui:col md="11">

			<h3 class="govuk-heading-m">
				<liferay-ui:message key="charitable-objects"/>
			</h3>

		</aui:col>
		<aui:col md="1">

			<span class="visibility-toggle" data-content-id="content__charitable-objects">
				<liferay-ui:message key="${ charitableObjectsHidden ? 'more' : 'less' }"/>
			</span>

		</aui:col>
	</aui:row>
	<aui:row>
		<aui:col>

			<div class="collapsible-content">
				<p id="content__charitable-objects" class="${ charitableObjectsHidden ? 'd-none' : '' }">${ charitableObjects }</p>
			</div>

		</aui:col>
	</aui:row>

</c:if>

<c:if test="${ not empty oneRegCharity.charityType }">

	<h3 class="govuk-heading-m">
		<liferay-ui:message key="charity-type"/>
	</h3>
	<p>${ oneRegCharity.charityType }</p>

</c:if>

<c:if test="${ not empty oneRegCharity.areaOfBenefit }">

	<h3 class="govuk-heading-m">
		<liferay-ui:message key="area-of-benefit"/>
	</h3>
	<p>${ oneRegCharity.areaOfBenefit }</p>

</c:if>

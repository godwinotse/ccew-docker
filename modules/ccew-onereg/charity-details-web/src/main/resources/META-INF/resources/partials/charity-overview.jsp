<aui:row>
	<aui:col>

		<c:if test="${ not empty oneRegCharity.oneRegRegulatoryAlerts }">

			<h3 class="govuk-heading-m">
				<liferay-ui:message key="regulatory-alerts" />
			</h3>
			<c:if test="${ not empty oneRegCharity.oneRegRegulatoryAlerts }">
				<ul class="govuk-list">
					<c:forEach var="regulatoryAlert" items="${ oneRegCharity.oneRegRegulatoryAlerts }">
						<c:if test="${ regulatoryAlert.getIncidentType().isPresent() }">
							<c:set var="incidentType" value="${ regulatoryAlert.getIncidentType().get() }" />
							<c:choose>
								<c:when test="${ incidentType.inquiryOpened }">

									<li class="govuk-!-margin-bottom-2">
										<liferay-ui:message key="the-charity-commission-opened-an-inquiry-x-into-the-charity-on-x" arguments="${ regulatoryAlert.messageArguments }" />
									</li>

								</c:when>
								<c:when test="${ incidentType.inquiryResultsPublished }">

									<li class="govuk-!-margin-bottom-2">
										<liferay-ui:message key="a-statement-of-the-results-of-an-inquiry-x-published-x" arguments="${ regulatoryAlert.messageArguments }" />
									</li>

								</c:when>
								<c:when test="${ incidentType.warningIssued }">

									<li class="govuk-!-margin-bottom-2">
										<liferay-ui:message key="decision-of-the-charity-commission-date-x-official-warning-issued" arguments="${ regulatoryAlert.messageArguments }" />
									</li>

								</c:when>
								<c:when test="${ incidentType.interimManagerAppointed }">

									<li class="govuk-!-margin-bottom-2">
										<liferay-ui:message key="the-charity-commission-appointed-x-to-act-as-interim-manager-of-the-charity-on-x"
											arguments="${ regulatoryAlert.messageArguments }" />
									</li>

								</c:when>
								<c:when test="${ incidentType.dissolutionNotice }">

									<li class="govuk-!-margin-bottom-2">
										<details class="govuk-details" data-module="govuk-details">
											<summary class="govuk-details__summary">
												<span class="govuk-details__summary-text">
													<liferay-ui:message key="notice-of-dissolution-on-application-of-the-cio" />
												</span>
											</summary>
											<div class="govuk-details__text">
												<liferay-ui:message key="notice-of-dissolution-body" arguments="${ regulatoryAlert.messageArguments }" />
											</div>
										</details>
									</li>

								</c:when>
								<c:when test="${ incidentType.caseReportPublished }">

									<li class="govuk-!-margin-bottom-2">
										<liferay-ui:message key="a-case-report-x-was-published-regarding-the-charity-on-x" arguments="${ regulatoryAlert.messageArguments }" />
									</li>

								</c:when>
								<c:when test="${ incidentType.charityInsolvent }">

									<li class="govuk-!-margin-bottom-2">
										<liferay-ui:message key="charity-is-insolvent" />
									</li>

								</c:when>
								<c:when test="${ incidentType.charityInAdministration }">

									<li class="govuk-!-margin-bottom-2">
										<liferay-ui:message key="charity-is-in-administration" />
									</li>

								</c:when>
							</c:choose>
						</c:if>
					</c:forEach>

				</ul>

			</c:if>

		</c:if>

		<c:if test="${ not empty oneRegCharity.activities }">

			<h3 class="govuk-heading-m govuk-!-padding-top-6">
				<liferay-ui:message key="activities-how-the-charity-spends-its-money" />
			</h3>
			<p>
				${ oneRegCharity.activities }
			</p>

		</c:if>

	</aui:col>
</aui:row>

<c:set var="incomeDataExists" value="${ not empty incomeTable }" />
<c:set var="expenditureDataExists" value="${ not empty expenditureTable }" />

<c:if test="${ incomeDataExists || expenditureDataExists }">

	<aui:row>
		<aui:col>

			<h3 class="govuk-heading-m  govuk-!-padding-top-4">
				<liferay-ui:message key="income-and-expenditure" />
			</h3>

			<p>
				<span class="govuk-!-font-weight-bold">
					<liferay-ui:message key="current-financial-year-end"/>:
				</span>
				${ incomeAndExpenditureSummary.currentFinancialYearEndFormatted }
			</p>

		</aui:col>
	</aui:row>

	<aui:row cssClass="govuk-!-padding-top-4">

		<c:if test="${ incomeDataExists }">

			<aui:col md="6" >
				<div class="text-center govuk-!-font-weight-bold govuk-!-padding-bottom-6">
					<liferay-ui:message key="total-income" />:
					<div>
						${totalCategoryIncome}
					</div>
				</div>

				<div>
					<canvas id="incomeChart" width="250" height="250" aria-label="<liferay-ui:message key="pie-chart-showing-income" />" role="img">
					<liferay-ui:message key="text-alternative-for-this-canvas-graphic-is-in-the-data-table-below" />
					</canvas>
				</div>

				<ccew-onereg:table table="${ incomeTable }" />

			</aui:col>

		</c:if>

		<c:if test="${ expenditureDataExists }">

			<aui:col md="6">
				<div class="text-center govuk-!-font-weight-bold govuk-!-padding-bottom-6">
					<liferay-ui:message key="total-expenditure" />:
					<div>
						${totalCategoryExpenditure}
					</div>
				</div>
				<div>
					<canvas id="expenditureChart" width="250" height="250" aria-label="<liferay-ui:message key="pie-chart-showing-expenditure" />" role="img">
					<liferay-ui:message key="text-alternative-for-this-canvas-graphic-is-in-the-data-table-below" />
					</canvas>
				</div>

				<ccew-onereg:table table="${ expenditureTable }" />

			</aui:col>

		</c:if>
	</aui:row>

	<aui:row>
		<aui:col>
			<p class="govuk-!-font-weight-bold govuk-!-font-size-16">
				<liferay-ui:message key="x-investment-gains-losses" arguments="${ investmentGains }" />
			</p>
			<p class="govuk-!-font-weight-bold govuk-!-font-size-16">
				<c:choose>
					<c:when test="${ incomeAndExpenditureSummary.governmentContractsQty == 0 || incomeAndExpenditureSummary.governmentGrantsQty == 0 }">
						<liferay-ui:message key="total-income-includes-x-from-government-contracts-and-x-from-government-grants"
							arguments="${ incomeAndExpenditureSummary.governmentContractsAndGrantsMessageArguments }" />
					</c:when>
					<c:otherwise>
						<liferay-ui:message key="total-income-includes-x-from-x-government-contracts-and-x-from-x-government-grants"
							arguments="${ incomeAndExpenditureSummary.governmentContractsAndGrantsMessageArguments }" />
					</c:otherwise>
				</c:choose>
			</p>

		</aui:col>
	</aui:row>

</c:if>

<c:if test="${ not empty employeesSummary.noOfPeopleBySalaryRange }">

	<aui:row>
		<aui:col>

			<h3 class="govuk-heading-m govuk-!-margin-top-6 govuk-!-margin-bottom-6">
				<liferay-ui:message key="staff-salaries" />
			</h3>

			<div class="text-center">
				<liferay-ui:message key="staff-earning-over-60000"/>
			</div>
			<div>
				<canvas id="noOfPeopleBySalaryChart" width="300" height="300" aria-label="<liferay-ui:message key="bar-chart-showing-number-of-people-by-salary"/>" role="img">
					<liferay-ui:message key="text-alternative-for-this-canvas-graphic-is-in-the-data-table-below" />
				</canvas>
			</div>

			<ccew-onereg:table table="${ noOfPeopleBySalaryTable }"/>

			<c:if test="${ employeesSummary.employingFormerTrustee }">
				<p>
					<liferay-ui:message key="one-or-more-of-the-employees-were-formerly-trustees" />
				</p>
			</c:if>

			<h3 class="govuk-heading-m govuk-!-margin-top-6">
				<liferay-ui:message key="people" />
			</h3>

			<aui:row>
				<aui:col md="4">

					<img src="/o/charity-details-web/images/people.png" alt="<liferay-ui:message key="graphic-to-accompany-people-data" />" class="img-fluid" />

				</aui:col>
				<aui:col md="8">

					<c:if test="${ employeesSummary.noOfEmployees > 0 }">

						<p>
							<strong>
								<liferay-ui:message key="x-employees" arguments="${ employeesSummary.noOfEmployees }" />
							</strong>
						</p>

					</c:if>

					<c:if test="${ trusteesTotal > 0 }">

						<p>
							<strong>
								<liferay-ui:message key="x-trustees" arguments="${ trusteesTotal }" />
							</strong>
						</p>

					</c:if>

					<c:if test="${ employeesSummary.noOfVolunteers > 0 }">

						<p>
							<strong>
								<liferay-ui:message key="x-volunteers" arguments="${ employeesSummary.noOfVolunteers }" />
							</strong>
						</p>

					</c:if>

				</aui:col>
			</aui:row>
		</aui:col>
	</aui:row>

</c:if>

<aui:row cssClass="govuk-!-margin-top-8">
	<aui:col md="4">

		<div class="govuk-!-font-weight-bold">
			<liferay-ui:message key="fundraising" />
		</div>

		<c:choose>
			<c:when test="${ fundraisingSummary.raisingFundsFromThePublic }">

				<div>
					<liferay-ui:message key="charity-does-raise-funds-from-the-public" />
				</div>

				<c:choose>
					<c:when test="${ fundraisingSummary.workingWithCommercialParticipants && fundraisingSummary.commercialAndProfessionalAgreementInPlace }">

						<div>
							<liferay-ui:message key="works-with-commercial-participants-and-professional-fundraisers-with-agreements-in-place" />
						</div>

					</c:when>
					<c:when test="${ fundraisingSummary.workingWithCommercialParticipants && !fundraisingSummary.commercialAndProfessionalAgreementInPlace }">

						<div>
							<liferay-ui:message key="works-with-commercial-participants-and-professional-fundraisers-without-agreements-in-place" />
						</div>

					</c:when>
					<c:otherwise>

						<div>
							<liferay-ui:message key="does-not-work-with-commercial-participants-and-professional-fundraisers" />
						</div>

					</c:otherwise>
				</c:choose>

			</c:when>
			<c:otherwise>

				<div>
					<liferay-ui:message key="charity-does-not-raise-funds-from-the-public" />
				</div>

			</c:otherwise>
		</c:choose>

	</aui:col>
	<aui:col md="4">

		<div class="govuk-!-font-weight-bold">
			<liferay-ui:message key="trading" />
		</div>

		<c:choose>
			<c:when test="${ oneRegCharity.tradingSubsidiaries }">

				<div>
					<liferay-ui:message key="one-or-more-trustees-receive-payments-and-or-benefits-from-the-charity-for-being-a-trustee" />
				</div>

				<c:if test="${ subsidiaryDirectorTrustee }">

					<div>
						<liferay-ui:message key="one-or-more-trustees-are-also-directors-of-the-subsidiaries" />
					</div>

				</c:if>

			</c:when>
			<c:otherwise>

				<div>
					<liferay-ui:message key="no-trustees-receive-payments-and-or-benefits-from-the-charity-for-being-a-trustee" />
				</div>

			</c:otherwise>
		</c:choose>

	</aui:col>
	<aui:col md="4">

		<div class="govuk-!-font-weight-bold">
			<liferay-ui:message key="trustee-payments" />
		</div>

		<c:choose>
			<c:when test="${ trusteeReceivesPaymentFromCharity }">

				<div>
					<liferay-ui:message key="one-or-more-trustees-receive-payments-and-or-benefits-from-the-charity-for-being-a-trustee" />
				</div>

			</c:when>
			<c:otherwise>

				<div>
					<liferay-ui:message key="no-trustees-receive-payments-and-or-benefits-from-the-charity-for-being-a-trustee" />
				</div>

			</c:otherwise>
		</c:choose>

	</aui:col>
</aui:row>


<aui:script use="uk-gov-ccew-onereg-chart">

	A.OneRegChart.initHorizontalBarChart('noOfPeopleBySalaryChart', ${ noOfPeopleBySalaryChart.dataJs }, {
		legend: {
			display: false
		}
	});

	A.OneRegChart.initPieChart('incomeChart', ${ incomeChart.dataJs }, {
		legend: {
			display: false
		}
	});

	A.OneRegChart.initPieChart('expenditureChart', ${ expenditureChart.dataJs }, {
		legend: {
			display: false
		}
	});

</aui:script>
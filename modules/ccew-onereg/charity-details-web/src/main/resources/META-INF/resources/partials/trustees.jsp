<aui:row>
	<aui:col>
		<div>
			<liferay-journal:journal-article groupId="${ scopeGroupId }" articleId="<%= OneRegWebContent.TRUSTEES_INTRO.getArticleId() %>"/>
		</div>
	</aui:col>
</aui:row>

<c:choose>
	<c:when test="${ not empty trusteesAndOtherCharities }">

		<aui:row>
			<aui:col>

				<table class="table">
					<caption class="heading-small">
						<liferay-ui:message key="x-trustees" arguments="${ trusteesAndOtherCharities.size() }"/>
					</caption>
					<thead>
						<tr>
							<th scope="col">
								<liferay-ui:message key="name"/>
							</th>
							<th scope="col">
								<liferay-ui:message key="role"/>
							</th>
							<th scope="col">
								<liferay-ui:message key="date-of-appointment"/>
							</th>
							<th scope="col">
								<liferay-ui:message key="other-trusteeships"/>
							</th>
							<th scope="col">
								<liferay-ui:message key="reporting-status-of-associated-charities"/>
							</th>
						</tr>

						<c:forEach var="trusteeAndOtherCharities" items="${ trusteesAndOtherCharities }">

							<c:set var="trustee" value="${ trusteeAndOtherCharities.key }"/>
							<c:set var="otherCharities" value="${ trusteeAndOtherCharities.value }"/>
							<c:set var="rowspan" value="${ otherCharities.size() > 0 ? otherCharities.size() + 1 : 1 }"/>

							<tr>
								<td rowspan="${ rowspan }">${ trustee.name }</td>

								<c:set var="trusteeRole" value="${ trustee.getRole().isPresent() ? trustee.getRole().get() : '' }"/>
								<c:choose>
									<c:when test="${ not empty trusteeRole && trusteeRole.chair }">
										<td rowspan="${ rowspan }">
											<liferay-ui:message key="chair"/>
										</td>
									</c:when>
									<c:when test="${ not empty trusteeRole && trusteeRole.trustee }">
										<td rowspan="${ rowspan }">
											<liferay-ui:message key="trustee"/>
										</td>
									</c:when>
								</c:choose>

								<td rowspan="${ rowspan }">
									${ trustee.dateOfAppointmentFormatted }
								</td>

								<c:forEach var="otherCharity" items="${ otherCharities }">
									<c:set var="charityAnnualReport" value="${ otherCharity.getOneRegCharityAnnualReport().isPresent() ? otherCharity.getOneRegCharityAnnualReport().get() : '' }"/>
									<tr>
										<td>
											<a href="${ otherCharity.displayUrl }">${ otherCharity.charityName }</a>
										</td>
										<td>

											<c:if test="${ not empty charityAnnualReport }">
												<c:set var="charityAnnualReportStatus" value="${ charityAnnualReport.getStatus().isPresent() ? charityAnnualReport.getStatus().get() : '' }"/>
												<c:choose>
													<c:when test="${ not empty charityAnnualReportStatus && charityAnnualReportStatus.overdue }">
														<span class="onereg-colour__error">
															<liferay-ui:message key="x-days-late" arguments="${ charityAnnualReport.daysOverdue }"/>
														</span>
													</c:when>
													<c:when test="${ not empty charityAnnualReportStatus && charityAnnualReportStatus.upToDate }">
														<liferay-ui:message key="on-time"/>
													</c:when>
												</c:choose>
											</c:if>
										</td>
									</tr>
								</c:forEach>

							</tr>

						</c:forEach>

					</thead>
				</table>
			</aui:col>
		</aui:row>

	</c:when>
	<c:otherwise>

		<aui:row>
			<aui:col md="12">
				<span>
					<liferay-ui:message key="there-is-no-trustee-information-available-for-this-charity"/>
				</span>
			</aui:col>
		</aui:row>

	</c:otherwise>
</c:choose>

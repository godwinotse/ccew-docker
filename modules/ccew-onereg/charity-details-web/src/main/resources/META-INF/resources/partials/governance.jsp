<aui:row cssClass="govuk-!-margin-bottom-2">
	<aui:col md="4">
		<span class="govuk-!-font-weight-bold govuk-!-font-size-19">
			<liferay-ui:message key="registration-history" />:
		</span>
	</aui:col>
	<aui:col md="8">
		<ul class="govuk-list">
			<c:forEach var="record" items="${ oneRegCharity.oneRegRegistrationHistory }">
				<li>${ record.registrationDateFormatted } : ${ record.registrationAction }</li>
			</c:forEach>
		</ul>
	</aui:col>
</aui:row>
<aui:row cssClass="govuk-!-margin-bottom-6">
	<aui:col md="4">
		<span class="govuk-!-font-weight-bold govuk-!-font-size-19">
			<liferay-ui:message key="organisation-type" />:
		</span>
	</aui:col>
	<aui:col md="8">
		${ oneRegCharity.organisationType }
	</aui:col>
</aui:row>
<aui:row cssClass="govuk-!-margin-bottom-2">
	<aui:col md="4">
		<span class="govuk-!-font-weight-bold govuk-!-font-size-19">
			<liferay-ui:message key="other-names" />:
		</span>
	</aui:col>
	<aui:col md="8">
		<c:choose>
			<c:when test="${ not empty oneRegCharity.otherCharityNames }">
				<ul class="govuk-list">
					<c:forEach var="otherCharityName" items="${ oneRegCharity.otherCharityNames }">
						<li>${ otherCharityName }</li>
					</c:forEach>
				</ul>
			</c:when>
			<c:otherwise>
				<liferay-ui:message key="no-other-names" />
			</c:otherwise>
		</c:choose>
	</aui:col>
</aui:row>

<c:if test="${ not empty oneRegCharity.companyNumber }">

	<aui:row cssClass="govuk-!-margin-bottom-6">
		<aui:col md="4">
			<span class="govuk-!-font-weight-bold govuk-!-font-size-19">
				<liferay-ui:message key="company-number" />:
			</span>
		</aui:col>
		<aui:col md="8">
			<a href="${ companyBaseUrl }/${ oneRegCharity.companyNumber }" target="_blank" class="govuk-link">${ oneRegCharity.companyNumber }</a>
		</aui:col>
	</aui:row>

</c:if>


<c:if test="${ oneRegCharity.getGiftAidStatus().isPresent() }">
	<c:set var="giftAidStatus" value="${ oneRegCharity.getGiftAidStatus().get() }" />

	<aui:row cssClass="govuk-!-margin-bottom-6">
		<aui:col md="4">
			<span class="govuk-!-font-weight-bold govuk-!-font-size-19">
				<liferay-ui:message key="gift-aid" />:
			</span>
		</aui:col>
		<aui:col md="8">
			<c:choose>
				<c:when test="${ giftAidStatus.claimsGiftAid() }">
					<liferay-ui:message key="recognised-by-hmrc-for-gift-aid" />
				</c:when>
				<c:otherwise>
					<liferay-ui:message key="not-recognised-by-hmrc-for-gift-aid" />
				</c:otherwise>
			</c:choose>
		</aui:col>
	</aui:row>

</c:if>

<c:if test="${ not empty oneRegCharity.otherRegulators }">

	<aui:row cssClass="govuk-!-margin-bottom-2">
		<aui:col md="4">
			<span class="govuk-!-font-weight-bold govuk-!-font-size-19">
				<liferay-ui:message key="other-regulators" />:
			</span>
		</aui:col>
		<aui:col md="8">
			<ul class="govuk-list">
				<c:forEach var="otherRegulator" items="${ oneRegCharity.otherRegulators }">
					<li>${ otherRegulator }</li>
				</c:forEach>
			</ul>
		</aui:col>
	</aui:row>

</c:if>

<c:if test="${ not empty oneRegCharity.policies }">

	<aui:row cssClass="govuk-!-margin-bottom-2">
		<aui:col md="4">
			<span class="govuk-!-font-weight-bold govuk-!-font-size-19">
				<liferay-ui:message key="policies" />:
			</span>
		</aui:col>
		<aui:col md="8">
			<ul class="govuk-list">
				<c:forEach var="policy" items="${ oneRegCharity.policies }">
					<li>${ policy }</li>
				</c:forEach>
			</ul>
		</aui:col>
	</aui:row>

</c:if>

<aui:row cssClass="govuk-!-margin-bottom-">
	<aui:col md="4">
		<span class="govuk-!-font-weight-bold govuk-!-font-size-19">
			<liferay-ui:message key="land-and-property" />:
		</span>
	</aui:col>
	<aui:col md="8">
		<c:choose>
			<c:when test="${ oneRegCharity.landAndProperty }">
				<liferay-ui:message key="this-charity-owns-and-or-leases-land-or-property" />
			</c:when>
			<c:otherwise>
				<liferay-ui:message key="this-charity-does-not-own-and-or-lease-land-or-property" />
			</c:otherwise>
		</c:choose>
	</aui:col>
</aui:row>

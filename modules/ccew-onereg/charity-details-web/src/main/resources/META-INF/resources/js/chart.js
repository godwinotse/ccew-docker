AUI.add(

	'uk-gov-ccew-onereg-chart',

	function (A) {

		var BAR_CHART_DEFAULT_OPTIONS = {

			responsive: true,
			maintainAspectRatio: false,
			legend: {
				position: 'bottom',
				align: 'start'
			},
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true,
						callback: formatYAxes
					}
				}],
				xAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			},
			tooltips: {
				callbacks: {
					label: formatToolTip
				}
			}

		};

		var PIE_CHART_DEFAULT_OPTIONS = {

			responsive: true,
			maintainAspectRatio: false,
			tooltips: {
				callbacks: {
					label: formatToolTip
				}
			}

		};

		function formatYAxes(value, index, values) {

			var formattedValue;

			if (isNaN(value)) {

				formattedValue = value;

			} else {

				formattedValue = new Intl.NumberFormat('en-GB', {
					style: 'currency',
					currency: 'GBP',
					minimumFractionDigits: 0,
				}).format(value);

			}

			return formattedValue;

		}

		function formatToolTip(tooltipItem, data) {

			var label = data.datasets[tooltipItem.datasetIndex].label || data.labels[tooltipItem.index] || '';
			var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] || '';

			if (isNaN(value)) {

				label = value;

			} else {

				if (label) {
					label += ': ';
				}

				var currencyFormatter = new Intl.NumberFormat('en-GB', {
					style: 'currency',
					currency: 'GBP',
				});

				label += currencyFormatter.format(value);

			}

			return label;

		}

		var OneRegChart = {

			initChart: function (canvasId, chartType, data, options) {

				$.getScript('/o/ccew-chart-theme-contributor/js/Chart.bundle.js', function () {

					var ctx = document.getElementById(canvasId);

					var myChart = new Chart(ctx, {
						type: chartType,
						data: data,
						options: options
					});

				});

			},

			initBarChart: function (canvasId, data, options) {

				var combinedOptions = $.extend(true, BAR_CHART_DEFAULT_OPTIONS, options);

				OneRegChart.initChart(canvasId, 'bar', data, combinedOptions);

			},

			initHorizontalBarChart: function (canvasId, data, options) {

				var combinedOptions = $.extend(true, BAR_CHART_DEFAULT_OPTIONS, options);

				OneRegChart.initChart(canvasId, 'horizontalBar', data, combinedOptions);

			},

			initPieChart: function (canvasId, data, options) {

				var combinedOptions = $.extend(true, PIE_CHART_DEFAULT_OPTIONS, options);

				OneRegChart.initChart(canvasId, 'pie', data, combinedOptions);

			}

		};

		A.OneRegChart = OneRegChart;
	},
	'',
	{
		requires: ['node-base']
	}

);





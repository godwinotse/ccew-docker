<c:if test="${ oneRegCharity.grantMaking }">

	<aui:row cssClass="govuk-!-margin-bottom-4">
		<aui:col md="4"></aui:col>
		<aui:col md="8">
			<span class="govuk-!-font-weight-bold">
				<liferay-ui:message key="primary-purpose-is-grant-making" />
			</span>
		</aui:col>
	</aui:row>

</c:if>

<c:if test="${ not empty oneRegCharity.whatTheCharityDoes }">

	<aui:row>
		<aui:col md="4" cssClass="govuk-!-font-weight-bold">
			<liferay-ui:message key="what-the-charity-does" />:
		</aui:col>
		<aui:col md="8">
			<ul class="govuk-list">
				<c:forEach var="whatTheCharityDoesItem" items="${ oneRegCharity.whatTheCharityDoes }">
					<li>${ whatTheCharityDoesItem }</li>
				</c:forEach>
			</ul>
		</aui:col>
	</aui:row>

</c:if>

<c:if test="${ not empty oneRegCharity.whoTheCharityHelps }">

	<aui:row>
		<aui:col md="4" cssClass="govuk-!-font-weight-bold">
			<liferay-ui:message key="who-the-charity-helps" />:
		</aui:col>
		<aui:col md="8">
			<ul class="govuk-list">
				<c:forEach var="whoTheCharityHelpsItem" items="${ oneRegCharity.whoTheCharityHelps }">
					<li>${ whoTheCharityHelpsItem }</li>
				</c:forEach>
			</ul>
		</aui:col>
	</aui:row>

</c:if>

<c:if test="${ not empty oneRegCharity.howTheCharityHelps }">

	<aui:row>
		<aui:col md="4" cssClass="govuk-!-font-weight-bold">
			<liferay-ui:message key="how-the-charity-helps" />:
		</aui:col>
		<aui:col md="8">
			<ul class="govuk-list">
				<c:forEach var="howTheCharityHelpsItem" items="${ oneRegCharity.howTheCharityHelps }">
					<li>${ howTheCharityHelpsItem }</li>
				</c:forEach>
			</ul>
		</aui:col>
	</aui:row>

</c:if>

<c:if test="${ not empty oneRegCharity.locations }">

	<aui:row>
		<aui:col md="4" cssClass="pr-0 govuk-!-font-weight-bold">
			<liferay-ui:message key="where-the-charity-operates" />:
		</aui:col>
		<aui:col md="8">
			<ul class="govuk-list">
				<c:forEach var="location" items="${ oneRegCharity.locations }">
					<li>${ location }</li>
				</c:forEach>
			</ul>
		</aui:col>
	</aui:row>
	
</c:if>
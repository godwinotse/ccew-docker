<%@ include file="init.jsp" %>

<aui:container cssClass="onereg__container">
	<aui:row cssClass="onereg__header mb-3">
		<aui:col md="7" cssClass="col-md-offset-2 py-3">

			<%@ include file="partials/header.jsp" %>

		</aui:col>
	</aui:row>
	<aui:row>
		<aui:col md="7" cssClass="col-md-offset-2">
			<aui:container cssClass="onereg__main">
				<aui:row>
					<aui:col md="2" cssClass="onereg__nav">

						<%@ include file="partials/nav-panel.jsp" %>

					</aui:col>
					<aui:col md="10" cssClass="onereg__body">

						<c:choose>
							<c:when test="${ empty mvcRenderCommandName || mvcRenderCommandName eq '/charity-overview' }">
								<%@ include file="partials/charity-overview.jsp" %>
							</c:when>
							<c:when test="${ mvcRenderCommandName eq '/what-who-how-where' }">
								<%@ include file="partials/what-who-how-where.jsp" %>
							</c:when>
							<c:when test="${ mvcRenderCommandName eq '/governance' }">
								<%@ include file="partials/governance.jsp" %>
							</c:when>
							<c:when test="${ mvcRenderCommandName eq '/trustees' }">
								<%@ include file="partials/trustees.jsp" %>
							</c:when>
							<c:when test="${ mvcRenderCommandName eq '/financial-history' }">
								<%@ include file="partials/financial-history.jsp" %>
							</c:when>
							<c:when test="${ mvcRenderCommandName eq '/assets-and-liabilities' }">
								<%@ include file="partials/assets-and-liabilities.jsp" %>
							</c:when>
							<c:when test="${ mvcRenderCommandName eq '/accounts-and-annual-returns' }">
								<%@ include file="partials/accounts-and-annual-returns.jsp" %>
							</c:when>
							<c:when test="${ mvcRenderCommandName eq '/governing-document' }">
								<%@ include file="partials/governing-document.jsp" %>
							</c:when>
							<c:when test="${ mvcRenderCommandName eq '/contact-and-location' }">
								<%@ include file="partials/contact-and-location.jsp" %>
							</c:when>
						</c:choose>

					</aui:col>
				</aui:row>
			</aui:container>
		</aui:col>
	</aui:row>
</aui:container>
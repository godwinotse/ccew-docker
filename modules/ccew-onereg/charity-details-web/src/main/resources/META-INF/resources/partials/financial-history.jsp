<h3>
	<liferay-ui:message key="financial-history" />
</h3>

<c:choose>
	<c:when test="${ empty webContentId }">

		<div>
			<canvas id="financialHistoryChart" width="600" height="400" aria-label="<liferay-ui:message key=" financial-history-bar-chart-showing-income-vs-expenditure" />">" role="img">
			<liferay-ui:message key="text-alternative-for-this-canvas-graphic-is-in-the-data-table-below" />
			</canvas>
		</div>
		<h5 class="text-center py-2">
			<liferay-ui:message key="financial-period" />
		</h5>
		<ccew-onereg:table table="${ incomeTable }" />

	</c:when>
	<c:otherwise>

		<liferay-journal:journal-article groupId="${ scopeGroupId }" articleId="${ webContentId }" />

	</c:otherwise>
</c:choose>

<aui:script use="uk-gov-ccew-onereg-chart">

	A.OneRegChart.initBarChart('financialHistoryChart', ${ financialHistoryChart.dataJs }, {
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				stacked: true,
				ticks: {
					maxTicksLimit: 6
				}
			}]
		}
	});

</aui:script>
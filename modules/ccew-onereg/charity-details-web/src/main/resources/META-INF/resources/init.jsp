<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/journal" prefix="liferay-journal" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://gov.uk.ccew/tld/ccew-onereg-taglib" prefix="ccew-onereg"%>

<%@ page import="uk.gov.ccew.onereg.charitydetails.web.constant.MVCCommandKeys" %>
<%@ page import="uk.gov.ccew.onereg.constant.OneRegWebContent" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<c:set var="mvcRenderCommandName" value="${ param.mvcRenderCommandName }"/>

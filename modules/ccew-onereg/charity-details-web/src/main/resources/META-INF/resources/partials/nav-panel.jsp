<ul class="govuk-list">

	<c:forEach var="navEntry" items="${ navEntries }">

		<portlet:renderURL var="navEntryURL">
			<portlet:param name="mvcRenderCommandName" value="${ navEntry.mvcRenderCommandName }" />
			<portlet:param name="organisationNumber" value="${ oneRegCharity.organisationNumber }" />
		</portlet:renderURL>

		<c:set var="navLinkCssClass" value="${ empty mvcRenderCommandName || mvcRenderCommandName eq navEntry.mvcRenderCommandName ? 'onereg__link-selected' : '' }" />

		<li class="mb-3">
			<a href="${ navEntryURL }" class="govuk-link ${ navLinkCssClass }">
				<liferay-ui:message key="${ navEntry.labelKey }" />
			</a>
		</li>

	</c:forEach>

</ul>
<h3>
	<liferay-ui:message key="assets-and-liabilities"/>
</h3>

<div>
	<canvas id="assetsAndLiabilitiesChart" width="600" height="400" aria-label="<liferay-ui:message key="assets-and-liabilities-bar-chart"/>">" role="img">
		<liferay-ui:message key="text-alternative-for-this-canvas-graphic-is-in-the-data-table-below"/>
	</canvas>
</div>

<ccew-onereg:table table="${ assetsAndLiabilitiesTable }"/>

<aui:script use="uk-gov-ccew-onereg-chart">

	A.OneRegChart.initBarChart('assetsAndLiabilitiesChart', ${ assetsAndLiabilitiesChart.dataJs }, {
		scales: {
			xAxes: [{
				stacked: false
			}],
			yAxes: [{
				stacked: false,
				ticks: {
					maxTicksLimit: 11
				}
			}]
		}
	});

</aui:script>

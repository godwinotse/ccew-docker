<aui:container cssClass="d-flex flex-column align-items-stretch h-100">
	<aui:row cssClass="flex-fill">
		<aui:col>

			<h2 class="govuk-heading-l">${ oneRegCharity.charityName }</h>

		</aui:col>
	</aui:row>

	<aui:row cssClass="flex-fill align-content-end govuk-!-font-size-24">
		<aui:col md="6" cssClass="align-self-end">

			<span>
				<liferay-ui:message key="charity-number"/>: ${ oneRegCharity.charityNumberFormatted }
			</span>

		</aui:col>
		<aui:col md="6">

			<c:if test="${ not empty oneRegCharity.regulatoryAlerts }">

				<aui:row cssClass="mb-2">
					<aui:col lg="2" cssClass="govuk-!-font-size-27 govuk-!-font-weight-bold">

						<img src="/o/charity-details-web/images/exclamation.png" />

					</aui:col>
					<aui:col lg="10">

						<liferay-ui:message key="regulatory-alert"/>

					</aui:col>
				</aui:row>

			</c:if>

			<c:set var="charityStatus" value="${ oneRegCharity.getCharityStatus().isPresent() ? oneRegCharity.getCharityStatus().get() : '' }"/>
			<c:set var="charityAnnualReport" value="${ oneRegCharity.getOneRegCharityAnnualReport().isPresent() ? oneRegCharity.getOneRegCharityAnnualReport().get() : '' }"/>

			<c:if test="${ not empty charityStatus }">

				<aui:row>

					<c:choose>
						<c:when test="${ charityStatus.linked }">

							<aui:col lg="10" cssClass="col-md-offset-2">
								<liferay-ui:message key="linked-charity"/>
							</aui:col>

						</c:when>
						<c:when test="${ charityStatus.removed }">

							<aui:col lg="10" cssClass="col-md-offset-2">
								<liferay-ui:message key="removed"/>
							</aui:col>

						</c:when>
						<c:when test="${ empty charityAnnualReport && charityStatus.recentlyRegistered }">

							<aui:col lg="10" cssClass="col-md-offset-2">
								<liferay-ui:message key="recently-registered"/>
							</aui:col>

						</c:when>
						<c:otherwise>

							<c:if test="${ not empty charityAnnualReport }">

								<c:set var="charityAnnualReportStatus" value="${ charityAnnualReport.getStatus().isPresent() ? charityAnnualReport.getStatus().get() : '' }"/>

								<c:choose>
									<c:when test="${ not empty charityAnnualReportStatus && charityAnnualReportStatus.upToDate }">

										<aui:col lg="2">
											<img src="/o/charity-details-web/images/success.png" />
										</aui:col>
										<aui:col lg="10">

											<div>
												<liferay-ui:message key="charity-reporting-is-up-to-date"/>
											</div>
											<div>
												<liferay-ui:message key="received"/> ${ charityAnnualReport.submittedDateFormatted }
											</div>

										</aui:col>

									</c:when>
									<c:when test="${ not empty charityAnnualReportStatus && charityAnnualReportStatus.overdue }">

										<aui:col lg="2">
											<img src="/o/charity-details-web/images/warning.png" />
										</aui:col>
										<aui:col lg="10">

											<div>
												<liferay-ui:message key="charity-reporting-is-not-up-to-date"/>
											</div>
											<div>
												<liferay-ui:message key="due-on"/> ${ charityAnnualReport.submittedDateFormatted }
											</div>

										</aui:col>

									</c:when>
								</c:choose>

							</c:if>

						</c:otherwise>
					</c:choose>

				</aui:row>

			</c:if>

		</aui:col>
	</aui:row>
</aui:container>

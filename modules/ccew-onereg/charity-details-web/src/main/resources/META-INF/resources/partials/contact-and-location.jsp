<c:if test="${ not empty oneRegCharity.address }">
	<aui:row>
		<aui:col md="4" cssClass="onereg__body__label">
			<liferay-ui:message key="address" />:
		</aui:col>
		<aui:col md="8">
			${ oneRegCharity.address }
		</aui:col>
	</aui:row>
</c:if>

<c:if test="${ not empty oneRegCharity.phone }">
	<aui:row>
		<aui:col md="4" cssClass="onereg__body__label">
			<liferay-ui:message key="phone" />:
		</aui:col>
		<aui:col md="8">
			${ oneRegCharity.phone }
		</aui:col>
	</aui:row>
</c:if>

<c:if test="${ not empty oneRegCharity.email }">
	<aui:row>
		<aui:col md="4" cssClass="onereg__body__label">
			<liferay-ui:message key="email" />:
		</aui:col>
		<aui:col md="8">
			<a href="mailto: ${ oneRegCharity.email }"> ${ oneRegCharity.email }</a>
		</aui:col>
	</aui:row>
</c:if>

<c:if test="${ not empty oneRegCharity.website }">
	<aui:row>
		<aui:col md="4" cssClass="onereg__body__label">
			<liferay-ui:message key="website" />:
		</aui:col>
		<aui:col md="8">
			<a href=" ${ oneRegCharity.website }" target="_blank">${ oneRegCharity.website }</a>
		</aui:col>
	</aui:row>
</c:if>
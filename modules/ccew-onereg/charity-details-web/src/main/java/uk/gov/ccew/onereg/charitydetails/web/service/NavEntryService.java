package uk.gov.ccew.onereg.charitydetails.web.service;

import uk.gov.ccew.onereg.charitydetails.web.constant.MVCCommandKeys;
import uk.gov.ccew.onereg.charitydetails.web.model.NavEntry;

import java.util.ArrayList;
import java.util.List;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = NavEntryService.class)
public class NavEntryService {

	private static final List<NavEntry> NAV_ENTRIES = new ArrayList<>();

	static {
		NAV_ENTRIES.add(new NavEntry("charity-overview", MVCCommandKeys.CHARITY_OVERVIEW));
		NAV_ENTRIES.add(new NavEntry("what-who-how-where", MVCCommandKeys.WHAT_WHO_HOW_WHERE));
		NAV_ENTRIES.add(new NavEntry("governance", MVCCommandKeys.GOVERNANCE));
		NAV_ENTRIES.add(new NavEntry("trustees", MVCCommandKeys.TRUSTEES));
		NAV_ENTRIES.add(new NavEntry("financial-history", MVCCommandKeys.FINANCIAL_HISTORY));
		NAV_ENTRIES.add(new NavEntry("assets-and-liabilities", MVCCommandKeys.ASSETS_AND_LIABILITIES));
		NAV_ENTRIES.add(new NavEntry("accounts-and-annual-returns", MVCCommandKeys.ACCOUNTS_AND_ANNUAL_RETURNS));
		NAV_ENTRIES.add(new NavEntry("governing-document", MVCCommandKeys.GOVERNING_DOCUMENT));
		NAV_ENTRIES.add(new NavEntry("contact-and-location", MVCCommandKeys.CONTACT_AND_LOCATION));
	}

	public List<NavEntry> getNavEntries() {

		return NAV_ENTRIES;

	}

}

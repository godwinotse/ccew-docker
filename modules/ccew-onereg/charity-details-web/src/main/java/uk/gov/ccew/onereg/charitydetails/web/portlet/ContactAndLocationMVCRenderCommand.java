package uk.gov.ccew.onereg.charitydetails.web.portlet;

import uk.gov.ccew.onereg.charitydetails.web.constant.MVCCommandKeys;
import uk.gov.ccew.onereg.constant.PortletKeys;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.CHARITY_DETAILS, "mvc.command.name=" + MVCCommandKeys.CONTACT_AND_LOCATION }, service = MVCRenderCommand.class)
public class ContactAndLocationMVCRenderCommand implements MVCRenderCommand {

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) {

		return "/view.jsp";

	}

}

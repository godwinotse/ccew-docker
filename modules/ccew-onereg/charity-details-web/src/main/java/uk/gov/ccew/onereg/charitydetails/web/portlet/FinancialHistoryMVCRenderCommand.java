package uk.gov.ccew.onereg.charitydetails.web.portlet;

import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import uk.gov.ccew.onereg.charitydetails.web.constant.MVCCommandKeys;
import uk.gov.ccew.onereg.constant.OneRegWebContent;
import uk.gov.ccew.onereg.constant.PortletKeys;
import uk.gov.ccew.onereg.constant.RequestKeys;
import uk.gov.ccew.onereg.model.Chart;
import uk.gov.ccew.onereg.model.OneRegCharity;
import uk.gov.ccew.onereg.model.OneRegFinancialAnnualReport;
import uk.gov.ccew.onereg.model.Table;
import uk.gov.ccew.onereg.service.OneRegFinancialService;
import uk.gov.ccew.publicdata.model.CharityStatus;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.CHARITY_DETAILS, "mvc.command.name=" + MVCCommandKeys.FINANCIAL_HISTORY }, service = MVCRenderCommand.class)
public class FinancialHistoryMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private OneRegFinancialService oneRegFinancialService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) {

		OneRegCharity charity = (OneRegCharity) renderRequest.getAttribute(RequestKeys.ONE_REG_CHARITY);
		String webContentId = getWebContentId(charity.getCharityStatus().get());

		if (webContentId == null) {
			long organisationNumber = charity.getOrganisationNumber();
			List<OneRegFinancialAnnualReport> financialHistory = oneRegFinancialService.getFinancialHistory(organisationNumber);

			Chart financialHistoryChart = oneRegFinancialService.getFinancialHistoryChart(financialHistory);
			Table incomeTable = oneRegFinancialService.getSpendingTable(financialHistory);

			renderRequest.setAttribute(RequestKeys.FINANCIAL_HISTORY_CHART, financialHistoryChart);
			renderRequest.setAttribute(RequestKeys.INCOME_TABLE, incomeTable);

			if (financialHistoryChart.getDataSets().isEmpty()) {
				webContentId = OneRegWebContent.FINANCIAL_HISTORY_NO_HISTORY.getArticleId();
			}
		}

		renderRequest.setAttribute(RequestKeys.WEB_CONTENT_ID, webContentId);

		return "/view.jsp";

	}

	private String getWebContentId(CharityStatus status) {
		OneRegWebContent result;
		switch (status) {
		case LINKED:
			result = OneRegWebContent.FINANCIAL_HISTORY_LINKED_CHARITY;
			break;
		case RECENTLY_REGISTERED:
			result = OneRegWebContent.FINANCIAL_HISTORY_NEW_CHARITY;
			break;
		case REMOVED:
			result = OneRegWebContent.FINANCIAL_HISTORY_REMOVED;
			break;
		default:
		    result = null;
			break;
		}
		return (result == null) ? null : result.getArticleId();
	}

}

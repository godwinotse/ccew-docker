package uk.gov.ccew.onereg.charitydetails.web.portlet;

import uk.gov.ccew.onereg.charitydetails.web.constant.MVCCommandKeys;
import uk.gov.ccew.onereg.constant.PortletKeys;
import uk.gov.ccew.onereg.constant.RequestKeys;
import uk.gov.ccew.onereg.model.OneRegCharity;
import uk.gov.ccew.onereg.model.OneRegTrustee;
import uk.gov.ccew.onereg.service.OneRegCharityService;
import uk.gov.ccew.onereg.service.OneRegTrusteeService;
import uk.gov.ccew.publicdata.model.Trustee;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.CHARITY_DETAILS, "mvc.command.name=" + MVCCommandKeys.TRUSTEES }, service = MVCRenderCommand.class)
public class TrusteesMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private OneRegCharityService oneRegCharityService;

	@Reference
	private OneRegTrusteeService oneRegTrusteeService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		OneRegCharity charity = (OneRegCharity) renderRequest.getAttribute(RequestKeys.ONE_REG_CHARITY);

		try {

			List<OneRegTrustee> trustees = oneRegTrusteeService.getTrustees(charity.getCharityNumber(), renderRequest);
			Map<Trustee, List<OneRegCharity>> trusteesAndOtherCharities = new LinkedHashMap<>();

			for (OneRegTrustee trustee : trustees) {

				List<Long> otherTrusteeships = trustee.getOtherCharityOrganisationNumbers();
				List<OneRegCharity> charities = oneRegCharityService.getCharities(otherTrusteeships, renderRequest);
				trusteesAndOtherCharities.put(trustee, charities);

			}

			renderRequest.setAttribute(RequestKeys.TRUSTEES_AND_OTHER_CHARITIES, trusteesAndOtherCharities);

			return "/view.jsp";

		} catch (PortalException e) {

			throw new PortletException(e);

		}

	}

}

package uk.gov.ccew.onereg.charitydetails.web.portlet;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;

import uk.gov.ccew.onereg.charitydetails.web.constant.MVCCommandKeys;
import uk.gov.ccew.onereg.constant.PortletKeys;
import uk.gov.ccew.onereg.service.ExternalSiteUrlsConfigurationUtil;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.CHARITY_DETAILS,
		"mvc.command.name=" + MVCCommandKeys.GOVERNANCE }, configurationPolicy = ConfigurationPolicy.OPTIONAL, service = MVCRenderCommand.class)
public class GovernanceMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private ExternalSiteUrlsConfigurationUtil externalSiteUrlsConfigurationUtil;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) {

		String companyBaseUrl = externalSiteUrlsConfigurationUtil.getCompanyHouseBaseUrl();

		renderRequest.setAttribute("companyBaseUrl", companyBaseUrl);

		return "/view.jsp";

	}

}

package uk.gov.ccew.onereg.charitydetails.web.model;

public class NavEntry {

	public final String labelKey;

	public final String mvcRenderCommandName;

	public NavEntry(String labelKey, String mvcRenderCommandName) {

		this.labelKey = labelKey;
		this.mvcRenderCommandName = mvcRenderCommandName;

	}

	public String getLabelKey() {
		return labelKey;
	}

	public String getMvcRenderCommandName() {
		return mvcRenderCommandName;
	}

}

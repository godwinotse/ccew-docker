package uk.gov.ccew.onereg.charitydetails.web.portlet;

import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;

import uk.gov.ccew.onereg.charitydetails.web.constant.MVCCommandKeys;
import uk.gov.ccew.onereg.constant.PortletKeys;
import uk.gov.ccew.onereg.constant.RequestKeys;
import uk.gov.ccew.onereg.model.OneRegAnnualReturnAndAccountsAndTar;
import uk.gov.ccew.onereg.model.OneRegCharity;
import uk.gov.ccew.onereg.service.ExternalSiteUrlsConfigurationUtil;
import uk.gov.ccew.onereg.service.OneRegFinancialService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.CHARITY_DETAILS, "mvc.command.name=" + MVCCommandKeys.ACCOUNTS_AND_ANNUAL_RETURNS }, service = MVCRenderCommand.class)
public class AccountsAndAnnualReturnsMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private ExternalSiteUrlsConfigurationUtil externalSiteUrlsConfigurationUtil;

	@Reference
	private OneRegFinancialService oneRegFinancialService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) {

		OneRegCharity charity = (OneRegCharity) renderRequest.getAttribute(RequestKeys.ONE_REG_CHARITY);
		long organisationNumber = charity.getOrganisationNumber();

		List<OneRegAnnualReturnAndAccountsAndTar> annualReturnsAndAccountsAndTars = oneRegFinancialService.getAnnualReturnsAndAccountsAndTars(organisationNumber);
		String charityLoginUrl = externalSiteUrlsConfigurationUtil.getCharityLoginUrl();

		renderRequest.setAttribute(RequestKeys.CHARITY_LOGIN_URL, charityLoginUrl);
		renderRequest.setAttribute(RequestKeys.ANNUAL_RETURNS_AND_ACCOUNTS_AND_TARS, annualReturnsAndAccountsAndTars);

		return "/view.jsp";

	}

}

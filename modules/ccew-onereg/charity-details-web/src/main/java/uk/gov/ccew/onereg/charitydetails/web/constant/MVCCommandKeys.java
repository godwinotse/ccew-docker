package uk.gov.ccew.onereg.charitydetails.web.constant;

public final class MVCCommandKeys {

	public static final String ACCOUNTS_AND_ANNUAL_RETURNS = "/accounts-and-annual-returns";

	public static final String ASSETS_AND_LIABILITIES = "/assets-and-liabilities";

	public static final String CHARITY_OVERVIEW = "/charity-overview";

	public static final String CONTACT_AND_LOCATION = "/contact-and-location";

	public static final String FINANCIAL_HISTORY = "/financial-history";

	public static final String GOVERNANCE = "/governance";

	public static final String GOVERNING_DOCUMENT = "/governing-document";

	public static final String TRUSTEES = "/trustees";
	
	public static final String WHAT_WHO_HOW_WHERE = "/what-who-how-where";

	private MVCCommandKeys() {
	}

}


package uk.gov.ccew.onereg.charitydetails.web.portlet;

import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;

import uk.gov.ccew.onereg.charitydetails.web.constant.MVCCommandKeys;
import uk.gov.ccew.onereg.constant.PortletKeys;
import uk.gov.ccew.onereg.constant.RequestKeys;
import uk.gov.ccew.onereg.model.OneRegCharity;
import uk.gov.ccew.onereg.model.OneRegEmployeesSummary;
import uk.gov.ccew.onereg.model.OneRegFundraisingSummary;
import uk.gov.ccew.onereg.model.OneRegIncomeAndExpenditureSummary;
import uk.gov.ccew.onereg.model.OneRegTrustee;
import uk.gov.ccew.onereg.service.CurrencyService;
import uk.gov.ccew.onereg.service.OneRegEmployeesSummaryService;
import uk.gov.ccew.onereg.service.OneRegFinancialService;
import uk.gov.ccew.onereg.service.OneRegTrusteeService;
import uk.gov.ccew.publicdata.model.Trustee;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.CHARITY_DETAILS, "mvc.command.name=/",
		"mvc.command.name=" + MVCCommandKeys.CHARITY_OVERVIEW }, service = MVCRenderCommand.class)
public class CharityOverviewMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private CurrencyService currencyService;

	@Reference
	private OneRegEmployeesSummaryService oneRegEmployeesSummaryService;

	@Reference
	private OneRegFinancialService oneRegFinancialService;

	@Reference
	private OneRegTrusteeService oneRegTrusteeService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		OneRegCharity charity = (OneRegCharity) renderRequest.getAttribute(RequestKeys.ONE_REG_CHARITY);
		long organisationNumber = charity.getOrganisationNumber();

		try {

			List<OneRegTrustee> trustees = oneRegTrusteeService.getTrustees(organisationNumber, renderRequest);
			boolean subsidiaryDirectorTrustee = trustees.stream().anyMatch(Trustee::isReceivingPaymentsFromCharity);
			boolean trusteeReceivesPaymentFromCharity = trustees.stream().anyMatch(Trustee::isReceivingPaymentsFromCharity);
			OneRegEmployeesSummary employeesSummary = oneRegEmployeesSummaryService.getOneRegEmployeesSummary(organisationNumber);
			OneRegFundraisingSummary fundraisingSummary = oneRegFinancialService.getFundraisingSummary(organisationNumber);
			OneRegIncomeAndExpenditureSummary incomeAndExpenditureSummary = oneRegFinancialService.getIncomeAndExpenditureSummary(organisationNumber);

			renderRequest.setAttribute(RequestKeys.SUBSIDIARY_DIRECTOR_TRUSTEE, subsidiaryDirectorTrustee);
			renderRequest.setAttribute(RequestKeys.TRUSTEE_RECEIVES_PAYMENT_FROM_CHARITY, trusteeReceivesPaymentFromCharity);
			renderRequest.setAttribute(RequestKeys.TRUSTEES_TOTAL, trustees.size());
			renderRequest.setAttribute(RequestKeys.EMPLOYEES_SUMMARY, employeesSummary);
			renderRequest.setAttribute(RequestKeys.NO_OF_PEOPLE_BY_SALARY_CHART, oneRegEmployeesSummaryService.getNoOfPeopleBySalaryChart(employeesSummary));
			renderRequest.setAttribute(RequestKeys.NO_OF_PEOPLE_BY_SALARY_TABLE, oneRegEmployeesSummaryService.getNoOfPeopleBySalaryTable(employeesSummary));
			renderRequest.setAttribute(RequestKeys.FUNDRAISING_SUMMARY, fundraisingSummary);
			renderRequest.setAttribute(RequestKeys.INCOME_AND_EXPENDITURE_SUMMARY, incomeAndExpenditureSummary);
			renderRequest.setAttribute(RequestKeys.INCOME_CHART, oneRegFinancialService.getIncomeChart(incomeAndExpenditureSummary));
			renderRequest.setAttribute(RequestKeys.INCOME_TABLE, oneRegFinancialService.getIncomeTable(incomeAndExpenditureSummary));
			renderRequest.setAttribute(RequestKeys.EXPENDITURE_CHART, oneRegFinancialService.getExpenditureChart(incomeAndExpenditureSummary));
			renderRequest.setAttribute(RequestKeys.EXPENDITURE_TABLE, oneRegFinancialService.getExpenditureTable(incomeAndExpenditureSummary));
			renderRequest.setAttribute(RequestKeys.INVESTMENT_GAINS, incomeAndExpenditureSummary.getInvestmentGainsFormatted());
			renderRequest.setAttribute(RequestKeys.TOTAL_CATEGORY_INCOME, currencyService.getTotalFormatted(incomeAndExpenditureSummary.getIncomeByCategory().values()));
			renderRequest.setAttribute(RequestKeys.TOTAL_CATEGORY_EXPENDITURE, currencyService.getTotalFormatted(incomeAndExpenditureSummary.getExpenditureByCategory().values()));

			return "/view.jsp";

		} catch (PortalException e) {

			throw new PortletException(e);

		}

	}

}

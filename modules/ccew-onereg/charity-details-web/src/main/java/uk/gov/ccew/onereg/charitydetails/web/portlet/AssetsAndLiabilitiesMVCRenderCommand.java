package uk.gov.ccew.onereg.charitydetails.web.portlet;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;

import uk.gov.ccew.onereg.charitydetails.web.constant.MVCCommandKeys;
import uk.gov.ccew.onereg.constant.PortletKeys;
import uk.gov.ccew.onereg.constant.RequestKeys;
import uk.gov.ccew.onereg.model.Chart;
import uk.gov.ccew.onereg.model.OneRegAssetsAndLiabilities;
import uk.gov.ccew.onereg.model.OneRegCharity;
import uk.gov.ccew.onereg.model.Table;
import uk.gov.ccew.onereg.service.OneRegFinancialService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.CHARITY_DETAILS, "mvc.command.name=" + MVCCommandKeys.ASSETS_AND_LIABILITIES }, service = MVCRenderCommand.class)
public class AssetsAndLiabilitiesMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private OneRegFinancialService oneRegFinancialService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) {

		OneRegCharity charity = (OneRegCharity) renderRequest.getAttribute(RequestKeys.ONE_REG_CHARITY);
		OneRegAssetsAndLiabilities assetsAndLiabilities = oneRegFinancialService.getAssetsAndLiabilities(charity.getOrganisationNumber());
		Table assetsAndLiabilitiesTable = oneRegFinancialService.getAssetsAndLiabilitiesTable(assetsAndLiabilities);
		Chart assetsAndLiabilitiesChart = oneRegFinancialService.getAssetsAndLiabilitiesChart(assetsAndLiabilities);

		renderRequest.setAttribute("assetsAndLiabilitiesChart", assetsAndLiabilitiesChart);
		renderRequest.setAttribute("assetsAndLiabilitiesTable", assetsAndLiabilitiesTable);

		return "/view.jsp";

	}

}

package uk.gov.ccew.onereg.charitydetails.web.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import uk.gov.ccew.onereg.charitydetails.web.model.NavEntry;
import uk.gov.ccew.onereg.charitydetails.web.service.NavEntryService;
import uk.gov.ccew.onereg.constant.PortletKeys;
import uk.gov.ccew.onereg.constant.RequestKeys;
import uk.gov.ccew.onereg.model.OneRegCharity;
import uk.gov.ccew.onereg.service.OneRegCharityService;
import uk.gov.ccew.onereg.service.OneRegTrusteeService;

@Component(immediate = true, property = { //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=charity-register-charity-details", //
		"com.liferay.portlet.display-category=category.hidden", //
		"com.liferay.portlet.header-portlet-javascript=/js/chart.js", //
		"com.liferay.portlet.header-portlet-javascript=/js/main.js", //
		"com.liferay.portlet.instanceable=true", //
		"com.liferay.portlet.use-default-template=true", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.name=" + PortletKeys.CHARITY_DETAILS, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=guest,power-user,user" //
}, service = Portlet.class)
public class CharityDetailsPortlet extends MVCPortlet {

	@Reference
	private NavEntryService navEntryService;

	@Reference
	private OneRegCharityService oneRegCharityService;

	@Reference
	private OneRegTrusteeService oneRegTrusteeService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {

		long organisationNumber = ParamUtil.getLong(renderRequest, RequestKeys.ORGANISATION_NUMBER);

		try {

			OneRegCharity charity = oneRegCharityService.getCharity(organisationNumber, renderRequest);
			List<NavEntry> navEntries = navEntryService.getNavEntries();

			renderRequest.setAttribute(RequestKeys.ONE_REG_CHARITY, charity);
			renderRequest.setAttribute(RequestKeys.NAV_ENTRIES, navEntries);

			disableBackLink(renderRequest);

			super.render(renderRequest, renderResponse);

		} catch (PortalException e) {

			throw new PortletException(e);

		}

	}

	private void disableBackLink(RenderRequest renderRequest) {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		PortletDisplay portletDisplay = themeDisplay.getPortletDisplay();
		portletDisplay.setShowBackIcon(false);

	}

}

package uk.gov.ccew.onereg.model;

public interface ChartData {

	String getColour();

	String getValue();

}

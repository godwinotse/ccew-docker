package uk.gov.ccew.onereg.configuration;

import aQute.bnd.annotation.metatype.Meta;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

@ExtendedObjectClassDefinition(category = "ccew")
@Meta.OCD(id = "uk.gov.ccew.onereg.configuration.ExternalSiteUrlsConfiguration")
public interface ExternalSiteUrlsConfiguration {

	@Meta.AD(deflt = "https://portal.update-charity-details.service.gov.uk/group/update-charity-details", required = false)
	String charityLoginUrl();

	@Meta.AD(deflt = "https://beta.companieshouse.gov.uk/company/", required = false)
	String companyHouseBaseUrl();

}

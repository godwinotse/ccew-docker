package uk.gov.ccew.onereg.constant;

public final class PortletKeys {

	public static final String CHARITY_DETAILS = "uk_gov_ccew_onereg_charitydetails_web_portlet_CharityDetailsPortlet";

	private PortletKeys() {
	}

}

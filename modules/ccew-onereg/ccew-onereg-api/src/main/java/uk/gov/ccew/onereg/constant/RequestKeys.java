package uk.gov.ccew.onereg.constant;

public final class RequestKeys {

	public static final String ANNUAL_RETURNS_AND_ACCOUNTS_AND_TARS = "annualReturnsAndAccountsAndTars";

	public static final String CHARITY_LOGIN_URL = "charityLoginUrl";

	public static final String CHARITY_NUMBER = "charityNumber";

	public static final String EMPLOYEES_SUMMARY = "employeesSummary";

	public static final String EXPENDITURE_CHART = "expenditureChart";

	public static final String EXPENDITURE_TABLE = "expenditureTable";

	public static final String FINANCIAL_HISTORY_CHART = "financialHistoryChart";

	public static final String FUNDRAISING_SUMMARY = "fundraisingSummary";

	public static final String INCOME_AND_EXPENDITURE_SUMMARY = "incomeAndExpenditureSummary";

	public static final String INCOME_CHART = "incomeChart";

	public static final String INCOME_TABLE = "incomeTable";

	public static final String INVESTMENT_GAINS = "investmentGains";

	public static final String NAV_ENTRIES = "navEntries";

	public static final String NO_OF_PEOPLE_BY_SALARY_CHART = "noOfPeopleBySalaryChart";

	public static final String NO_OF_PEOPLE_BY_SALARY_TABLE = "noOfPeopleBySalaryTable";

	public static final String ONE_REG_CHARITY = "oneRegCharity";

	public static final String ORGANISATION_NUMBER = "organisationNumber";

	public static final String SUBSIDIARY_DIRECTOR_TRUSTEE = "subsidiaryDirectorTrustee";

	public static final String TOTAL_CATEGORY_EXPENDITURE = "totalCategoryExpenditure";

	public static final String TOTAL_CATEGORY_INCOME = "totalCategoryIncome";

	public static final String TOTAL_EXPENDITURE = "totalExpenditure";

	public static final String TRUSTEES_TOTAL = "trusteesTotal";

	public static final String TRUSTEES_AND_OTHER_CHARITIES = "trusteesAndOtherCharities";

	public static final String TRUSTEE_RECEIVES_PAYMENT_FROM_CHARITY = "trusteeReceivesPaymentFromCharity";

	public static final String WEB_CONTENT_ID = "webContentId";

	private RequestKeys() {
	}

}

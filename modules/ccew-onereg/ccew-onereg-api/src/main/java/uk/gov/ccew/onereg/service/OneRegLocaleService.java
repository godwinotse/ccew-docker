package uk.gov.ccew.onereg.service;

import java.util.List;
import java.util.Locale;
import java.util.Map;

public interface OneRegLocaleService {

	Map<Locale, String> getLocaleMap(List<Locale> locales, String value);

	List<Locale> getSupportedLocales();

}

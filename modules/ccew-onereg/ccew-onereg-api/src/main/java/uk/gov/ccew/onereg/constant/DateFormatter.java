package uk.gov.ccew.onereg.constant;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import com.liferay.petra.string.StringPool;

public class DateFormatter {

	private static final DateFormat dd_MM_YY = new SimpleDateFormat("dd/MM/YY");

	private static final DateFormat dd_MMMM = new SimpleDateFormat("dd MMMM");

	private static final DateFormat dd_MMMM_YYYY = new SimpleDateFormat("dd MMMM YYYY");

	private static final DateFormat MMM_YYYY = new SimpleDateFormat("MMM YYYY");

	private static final DateFormat yyyy_MM_dd = new SimpleDateFormat("yyyy-MM-dd");

	private DateFormatter() {
	}

	public static synchronized String format_dd_MM_YY(Date date) {
		return dd_MM_YY.format(date);
	}

	public static synchronized String format_dd_MM_YY(Optional<Date> optionalDate) {
		return optionalDate.map(DateFormatter::format_dd_MM_YY).orElse(StringPool.BLANK);
	}

	public static synchronized String format_dd_MMMM(Date date) {
		return dd_MMMM.format(date);
	}

	public static synchronized String format_dd_MMMM(Optional<Date> optionalDate) {
		return optionalDate.map(DateFormatter::format_dd_MMMM).orElse(StringPool.BLANK);
	}

	public static synchronized String format_dd_MMMM_YYYY(Date date) {
		return dd_MMMM_YYYY.format(date);
	}

	public static synchronized String format_dd_MMMM_YYYY(Optional<Date> optionalDate) {
		return optionalDate.map(DateFormatter::format_dd_MMMM_YYYY).orElse(StringPool.BLANK);
	}

	public static synchronized String format_MMM_YYYY(Date date) {
		return MMM_YYYY.format(date);
	}

	public static synchronized String format_MMM_YYYY(Optional<Date> optionalDate) {
		return optionalDate.map(DateFormatter::format_MMM_YYYY).orElse(StringPool.BLANK);
	}

	public static synchronized String format_yyyy_MM_dd(Date date) {
		return yyyy_MM_dd.format(date);
	}

}

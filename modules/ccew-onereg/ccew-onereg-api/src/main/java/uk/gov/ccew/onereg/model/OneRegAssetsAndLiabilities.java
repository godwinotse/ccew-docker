package uk.gov.ccew.onereg.model;

import uk.gov.ccew.publicdata.model.AssetsAndLiabilities;

public interface OneRegAssetsAndLiabilities extends AssetsAndLiabilities {

	String getDefinedPensionBenefitTotalFormatted();

	String getLiabilitiesTotalFormatted();

	String getLongTermInvestmentTotalFormatted();

	String getOtherAssetsTotalFormatted();

	String getOwnUseAssetsTotalFormatted();

}

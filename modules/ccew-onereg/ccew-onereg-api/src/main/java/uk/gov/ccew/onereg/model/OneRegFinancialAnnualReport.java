package uk.gov.ccew.onereg.model;

import java.util.Map;

import uk.gov.ccew.publicdata.model.FinancialAnnualReport;

public interface OneRegFinancialAnnualReport extends FinancialAnnualReport {

	Map<String, String> getExpenditureTotalsByCategoryFormatted();

	String getFinancialYearEndFormatted();

	Map<String, String> getIncomeTotalsByCategoryFormatted();

}

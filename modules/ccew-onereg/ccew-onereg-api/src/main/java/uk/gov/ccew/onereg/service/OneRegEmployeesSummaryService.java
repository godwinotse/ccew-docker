package uk.gov.ccew.onereg.service;

import uk.gov.ccew.onereg.model.Chart;
import uk.gov.ccew.onereg.model.OneRegEmployeesSummary;
import uk.gov.ccew.onereg.model.Table;

public interface OneRegEmployeesSummaryService {

	OneRegEmployeesSummary getOneRegEmployeesSummary(long organisationNumber);

	Chart getNoOfPeopleBySalaryChart(OneRegEmployeesSummary employeesSummary);

	Table getNoOfPeopleBySalaryTable(OneRegEmployeesSummary employeesSummary);

}

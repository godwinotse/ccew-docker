package uk.gov.ccew.onereg.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;

public interface OneRegGroupService {

	String REGISTER_FRIENDLY_URL = "/register";

	Group addOneRegGroup() throws PortalException;

	Group getOneRegGroup() throws PortalException;

}

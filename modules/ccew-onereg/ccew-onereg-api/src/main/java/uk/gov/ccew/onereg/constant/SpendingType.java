package uk.gov.ccew.onereg.constant;

public enum SpendingType {

	INCOME,

	EXPENDITURE

}

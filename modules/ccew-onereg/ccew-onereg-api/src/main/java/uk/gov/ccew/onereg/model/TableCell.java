package uk.gov.ccew.onereg.model;

public interface TableCell {

	TableCellType getType();

	String getValue();

	boolean isNumeric();

}

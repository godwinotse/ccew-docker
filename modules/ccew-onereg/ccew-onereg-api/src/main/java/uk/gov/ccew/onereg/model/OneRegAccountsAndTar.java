package uk.gov.ccew.onereg.model;

import uk.gov.ccew.publicdata.model.AccountsAndTar;

public interface OneRegAccountsAndTar extends AccountsAndTar {

	String getReceivedDateFormatted();

	String getReportingYearFormatted();

}

package uk.gov.ccew.onereg.service;

import uk.gov.ccew.onereg.model.OneRegCharity;

import java.util.List;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.exception.PortalException;

public interface OneRegCharityService {

	List<OneRegCharity> getCharities(List<Long> organisationNumbers, PortletRequest portletRequest) throws PortalException;

	OneRegCharity getCharity(long organisationNumber, PortletRequest portletRequest) throws PortalException;

}

package uk.gov.ccew.onereg.service;

import java.util.Map;

public interface ChartConfigurationUtil {

	Map<String, String> getColourNamesAndValues();


}

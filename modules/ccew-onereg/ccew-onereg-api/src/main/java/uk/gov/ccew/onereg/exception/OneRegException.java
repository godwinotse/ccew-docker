package uk.gov.ccew.onereg.exception;

public class OneRegException extends Exception {

	public OneRegException(String msg, Throwable cause) {
		super(msg, cause);
	}

}

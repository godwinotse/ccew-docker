package uk.gov.ccew.onereg.model;

import uk.gov.ccew.publicdata.model.RegistrationRecord;

public interface OneRegRegistrationRecord extends RegistrationRecord {

    public String getRegistrationDateFormatted();
}
package uk.gov.ccew.onereg.constant;

public enum OneRegWebContent {

	FINANCIAL_HISTORY_LINKED_CHARITY("FINANCIAL-HISTORY-LINKED-CHARITY", "Financial history linked charity"),
	FINANCIAL_HISTORY_NEW_CHARITY("FINANCIAL-HISTORY-NEW-CHARITY", "Financial history new charity"),
	FINANCIAL_HISTORY_NO_HISTORY("FINANCIAL-HISTORY-NO-HISTORY", "Financial history no history"),
	FINANCIAL_HISTORY_REMOVED("FINANCIAL-HISTORY-REMOVED", "Financial history removed"),
	TRUSTEES_INTRO("TRUSTEES-INTRO", "Trustees intro");

	private final String articleId;

	private final String title;

	OneRegWebContent(String articleId, String title) {
		this.articleId = articleId;
		this.title = title;
	}

	public String getArticleId() {
		return articleId;
	}

	public String getTitle() {
		return title;
	}

}

package uk.gov.ccew.onereg.service;

import java.util.Collection;
import java.util.List;

import uk.gov.ccew.onereg.model.Chart;
import uk.gov.ccew.onereg.model.ChartData;
import uk.gov.ccew.onereg.model.ChartDataSet;

public interface ChartService {

	Chart getChart(List<ChartDataSet> dataSets);

	Chart getChart(Collection<String> labels, List<ChartDataSet> dataSets);

	ChartDataSet getChartDataSet(List<ChartData> data);

	ChartDataSet getChartDataSet(String label, List<String> data);

	ChartDataSet getChartDataSet(String label, List<String> data, String colour);

}

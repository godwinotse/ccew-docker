package uk.gov.ccew.onereg.model;

import uk.gov.ccew.publicdata.model.Trustee;

import java.util.List;

public interface OneRegTrustee extends Trustee {

	String getDateOfAppointmentFormatted();

}

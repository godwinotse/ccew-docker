package uk.gov.ccew.onereg.service;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;

public interface OneRegUrlService {
	
	String getCharityDetailsUrl(long organisationNumber, PortletRequest portletRequest) throws PortalException;

	String getCharityDetailsUrl(long organisationNumber, LiferayPortletRequest liferayPortletRequest) throws PortalException;

}

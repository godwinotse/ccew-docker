package uk.gov.ccew.onereg.model;

import java.util.Map;

import uk.gov.ccew.publicdata.model.IncomeAndExpenditureSummary;

public interface OneRegIncomeAndExpenditureSummary extends IncomeAndExpenditureSummary {

	String getCurrentFinancialYearEndFormatted();

	Map<String, String> getExpenditureByCategoryFormatted();

	Object[] getGovernmentContractsAndGrantsMessageArguments();

	Map<String, String> getIncomeByCategoryFormatted();

	String getInvestmentGainsFormatted();

}

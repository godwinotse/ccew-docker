package uk.gov.ccew.onereg.model;

public enum TableCellType {

	COLOUR_BLOCK,

	DATA,

	HEADING;

	public boolean isColourBlock() {
		return this == COLOUR_BLOCK;
	}

	public boolean isData() {
		return this == DATA;
	}

	public boolean isHeading() {
		return this == HEADING;
	}

}

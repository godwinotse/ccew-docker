package uk.gov.ccew.onereg.service;

import java.util.Locale;

import com.liferay.portal.kernel.service.ServiceContext;

public interface ServiceContextService {

	ServiceContext getServiceContext(long companyId);

	ServiceContext getServiceContext(long companyId, long groupId, long userId, Locale locale);

}

package uk.gov.ccew.onereg.service;

import uk.gov.ccew.onereg.model.OneRegTrustee;
import uk.gov.ccew.publicdata.model.Trustee;

import java.util.List;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.exception.PortalException;

public interface OneRegTrusteeService {

	OneRegTrustee getTrustee(Trustee trustee);

	int getTrusteeCount(long organisationNumber);

	List<OneRegTrustee> getTrustees(long organisationNumber, PortletRequest portletRequest) throws PortalException;

}

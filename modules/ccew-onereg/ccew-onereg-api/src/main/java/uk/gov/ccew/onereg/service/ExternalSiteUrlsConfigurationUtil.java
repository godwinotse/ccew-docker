package uk.gov.ccew.onereg.service;

import java.util.Map;

public interface ExternalSiteUrlsConfigurationUtil {

	String getCharityLoginUrl();

	String getCompanyHouseBaseUrl();

}

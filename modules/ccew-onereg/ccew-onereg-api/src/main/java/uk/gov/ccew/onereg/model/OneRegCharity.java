package uk.gov.ccew.onereg.model;

import java.util.List;
import java.util.Optional;

import uk.gov.ccew.publicdata.model.Charity;

public interface OneRegCharity extends Charity {

	String getCharityNumberFormatted();

	String getDisplayUrl();

	void setDisplayUrl(String url);

	String getIncomeFormatted();

	Optional<OneRegCharityAnnualReport> getOneRegCharityAnnualReport();

	void setOneRegCharityAnnualReport(OneRegCharityAnnualReport oneRegCharityAnnualReport);

	List<OneRegRegulatoryAlert> getOneRegRegulatoryAlerts();

	void setOneRegRegulatoryAlerts(List<OneRegRegulatoryAlert> oneRegRegulatoryAlerts);

	String getRegistrationDateFormatted();

	List<OneRegRegistrationRecord> getOneRegRegistrationHistory();

	void setOneRegRegistrationHistory(List<OneRegRegistrationRecord> oneRegRegistrationHistory);
}

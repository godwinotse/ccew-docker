package uk.gov.ccew.onereg.model;

import java.util.List;

public interface TableRow {

	List<TableCell> getCells();

}

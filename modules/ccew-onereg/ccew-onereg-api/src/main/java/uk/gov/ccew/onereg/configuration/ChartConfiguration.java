package uk.gov.ccew.onereg.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "ccew")
@Meta.OCD(id = "uk.gov.ccew.onereg.configuration.ChartConfiguration")
public interface ChartConfiguration {

	@Meta.AD(deflt = "blue/#1d70b8|yellow/#ffdd00|green/#00703c|purple/#4c2c92|orange/#f47738|red/#d4351c|mid-grey/#b1b4b6", required = false)
	String[] coloursNamesAndValues();

}

package uk.gov.ccew.onereg.model;

import java.util.List;

public interface ChartDataSet {

	List<ChartData> getChartData();

	String getJs();

	String getLabel();

	int getStackId();

	boolean isStacked();

	void setStacked(boolean stacked);

	void setStackId(int stackId);

}

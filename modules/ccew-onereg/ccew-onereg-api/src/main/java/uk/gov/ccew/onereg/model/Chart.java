package uk.gov.ccew.onereg.model;

import java.util.Collection;
import java.util.List;

public interface Chart {

	String getDataJs();

	Collection<String> getLabels();

	List<ChartDataSet> getDataSets();

}

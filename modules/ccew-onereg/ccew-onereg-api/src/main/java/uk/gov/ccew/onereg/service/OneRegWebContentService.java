package uk.gov.ccew.onereg.service;

import uk.gov.ccew.onereg.constant.OneRegWebContent;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;

public interface OneRegWebContentService {

	JournalArticle addBasicWebContent(OneRegWebContent oneRegWebContent, ServiceContext serviceContext) throws PortalException;

}

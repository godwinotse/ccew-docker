package uk.gov.ccew.onereg.constant;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

public class NumberFormatter {

	private static final NumberFormat NUMBER_SHORT;

	private static final NumberFormat UK_CURRENCY_SHORT;

	static {

		UK_CURRENCY_SHORT = NumberFormat.getCurrencyInstance(Locale.UK);
		UK_CURRENCY_SHORT.setMaximumFractionDigits(0);

		NUMBER_SHORT = NumberFormat.getInstance();
		NUMBER_SHORT.setGroupingUsed(true);

	}

	private NumberFormatter() {
	}

	public static synchronized String formatNumberShort(BigDecimal value) {
		return NUMBER_SHORT.format(value);
	}

	public static synchronized String formatUkCurrencyShort(BigDecimal value) {
		return UK_CURRENCY_SHORT.format(value);
	}

}

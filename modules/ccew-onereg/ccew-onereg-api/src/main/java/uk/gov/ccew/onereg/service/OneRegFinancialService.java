package uk.gov.ccew.onereg.service;

import java.util.List;

import uk.gov.ccew.onereg.constant.SpendingType;
import uk.gov.ccew.onereg.model.Chart;
import uk.gov.ccew.onereg.model.OneRegAnnualReturnAndAccountsAndTar;
import uk.gov.ccew.onereg.model.OneRegAssetsAndLiabilities;
import uk.gov.ccew.onereg.model.OneRegFinancialAnnualReport;
import uk.gov.ccew.onereg.model.OneRegFundraisingSummary;
import uk.gov.ccew.onereg.model.OneRegIncomeAndExpenditureSummary;
import uk.gov.ccew.onereg.model.Table;

public interface OneRegFinancialService {

	OneRegAssetsAndLiabilities getAssetsAndLiabilities(long organisationNumber);

	Chart getAssetsAndLiabilitiesChart(OneRegAssetsAndLiabilities assetsAndLiabilities);

	Table getAssetsAndLiabilitiesTable(OneRegAssetsAndLiabilities assetsAndLiabilities);

	List<OneRegAnnualReturnAndAccountsAndTar> getAnnualReturnsAndAccountsAndTars(long organisationNumber);

	Chart getExpenditureChart(OneRegIncomeAndExpenditureSummary incomeAndExpenditureSummary);

	Table getExpenditureTable(OneRegIncomeAndExpenditureSummary incomeAndExpenditureSummary);

	List<OneRegFinancialAnnualReport> getFinancialHistory(long organisationNumber);

	Chart getFinancialHistoryChart(List<OneRegFinancialAnnualReport> financialHistory);

	OneRegFundraisingSummary getFundraisingSummary(long organisationNumber);

	OneRegIncomeAndExpenditureSummary getIncomeAndExpenditureSummary(long organisationNumber);

	Chart getIncomeChart(OneRegIncomeAndExpenditureSummary incomeAndExpenditureSummary);

	Table getIncomeTable(OneRegIncomeAndExpenditureSummary incomeAndExpenditureSummary);

	Table getSpendingTable(List<OneRegFinancialAnnualReport> financialHistory);

	String getSpendingTotal(List<OneRegFinancialAnnualReport> financialHistory, SpendingType spendingType);

}

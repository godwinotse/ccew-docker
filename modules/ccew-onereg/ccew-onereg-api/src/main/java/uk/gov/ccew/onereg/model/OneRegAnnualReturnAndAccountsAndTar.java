package uk.gov.ccew.onereg.model;

import java.util.Optional;

import uk.gov.ccew.publicdata.model.AnnualReturnAndAccountsAndTar;

public interface OneRegAnnualReturnAndAccountsAndTar extends AnnualReturnAndAccountsAndTar {

	Optional<OneRegAnnualReturn> getOneRegAnnualReturn();

	Optional<OneRegAccountsAndTar> getOneRegAccountsAndTar();

}

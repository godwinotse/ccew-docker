package uk.gov.ccew.onereg.service;

import java.math.BigDecimal;
import java.util.Collection;

public interface CurrencyService {

	String getTotalFormatted(Collection<BigDecimal> values);

}

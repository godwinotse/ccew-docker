package uk.gov.ccew.onereg.model;

import uk.gov.ccew.publicdata.model.RegulatoryAlert;

public interface OneRegRegulatoryAlert extends RegulatoryAlert {

	String getIncidentDateFormatted();

	Object[] getMessageArguments();

}

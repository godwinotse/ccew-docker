package uk.gov.ccew.onereg.model;

import uk.gov.ccew.publicdata.model.AnnualReturn;

public interface OneRegAnnualReturn extends AnnualReturn {

	String getReceivedDateFormatted();

	String getReportingYearFormatted();


}

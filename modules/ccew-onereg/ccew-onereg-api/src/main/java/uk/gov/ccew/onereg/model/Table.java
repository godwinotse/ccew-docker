package uk.gov.ccew.onereg.model;

import java.util.List;
import java.util.Optional;

public interface Table {

	List<TableRow> getBodyRows();

	Optional<TableRow> getHeaderRow();

}

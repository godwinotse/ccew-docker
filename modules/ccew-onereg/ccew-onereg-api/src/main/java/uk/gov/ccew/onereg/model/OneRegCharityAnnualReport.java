package uk.gov.ccew.onereg.model;

import uk.gov.ccew.publicdata.model.CharityAnnualReport;

public interface OneRegCharityAnnualReport extends CharityAnnualReport {

	String getSubmittedDateFormatted();

}

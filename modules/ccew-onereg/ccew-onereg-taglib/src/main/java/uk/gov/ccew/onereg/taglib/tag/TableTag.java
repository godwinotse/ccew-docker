package uk.gov.ccew.onereg.taglib.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import com.liferay.taglib.servlet.PipingServletResponse;
import com.liferay.taglib.util.IncludeTag;

import uk.gov.ccew.onereg.model.Table;
import uk.gov.ccew.onereg.taglib.servletcontext.ServletContextUtil;

public class TableTag extends IncludeTag {

	private static final boolean CLEAN_UP_SET_ATTRIBUTES = false;

	private static final String PAGE = "/table/page.jsp";

	private String cssClass;

	private Table table;

	public HttpServletResponse createPipingServletResponse(PageContext pageContext) {
		return PipingServletResponse.createPipingServletResponse(pageContext);
	}

	@Override
	protected String getPage() {
		return PAGE;
	}

	@Override
	protected boolean isCleanUpSetAttributes() {
		return CLEAN_UP_SET_ATTRIBUTES;
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {
		request.setAttribute("cssClass", cssClass);
		request.setAttribute("table", table);
	}

	@Override
	public void setPageContext(PageContext pageContext) {

		super.setPageContext(pageContext);

		setServletContext(ServletContextUtil.getServletContext());

	}

	public void setCssClass(String ccsClass) {
		this.cssClass = ccsClass;
	}

	public void setTable(Table table) {
		this.table = table;
	}

}

<%@ include file="../init.jsp" %>

<c:set var="headerRow" value="${ table.headerRow.isPresent() ? table.headerRow.get() : '' }"/>

<table class="govuk-table ${ cssClass }">

	<c:if test="${ not empty headerRow }">

		<thead class="govuk-table__head">
			<tr class="govuk-table__row">

				<c:forEach var="headerCell" items="${ headerRow.cells }">

					<c:set var="cssClass" value="${ headerCell.numeric ? 'govuk-table__header--numeric' : '' }" />
					<th scope="col" class="govuk-table__header ${ cssClass }">${ headerCell.value }</th>

				</c:forEach>

			</tr>
		</thead>

	</c:if>

	<tbody>

		<c:forEach var="bodyRow" items="${ table.bodyRows }">

			<tr>

				<c:forEach var="bodyCell" items="${ bodyRow.cells }">
					<c:choose>
						<c:when test="${ bodyCell.type.heading }">

							<th scope="row" class="govuk-table__header">${ bodyCell.value }</th>

						</c:when>
						<c:when test="${ bodyCell.type.colourBlock }">

							<td class="govuk-table__cell">
								<span class="colour_block__${ bodyCell.value }" style="width: 20px; height: 20px; background-color: ${ bodyCell.value }; display: block; margin: 5px 0px;"/>
							</td>

						</c:when>
						<c:otherwise>

							<c:set var="cssClass" value="${ bodyCell.numeric ? 'govuk-table__cell--numeric' : '' }" />
							<td class="govuk-table__cell ${ cssClass }">${ bodyCell.value }</td>

						</c:otherwise>
					</c:choose>
				</c:forEach>

			</tr>

		</c:forEach>

	</tbody>

</table>
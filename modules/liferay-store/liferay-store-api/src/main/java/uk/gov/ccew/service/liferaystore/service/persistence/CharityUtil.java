/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package uk.gov.ccew.service.liferaystore.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

import uk.gov.ccew.service.liferaystore.model.Charity;

/**
 * The persistence utility for the charity service. This utility wraps <code>uk.gov.ccew.service.liferaystore.service.persistence.impl.CharityPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CharityPersistence
 * @generated
 */
public class CharityUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Charity charity) {
		getPersistence().clearCache(charity);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Charity> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Charity> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Charity> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Charity> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Charity> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Charity update(Charity charity) {
		return getPersistence().update(charity);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Charity update(
		Charity charity, ServiceContext serviceContext) {

		return getPersistence().update(charity, serviceContext);
	}

	/**
	 * Returns the charity where organizationId = &#63; or throws a <code>NoSuchCharityException</code> if it could not be found.
	 *
	 * @param organizationId the organization ID
	 * @return the matching charity
	 * @throws NoSuchCharityException if a matching charity could not be found
	 */
	public static Charity findByOrganizationId(long organizationId)
		throws uk.gov.ccew.service.liferaystore.exception.
			NoSuchCharityException {

		return getPersistence().findByOrganizationId(organizationId);
	}

	/**
	 * Returns the charity where organizationId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param organizationId the organization ID
	 * @return the matching charity, or <code>null</code> if a matching charity could not be found
	 */
	public static Charity fetchByOrganizationId(long organizationId) {
		return getPersistence().fetchByOrganizationId(organizationId);
	}

	/**
	 * Returns the charity where organizationId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param organizationId the organization ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching charity, or <code>null</code> if a matching charity could not be found
	 */
	public static Charity fetchByOrganizationId(
		long organizationId, boolean useFinderCache) {

		return getPersistence().fetchByOrganizationId(
			organizationId, useFinderCache);
	}

	/**
	 * Removes the charity where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @return the charity that was removed
	 */
	public static Charity removeByOrganizationId(long organizationId)
		throws uk.gov.ccew.service.liferaystore.exception.
			NoSuchCharityException {

		return getPersistence().removeByOrganizationId(organizationId);
	}

	/**
	 * Returns the number of charities where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching charities
	 */
	public static int countByOrganizationId(long organizationId) {
		return getPersistence().countByOrganizationId(organizationId);
	}

	/**
	 * Caches the charity in the entity cache if it is enabled.
	 *
	 * @param charity the charity
	 */
	public static void cacheResult(Charity charity) {
		getPersistence().cacheResult(charity);
	}

	/**
	 * Caches the charities in the entity cache if it is enabled.
	 *
	 * @param charities the charities
	 */
	public static void cacheResult(List<Charity> charities) {
		getPersistence().cacheResult(charities);
	}

	/**
	 * Creates a new charity with the primary key. Does not add the charity to the database.
	 *
	 * @param charityId the primary key for the new charity
	 * @return the new charity
	 */
	public static Charity create(long charityId) {
		return getPersistence().create(charityId);
	}

	/**
	 * Removes the charity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param charityId the primary key of the charity
	 * @return the charity that was removed
	 * @throws NoSuchCharityException if a charity with the primary key could not be found
	 */
	public static Charity remove(long charityId)
		throws uk.gov.ccew.service.liferaystore.exception.
			NoSuchCharityException {

		return getPersistence().remove(charityId);
	}

	public static Charity updateImpl(Charity charity) {
		return getPersistence().updateImpl(charity);
	}

	/**
	 * Returns the charity with the primary key or throws a <code>NoSuchCharityException</code> if it could not be found.
	 *
	 * @param charityId the primary key of the charity
	 * @return the charity
	 * @throws NoSuchCharityException if a charity with the primary key could not be found
	 */
	public static Charity findByPrimaryKey(long charityId)
		throws uk.gov.ccew.service.liferaystore.exception.
			NoSuchCharityException {

		return getPersistence().findByPrimaryKey(charityId);
	}

	/**
	 * Returns the charity with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param charityId the primary key of the charity
	 * @return the charity, or <code>null</code> if a charity with the primary key could not be found
	 */
	public static Charity fetchByPrimaryKey(long charityId) {
		return getPersistence().fetchByPrimaryKey(charityId);
	}

	/**
	 * Returns all the charities.
	 *
	 * @return the charities
	 */
	public static List<Charity> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the charities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CharityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of charities
	 * @param end the upper bound of the range of charities (not inclusive)
	 * @return the range of charities
	 */
	public static List<Charity> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the charities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CharityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of charities
	 * @param end the upper bound of the range of charities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of charities
	 */
	public static List<Charity> findAll(
		int start, int end, OrderByComparator<Charity> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the charities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CharityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of charities
	 * @param end the upper bound of the range of charities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of charities
	 */
	public static List<Charity> findAll(
		int start, int end, OrderByComparator<Charity> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the charities from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of charities.
	 *
	 * @return the number of charities
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static CharityPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<CharityPersistence, CharityPersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(CharityPersistence.class);

		ServiceTracker<CharityPersistence, CharityPersistence> serviceTracker =
			new ServiceTracker<CharityPersistence, CharityPersistence>(
				bundle.getBundleContext(), CharityPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package uk.gov.ccew.service.liferaystore.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Charity}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Charity
 * @generated
 */
public class CharityWrapper
	extends BaseModelWrapper<Charity>
	implements Charity, ModelWrapper<Charity> {

	public CharityWrapper(Charity charity) {
		super(charity);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("charityId", getCharityId());
		attributes.put("registrationId", getRegistrationId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("groupCharityId", getGroupCharityId());
		attributes.put("parentCharityId", getParentCharityId());
		attributes.put("displayId", getDisplayId());
		attributes.put("govDocId", getGovDocId());
		attributes.put("govDocDescription", getGovDocDescription());
		attributes.put("govDocVerified", isGovDocVerified());
		attributes.put("fullName", getFullName());
		attributes.put("rulesHeader", getRulesHeader());
		attributes.put("rulesData", getRulesData());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long charityId = (Long)attributes.get("charityId");

		if (charityId != null) {
			setCharityId(charityId);
		}

		Long registrationId = (Long)attributes.get("registrationId");

		if (registrationId != null) {
			setRegistrationId(registrationId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long groupCharityId = (Long)attributes.get("groupCharityId");

		if (groupCharityId != null) {
			setGroupCharityId(groupCharityId);
		}

		Long parentCharityId = (Long)attributes.get("parentCharityId");

		if (parentCharityId != null) {
			setParentCharityId(parentCharityId);
		}

		String displayId = (String)attributes.get("displayId");

		if (displayId != null) {
			setDisplayId(displayId);
		}

		Long govDocId = (Long)attributes.get("govDocId");

		if (govDocId != null) {
			setGovDocId(govDocId);
		}

		String govDocDescription = (String)attributes.get("govDocDescription");

		if (govDocDescription != null) {
			setGovDocDescription(govDocDescription);
		}

		Boolean govDocVerified = (Boolean)attributes.get("govDocVerified");

		if (govDocVerified != null) {
			setGovDocVerified(govDocVerified);
		}

		String fullName = (String)attributes.get("fullName");

		if (fullName != null) {
			setFullName(fullName);
		}

		String rulesHeader = (String)attributes.get("rulesHeader");

		if (rulesHeader != null) {
			setRulesHeader(rulesHeader);
		}

		String rulesData = (String)attributes.get("rulesData");

		if (rulesData != null) {
			setRulesData(rulesData);
		}
	}

	/**
	 * Returns the charity ID of this charity.
	 *
	 * @return the charity ID of this charity
	 */
	@Override
	public long getCharityId() {
		return model.getCharityId();
	}

	/**
	 * Returns the company ID of this charity.
	 *
	 * @return the company ID of this charity
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the display ID of this charity.
	 *
	 * @return the display ID of this charity
	 */
	@Override
	public String getDisplayId() {
		return model.getDisplayId();
	}

	/**
	 * Returns the full name of this charity.
	 *
	 * @return the full name of this charity
	 */
	@Override
	public String getFullName() {
		return model.getFullName();
	}

	/**
	 * Returns the gov doc description of this charity.
	 *
	 * @return the gov doc description of this charity
	 */
	@Override
	public String getGovDocDescription() {
		return model.getGovDocDescription();
	}

	/**
	 * Returns the gov doc ID of this charity.
	 *
	 * @return the gov doc ID of this charity
	 */
	@Override
	public long getGovDocId() {
		return model.getGovDocId();
	}

	/**
	 * Returns the gov doc verified of this charity.
	 *
	 * @return the gov doc verified of this charity
	 */
	@Override
	public boolean getGovDocVerified() {
		return model.getGovDocVerified();
	}

	/**
	 * Returns the group charity ID of this charity.
	 *
	 * @return the group charity ID of this charity
	 */
	@Override
	public long getGroupCharityId() {
		return model.getGroupCharityId();
	}

	/**
	 * Returns the organization ID of this charity.
	 *
	 * @return the organization ID of this charity
	 */
	@Override
	public long getOrganizationId() {
		return model.getOrganizationId();
	}

	/**
	 * Returns the parent charity ID of this charity.
	 *
	 * @return the parent charity ID of this charity
	 */
	@Override
	public long getParentCharityId() {
		return model.getParentCharityId();
	}

	/**
	 * Returns the primary key of this charity.
	 *
	 * @return the primary key of this charity
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the registration ID of this charity.
	 *
	 * @return the registration ID of this charity
	 */
	@Override
	public long getRegistrationId() {
		return model.getRegistrationId();
	}

	@Override
	public Boolean getRule(String flagName) {
		return model.getRule(flagName);
	}

	/**
	 * Returns the rules data of this charity.
	 *
	 * @return the rules data of this charity
	 */
	@Override
	public String getRulesData() {
		return model.getRulesData();
	}

	/**
	 * Returns the rules header of this charity.
	 *
	 * @return the rules header of this charity
	 */
	@Override
	public String getRulesHeader() {
		return model.getRulesHeader();
	}

	@Override
	public Map<String, Boolean> getRulesMap() {
		return model.getRulesMap();
	}

	/**
	 * Returns <code>true</code> if this charity is gov doc verified.
	 *
	 * @return <code>true</code> if this charity is gov doc verified; <code>false</code> otherwise
	 */
	@Override
	public boolean isGovDocVerified() {
		return model.isGovDocVerified();
	}

	@Override
	public boolean isGroupCharity() {
		return model.isGroupCharity();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the charity ID of this charity.
	 *
	 * @param charityId the charity ID of this charity
	 */
	@Override
	public void setCharityId(long charityId) {
		model.setCharityId(charityId);
	}

	/**
	 * Sets the company ID of this charity.
	 *
	 * @param companyId the company ID of this charity
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the display ID of this charity.
	 *
	 * @param displayId the display ID of this charity
	 */
	@Override
	public void setDisplayId(String displayId) {
		model.setDisplayId(displayId);
	}

	/**
	 * Sets the full name of this charity.
	 *
	 * @param fullName the full name of this charity
	 */
	@Override
	public void setFullName(String fullName) {
		model.setFullName(fullName);
	}

	/**
	 * Sets the gov doc description of this charity.
	 *
	 * @param govDocDescription the gov doc description of this charity
	 */
	@Override
	public void setGovDocDescription(String govDocDescription) {
		model.setGovDocDescription(govDocDescription);
	}

	/**
	 * Sets the gov doc ID of this charity.
	 *
	 * @param govDocId the gov doc ID of this charity
	 */
	@Override
	public void setGovDocId(long govDocId) {
		model.setGovDocId(govDocId);
	}

	/**
	 * Sets whether this charity is gov doc verified.
	 *
	 * @param govDocVerified the gov doc verified of this charity
	 */
	@Override
	public void setGovDocVerified(boolean govDocVerified) {
		model.setGovDocVerified(govDocVerified);
	}

	/**
	 * Sets the group charity ID of this charity.
	 *
	 * @param groupCharityId the group charity ID of this charity
	 */
	@Override
	public void setGroupCharityId(long groupCharityId) {
		model.setGroupCharityId(groupCharityId);
	}

	/**
	 * Sets the organization ID of this charity.
	 *
	 * @param organizationId the organization ID of this charity
	 */
	@Override
	public void setOrganizationId(long organizationId) {
		model.setOrganizationId(organizationId);
	}

	/**
	 * Sets the parent charity ID of this charity.
	 *
	 * @param parentCharityId the parent charity ID of this charity
	 */
	@Override
	public void setParentCharityId(long parentCharityId) {
		model.setParentCharityId(parentCharityId);
	}

	/**
	 * Sets the primary key of this charity.
	 *
	 * @param primaryKey the primary key of this charity
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the registration ID of this charity.
	 *
	 * @param registrationId the registration ID of this charity
	 */
	@Override
	public void setRegistrationId(long registrationId) {
		model.setRegistrationId(registrationId);
	}

	/**
	 * Sets the rules data of this charity.
	 *
	 * @param rulesData the rules data of this charity
	 */
	@Override
	public void setRulesData(String rulesData) {
		model.setRulesData(rulesData);
	}

	/**
	 * Sets the rules header of this charity.
	 *
	 * @param rulesHeader the rules header of this charity
	 */
	@Override
	public void setRulesHeader(String rulesHeader) {
		model.setRulesHeader(rulesHeader);
	}

	@Override
	protected CharityWrapper wrap(Charity charity) {
		return new CharityWrapper(charity);
	}

}
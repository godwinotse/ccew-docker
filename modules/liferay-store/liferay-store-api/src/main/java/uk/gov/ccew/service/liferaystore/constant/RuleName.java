package uk.gov.ccew.service.liferaystore.constant;

public enum RuleName {
	CHARITY_IN_DEFAULT("charityInDefault"),
	ACT_OF_PARLIAMENT("actOfParliament"),
	CIO("cio"),
	CHARITABLE_COMPANY("charitableCompany"),
	ROYAL_CHARTER("royalCharter"),
	UNINCORPORATED_CHARITY("unincorporatedCharity"),
	TRUST("trust"),
	UNKNOWN_CHARITY("unknownCharity"),
	GROUP_CHARITY("groupCharity"),
	SCOUTS("scout"),
	PENDING_MAIN_NAME_CHANGE("pendingMainNameChangeRequest"),
	NULL_INCOME("nullIncome");

	private String ruleName;
	RuleName(String name) {
		this.ruleName = name;
	}
	
	public String ruleName() {
        return ruleName;
    }
	
    public static RuleName fromString(String text) {
        if (text != null) {
            for (RuleName rule : RuleName.values()) {
                if (text.equalsIgnoreCase(rule.ruleName)) {
                    return rule;
                }
            }
        }
        return null;
    }
}
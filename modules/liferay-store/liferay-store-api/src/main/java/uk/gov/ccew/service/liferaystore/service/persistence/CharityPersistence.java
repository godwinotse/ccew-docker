/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package uk.gov.ccew.service.liferaystore.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import org.osgi.annotation.versioning.ProviderType;

import uk.gov.ccew.service.liferaystore.exception.NoSuchCharityException;
import uk.gov.ccew.service.liferaystore.model.Charity;

/**
 * The persistence interface for the charity service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CharityUtil
 * @generated
 */
@ProviderType
public interface CharityPersistence extends BasePersistence<Charity> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CharityUtil} to access the charity persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns the charity where organizationId = &#63; or throws a <code>NoSuchCharityException</code> if it could not be found.
	 *
	 * @param organizationId the organization ID
	 * @return the matching charity
	 * @throws NoSuchCharityException if a matching charity could not be found
	 */
	public Charity findByOrganizationId(long organizationId)
		throws NoSuchCharityException;

	/**
	 * Returns the charity where organizationId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param organizationId the organization ID
	 * @return the matching charity, or <code>null</code> if a matching charity could not be found
	 */
	public Charity fetchByOrganizationId(long organizationId);

	/**
	 * Returns the charity where organizationId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param organizationId the organization ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching charity, or <code>null</code> if a matching charity could not be found
	 */
	public Charity fetchByOrganizationId(
		long organizationId, boolean useFinderCache);

	/**
	 * Removes the charity where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @return the charity that was removed
	 */
	public Charity removeByOrganizationId(long organizationId)
		throws NoSuchCharityException;

	/**
	 * Returns the number of charities where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching charities
	 */
	public int countByOrganizationId(long organizationId);

	/**
	 * Caches the charity in the entity cache if it is enabled.
	 *
	 * @param charity the charity
	 */
	public void cacheResult(Charity charity);

	/**
	 * Caches the charities in the entity cache if it is enabled.
	 *
	 * @param charities the charities
	 */
	public void cacheResult(java.util.List<Charity> charities);

	/**
	 * Creates a new charity with the primary key. Does not add the charity to the database.
	 *
	 * @param charityId the primary key for the new charity
	 * @return the new charity
	 */
	public Charity create(long charityId);

	/**
	 * Removes the charity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param charityId the primary key of the charity
	 * @return the charity that was removed
	 * @throws NoSuchCharityException if a charity with the primary key could not be found
	 */
	public Charity remove(long charityId) throws NoSuchCharityException;

	public Charity updateImpl(Charity charity);

	/**
	 * Returns the charity with the primary key or throws a <code>NoSuchCharityException</code> if it could not be found.
	 *
	 * @param charityId the primary key of the charity
	 * @return the charity
	 * @throws NoSuchCharityException if a charity with the primary key could not be found
	 */
	public Charity findByPrimaryKey(long charityId)
		throws NoSuchCharityException;

	/**
	 * Returns the charity with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param charityId the primary key of the charity
	 * @return the charity, or <code>null</code> if a charity with the primary key could not be found
	 */
	public Charity fetchByPrimaryKey(long charityId);

	/**
	 * Returns all the charities.
	 *
	 * @return the charities
	 */
	public java.util.List<Charity> findAll();

	/**
	 * Returns a range of all the charities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CharityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of charities
	 * @param end the upper bound of the range of charities (not inclusive)
	 * @return the range of charities
	 */
	public java.util.List<Charity> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the charities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CharityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of charities
	 * @param end the upper bound of the range of charities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of charities
	 */
	public java.util.List<Charity> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Charity>
			orderByComparator);

	/**
	 * Returns an ordered range of all the charities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CharityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of charities
	 * @param end the upper bound of the range of charities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of charities
	 */
	public java.util.List<Charity> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Charity>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the charities from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of charities.
	 *
	 * @return the number of charities
	 */
	public int countAll();

}
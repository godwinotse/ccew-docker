/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package uk.gov.ccew.service.liferaystore.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class CharitySoap implements Serializable {

	public static CharitySoap toSoapModel(Charity model) {
		CharitySoap soapModel = new CharitySoap();

		soapModel.setCharityId(model.getCharityId());
		soapModel.setRegistrationId(model.getRegistrationId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setGroupCharityId(model.getGroupCharityId());
		soapModel.setParentCharityId(model.getParentCharityId());
		soapModel.setDisplayId(model.getDisplayId());
		soapModel.setGovDocId(model.getGovDocId());
		soapModel.setGovDocDescription(model.getGovDocDescription());
		soapModel.setGovDocVerified(model.isGovDocVerified());
		soapModel.setFullName(model.getFullName());
		soapModel.setRulesHeader(model.getRulesHeader());
		soapModel.setRulesData(model.getRulesData());

		return soapModel;
	}

	public static CharitySoap[] toSoapModels(Charity[] models) {
		CharitySoap[] soapModels = new CharitySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CharitySoap[][] toSoapModels(Charity[][] models) {
		CharitySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CharitySoap[models.length][models[0].length];
		}
		else {
			soapModels = new CharitySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CharitySoap[] toSoapModels(List<Charity> models) {
		List<CharitySoap> soapModels = new ArrayList<CharitySoap>(
			models.size());

		for (Charity model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CharitySoap[soapModels.size()]);
	}

	public CharitySoap() {
	}

	public long getPrimaryKey() {
		return _charityId;
	}

	public void setPrimaryKey(long pk) {
		setCharityId(pk);
	}

	public long getCharityId() {
		return _charityId;
	}

	public void setCharityId(long charityId) {
		_charityId = charityId;
	}

	public long getRegistrationId() {
		return _registrationId;
	}

	public void setRegistrationId(long registrationId) {
		_registrationId = registrationId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getGroupCharityId() {
		return _groupCharityId;
	}

	public void setGroupCharityId(long groupCharityId) {
		_groupCharityId = groupCharityId;
	}

	public long getParentCharityId() {
		return _parentCharityId;
	}

	public void setParentCharityId(long parentCharityId) {
		_parentCharityId = parentCharityId;
	}

	public String getDisplayId() {
		return _displayId;
	}

	public void setDisplayId(String displayId) {
		_displayId = displayId;
	}

	public long getGovDocId() {
		return _govDocId;
	}

	public void setGovDocId(long govDocId) {
		_govDocId = govDocId;
	}

	public String getGovDocDescription() {
		return _govDocDescription;
	}

	public void setGovDocDescription(String govDocDescription) {
		_govDocDescription = govDocDescription;
	}

	public boolean getGovDocVerified() {
		return _govDocVerified;
	}

	public boolean isGovDocVerified() {
		return _govDocVerified;
	}

	public void setGovDocVerified(boolean govDocVerified) {
		_govDocVerified = govDocVerified;
	}

	public String getFullName() {
		return _fullName;
	}

	public void setFullName(String fullName) {
		_fullName = fullName;
	}

	public String getRulesHeader() {
		return _rulesHeader;
	}

	public void setRulesHeader(String rulesHeader) {
		_rulesHeader = rulesHeader;
	}

	public String getRulesData() {
		return _rulesData;
	}

	public void setRulesData(String rulesData) {
		_rulesData = rulesData;
	}

	private long _charityId;
	private long _registrationId;
	private long _companyId;
	private long _organizationId;
	private long _groupCharityId;
	private long _parentCharityId;
	private String _displayId;
	private long _govDocId;
	private String _govDocDescription;
	private boolean _govDocVerified;
	private String _fullName;
	private String _rulesHeader;
	private String _rulesData;

}
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package uk.gov.ccew.service.liferaystore.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CharityLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see CharityLocalService
 * @generated
 */
public class CharityLocalServiceWrapper
	implements CharityLocalService, ServiceWrapper<CharityLocalService> {

	public CharityLocalServiceWrapper(CharityLocalService charityLocalService) {
		_charityLocalService = charityLocalService;
	}

	/**
	 * Adds the charity to the database. Also notifies the appropriate model listeners.
	 *
	 * @param charity the charity
	 * @return the charity that was added
	 */
	@Override
	public uk.gov.ccew.service.liferaystore.model.Charity addCharity(
		uk.gov.ccew.service.liferaystore.model.Charity charity) {

		return _charityLocalService.addCharity(charity);
	}

	/**
	 * Creates a new charity with the primary key. Does not add the charity to the database.
	 *
	 * @param charityId the primary key for the new charity
	 * @return the new charity
	 */
	@Override
	public uk.gov.ccew.service.liferaystore.model.Charity createCharity(
		long charityId) {

		return _charityLocalService.createCharity(charityId);
	}

	/**
	 * Deletes the charity from the database. Also notifies the appropriate model listeners.
	 *
	 * @param charity the charity
	 * @return the charity that was removed
	 */
	@Override
	public uk.gov.ccew.service.liferaystore.model.Charity deleteCharity(
		uk.gov.ccew.service.liferaystore.model.Charity charity) {

		return _charityLocalService.deleteCharity(charity);
	}

	/**
	 * Deletes the charity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param charityId the primary key of the charity
	 * @return the charity that was removed
	 * @throws PortalException if a charity with the primary key could not be found
	 */
	@Override
	public uk.gov.ccew.service.liferaystore.model.Charity deleteCharity(
			long charityId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _charityLocalService.deleteCharity(charityId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _charityLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _charityLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _charityLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>uk.gov.ccew.service.liferaystore.model.impl.CharityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _charityLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>uk.gov.ccew.service.liferaystore.model.impl.CharityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _charityLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _charityLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _charityLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public uk.gov.ccew.service.liferaystore.model.Charity fetchCharity(
		long charityId) {

		return _charityLocalService.fetchCharity(charityId);
	}

	@Override
	public uk.gov.ccew.service.liferaystore.model.Charity findById(
		long charityId) {

		return _charityLocalService.findById(charityId);
	}

	@Override
	public uk.gov.ccew.service.liferaystore.model.Charity findByOrgId(
		long organizationId) {

		return _charityLocalService.findByOrgId(organizationId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _charityLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns a range of all the charities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>uk.gov.ccew.service.liferaystore.model.impl.CharityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of charities
	 * @param end the upper bound of the range of charities (not inclusive)
	 * @return the range of charities
	 */
	@Override
	public java.util.List<uk.gov.ccew.service.liferaystore.model.Charity>
		getCharities(int start, int end) {

		return _charityLocalService.getCharities(start, end);
	}

	/**
	 * Returns the number of charities.
	 *
	 * @return the number of charities
	 */
	@Override
	public int getCharitiesCount() {
		return _charityLocalService.getCharitiesCount();
	}

	/**
	 * Returns the charity with the primary key.
	 *
	 * @param charityId the primary key of the charity
	 * @return the charity
	 * @throws PortalException if a charity with the primary key could not be found
	 */
	@Override
	public uk.gov.ccew.service.liferaystore.model.Charity getCharity(
			long charityId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _charityLocalService.getCharity(charityId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _charityLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public uk.gov.ccew.service.liferaystore.model.Charity getOrCreateCharity(
			long charityId, long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _charityLocalService.getOrCreateCharity(
			charityId, companyId, groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _charityLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _charityLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the charity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param charity the charity
	 * @return the charity that was updated
	 */
	@Override
	public uk.gov.ccew.service.liferaystore.model.Charity updateCharity(
		uk.gov.ccew.service.liferaystore.model.Charity charity) {

		return _charityLocalService.updateCharity(charity);
	}

	@Override
	public uk.gov.ccew.service.liferaystore.model.Charity updateCharity(
			uk.gov.ccew.service.liferaystore.model.Charity charity,
			long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _charityLocalService.updateCharity(charity, groupId);
	}

	@Override
	public CharityLocalService getWrappedService() {
		return _charityLocalService;
	}

	@Override
	public void setWrappedService(CharityLocalService charityLocalService) {
		_charityLocalService = charityLocalService;
	}

	private CharityLocalService _charityLocalService;

}
package uk.gov.ccew.service.liferaystore.service.impl;

import java.util.Date;

import com.liferay.asset.kernel.exception.NoSuchEntryException;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.util.ContentTypes;

import org.osgi.service.component.annotations.Component;

import uk.gov.ccew.service.liferaystore.exception.NoSuchCharityException;
import uk.gov.ccew.service.liferaystore.model.Charity;
import uk.gov.ccew.service.liferaystore.service.base.CharityLocalServiceBaseImpl;

/**
 * The implementation of the charity local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>uk.gov.ccew.service.liferaystore.service.CharityLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CharityLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=uk.gov.ccew.service.liferaystore.model.Charity",
	service = AopService.class
)
public class CharityLocalServiceImpl extends CharityLocalServiceBaseImpl {

	private static final long DEFAULT_ID = 0;

	@Indexable(type = IndexableType.REINDEX)
	@Override
	public Charity getOrCreateCharity(long charityId, long companyId, long groupId) throws PortalException {
		Charity charity;
		try {
			return getCharity(charityId);
		} catch (NoSuchCharityException e) {
			charity = createCharity(charityId);
			charity.setCompanyId(companyId);

			assetEntryLocalService.updateEntry(DEFAULT_ID, groupId, new Date(), new Date(),
					Charity.class.getName(), charityId, Long.toString(charityId), DEFAULT_ID, null, null, true,
					true, null, null, new Date(), null, ContentTypes.TEXT_HTML, null, null, null, null, null, 0, 0,
					null);
		}

		return addCharity(charity);
	}

	@Indexable(type = IndexableType.REINDEX)
	@Override
	public Charity updateCharity(Charity charity, long groupId) throws PortalException {

		try {

			getCharity(charity.getCharityId());
			charity.setNew(false);

		} catch (NoSuchCharityException e) {
			charity.setNew(true);

			assetEntryLocalService.updateEntry(DEFAULT_ID, groupId, new Date(), new Date(),
					Charity.class.getName(), charity.getCharityId(), Long.toString(charity.getCharityId()), DEFAULT_ID, null, null, true,
					true, null, null, new Date(), null, ContentTypes.TEXT_HTML, null, null, null, null, null, 0, 0,
					null);
		}

		return updateCharity(charity);

	}

	@Override
	public Charity deleteCharity(long charityId) throws PortalException {

		try {
			AssetEntry assetEntry = assetEntryLocalService.getEntry(Charity.class.getName(), charityId);
			assetEntryLocalService.deleteEntry(assetEntry);
		} catch (NoSuchEntryException e) {
			// No entry to delete
		}

		return super.deleteCharity(charityId);
	}

	public Charity findByOrgId(long organizationId) {
		try {
			return charityPersistence.findByOrganizationId(organizationId);
		} catch (NoSuchCharityException e) {
			return null;
		}
	}

	public Charity findById(long charityId) {
		try {
			return charityPersistence.findByPrimaryKey(charityId);
		} catch (NoSuchCharityException e) {
			return null;
		}
	}

}
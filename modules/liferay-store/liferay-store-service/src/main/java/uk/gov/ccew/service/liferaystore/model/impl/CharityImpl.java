package uk.gov.ccew.service.liferaystore.model.impl;

import java.util.Collections;
import java.util.Map;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;

/**
 * The extended model implementation for the Charity service. Represents a row in the &quot;CCEW_Charity&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>uk.gov.ccew.service.liferaystore.model.Charity</code> interface.
 * </p>
 *
 * @author Brian Wing Shun Chan
 */
public class CharityImpl extends CharityBaseImpl {

	public CharityImpl() {
	}

	public boolean isGroupCharity(){
		return getGroupCharityId() != 0;
	}

	public Map<String, Boolean> getRulesMap() {
		String rulesData = super.getRulesData();
		if (!Validator.isBlank(rulesData)) {
			return (Map<String, Boolean>) JSONFactoryUtil.looseDeserialize(rulesData, Map.class);
		}
		return Collections.emptyMap();
	}

	public Boolean getRule(String flagName){
		return GetterUtil.getBoolean(getRulesMap().get(flagName));
	}

}
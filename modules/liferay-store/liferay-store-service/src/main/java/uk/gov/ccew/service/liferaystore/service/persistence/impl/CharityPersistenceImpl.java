/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package uk.gov.ccew.service.liferaystore.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringUtil;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

import uk.gov.ccew.service.liferaystore.exception.NoSuchCharityException;
import uk.gov.ccew.service.liferaystore.model.Charity;
import uk.gov.ccew.service.liferaystore.model.impl.CharityImpl;
import uk.gov.ccew.service.liferaystore.model.impl.CharityModelImpl;
import uk.gov.ccew.service.liferaystore.service.persistence.CharityPersistence;
import uk.gov.ccew.service.liferaystore.service.persistence.impl.constants.CCEWPersistenceConstants;

/**
 * The persistence implementation for the charity service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = CharityPersistence.class)
public class CharityPersistenceImpl
	extends BasePersistenceImpl<Charity> implements CharityPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>CharityUtil</code> to access the charity persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		CharityImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathFetchByOrganizationId;
	private FinderPath _finderPathCountByOrganizationId;

	/**
	 * Returns the charity where organizationId = &#63; or throws a <code>NoSuchCharityException</code> if it could not be found.
	 *
	 * @param organizationId the organization ID
	 * @return the matching charity
	 * @throws NoSuchCharityException if a matching charity could not be found
	 */
	@Override
	public Charity findByOrganizationId(long organizationId)
		throws NoSuchCharityException {

		Charity charity = fetchByOrganizationId(organizationId);

		if (charity == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("organizationId=");
			sb.append(organizationId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchCharityException(sb.toString());
		}

		return charity;
	}

	/**
	 * Returns the charity where organizationId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param organizationId the organization ID
	 * @return the matching charity, or <code>null</code> if a matching charity could not be found
	 */
	@Override
	public Charity fetchByOrganizationId(long organizationId) {
		return fetchByOrganizationId(organizationId, true);
	}

	/**
	 * Returns the charity where organizationId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param organizationId the organization ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching charity, or <code>null</code> if a matching charity could not be found
	 */
	@Override
	public Charity fetchByOrganizationId(
		long organizationId, boolean useFinderCache) {

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {organizationId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByOrganizationId, finderArgs, this);
		}

		if (result instanceof Charity) {
			Charity charity = (Charity)result;

			if (organizationId != charity.getOrganizationId()) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_SELECT_CHARITY_WHERE);

			sb.append(_FINDER_COLUMN_ORGANIZATIONID_ORGANIZATIONID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(organizationId);

				List<Charity> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByOrganizationId, finderArgs, list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {organizationId};
							}

							_log.warn(
								"CharityPersistenceImpl.fetchByOrganizationId(long, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Charity charity = list.get(0);

					result = charity;

					cacheResult(charity);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(
						_finderPathFetchByOrganizationId, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Charity)result;
		}
	}

	/**
	 * Removes the charity where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @return the charity that was removed
	 */
	@Override
	public Charity removeByOrganizationId(long organizationId)
		throws NoSuchCharityException {

		Charity charity = findByOrganizationId(organizationId);

		return remove(charity);
	}

	/**
	 * Returns the number of charities where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching charities
	 */
	@Override
	public int countByOrganizationId(long organizationId) {
		FinderPath finderPath = _finderPathCountByOrganizationId;

		Object[] finderArgs = new Object[] {organizationId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_CHARITY_WHERE);

			sb.append(_FINDER_COLUMN_ORGANIZATIONID_ORGANIZATIONID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(organizationId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ORGANIZATIONID_ORGANIZATIONID_2 =
		"charity.organizationId = ?";

	public CharityPersistenceImpl() {
		setModelClass(Charity.class);

		setModelImplClass(CharityImpl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the charity in the entity cache if it is enabled.
	 *
	 * @param charity the charity
	 */
	@Override
	public void cacheResult(Charity charity) {
		entityCache.putResult(
			entityCacheEnabled, CharityImpl.class, charity.getPrimaryKey(),
			charity);

		finderCache.putResult(
			_finderPathFetchByOrganizationId,
			new Object[] {charity.getOrganizationId()}, charity);

		charity.resetOriginalValues();
	}

	/**
	 * Caches the charities in the entity cache if it is enabled.
	 *
	 * @param charities the charities
	 */
	@Override
	public void cacheResult(List<Charity> charities) {
		for (Charity charity : charities) {
			if (entityCache.getResult(
					entityCacheEnabled, CharityImpl.class,
					charity.getPrimaryKey()) == null) {

				cacheResult(charity);
			}
			else {
				charity.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all charities.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(CharityImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the charity.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Charity charity) {
		entityCache.removeResult(
			entityCacheEnabled, CharityImpl.class, charity.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((CharityModelImpl)charity, true);
	}

	@Override
	public void clearCache(List<Charity> charities) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Charity charity : charities) {
			entityCache.removeResult(
				entityCacheEnabled, CharityImpl.class, charity.getPrimaryKey());

			clearUniqueFindersCache((CharityModelImpl)charity, true);
		}
	}

	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(
				entityCacheEnabled, CharityImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(CharityModelImpl charityModelImpl) {
		Object[] args = new Object[] {charityModelImpl.getOrganizationId()};

		finderCache.putResult(
			_finderPathCountByOrganizationId, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByOrganizationId, args, charityModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		CharityModelImpl charityModelImpl, boolean clearCurrent) {

		if (clearCurrent) {
			Object[] args = new Object[] {charityModelImpl.getOrganizationId()};

			finderCache.removeResult(_finderPathCountByOrganizationId, args);
			finderCache.removeResult(_finderPathFetchByOrganizationId, args);
		}

		if ((charityModelImpl.getColumnBitmask() &
			 _finderPathFetchByOrganizationId.getColumnBitmask()) != 0) {

			Object[] args = new Object[] {
				charityModelImpl.getOriginalOrganizationId()
			};

			finderCache.removeResult(_finderPathCountByOrganizationId, args);
			finderCache.removeResult(_finderPathFetchByOrganizationId, args);
		}
	}

	/**
	 * Creates a new charity with the primary key. Does not add the charity to the database.
	 *
	 * @param charityId the primary key for the new charity
	 * @return the new charity
	 */
	@Override
	public Charity create(long charityId) {
		Charity charity = new CharityImpl();

		charity.setNew(true);
		charity.setPrimaryKey(charityId);

		charity.setCompanyId(CompanyThreadLocal.getCompanyId());

		return charity;
	}

	/**
	 * Removes the charity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param charityId the primary key of the charity
	 * @return the charity that was removed
	 * @throws NoSuchCharityException if a charity with the primary key could not be found
	 */
	@Override
	public Charity remove(long charityId) throws NoSuchCharityException {
		return remove((Serializable)charityId);
	}

	/**
	 * Removes the charity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the charity
	 * @return the charity that was removed
	 * @throws NoSuchCharityException if a charity with the primary key could not be found
	 */
	@Override
	public Charity remove(Serializable primaryKey)
		throws NoSuchCharityException {

		Session session = null;

		try {
			session = openSession();

			Charity charity = (Charity)session.get(
				CharityImpl.class, primaryKey);

			if (charity == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCharityException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(charity);
		}
		catch (NoSuchCharityException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Charity removeImpl(Charity charity) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(charity)) {
				charity = (Charity)session.get(
					CharityImpl.class, charity.getPrimaryKeyObj());
			}

			if (charity != null) {
				session.delete(charity);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (charity != null) {
			clearCache(charity);
		}

		return charity;
	}

	@Override
	public Charity updateImpl(Charity charity) {
		boolean isNew = charity.isNew();

		if (!(charity instanceof CharityModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(charity.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(charity);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in charity proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom Charity implementation " +
					charity.getClass());
		}

		CharityModelImpl charityModelImpl = (CharityModelImpl)charity;

		Session session = null;

		try {
			session = openSession();

			if (charity.isNew()) {
				session.save(charity);

				charity.setNew(false);
			}
			else {
				charity = (Charity)session.merge(charity);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!_columnBitmaskEnabled) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else if (isNew) {
			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(
			entityCacheEnabled, CharityImpl.class, charity.getPrimaryKey(),
			charity, false);

		clearUniqueFindersCache(charityModelImpl, false);
		cacheUniqueFindersCache(charityModelImpl);

		charity.resetOriginalValues();

		return charity;
	}

	/**
	 * Returns the charity with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the charity
	 * @return the charity
	 * @throws NoSuchCharityException if a charity with the primary key could not be found
	 */
	@Override
	public Charity findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCharityException {

		Charity charity = fetchByPrimaryKey(primaryKey);

		if (charity == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCharityException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return charity;
	}

	/**
	 * Returns the charity with the primary key or throws a <code>NoSuchCharityException</code> if it could not be found.
	 *
	 * @param charityId the primary key of the charity
	 * @return the charity
	 * @throws NoSuchCharityException if a charity with the primary key could not be found
	 */
	@Override
	public Charity findByPrimaryKey(long charityId)
		throws NoSuchCharityException {

		return findByPrimaryKey((Serializable)charityId);
	}

	/**
	 * Returns the charity with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param charityId the primary key of the charity
	 * @return the charity, or <code>null</code> if a charity with the primary key could not be found
	 */
	@Override
	public Charity fetchByPrimaryKey(long charityId) {
		return fetchByPrimaryKey((Serializable)charityId);
	}

	/**
	 * Returns all the charities.
	 *
	 * @return the charities
	 */
	@Override
	public List<Charity> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the charities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CharityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of charities
	 * @param end the upper bound of the range of charities (not inclusive)
	 * @return the range of charities
	 */
	@Override
	public List<Charity> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the charities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CharityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of charities
	 * @param end the upper bound of the range of charities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of charities
	 */
	@Override
	public List<Charity> findAll(
		int start, int end, OrderByComparator<Charity> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the charities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CharityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of charities
	 * @param end the upper bound of the range of charities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of charities
	 */
	@Override
	public List<Charity> findAll(
		int start, int end, OrderByComparator<Charity> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<Charity> list = null;

		if (useFinderCache) {
			list = (List<Charity>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_CHARITY);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_CHARITY;

				sql = sql.concat(CharityModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<Charity>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the charities from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Charity charity : findAll()) {
			remove(charity);
		}
	}

	/**
	 * Returns the number of charities.
	 *
	 * @return the number of charities
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_CHARITY);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "charityId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_CHARITY;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return CharityModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the charity persistence.
	 */
	@Activate
	public void activate() {
		CharityModelImpl.setEntityCacheEnabled(entityCacheEnabled);
		CharityModelImpl.setFinderCacheEnabled(finderCacheEnabled);

		_finderPathWithPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, CharityImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, CharityImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);

		_finderPathFetchByOrganizationId = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, CharityImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByOrganizationId",
			new String[] {Long.class.getName()},
			CharityModelImpl.ORGANIZATIONID_COLUMN_BITMASK);

		_finderPathCountByOrganizationId = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOrganizationId",
			new String[] {Long.class.getName()});
	}

	@Deactivate
	public void deactivate() {
		entityCache.removeCache(CharityImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	@Reference(
		target = CCEWPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
		super.setConfiguration(configuration);

		_columnBitmaskEnabled = GetterUtil.getBoolean(
			configuration.get(
				"value.object.column.bitmask.enabled.uk.gov.ccew.service.liferaystore.model.Charity"),
			true);
	}

	@Override
	@Reference(
		target = CCEWPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = CCEWPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private boolean _columnBitmaskEnabled;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_CHARITY =
		"SELECT charity FROM Charity charity";

	private static final String _SQL_SELECT_CHARITY_WHERE =
		"SELECT charity FROM Charity charity WHERE ";

	private static final String _SQL_COUNT_CHARITY =
		"SELECT COUNT(charity) FROM Charity charity";

	private static final String _SQL_COUNT_CHARITY_WHERE =
		"SELECT COUNT(charity) FROM Charity charity WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "charity.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No Charity exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No Charity exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		CharityPersistenceImpl.class);

	static {
		try {
			Class.forName(CCEWPersistenceConstants.class.getName());
		}
		catch (ClassNotFoundException classNotFoundException) {
			throw new ExceptionInInitializerError(classNotFoundException);
		}
	}

}
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package uk.gov.ccew.service.liferaystore.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import uk.gov.ccew.service.liferaystore.model.Charity;

/**
 * The cache model class for representing Charity in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class CharityCacheModel implements CacheModel<Charity>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CharityCacheModel)) {
			return false;
		}

		CharityCacheModel charityCacheModel = (CharityCacheModel)obj;

		if (charityId == charityCacheModel.charityId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, charityId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{charityId=");
		sb.append(charityId);
		sb.append(", registrationId=");
		sb.append(registrationId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", groupCharityId=");
		sb.append(groupCharityId);
		sb.append(", parentCharityId=");
		sb.append(parentCharityId);
		sb.append(", displayId=");
		sb.append(displayId);
		sb.append(", govDocId=");
		sb.append(govDocId);
		sb.append(", govDocDescription=");
		sb.append(govDocDescription);
		sb.append(", govDocVerified=");
		sb.append(govDocVerified);
		sb.append(", fullName=");
		sb.append(fullName);
		sb.append(", rulesHeader=");
		sb.append(rulesHeader);
		sb.append(", rulesData=");
		sb.append(rulesData);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Charity toEntityModel() {
		CharityImpl charityImpl = new CharityImpl();

		charityImpl.setCharityId(charityId);
		charityImpl.setRegistrationId(registrationId);
		charityImpl.setCompanyId(companyId);
		charityImpl.setOrganizationId(organizationId);
		charityImpl.setGroupCharityId(groupCharityId);
		charityImpl.setParentCharityId(parentCharityId);

		if (displayId == null) {
			charityImpl.setDisplayId("");
		}
		else {
			charityImpl.setDisplayId(displayId);
		}

		charityImpl.setGovDocId(govDocId);

		if (govDocDescription == null) {
			charityImpl.setGovDocDescription("");
		}
		else {
			charityImpl.setGovDocDescription(govDocDescription);
		}

		charityImpl.setGovDocVerified(govDocVerified);

		if (fullName == null) {
			charityImpl.setFullName("");
		}
		else {
			charityImpl.setFullName(fullName);
		}

		if (rulesHeader == null) {
			charityImpl.setRulesHeader("");
		}
		else {
			charityImpl.setRulesHeader(rulesHeader);
		}

		if (rulesData == null) {
			charityImpl.setRulesData("");
		}
		else {
			charityImpl.setRulesData(rulesData);
		}

		charityImpl.resetOriginalValues();

		return charityImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		charityId = objectInput.readLong();

		registrationId = objectInput.readLong();

		companyId = objectInput.readLong();

		organizationId = objectInput.readLong();

		groupCharityId = objectInput.readLong();

		parentCharityId = objectInput.readLong();
		displayId = objectInput.readUTF();

		govDocId = objectInput.readLong();
		govDocDescription = objectInput.readUTF();

		govDocVerified = objectInput.readBoolean();
		fullName = objectInput.readUTF();
		rulesHeader = objectInput.readUTF();
		rulesData = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeLong(charityId);

		objectOutput.writeLong(registrationId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(organizationId);

		objectOutput.writeLong(groupCharityId);

		objectOutput.writeLong(parentCharityId);

		if (displayId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(displayId);
		}

		objectOutput.writeLong(govDocId);

		if (govDocDescription == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(govDocDescription);
		}

		objectOutput.writeBoolean(govDocVerified);

		if (fullName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(fullName);
		}

		if (rulesHeader == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(rulesHeader);
		}

		if (rulesData == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(rulesData);
		}
	}

	public long charityId;
	public long registrationId;
	public long companyId;
	public long organizationId;
	public long groupCharityId;
	public long parentCharityId;
	public String displayId;
	public long govDocId;
	public String govDocDescription;
	public boolean govDocVerified;
	public String fullName;
	public String rulesHeader;
	public String rulesData;

}
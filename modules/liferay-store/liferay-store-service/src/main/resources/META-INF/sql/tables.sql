create table CCEW_Charity (
	charityId LONG not null primary key,
	registrationId LONG,
	companyId LONG,
	organizationId LONG,
	groupCharityId LONG,
	parentCharityId LONG,
	displayId VARCHAR(75) null,
	govDocId LONG,
	govDocDescription TEXT null,
	govDocVerified BOOLEAN,
	fullName VARCHAR(255) null,
	rulesHeader VARCHAR(500) null,
	rulesData STRING null
);
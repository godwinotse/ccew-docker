$.getScript('/o/ccew-govuk-theme-contributor/js/govuk-frontend-3.6.0.min.js', function () {
	$(document).ready(function () {
		document.body.className = ((document.body.className) ? document.body.className + ' js-enabled' : 'js-enabled');
		window.GOVUKFrontend.initAll();
	});
});
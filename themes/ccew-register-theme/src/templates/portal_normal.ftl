<!DOCTYPE html>

<#include init />

<html class="${ root_css_class }" dir="<@liferay.language key="lang.dir" />" lang="${ w3c_language_id }">

	<head>
		<title>${ layout.getHTMLTitle(locale) }</title>

		<meta content="initial-scale=1.0, width=device-width" name="viewport" />

		<@liferay_util["include"] page=top_head_include />

	</head>

	<body class="${ body_css_class }">

		<@liferay_ui["quick-access"] contentId="#main-content" />

		<@liferay.control_menu />

		<div id="wrapper" class="govuk-body">

			<#include "${full_templates_path}/header.ftl" />

			<@liferay_util["include"] page=body_top_include />

			<section id="content">
				<@liferay_util["include"] page=content_include />
			</section>

			<#include "${full_templates_path}/footer.ftl" />

		</div>

		<@liferay_util["include"] page=body_bottom_include />
		<@liferay_util["include"] page=bottom_include />

	</body>
</html>

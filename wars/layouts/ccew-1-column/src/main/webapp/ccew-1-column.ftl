<div class="ccew-1-column" id="main-content" role="main">
	<div class="portlet-layout row mx-0">
		<div class="col-md-7 col-md-offset-2 portlet-column portlet-column-only" id="column-1">
			${processor.processColumn("column-1", "portlet-column-content portlet-column-content-only")}
		</div>
	</div>
</div>

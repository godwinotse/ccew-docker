#  Liferay workspace for CCEW
## Requirements
* Gradle 5.6+
* JDK 8

## Build Liferay Docker image including workspace modules, third party modules and database driver.

1. Clone ccew-docker-liferay
    * `git clone <REPO_URL>`
2. Clone ccew-liferay-phase-3
    * `git clone <REPO_URL>`
3. Create `~/.gradle/gradle.properties` and set the path to the Liferay home and deploy directories. In this case it will be a path in the Liferay Docker build directory.
    * `liferay.workspace.home.dir=../ccew-docker-liferay/build`
    * `auto.deploy.dir=../ccew-docker-liferay/build/deploy`
4. Build and copy Liferay workspace customisations to Docker build directory
    * `cd ccew-liferay-phase-3`
    * `gradle build deploy`
5. Build Liferay Docker image
    * `cd ../ccew-docker-liferay`
    * `./build_image.sh`

## Typical developer workflow 
1. Create Docker Compose override file and configure Liferay Docker volume mount to enable hot deploy
    * create override file - `<INSERT_PATH>/ccew-liferay-phase-3/docker/docker-compose.override.yml`
    * add volume mount configuration to override file

    ```
    version: '3'
    services:

        liferay:
            volumes:
                - <INSERT_PATH>/ccew-liferay-phase-3/docker/liferay/resources:/etc/liferay/mount
    ```

2. Configure hot deploy directory by setting path to Liferay deploy directory in `~/.gradle/gradle.properties`
    * `auto.deploy.dir=<INSERT_PATH>/ccew-liferay-phase-3/docker/liferay/resources/deploy`
3. Start up the Liferay Docker Compose environment.
    * `docker-compose -f docker/docker-compose.yml up`
4. Build and deploy Liferay customisations
    * `gradle clean build deploy`

## Useful tasks
### Docker Compose
Start all project containers

`docker-compose -f docker/docker-compose.yml up`

Delete all project containers excluding volumes 

`docker-compose -f docker/docker-compose.yml down`

Delete all project containers including volumes

`docker-compose -f docker/docker-compose.yml down -v` 

### Gradle
Downloads the database driver

`gradle downloadDatabaseDriver` 

#### Properties 
* `liferay.workspace.home.dir` - configures the Liferay home directory, which is used to determine where to download the driver to
* `ccew.liferay.database.driver.download.url` - configures the URL to download the driver from 

Downloads third party OSGI modules

`gradle downloadThirdPartyOSGIModules` 

#### Properties
* `auto.deploy.dir` - configures the Liferay deploy directory, which is where the third party OSGI modules will be downloaded to
* `ccew.third.party.osgi.module.download.urls` - configures the URLs to download the third party OSGI modules from  

Deploy configs

`gradle deployConfigs`

#### Properties
* `liferay.workspace.home.dir` - configures the Liferay home directory, which is used to determine where to deploy the content of the configs directory to
* `liferay.workspace.environment` - configures the target environment to deploy configurations for e.g. local, dev, uat and prod
